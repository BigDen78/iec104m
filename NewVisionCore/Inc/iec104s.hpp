#pragma once
#include "IECSlaveBase.hpp"
#include "cs104_slave.h"

class IEC104SDriver : virtual public IECSlaveBase
{
public:
	enum RunningState : uint32_t
	{
		rsPreInit,
		rsInitializing,
		rsRunning,
		rsSpontDataAwait,
		rsGI,
		rsFileTransfer,
		rsRecoverableError,
		rsUnrecoverableError
	};

	IEC104SDriver();
	virtual bool setParameterById(uint32_t paramId, VarData newValue) override;
	virtual bool setRuntimeParameter(const Tag &tag, uint32_t paramId, VarData paramValue) override;
	virtual VarData getRuntimeParameter(uint32_t paramId) override;
	virtual void initialize() override;
	virtual bool tagChanged(const Tag &tag, uint32_t ts, VarData newValue) override;
	virtual void run() override;

protected:
	void setRedGroups();
	static bool clockSyncHandlerStatic(void *p, IMasterConnection connection, CS101_ASDU asdu, CP56Time2a newTime)
	{
		return reinterpret_cast<IEC104SDriver *>(p)->clockSyncHandler(connection, asdu, newTime);
	}
	static bool interrogationHandlerStatic(void *p, IMasterConnection connection, CS101_ASDU asdu, uint8_t qoi)
	{
		return reinterpret_cast<IEC104SDriver *>(p)->interrogationHandler(connection, asdu, qoi);
	}
	static bool asduHandlerStatic(void *p, IMasterConnection connection, CS101_ASDU asdu)
	{
		return reinterpret_cast<IEC104SDriver *>(p)->asduHandler(connection, asdu);
	}
	RunningState runState;
	uint32_t port;
	CString redPath;
	CS104_Slave slave = 0;
	static const ParameterVector parameters;
};