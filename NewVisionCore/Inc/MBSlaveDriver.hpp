#pragma once
#include "ParameterizedObject.hpp"
#include "driver.hpp"
#include "usart.h"
#include "lib_bserial.h"

class MBDatabaseType;
class MBSlaveTagDriverContext;

typedef enum // <- RegisterType
{
  DO = 0,
  DI,
  AO,
  AI,
  UNDEFINED = 0xFF
} RegisterType;

typedef enum // <- RequestType
{
  SET_SINGLE_REQUEST,
  SET_MULTIPLE_REQUEST,
  GET_REQUEST,
  INVALID_REQUEST  
} RequestType;

typedef enum // <- errorCode
{
  RETURN_OK = 0,                ///< MODBUS frame is recognised, you can send answer - Фрейм распознан, можно отправить ответ
  ILLEGAL_FUNCTION,             ///< The accepted function code cannot be processed - Принятый код функции не может быть обработан
  ILLEGAL_DATA_ADDRESS,         ///< The data address specified in the request is not available - Адрес данных, указанный в запросе, недоступен
  ILLEGAL_DATA_VALUE,           ///< The value contained in the query data field is not a valid value - Значение, содержащееся в поле данных запроса, не является допустимым значением
  SLAVE_DEVICE_FAILURE,         ///< A non-recoverable error occurred while the slave was trying to perform the requested action - Неисправимая ошибка произошла, когда ведомое устройство пыталось выполнить запрошенное действие
  ACKNOWLEDGE,                  ///< The slave has accepted the request and is processing it, but this takes a long time - Подчиненный принял запрос и обрабатывает его, но это занимает много времени
  SLAVE_DEVICE_BUSY,            ///< The slave is busy processing the command - Ведомый занят обработкой команды
  NEGATIVE_ACKNOWLEDGE,         ///< The slave cannot perform the software function specified in the request - Ведомое устройство не может выполнять программную функцию, указанную в запросе
  MEMORY_PARITY_ERROR,          ///< The slave detected a parity error while reading extended memory - Ведомый обнаружил ошибку четности при чтении расширенной памяти
  RETURN_ERROR,                 ///< No MODBUS frame or some other problem - Отсутствует фрейм или иная проблема
  GATEWAY_PATH_UNAVAILABLE = 10,///< Gateway is incorrectly configured or overloaded with requests - Шлюз неправильно настроен или перегружен запросами
  GATEWAY_DEVICE_FAILED    = 11 ///< The slave device is not online or there is no response from it - Ведомое устройство не подключено или от него нет ответа
} errorCode;

typedef enum // <- SpanEnum
{
  sSingle,
  sDouble,
  sUnimportant
} SpanEnum;

typedef enum // <- SpanRankEnum
{
  sLo,
  sHi
} SpanRankEnum;

typedef struct { // <- PDU
  uint8_t* pduBufferPtr = NULL;
  uint8_t pduLength = 0;
} PDU;

class MBMessageParser 
{
public:
  enum CodesOfFunctions : uint8_t
  {
    readCoilStatus = 0x01,
    readInputStatus,
    readHoldingRegisters,
    readInputRegisters,
    
    forceSingleCoil,
    presetSingleRegister,
    forceMultipleCoils = 0x0F,
    presetMultipleRegisters
  };

  MBMessageParser() {};
  ~MBMessageParser() {};
  
  RequestType checkRequestType(uint8_t *dataBuf);
  RegisterType getDataType(uint8_t *dataBuf);
  
  errorCode processGetRequest(RegisterType regType, uint16_t firstReg, uint16_t regsAmount, uint8_t funCode); 
  errorCode processSetSingleRequest(RegisterType regType, uint8_t funCode, uint16_t firstReg, uint16_t regValue);
  errorCode processSetMultipleRequest(RegisterType regType, uint8_t funCode, uint16_t firstReg, uint16_t regsAmount, uint8_t* modbusFrame);
  
  bool pushNextRegIntoPDU(RegisterType, uint16_t* regNum);
  
  void processModbusFrame(uint8_t* frame, uint32_t length);
 
  errorCode modbusFrameParser(uint8_t* modbusFrame, uint32_t length, uint8_t* responsePtr);
    
  MBDatabaseType* regsDataBasePtr = NULL;
  PDU pduBufferStruct;
  uint8_t* responseBufferPtr;
protected:   
};

using OneTypeRegistersMap = std::map <uint32_t, std::pair<Tag*, SpanRankEnum>>;

class MBDatabaseType
{
public:
  MBDatabaseType():ownerId(-1) {};
  ~MBDatabaseType() {};
  
  OneTypeRegistersMap coilsDataBase;
  OneTypeRegistersMap inputsDataBase;
  OneTypeRegistersMap inputRegsDataBase;
  OneTypeRegistersMap holdingRegsDataBase;
  
  OneTypeRegistersMap* dataBaseArray[4] = {&coilsDataBase, &inputsDataBase, &holdingRegsDataBase, &inputRegsDataBase};
  
  bool addNewRegister(RegisterType type, uint32_t address, SpanRankEnum spanRank, Tag* tagPtr);
  errorCode getRegisterValue(RegisterType type, uint32_t address, uint16_t* valuePtr);
  errorCode changeRegisterValue(RegisterType type, uint16_t address, uint16_t rawValue);
  bool checkRange(RegisterType regType, uint16_t firstReg, uint16_t lastReg);
  void setOwnerId(uint32_t id) {ownerId = id;}
  
private:       
  uint32_t ownerId;
};

class MBSlaveDriver : virtual public Driver
{  
public:
  MBSlaveDriver();  

  enum RunningState : uint32_t
  {
    rsPreInit,
    rsInitializing,
    rsRunning,
    rsWaitingRequest,
    rsUnrecoverableError
  };

  RunningState currentState;

  virtual void registerTag(const Tag &tag, void * ctx) override;
  virtual bool tagChanged(const Tag &tag, uint32_t paramId, VarData newValue) override;
  virtual bool setParameterById(uint32_t id, VarData newValue) override;
  virtual bool setRuntimeParameter(const Tag &tag, uint32_t paramId, VarData paramValue ) override;
  virtual bool setTagRuntimeParameter(const Tag &tag, uint32_t paramId, VarData paramValue) override {  return false; }
  virtual VarData getRuntimeParameter(uint32_t paramId) override { return NO_DATA; }
  virtual VarData getTagRuntimeParameter(const Tag &tag, uint32_t paramId) override { return NO_DATA; }
  virtual DriverContext *createTagContext() const override;
  virtual void initialize() override;
  virtual void run() override;

  static const ParameterVector myParameters;
  uint32_t linkAddress;
  static uint16_t getCRC(uint8_t *buf, uint8_t len);

  uint8_t* dataBufferPtr;
  uint32_t bufLen;
  void modbusRTUParser();
  bool checkID();
  bool checkCRC();
  void sendErrorMessage(errorCode error);
  //uint16_t convertBitOrder(uint16_t word);
  MBMessageParser parser;
    
private:
  UART_HandleTypeDef *huart;
  uint32_t baudRate;
  uint8_t stopBits;
  char parity;
  Serial* serialMBS = NULL;
  DriverCommand dcmdSerialIdle;

  uint8_t responseBuffer[256] = { };
  uint8_t responseLen = 0;

  MBDatabaseType regsBase;  
};

union MBData
{
  //float fltmData; // <- for otladka, not used in real life
  uint32_t dwordmData;
  uint16_t wordmData[2];
};

class MBSlaveTagDriverContext : virtual public DriverContext
{
public:
  MBSlaveTagDriverContext() : ParameterizedObject(myParameters) {  }
  ~MBSlaveTagDriverContext() {  }
  virtual bool setParameterById(uint32_t id, VarData newValue) override;

  uint16_t address;
  RegisterType type;
  SpanEnum span;

  uint8_t writeMask = 0x00;
  MBData data;

  static const ParameterVector myParameters;  
};