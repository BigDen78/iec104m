#pragma once
#include "ParameterizedObject.hpp"
#include "driver.hpp"
#include <deque>

class HTTPTagDriverContext  : virtual public DriverContext
{
public:
  HTTPTagDriverContext() : ParameterizedObject(myParameters),
                           cmdEnable(false){  }         
                           
  virtual bool setParameterById(uint32_t id, VarData newValue) override;
	static const ParameterVector myParameters; 
  bool         cmdEnable; ///< If it is true, you can manage Tag from Web
  std::string  tagName;   ///< std::string pointer that contains tag name to be show in Web 
};

class HTTPDriver : virtual public Driver
{  
  
  enum RunningState : uint32_t
  {
		rsInitializing,
		rsRunning
  };
  
  using SignalMapHTTP = std::map<std::string, bool>; 
  using AnalogMapHTTP = std::map<std::string, std::string>;     

  using SpontTagsDeque = std::deque<Tag*>;
  using HTTPTagsList   = std::forward_list<Tag*>;

  SignalMapHTTP   signalMap;///< bool tags without commands
  AnalogMapHTTP   analogMap;///< all another tags without commands
  SignalMapHTTP   cntrolMap;///< bool tags with commands
  AnalogMapHTTP   pointsMap;///< all another tags with commands

  SpontTagsDeque  spontTags;///< Deque for spont data
  HTTPTagsList    httpTags; ///< Create my own tags list because tagsList was empty???
    
  RunningState    runState;///< Status of state machine

  const uint8_t maxNumOfSpontTags = 50;
  
public: 
  virtual void registerTag(const Tag &tag, void *ctx);
  virtual bool tagChanged(const Tag &tag, uint32_t paramId, VarData newValue);
  virtual bool setParameterById(uint32_t id, VarData newValue) override { return false;};
  virtual bool setRuntimeParameter(const Tag &tag, uint32_t paramId, VarData paramValue) {  return false; }
  virtual bool setTagRuntimeParameter(const Tag &tag, uint32_t paramId, VarData paramValue) {  return false; }
  virtual VarData getRuntimeParameter(uint32_t paramId){ return NO_DATA; }
  virtual VarData getTagRuntimeParameter(const Tag &tag, uint32_t paramId){ return NO_DATA; }
  virtual DriverContext *createTagContext() const;
  virtual void initialize();
  virtual void run();
  
  bool     parseCmdFromAJAX(char* param, char* values, char *errSSI);
  uint32_t mapsToJSON(char *pcInsert, uint8_t type);
  
  HTTPDriver():ParameterizedObject(ParameterizedObject::NO_PARAMETERS),
               runState(rsInitializing){ }
  
};