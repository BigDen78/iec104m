#ifndef ADCDRIVER_H
#define ADCDRIVER_H

#include "main.h"
#include "timed.h"
#include "FreeRTOS.h"
#include "queue.h"
#include "task.h"
#include <stdbool.h>
#include <stdlib.h>
#include <arm_math.h>
#include <math.h> 
#ifndef TEST_UPS
#include "adcRead.h"
#else
#include "ads8320.h"
#endif

#define USE32BIT_VAR_ADC     (0)

#if (USE32BIT_VAR_ADC == 1)
typedef float ADC_ChannelsLogger;    ///< Contain logs for group of channels
#else
typedef uint16_t ADC_ChannelsLogger; ///< Contain logs for group of channels
#endif

typedef float ADC_RawType;           ///< Can be any type in future

/**
  * @brief Aim specific filter types
  */
typedef enum 
{ 
  ADC_KALMAN
}ADC_Filter;

/**
  * @brief Base TIM2 Freq
  */
typedef enum 
{
  ADC_TIM_1S    = 1,     ///< 1 Sec
  ADC_TIM_500MS = 2,     ///< 0,5 Sec
  ADC_TIM_1MS   = 1000,  ///< 1 mS
  ADC_TIM_100US = 10000, ///< 100 uS
}ADC_Period;

#ifdef ADS8320_H
/**
  * @brief Type of ADCType
  */
typedef enum 
{
  ADC_NONE,    ///< Disable analog input channel
  ADC_ADS8320  ///< Enable analog input channel
}ADC_Type;

/**
  * @brief Board specific hardware cannel parametr
  */
typedef struct
{
  SPI_TypeDef*  ADC_SpiPorts; ///< AI SPI port
  GPIO_TypeDef* ADC_CSPorts;  ///< AI port
  uint16_t      ADC_CSPins;   ///< AI pin
  ADC_Type      adcTypes;     ///< Soldering ADC chip (that reading low level method will be used)
  ADC_RawType   highRawPoint; ///< Max high raw read point usually it is the max adc value 65535
}ADC_BoardSpec;
#else

/**
  * @brief Board specific hardware cannel parametr
  */
typedef struct
{
  SPI_TypeDef*          LL_SPI;         ///< LL SPI port
  SPI_HandleTypeDef*    HAL_SPI;        ///< HAL SPI port
  GPIO_TypeDef*         ADC_CSPorts;    ///< AI port
  uint16_t              ADC_CSPins;     ///< AI pin
  ADC_Type              adcTypes;       ///< Soldering ADC chip (that reading low level method will be used)
  ADC_RawType           highRawPoint;   ///< Max high raw read point usually it is the max adc value 65535
}ADC_BoardSpec;
#endif
/**
  * @brief Struct contain params for channel and current measurement
  */
typedef struct
{
  bool          test;            ///< AI channel simulator mode
  void*         dcmdAdcRdy;      ///< Pointer to Command 
  ADC_BoardSpec *pADC_BoardSpec;   ///< Pointer to array member of ADC_BoardSpec[]
  uint16_t      numPoints;       ///< Number of points to be filtering per one Runtime send    
  uint8_t       windowCnt;       ///< How many points in fBuffer is now  
  ADC_RawType  *fBuffer;         ///< Buffer contains values that will be filtering soon
  ADC_Filter    filterType;      ///< Type of using filter, index in enum  
  void         *fParams;         ///< Pointer to filter params struct  
  float         zeroPoint;       ///< ZeroPoint in signal units
  float         highPoint;       ///< MaxPoint in signal units  
  float         value;           ///< Secondary value  
  ADC_RawType   aperture;        ///< If raw value change low not send to Runtime
  ADC_RawType   lastValue;       ///< Last value that was sent to Runtime  
  bool          loggerEn;        ///< If true it will be used logBuffer   
}ADC_Channel;

/**
  * @brief Log buffer for oscillo
  */
typedef struct
{
  bool  busy;                         ///< Contains oscillo now 
  ADC_ChannelsLogger **padcChannels;  ///< Pointer to loggerEn channels logUnits
}ADC_LogBuffer;

/**
  * @brief Logging oscillo control struct
  */
typedef struct 
{ 
  uint8_t        enabledLogChannels; ///< Enabled log channels
  uint8_t        currentBuffer;      ///< Buffers 0 or 1 for non-stop writing
  uint16_t       samplesPerMs;       ///< Points per ms
  uint32_t       nSamples[2];        ///< [1] Total number of points in oscillogram and in linear buffer, [0] - 20% of linear buffer used by ring buffer
  uint32_t       logSize;            ///< Default length of oscillo in ms
  uint64_t       systemTs;           ///< System time for return to absolute time
  ADC_LogBuffer  logBuffers[2];      ///< Two buffers for oscillo linear and ring
  uint32_t       bufPos[2];          ///< Current position in ring or linear buffer
  TaskHandle_t   xLoggerTask;        ///< Logging Task handle
  TaskHandle_t   xAdcTask;           ///< Task Handle for ADC_Task
}ADC_LogCntr;

/*---------------Prototypes-------------------*/

void ADC_Init(ADC_Channel *pADC_Channel);
void ADC_InitTask(void);                       ///< Start low-level adc task and TIM
void ADC_TIM_SetPeriod(ADC_Period periodType); ///< Set TIM BASE Period
ADC_Period ADC_TIM_GetPeriod(void);            ///< Get TIM BASE Period
void ADC_SetLogSize(uint32_t logSize);  
uint32_t ADC_GetLogSize(void);  
float ADC_ConvertRawToSecondary(ADC_Channel *pADC_Channel, ADC_RawType x);
ADC_RawType ADC_ConvertSecondaryToRaw(ADC_Channel *pADC_Channel, float y);
void ADC_SetNumChannels(uint8_t num);
void ADC_SetChannelsPointer(ADC_Channel **padcChannel);
void ADC_SetRDYQueue(QueueHandle_t hQueue);
ADC_LogCntr* ADC_GetLogCtrStruct(void);

#endif /* ADCDRIVER_H */