#pragma once

#include "ParameterizedObject.hpp"
#include "driver.hpp" 

#include "main.h"
#include "stdlib.h"  
#include "nwUtils.hpp"

#include <map>  
#include <sstream>
#include <fstream>   
#include <string>    

#include "usart.h"
#include "hal_serial.h"
#include "cs101_slave.h"
		  
// Типы данных -----------------------------------------------------------------------------------------//  
class ZNOYMasterTagContext;
class ZNOYMasterDriver;

using registersArray = std::map <uint32_t, ZNOYMasterTagContext *>;

/**
  * Класс SNMP - драйвера
  *
  */
class ZNOYMasterDriver : virtual public Driver
{
  public:      
   ZNOYMasterDriver();
			    
   // Методы от родительского класса ---------------------------------------------------------------------------//
   virtual void registerTag(const Tag& tag, void* ctx) ; 
   virtual void initialize();     
   virtual void run();
   
   virtual bool tagChanged(const Tag &tag, uint32_t paramId, VarData newValue);
   virtual bool setParameterById(uint32_t id, VarData newValue) override;
   virtual bool setRuntimeParameter(const Tag &tag, uint32_t paramId, VarData paramValue);
   virtual bool setTagParameter(const Tag& tag, uint32_t paramId, VarData newValue);
   virtual bool setTagRuntimeParameter(const Tag& tag, uint32_t paramId, VarData paramValue);
   
   virtual VarData getRuntimeParameter(uint32_t paramId);
   virtual VarData getTagRuntimeParameter(const Tag& tag, uint32_t paramId);
   
   virtual DriverContext *createTagContext() const;  
   
   // Новые методы ---------------------------------------------------------------------------// 
   void requestHoldingRegs(uint16_t startNum, uint16_t amount);
   bool parseRequest(SerialPort port);
   void setAllRegistersValue(uint16_t startNum, uint16_t amount, float value);
   void markRegistersAsInvalid(uint16_t startNum, uint16_t amount);
   //---------------------------------------------------------------------------------------------//
   
   static const ParameterVector znoyParameters; 
   static registersArray Registers;
  
   uint8_t requestBuffer[8];
   DriverCommand dcmdSerialIdle;
   
   uint32_t slaveAddress;
   uint32_t requestFrequencyMS;
   uint32_t attemptsAmount;
   UART_HandleTypeDef *huart;
   uint32_t baudRate;
   uint8_t stopBits;
   char parity;         
   SerialPort port;  
  
   // Запрашиваемое количество регистров //
   uint32_t requestedAmount = 0;
      
   enum RunningState : uint32_t
   {
      rsPreInit,
      rsInitializing,
      rsRunning,
      rsAnswerWaiting,
      rsUnrecoverableError
   };
   
   RunningState currentState;
};

/**
  * Контекст тега
  *
  */
class ZNOYMasterTagContext : virtual public DriverContext
{     
  public:
   ZNOYMasterTagContext() : ParameterizedObject(znoyTagParameters) {}
   virtual ~ZNOYMasterTagContext() = default;    
   
   virtual bool setParameterById(uint32_t id, VarData newValue) override;
  
   static const ParameterVector znoyTagParameters;
  
   Tag* tagPtr;
   uint32_t address;
   float value;
};
