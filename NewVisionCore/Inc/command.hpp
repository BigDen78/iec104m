#pragma once
#include <core.hpp>
#include <timed.h>

class Command
{
public:
	virtual void execute() = 0;
};

class ChangeTagCommand : virtual public Command
{
public:
	ChangeTagCommand() : pTag(0), data({0}), valid(false), senderId(-1), ts(0) {};
	ChangeTagCommand(const ChangeTagCommand&) = default;
	ChangeTagCommand(Tag *pTag, VarData newData, bool valid, uint32_t senderId) : pTag(pTag), data(newData), valid(valid), senderId(senderId), ts(getShortTS()){};
	ChangeTagCommand(Tag *pTag, VarData newData, bool valid, uint32_t senderId, uint32_t ts) : pTag(pTag), data(newData), valid(valid), senderId(senderId), ts(ts){};
	const Tag *getTag() const { return pTag; };
	VarData getData() const {return data;}
	uint32_t getTS() const {return ts;}
	bool getValid() const {return valid;}
	uint32_t getSenderId() const {return senderId;}
	virtual void execute()
	{
		pTag->setValue(data);
		pTag->setValid(valid);
		pTag->updateTS(ts);
	}
protected:
	Tag *pTag;
	VarData data;
	uint32_t senderId;
	uint32_t ts;
	bool valid;
};
