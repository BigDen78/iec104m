#ifndef KALMAN_H
#define KALMAN_H

#include "ADCDriver.h"

#define VAR_VALUE_DEFAULT		(float)10.0
#define VAR_PROCESS_DEFAULT	(float)1e-5
#define P_DEFAULT			      (float)1000.0

typedef struct 
{
	float varValue; /* With an increase in this coefficient, the reaction rate decreases */
	float varProcess;
	float pc;
	float gain;
	float p;
	float xp;
	float zp;
	float xe;
}sKalman;

void Kalman_init(sKalman *S, float varValue, float varProcess);
ADC_RawType Kalman(sKalman *S, ADC_RawType value);

#endif /* KALMAN_H */
