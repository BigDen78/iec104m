#pragma once
#include <vector>
#include <string>
#include <ci_char_traits.hpp>
#include <FreeRTOS.h>
#include <task.h>

#pragma diag_suppress = Pe540
#pragma diag_suppress = Pe111
#pragma diag_suppress = Pe186
#define JSON_THROW_USER(exception) \
  vTaskDelete(NULL);               \
  ::abort();
#include <json.hpp>
#pragma diag_default = Pe540
#pragma diag_default = Pe511
#pragma diag_default = Pe186

struct StaticParameterInfo;
using String = std::basic_string<char, ci_char_traits>;
using CString = std::string;
using ParameterPair = std::pair<String, StaticParameterInfo>;
using ParameterVector = std::vector<ParameterPair>;
using Json = nlohmann::basic_json<std::map, std::vector, String, bool, int32_t, uint32_t, float, std::allocator, nlohmann::adl_serializer>;
using JsonObject = Json::object_t;

///metadata: type enum
enum DataType
{
  dtVoid,         ///<no actual data(default unitialized state
  dtBool,         ///<boolean
  dtByte,         ///<1 byte unsigned int
  dtWord,         ///<2 bytes unsigned short int
  dtDWord,        ///<4 bytes unsigned int
  dtShortInt,     ///<2 bytes signed int
  dtInt,          ///<4 bytes signed int
  dtFloat,        ///<4 bytes floating point
  dtString,       ///<ASCII string
  dtWString,      ///<unicode UTF-8 string
  dtTemplateInst, ///<special value meaning that tag type will be deduced from template
  dtMaxType       ///<dummy name that marks max possible enum value
};

static const uint32_t dataSizeOfType[dtMaxType] = {
    0,                //dtVoid
    sizeof(bool),     //dtBool
    sizeof(uint8_t),  //dtByte
    sizeof(uint16_t), //dtWord
    sizeof(uint32_t), //dtDWord
    sizeof(int16_t),  //dtShortint
    sizeof(int32_t),  //dtInt
    sizeof(float),    //dtFloat
    sizeof(char *),   //dtString
    sizeof(wchar_t *),//dtWString
    sizeof(nullptr_t) //dtTemplateInst
};

/**
 * @brief union containing actual tag data
 * field used to access data is depending on StaticTagInfo#type member of tag's static info
 */
union VarData {
  bool boolVal;
  uint8_t byteVal;
  uint16_t wordVal;
  int16_t shortVal;
  uint32_t dwordVal;
  int32_t intVal;
  float fltVal;
  char *pStr;
};

static const VarData NO_DATA{false};

/**
 * @brief struct that describes tag parameter or driver parameter
 *
 */
struct StaticParameterInfo
{
  ///parameter id defined by driver
  uint32_t id;
  ///type of parameter
  DataType type;
  ///flags of parameter
  enum ParameterFlags
  {
    ///No flags at all
    pfNone = 0,
    ///Parameter is required. If requred parameter is missing in json the corresponding tag/driver won't be initialized
    pfRequired = 1,
    /**
		 * @brief Parameter can be changed at run time.
		 * Driver that declared one of it's (or it's tags) parameter as runtime changeable have to implement
		 * Driver#updateRuntimeParameter and/or Driver#setTagRuntimeParameter methods
		 */
    pfRuntimeChangeable = 2,
    /**
		 * @brief changing of parameters value at run time requires system reset
		 */
    pfRebootRequired = 4
  };
  uint32_t flags;
};

class ParameterizedObject
{
public:
  ParameterizedObject() = delete;
  ParameterizedObject(const ParameterVector &parameters) : myParameters(parameters){};
  virtual ~ParameterizedObject() = default;
  virtual bool setParameterById(uint32_t id, VarData newValue) = 0;
  virtual bool getParameterById(uint32_t id, VarData &value) const { return false; }
 
  virtual const StaticParameterInfo *getParameterInfo(const String &paramName) const
  {
    auto findResult = std::find_if(myParameters.begin(), myParameters.end(),
                                   [&paramName](auto item) -> bool { return !item.first.compare(paramName); });
    return ((myParameters.end() != findResult) ? &(findResult->second) : ((StaticParameterInfo *)nullptr));
  }
  virtual const ParameterVector& getParameters() { return myParameters; }
  static const ParameterVector NO_PARAMETERS; ///< Equal to nullptr but for reference
  bool initFromJson(const JsonObject &json);
protected:
  bool ReadParameterByNameFromJson(const String &name, const Json &json);
  const ParameterVector &myParameters;
};
