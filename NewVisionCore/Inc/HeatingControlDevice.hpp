#pragma once
#include "driver.hpp"
#include "CommutationBlock.hpp"
#include "IMD.hpp"

enum HeatingParams : uint32_t
{
  timer
};

enum tagsType : uint8_t
{
  
};

class HeatingControlDevice : virtual public Driver
{
public:

	HeatingControlDevice();
	virtual void registerTag(const Tag& tag, void* ctx);
	virtual bool tagChanged(const Tag &tag, uint32_t paramId, VarData newValue);
	virtual bool setParameterById(uint32_t id, VarData newValue) override  ;
	virtual bool setRuntimeParameter(const Tag &tag, uint32_t paramId, VarData paramValue);
	virtual bool setTagParameter(const Tag &tag, uint32_t paramId, VarData newValue);
	virtual bool setTagRuntimeParameter(const Tag& tag, uint32_t paramId, VarData paramValue);
        virtual DriverContext* createTagContext() const override;
	virtual VarData getRuntimeParameter(uint32_t paramId);
	virtual VarData getTagRuntimeParameter(const Tag& tag, uint32_t paramId);
	virtual void initialize();
	virtual void run();

        static const ParameterVector parameters;
    
        uint32_t spontaneousTime = 500;

        IMD imd;
        CommutationBlock commBlock[12];

};



class HeatingDriverTagContext : virtual public DriverContext
{
public:
  HeatingDriverTagContext() : ParameterizedObject(NO_PARAMETERS){ }
  ~HeatingDriverTagContext(){}

  virtual bool setParameterById(uint32_t id, VarData newValue) override {return false;}
  static const ParameterVector myParameters;
};