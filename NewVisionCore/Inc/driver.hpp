#pragma once
#include <string>
#include <forward_list>
#include <command.hpp>
#include <CommandQueue.hpp>

using TagList = std::forward_list<Tag *>;

class DriverCommand;
class Device;
class DeviceTemplate;

using DeviceList = std::forward_list<Device *>;

struct HistoricalData
{
  const TagList *pTagList; ///< List of asked tags
  VarData  *pData;         ///< Data from AIdriver
  uint64_t  tsBegin;       ///< Unix time [s]
  uint16_t  samplesPerMs;  ///< Number of points per ms
  uint32_t  nSamples;      ///< Total number of points in oscillo
};

class Driver : virtual public ParameterizedObject
{
	friend class DriverBuilder;

public:
	using QueType = InterThreadQueue<DriverCommand>;
	typedef bool (Driver::*ThreadSafeFn)(const Tag &tag, uint32_t paramId, VarData newValue); // Method pointer type
	static const uint32_t NO_PARAM_ID = 0xffffffffu;
	Driver() : scanRate(10), queueDepth(10), stackDepth(1024), devicesSupported(false), devicesList(nullptr){};
	virtual void registerTag(const Tag &tag, void *ctx) = 0;
	virtual void postTagChanged(const Tag &pTag, VarData newData, uint32_t ts);
	virtual void postSetRuntimeParameter(uint32_t paramId, VarData paramValue);
	virtual void postSetTagRuntimeParameter(Tag *tag, uint32_t paramId, VarData paramValue);
	virtual bool tagChanged(const Tag &tag, uint32_t paramId, VarData newValue) = 0;
	virtual bool setRuntimeParameter(const Tag &tag, uint32_t paramId, VarData paramValue) = 0;
	virtual bool setTagRuntimeParameter(const Tag &tag, uint32_t paramId, VarData paramValue) = 0;
	virtual VarData getRuntimeParameter(uint32_t paramId) = 0;
	virtual VarData getTagRuntimeParameter(const Tag &tag, uint32_t paramId) = 0;
	virtual void initialize();
	virtual void run() = 0;
	virtual DriverContext *createTagContext() const { return nullptr; }
	virtual void setTagsList(TagList *list) { tagsList = list; }
	virtual bool supportsDevices() { return devicesSupported; }
	virtual void setDevicesList(DeviceList *list) { devicesList = list; }
	virtual bool registerDevice(Device *device, void *ctx) { return false; }
	virtual const String &getName() const { return name; }
	virtual uint32_t getId() const { return id; }
	virtual void setScanRate(uint32_t scanRateMs) { scanRate = scanRateMs; }
	virtual void setQueueDepth(uint32_t queueDepth) { this->queueDepth = queueDepth; }
	virtual void setStackDepth(uint32_t stackDepth) { this->stackDepth = stackDepth; }
  virtual bool getHistoricalData(uint64_t ts, HistoricalData *pData) { return false; }
  virtual bool supportsHistory() { return false; }
	static void driverThreadProc(void *pData);
	virtual ~Driver() = default; //to supress Pa096 warning
protected:
	virtual void setId(uint32_t id) { this->id = id; }
	virtual void setName(const String &name) { this->name = name; }
	uint32_t id;
	uint32_t queueDepth;
	uint32_t scanRate;
	uint32_t stackDepth;
	bool devicesSupported;
	QueType *commandQueue;
	String name;
	TaskHandle_t threadHandle;
	TagList    *tagsList;
	DeviceList *devicesList;
};

class DriverCommand : virtual public Command
{
public:
	DriverCommand() = default;
	DriverCommand(const DriverCommand &) = delete;
	DriverCommand(Driver *driver, const Tag *tag, VarData newValue, uint32_t ts) : type(dctUpdateTag), driver(driver), tag(tag), shortTS(ts), newValue(newValue){};
	DriverCommand(Driver *driver, uint32_t paramId, VarData newValue) : type(dctUpdateDriverMeta), driver(driver), tag(&Tag::NoTag), paramId(paramId), newValue(newValue){};
	DriverCommand(Driver *driver, const Tag *tag, uint32_t paramId, VarData newValue) : type(dctUpdateTagMeta), driver(driver), tag(tag), paramId(paramId), newValue(newValue){};
	virtual void execute()
	{
		(driver->*driverMethods[type])(*tag, paramId, newValue);
	}

protected:
	enum DriverCommandType
	{
		dctUpdateTag,
		dctUpdateTagMeta,
		dctUpdateDriverMeta,
		dctMax
	} type;
	Driver *driver;
	const Tag *tag;
	union{
		uint32_t paramId;
		uint32_t shortTS;
	};
	VarData newValue;
	static const Driver::ThreadSafeFn driverMethods[dctMax];
};

class Device : virtual public ParameterizedObject
{
	friend class DeviceBuilder;

public:
	Device() : parent(nullptr) {}
	virtual bool registerTag(const Tag &tag, void *ctx) = 0;
	virtual void initialize(void *pvParameter) = 0;
	virtual void select(void *pvParameter) = 0;
	virtual bool tagChanged(const Tag &tag, uint32_t paramId, VarData newValue) = 0;
	virtual bool run() = 0;
	virtual bool canRelease() = 0;
	virtual bool release() = 0;
	virtual bool handleIncomingData(void *pData) = 0;
	virtual void handleLinkLayerMessage(void *pData) = 0;
	virtual void *getUnderlyingObject() = 0;
	virtual uint32_t getId() const
	{
		return id;
	}
	virtual const String &getName() const { return name; }
	virtual const Driver *getParent() const { return parent; }
	virtual const DeviceTemplate *getTemplate() const { return myTemplate; }
	virtual ~Device() = default;

protected:
	virtual void setId(uint32_t id) { this->id = id; }
	virtual void setParent(const Driver *parent) { this->parent = parent; }
	virtual void setName(String &&name) { this->name = name; }
	virtual void setTemplate(const DeviceTemplate *devTemplate) { myTemplate = devTemplate; }
	uint32_t id;
	const Driver *parent;
	const DeviceTemplate *myTemplate;
	String name;
};

class DeviceTemplate
{
	friend class DeviceBuilder;

public:
	TagTemplate *getTemplate(const String &name) const
	{
		auto it = templateNamesMap.find(name);
		if (templateNamesMap.end() != it)
			return it->second;
		return nullptr;
	}
	void registerTemplate(TagTemplate *tpl)
	{
		templateNamesMap.insert_or_assign(tpl->getName(), tpl);
	}

	NamesOfTagTemplates &getTemplateNames() { return templateNamesMap; }

protected:
	NamesOfTagTemplates templateNamesMap;
};
