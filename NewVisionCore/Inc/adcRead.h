#ifndef ADCREAD_H
#define ADCREAD_H

#include <stdint.h>
#include "main.h"
#include "timed.h"

/*---------------Prototypes-------------------*/
/**
  * @brief Type of ADCType
  */
typedef enum 
{
  ADC_NONE,
  ads8320,  
  ltc2452,
  mcp3208,
  maximumNum ///<maximum number of adc
}ADC_Type;

typedef int (*pReadAdcLLFunc)(SPI_TypeDef * spi, GPIO_TypeDef  *nssPort, uint16_t nssPin); 
typedef int (*pReadAdcHALFunc)(SPI_HandleTypeDef * spi, GPIO_TypeDef  *nssPort, uint16_t nssPin); 


extern pReadAdcLLFunc readLLADC[maximumNum];
extern pReadAdcHALFunc readHALADC[maximumNum];

#endif /* ADCREAD_H */