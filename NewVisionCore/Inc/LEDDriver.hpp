#pragma once
#include "driver.hpp"

extern "C"
{
#include "main.h"
}

class LEDDriverTagContext : virtual public DriverContext
{
public:
  LEDDriverTagContext(): ParameterizedObject(myParameters), pinState(0), invert(false){}
  bool             invert;
  uint8_t          pinState;    ///< Last state of led
  uint8_t          driverType;
  uint8_t          ledNumber;
  Tag*      pTag;
  virtual bool setParameterById(uint32_t id, VarData newValue) override;
  static const ParameterVector myParameters; 
};

class LEDDriver : virtual public Driver
{
public:
	virtual void registerTag(const Tag& tag, void* ctx);
	virtual bool tagChanged(const Tag &tag, uint32_t paramId, VarData newValue);
	virtual bool setParameterById(uint32_t paramId, VarData newValue){return false;};
	virtual bool setRuntimeParameter(const Tag &tag, uint32_t paramId, VarData paramValue) { return false; };
	virtual bool setTagParameter(const Tag& tag, uint32_t paramId, VarData newValue);
	virtual bool setTagRuntimeParameter(const Tag& tag, uint32_t paramId, VarData paramValue){return false;};
	virtual VarData getRuntimeParameter(uint32_t paramId);
	virtual VarData getTagRuntimeParameter(const Tag& tag, uint32_t paramId);
	virtual DriverContext* createTagContext() const override;
	virtual void initialize();
	virtual void run();
  LEDDriver():ParameterizedObject(ParameterizedObject::NO_PARAMETERS){}
};