#pragma once
#include "driver.hpp"


enum LogicParams : uint32_t
{
  timer
};

class LogicDriver : virtual public Driver
{
public:

	LogicDriver();
	virtual void registerTag(const Tag& tag, void* ctx);
	virtual bool tagChanged(const Tag &tag, uint32_t paramId, VarData newValue);
	virtual bool setParameterById(uint32_t id, VarData newValue) override  ;
	virtual bool setRuntimeParameter(const Tag &tag, uint32_t paramId, VarData paramValue);
	virtual bool setTagParameter(const Tag &tag, uint32_t paramId, VarData newValue);
	virtual bool setTagRuntimeParameter(const Tag& tag, uint32_t paramId, VarData paramValue);
        virtual DriverContext* createTagContext() const override;
	virtual VarData getRuntimeParameter(uint32_t paramId);
	virtual VarData getTagRuntimeParameter(const Tag& tag, uint32_t paramId);
	virtual void initialize();
	virtual void run();

        static const ParameterVector parameters;

        uint32_t spontaneousTime = 500;
        /*
protected:
	CString configPath;*/

};



class LogicDriverTagContext : virtual public DriverContext
{
public:
  LogicDriverTagContext() : ParameterizedObject(NO_PARAMETERS){ }
  ~LogicDriverTagContext(){}

  virtual bool setParameterById(uint32_t id, VarData newValue) override {return false;}
  static const ParameterVector myParameters;
};