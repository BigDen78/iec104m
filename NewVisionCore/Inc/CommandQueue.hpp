#pragma once
#include <queue>
#include <deque>
#include <functional>
#include <algorithm>
#include <FreeRTOS.h>
#include <queue.h>
#include <task.h>
#include <time.h>

class QTimer
{
  public:
   QTimer(clock_t period)      : armed(false) 	 , period(period)    	    , timeoutMark(0)    	    {} 	;
	QTimer()
		: QTimer(-1)
	{}
	;
	void rearm()
	{
		if (period != -1)
		{
			timeoutMark = clock() + period;
			armed = true;
		}
	}
 
	bool ready()
	{
		if (armed && (timeoutMark <= clock()))
		{
			armed = false;
			return true;
		}
		return false;
	}

	bool isLate()
	{
		return armed && (timeoutMark <= clock());
	}
	
	bool isLate(clock_t now)
	{
		return armed && (timeoutMark <= now);
	}
	
	void setPeriod(clock_t period) { this->period = period; }
	clock_t getPeriod() { return period; }
	bool isArmed() { return armed; }
	clock_t getTimeoutMark() { return armed ? timeoutMark : 0; }

protected:
	clock_t timeoutMark;
	clock_t period;
	bool armed;
};

template <class T>
	class InterThreadQueue
	{
	public:
		typedef T value_type;

		InterThreadQueue<T>(int size, const char *name, TickType_t timeout) : timeout(timeout), timer()
		{
			handle = xQueueCreate(size, sizeof(value_type));
			vQueueAddToRegistry(handle, name);
		}

		bool push(const T *ptr)
		{
			return (pdTRUE == xQueueSendToBack(handle, ptr, portMAX_DELAY));
		}

		bool pop(T *ptr)
		{
			auto ticksToWait = timeout;
			taskENTER_CRITICAL();
			auto now = clock();
			if (timer.isArmed() )
			{
				if (!timer.isLate(now))
				{
					auto altTicksToWait = timer.getTimeoutMark() - now;
					if (altTicksToWait > 0 && (altTicksToWait < ticksToWait || -1 == ticksToWait))
						ticksToWait = altTicksToWait;
				}
				else
				{
					ticksToWait = 0;
				}
			}			
			taskEXIT_CRITICAL();
			return (pdTRUE == xQueueReceive(handle, ptr, ticksToWait));
		}

		size_t size()
		{
			return static_cast<size_t>(uxQueueMessagesWaiting(handle));
		}

		QTimer &getTimer()
		{
			return timer;
		}

		QueueHandle_t getHandle()
		{
			return handle;
		}

	protected:
		QueueHandle_t handle;
		TickType_t timeout;
		QTimer timer;
	};