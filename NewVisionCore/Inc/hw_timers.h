#ifndef HW_TIMERS_H_
#define HW_TIMERS_H_

#ifdef __cplusplus
extern "C" {
#endif
  
#include "main.h"
#include "FreeRTOS.h"
#include "task.h"

typedef struct 
{
  TIM_TypeDef* tim;         ///< example: TIM2
  TaskHandle_t xTask;       ///< Set it to NULL if you want to use callback else it will send Notify to task
  uint32_t     period;      ///< example: 1 - 1s, 1000 - 1ms, This will be period = a * b, so a and b will calc automaticaly, but you must check float not exist
  uint8_t      nvicPriority;///< example: 5
  IRQn_Type    irqType;     ///< example: TIM2_IRQn
  uint32_t     apbType;     ///< example: LL_APB1_GRP1_PERIPH_TIM2
  void         (*func)(void*);
  void         *callbackParams;
}TimerCntr;
  
void timerInit(TimerCntr *pTimCntr);
void timerStart(TimerCntr *pTimCntr);
void timerStop(TimerCntr *pTimCntr);

#ifdef __cplusplus
}
#endif

#endif	/* HW_TIMERS_H_ */
