#pragma once

const char* jsonStr = R"JSON(
{
  "core": {
    "queueDepth": 100
  },
  "drivers": [
  {
    "name": "Settings",
    "class": "SettingsDriver",
    "scanRate": -1,
    "queueDepth": 10,
    "stackDepth": 512,
    "parameters": {
      "file": "0:\\settings.json",
      "writeTimeout": 2000,
      "resetTimeout": 2000
    }
  },
  {
    "name": "IEC104M",
    "class": "IEC104MasterDriver",
    "parameters": {
      "port": 2404,
      "IPAddr": "192.168.137.1",
      "spontDelay": 50,
      "linkAddress": 3,
      "ASDUAddress": 3
    }
  },
  {
    "name":"Logic",
    "class":"LogicDriver",
    "parameters":{
      "spontaneousTime":1000
    }
  }
    ],
    
    "devices": [
    {
      "class": "IEC104GenericSlave",
      "parent": "IEC104M",
      "name": "IEC104T",
      "template": "IEC104T",
      "parameters": {
        "linkAddress": 3,
        "ASDUAddress": 3
      }
    }    
      ],
    
      "tags": [
         {
           "type":"Instance",
           "name":"Pixel_0000",
           "base": "TPixel_0000",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0100",
           "base": "TPixel_0100",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0200",
           "base": "TPixel_0200",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0300",
           "base": "TPixel_0300",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0400",
           "base": "TPixel_0400",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0500",
           "base": "TPixel_0500",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0600",
           "base": "TPixel_0600",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0700",
           "base": "TPixel_0700",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0800",
           "base": "TPixel_0800",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0900",
           "base": "TPixel_0900",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_1000",
           "base": "TPixel_1000",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_1100",
           "base": "TPixel_1100",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_1200",
           "base": "TPixel_1200",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_1300",
           "base": "TPixel_1300",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_1400",
           "base": "TPixel_1400",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_1500",
           "base": "TPixel_1500",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0001",
           "base": "TPixel_0001",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0101",
           "base": "TPixel_0101",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0201",
           "base": "TPixel_0201",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0301",
           "base": "TPixel_0301",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0401",
           "base": "TPixel_0401",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0501",
           "base": "TPixel_0501",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0601",
           "base": "TPixel_0601",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0701",
           "base": "TPixel_0701",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0801",
           "base": "TPixel_0801",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0901",
           "base": "TPixel_0901",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_1001",
           "base": "TPixel_1001",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_1101",
           "base": "TPixel_1101",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_1201",
           "base": "TPixel_1201",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_1301",
           "base": "TPixel_1301",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_1401",
           "base": "TPixel_1401",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_1501",
           "base": "TPixel_1501",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0002",
           "base": "TPixel_0002",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0102",
           "base": "TPixel_0102",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0202",
           "base": "TPixel_0202",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0302",
           "base": "TPixel_0302",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0402",
           "base": "TPixel_0402",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0502",
           "base": "TPixel_0502",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0602",
           "base": "TPixel_0602",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0702",
           "base": "TPixel_0702",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0802",
           "base": "TPixel_0802",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0902",
           "base": "TPixel_0902",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_1002",
           "base": "TPixel_1002",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_1102",
           "base": "TPixel_1102",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_1202",
           "base": "TPixel_1202",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_1302",
           "base": "TPixel_1302",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_1402",
           "base": "TPixel_1402",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_1502",
           "base": "TPixel_1502",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0003",
           "base": "TPixel_0003",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0103",
           "base": "TPixel_0103",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0203",
           "base": "TPixel_0203",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0303",
           "base": "TPixel_0303",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0403",
           "base": "TPixel_0403",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0503",
           "base": "TPixel_0503",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0603",
           "base": "TPixel_0603",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0703",
           "base": "TPixel_0703",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0803",
           "base": "TPixel_0803",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0903",
           "base": "TPixel_0903",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_1003",
           "base": "TPixel_1003",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_1103",
           "base": "TPixel_1103",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_1203",
           "base": "TPixel_1203",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_1303",
           "base": "TPixel_1303",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_1403",
           "base": "TPixel_1403",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_1503",
           "base": "TPixel_1503",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0004",
           "base": "TPixel_0004",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0104",
           "base": "TPixel_0104",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0204",
           "base": "TPixel_0204",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0304",
           "base": "TPixel_0304",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0404",
           "base": "TPixel_0404",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0504",
           "base": "TPixel_0504",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0604",
           "base": "TPixel_0604",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0704",
           "base": "TPixel_0704",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0804",
           "base": "TPixel_0804",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0904",
           "base": "TPixel_0904",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_1004",
           "base": "TPixel_1004",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_1104",
           "base": "TPixel_1104",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_1204",
           "base": "TPixel_1204",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_1304",
           "base": "TPixel_1304",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_1404",
           "base": "TPixel_1404",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_1504",
           "base": "TPixel_1504",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0005",
           "base": "TPixel_0005",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0105",
           "base": "TPixel_0105",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0205",
           "base": "TPixel_0205",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0305",
           "base": "TPixel_0305",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0405",
           "base": "TPixel_0405",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0505",
           "base": "TPixel_0505",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0605",
           "base": "TPixel_0605",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0705",
           "base": "TPixel_0705",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0805",
           "base": "TPixel_0805",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0905",
           "base": "TPixel_0905",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_1005",
           "base": "TPixel_1005",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_1105",
           "base": "TPixel_1105",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_1205",
           "base": "TPixel_1205",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_1305",
           "base": "TPixel_1305",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_1405",
           "base": "TPixel_1405",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_1505",
           "base": "TPixel_1505",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0006",
           "base": "TPixel_0006",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0106",
           "base": "TPixel_0106",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0206",
           "base": "TPixel_0206",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0306",
           "base": "TPixel_0306",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0406",
           "base": "TPixel_0406",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0506",
           "base": "TPixel_0506",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0606",
           "base": "TPixel_0606",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0706",
           "base": "TPixel_0706",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0806",
           "base": "TPixel_0806",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0906",
           "base": "TPixel_0906",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_1006",
           "base": "TPixel_1006",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_1106",
           "base": "TPixel_1106",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_1206",
           "base": "TPixel_1206",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_1306",
           "base": "TPixel_1306",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_1406",
           "base": "TPixel_1406",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_1506",
           "base": "TPixel_1506",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0007",
           "base": "TPixel_0007",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0107",
           "base": "TPixel_0107",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0207",
           "base": "TPixel_0207",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0307",
           "base": "TPixel_0307",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0407",
           "base": "TPixel_0407",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0507",
           "base": "TPixel_0507",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0607",
           "base": "TPixel_0607",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0707",
           "base": "TPixel_0707",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0807",
           "base": "TPixel_0807",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0907",
           "base": "TPixel_0907",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_1007",
           "base": "TPixel_1007",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_1107",
           "base": "TPixel_1107",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_1207",
           "base": "TPixel_1207",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_1307",
           "base": "TPixel_1307",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_1407",
           "base": "TPixel_1407",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_1507",
           "base": "TPixel_1507",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0008",
           "base": "TPixel_0008",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0108",
           "base": "TPixel_0108",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0208",
           "base": "TPixel_0208",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0308",
           "base": "TPixel_0308",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0408",
           "base": "TPixel_0408",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0508",
           "base": "TPixel_0508",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0608",
           "base": "TPixel_0608",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0708",
           "base": "TPixel_0708",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0808",
           "base": "TPixel_0808",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0908",
           "base": "TPixel_0908",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_1008",
           "base": "TPixel_1008",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_1108",
           "base": "TPixel_1108",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_1208",
           "base": "TPixel_1208",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_1308",
           "base": "TPixel_1308",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_1408",
           "base": "TPixel_1408",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_1508",
           "base": "TPixel_1508",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0009",
           "base": "TPixel_0009",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0109",
           "base": "TPixel_0109",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0209",
           "base": "TPixel_0209",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0309",
           "base": "TPixel_0309",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0409",
           "base": "TPixel_0409",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0509",
           "base": "TPixel_0509",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0609",
           "base": "TPixel_0609",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0709",
           "base": "TPixel_0709",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0809",
           "base": "TPixel_0809",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0909",
           "base": "TPixel_0909",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_1009",
           "base": "TPixel_1009",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_1109",
           "base": "TPixel_1109",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_1209",
           "base": "TPixel_1209",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_1309",
           "base": "TPixel_1309",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_1409",
           "base": "TPixel_1409",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_1509",
           "base": "TPixel_1509",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0010",
           "base": "TPixel_0010",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0110",
           "base": "TPixel_0110",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0210",
           "base": "TPixel_0210",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0310",
           "base": "TPixel_0310",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0410",
           "base": "TPixel_0410",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0510",
           "base": "TPixel_0510",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0610",
           "base": "TPixel_0610",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0710",
           "base": "TPixel_0710",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0810",
           "base": "TPixel_0810",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0910",
           "base": "TPixel_0910",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_1010",
           "base": "TPixel_1010",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_1110",
           "base": "TPixel_1110",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_1210",
           "base": "TPixel_1210",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_1310",
           "base": "TPixel_1310",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_1410",
           "base": "TPixel_1410",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_1510",
           "base": "TPixel_1510",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0011",
           "base": "TPixel_0011",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0111",
           "base": "TPixel_0111",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0211",
           "base": "TPixel_0211",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0311",
           "base": "TPixel_0311",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0411",
           "base": "TPixel_0411",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0511",
           "base": "TPixel_0511",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0611",
           "base": "TPixel_0611",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0711",
           "base": "TPixel_0711",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0811",
           "base": "TPixel_0811",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0911",
           "base": "TPixel_0911",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_1011",
           "base": "TPixel_1011",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_1111",
           "base": "TPixel_1111",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_1211",
           "base": "TPixel_1211",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_1311",
           "base": "TPixel_1311",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_1411",
           "base": "TPixel_1411",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_1511",
           "base": "TPixel_1511",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0012",
           "base": "TPixel_0012",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0112",
           "base": "TPixel_0112",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0212",
           "base": "TPixel_0212",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0312",
           "base": "TPixel_0312",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0412",
           "base": "TPixel_0412",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0512",
           "base": "TPixel_0512",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0612",
           "base": "TPixel_0612",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0712",
           "base": "TPixel_0712",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0812",
           "base": "TPixel_0812",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0912",
           "base": "TPixel_0912",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_1012",
           "base": "TPixel_1012",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_1112",
           "base": "TPixel_1112",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_1212",
           "base": "TPixel_1212",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_1312",
           "base": "TPixel_1312",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_1412",
           "base": "TPixel_1412",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_1512",
           "base": "TPixel_1512",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0013",
           "base": "TPixel_0013",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0113",
           "base": "TPixel_0113",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0213",
           "base": "TPixel_0213",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0313",
           "base": "TPixel_0313",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0413",
           "base": "TPixel_0413",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0513",
           "base": "TPixel_0513",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0613",
           "base": "TPixel_0613",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0713",
           "base": "TPixel_0713",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0813",
           "base": "TPixel_0813",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0913",
           "base": "TPixel_0913",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_1013",
           "base": "TPixel_1013",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_1113",
           "base": "TPixel_1113",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_1213",
           "base": "TPixel_1213",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_1313",
           "base": "TPixel_1313",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_1413",
           "base": "TPixel_1413",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_1513",
           "base": "TPixel_1513",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0014",
           "base": "TPixel_0014",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0114",
           "base": "TPixel_0114",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0214",
           "base": "TPixel_0214",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0314",
           "base": "TPixel_0314",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0414",
           "base": "TPixel_0414",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0514",
           "base": "TPixel_0514",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0614",
           "base": "TPixel_0614",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0714",
           "base": "TPixel_0714",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0814",
           "base": "TPixel_0814",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0914",
           "base": "TPixel_0914",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_1014",
           "base": "TPixel_1014",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_1114",
           "base": "TPixel_1114",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_1214",
           "base": "TPixel_1214",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_1314",
           "base": "TPixel_1314",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_1414",
           "base": "TPixel_1414",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_1514",
           "base": "TPixel_1514",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0015",
           "base": "TPixel_0015",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0115",
           "base": "TPixel_0115",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0215",
           "base": "TPixel_0215",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0315",
           "base": "TPixel_0315",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0415",
           "base": "TPixel_0415",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0515",
           "base": "TPixel_0515",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0615",
           "base": "TPixel_0615",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0715",
           "base": "TPixel_0715",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0815",
           "base": "TPixel_0815",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_0915",
           "base": "TPixel_0915",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_1015",
           "base": "TPixel_1015",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_1115",
           "base": "TPixel_1115",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_1215",
           "base": "TPixel_1215",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_1315",
           "base": "TPixel_1315",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_1415",
           "base": "TPixel_1415",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         },
         {
           "type":"Instance",
           "name":"Pixel_1515",
           "base": "TPixel_1515",
           "device": "IEC104T",
           "mappings":{ 
             "Logic": { } 
             } 
         }
      ]
}
)JSON";