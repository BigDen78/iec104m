//----------------------------------------------------------------------------------------------//
//                                                                                              //
// 			ПОДКЛЮЧЕНИЕ (если с CubeMX, пропускаем шаги 1 и 2)	                //
//                                                                                              //
// 1) Добавляем исходники из "lwip-x.x.x\src\apps\snmp" в проект:				//
//	                                                                                        //
//	- snmp_core.c                                                                           //
//	- snmp_asn1.c										//
//	- snmp_msg.c										//
//	- snmp_pbuf_stream									//
//	- snmp_raw.c										//
//	- snmp_scalar.c										//
//	- snmp_table.c										//
//	- snmp_traps.c										//
//												//
// 1*) При необходимости использовать MIB2 добавить все исходники с той же папки, содержащие    //
//     "mib2" в названии;									//
//												//
// 2) Включить заголовочники:	                                                                //
//    "...\Middlewares\Third_Party\lwip-x.x.x\src\apps\snmp"					//
//    "...\Middlewares\Third_Party\lwip-x.x.x\src\include\lwip"  				//
//    "...\Middlewares\Third_Party\lwip-x.x.x\src\include\lwip\apps" 				//
//	                                                                                        //
// 3) Конфигурируем константы (описаны ниже)                                                    //
//                                                                                              //
//----------------------------------------------------------------------------------------------//
//----------------------------------------------------------------------------------------------//
//                                                                                              //
// 				 КОНСТАНТЫ (аналогично в CubeMX)	                        //
//                                                                                              //
// 1) "LWIP_SNMP" - НЕОБХОДИМА ДЛЯ РАБОТЫ, включает SNMP в LWIP, задается глобально		//
// 2) "SNMP_TRAP_DESTINATIONS" - "snmp_opts.h", макисмальное количество адресатов для ловушек   //
// 2) При необходимости использовать MIB-2 (RFC 1213) в файле lwipopts.h задать:                //
//                                                                                              //
//  	#define LWIP_STATS	1                                                               //
//      #define SNMP_LWIP_MIB2  1                                                               //
//      #define MIB2_STATS	1 								//
//                                                                                              //
//    Если не нужна, то определить:                                                             //
//                                                                                              //
//	#define LWIP_STATS	0                                                               //                 
//      #define SNMP_LWIP_MIB2  0                                                               //
//	#define MIB2_STATS	0                                                            	//
//----------------------------------------------------------------------------------------------//				
//                                                                                              //
//                          	        ДОПОЛНИТЕЛЬНО                                           //
//                                                                                              //
//  - "NewVisionCore\Examples of root.json" - папка с примерами конфигурации драйвера и тегов 	//
//  - Для удобной работы категорически рекомендую iReasoning MIB Browser (в нем команда WALK)   // 
//    работает, как надо - одним нажатием можно посмотреть все дерево, содержащееся на МК	//
//                                                                                              //
//----------------------------------------------------------------------------------------------//	
//----------------------------------------------------------------------------------------------//

#pragma once

//#define SNMP_TEST           // Если оставить, создатся тестовая MIB. Отрубаем, если не нужен рабочий пример.
#define USE_IN_NVCORE_RUNTIME

#if MIB2_STATS && SNMP_LWIP_MIB2   
#define LAMBDA_MIB2 
#endif

//-------------------------------------------------------------------------------------------------------
#include "ParameterizedObject.hpp"
#include "driver.hpp" 

//#include "main.h"
#include "stdlib.h"  
#include "utils.hpp"
#include "lwip/mem.h"

#include <map>  
#include <sstream>
#include <fstream>   
#include <string>    

// LWIP SNMP -------------------------------------------------------------------------------------------//
#include "snmp.h"
#include "snmp_core.h"
#include "snmp_msg.h" 
#include "snmp_opts.h"
#include "snmp_table.h"     
#include "snmp_scalar.h"
#include "snmp_core_priv.h"  
	  
#ifdef LAMBDA_MIB2 
#include "snmp_mib2.h"  
#endif

using namespace std;

// Указатели на структуры, введенные разработчиками LWIP --------------------------------------------------------
typedef struct snmp_node*        snmp_nodePtr;
typedef struct snmp_scalar_node* snmp_scalarNodePtr;
typedef struct snmp_tree_node*   snmp_treeNodePtr;
typedef struct snmp_table_node*  snmp_tableNodePtr;
typedef struct snmp_mib*         snmp_mibPtr;
typedef struct snmp_varbind*     snmp_varbindPtr; 

// Работа с памятью ---------------------------------------------------------------------------------------------
#define Lambda_Malloc malloc
#define Lambda_Free   free
#define	Lambda_Memcpy MEMCPY 
template <typename _Typename> static _Typename* Lambda_Realloc(_Typename* array, u32_t currentAmount, u32_t newAmount);	

// Типы данных -----------------------------------------------------------------------------------------// 
struct EndTableNodeValueType;  
struct TrapInfo;         
struct SNMP_InitInfoStruct;
  
class EndNodeValueType;
class OIDInfo;  
class SNMPDriverTagContext;
class SNMPsDriver;
      
using OIDString = std::string;                       	 
using TagsPointersList = std::forward_list <SNMPDriverTagContext *>;
    
// Табличный узел - это указатель на саму таблицу, если рассматривать терминологию SNMP. //
// Этот map - список значений, на который показывает этот табличный узел                 //
using TableCellsListType = std::map <u32_t, EndTableNodeValueType>;   

using ScalarDataBaseType = std::map <snmp_nodePtr, EndNodeValueType>;
using TableDataBaseType  = std::map <snmp_nodePtr, TableCellsListType>;

typedef enum 
{
   VALID,
   INVALID
} ActualityType;    

typedef enum 
{          
   TABLE      = SNMP_NODE_TABLE,
   SCALAR     = SNMP_NODE_SCALAR, 
   BASE       = 0xF0,
   AUTO_TYPE  = 0xF1,
   WRONG_TYPE = 0xF2,
} IDtype;
		 
typedef enum
{
   ENABLED,
   DISABLED
} EnabilityType;

// Добавленные типы к ASN.1 ----------------------------------------------------------------------------//
// Действительно, а зачем FLOAT вообще нужен?
// В стандарте НЕТ этого типа среди используемых. Есть некая реализация из 7-и октетов в MIB под названием
// "UCD-SNMP-MIB". Кодирование происходит согласно BER (Basic Encoding Rules из ASN.1)
// На данный момент решил представить FLOAT в виде строки, т.к.
// 1) Этот вариант не вызовет сложностей при описании MIB на втором устройстве;
// 2) В MIB узел может быть описан любым типом, но запрос значения происходит по его OID, и прислать
//    в ответ можно прислать данные другого типа менеджеру   			 
typedef enum
{              
   // Должны быть заведомо больше 0xFF (в SNMP из LWIP тип восьмибитный)
   END_NODE_RESET_VALUE   = 0xFFFF,      // сброс значения узла
   END_NODE_INVALID_TYPE  = 0xF000,
   END_NODE_NO_EXTRA_TYPE = 0xE000,
   
   // Стандартные типы //    
   END_NODE_TYPE_END_OF_CONTENT = SNMP_ASN1_TYPE_END_OF_CONTENT,
   END_NODE_TYPE_NULL 		= SNMP_ASN1_TYPE_NULL,
   END_NODE_TYPE_INTEGER	= SNMP_ASN1_TYPE_INTEGER,
   END_NODE_TYPE_OCTET_STRING   = SNMP_ASN1_TYPE_OCTET_STRING,
   END_NODE_TYPE_OBJECT_ID 	= SNMP_ASN1_TYPE_OBJECT_ID,
   END_NODE_TYPE_SEQUENCE 	= SNMP_ASN1_TYPE_SEQUENCE,
   END_NODE_TYPE_IPADDR 	= SNMP_ASN1_TYPE_IPADDR,
   END_NODE_TYPE_IPADDRESS 	= SNMP_ASN1_TYPE_IPADDRESS,
   END_NODE_TYPE_COUNTER	= SNMP_ASN1_TYPE_COUNTER,
   END_NODE_TYPE_COUNTER32 	= SNMP_ASN1_TYPE_COUNTER32,
   END_NODE_TYPE_GAUGE		= SNMP_ASN1_TYPE_GAUGE,
   END_NODE_TYPE_UNSIGNED32 	= SNMP_ASN1_TYPE_UNSIGNED32,
   END_NODE_TYPE_TIMETICKS	= SNMP_ASN1_TYPE_TIMETICKS,
   END_NODE_TYPE_OPAQUE 	= SNMP_ASN1_TYPE_OPAQUE,
#if LWIP_HAVE_INT64
   END_NODE_TYPE_COUNTER64      = SNMP_ASN1_TYPE_COUNTER64,
#endif
   
   // Свои типы //
   END_NODE_TYPE_FLOAT = 0xFF01
} extraType;

/**
  * Нумерованные соответствия параметров драйвера 
  *
  */
enum snmpParamIDs : uint32_t
{
   communityString_read  = 0x02,
   communityString_write,
   communityString_trap,
   enterpriseOID_param
};

/**
  * Нумерованные соответствия параметров тега 
  *
  */
enum snmpNodeParamIDs : uint32_t
{
   snmpOID,
   snmpDataTypeDetalization,
   snmpTrapOnChange,
   snmpAccessParam
};

//-------------------------------------------------------------------------------------------------------
/**
  * Класс идентификатора узла: содержит строковое и векторное представление идентификатора
  *
  */
class OIDInfo
{   
  public:     
   // Конструктор //
   OIDInfo() = delete;   
   OIDInfo( const OIDString & oid_string, IDtype );
  
   // Поля -----------------------------------------------------------------------------------------------------------//
   ActualityType actuality = VALID; 
   IDtype nodeType = WRONG_TYPE;
   snmp_obj_id objID = {NULL};
   u32_t numOfNodes;
   u32_t nodePosition;    
   
   // Методы ----------------------------------------------------------------------------------------------------------//  
  private:
   ActualityType checkOIDActuality(const OIDString & oid_string, IDtype nodeType); 
   IDtype getOIDType(const snmp_obj_id & oid_string);
   ActualityType checkOIDconformity(const snmp_obj_id & oidVector, IDtype nodeType);
};                   
	   
struct CommunityStruct
{
   char* communityRead;
   char* communityWrite;
   char* communityTrap;
};    
	
struct TrapInfo
{  
  public:   
   TrapInfo(u32_t* id, u32_t len);
   ~TrapInfo();
   
   EnabilityType trapEnability = DISABLED;     // Это поле в принципе можно убрать
   snmp_obj_id_const_ref* objID = NULL;  
};
    
// Сам факт использования в RUNTIME довольно сомнителен, можно хранить указатель на сам тег и из него непосредственно извлекать значение и 
// в него же  записывать новое. Но этот подход делает возможным использование узлов, не регистрируемых через конфигуратор.
class EndNodeValueType
{
  public:
   ~EndNodeValueType()
   { 
      if(trapInfoPtr)
	 delete trapInfoPtr;
   };
  
   VarData value = {.intVal = 0};    	      // <---[оставить это?]  Пока оставим значение (ввиду бОльшего количества, чем в CORE типов данных SNMP
   u16_t length = 0;  			      // Длину мне хранить по-любому надо
   extraType exType = END_NODE_NO_EXTRA_TYPE; // Уточнение по типу - например, Float. Его нет в SNMP, реализовывать его согласно стандартну ASN.1
   					      // мне запрещено, поэтому такие типы я привожу к OCTET_STRING и пуляю на выдачу. 
   					      // Но инфу об этом нужно где-то хранить 
   TrapInfo* trapInfoPtr = NULL;
   void* userDataReference = NULL;
   static void (*writeCallback)(EndNodeValueType* endNodeValuePtr, VarData outerWorldVar);
   
   static void setWriteCompleteCallback(void (*func)(EndNodeValueType* endNodeValuePtr, VarData outerWorldVar));
   static void setRefToUserData(EndNodeValueType* endNodeValuePtr, void* userDataPtr);  
};

struct EndTableNodeValueType
{   
   ~EndTableNodeValueType()
   {
      delete nodeConfigPointer;
   }
   
   snmp_table_col_def* nodeConfigPointer;   //можно обойтись и без этого поля, но придется производить подбор при запросе значения элемента
   EndNodeValueType nodeEndValue;
};
			 
struct DataBaseType
{   
   ScalarDataBaseType ScalarDataBase;
   TableDataBaseType  TableDataBase;
};
      
// Постоянно хранить эти данные именно в контексте - избыточный вариант //       
// Поэтому она создается временно и очищается после инициализации       //
struct SNMP_InitInfoStruct
{
   ~SNMP_InitInfoStruct()
   {                                        
   }
   snmp_access_t access = SNMP_NODE_INSTANCE_READ_WRITE;
   OIDString oidString;
   extraType nodeDataType;    
   Tag* tagPtr = NULL;       // Сейчас используется только для инициализации начальным значением (и то, в случае его наличия)
   EnabilityType trapEnability = DISABLED;  
};

union EconomyStructType
{
  SNMP_InitInfoStruct* tempInitInfoStruct = NULL;
  EndNodeValueType* endNodeValuePtr;
};       

/**
  * Класс SNMP - драйвера
  *
  */
class SNMPsDriver : virtual public Driver
{
  public:      
   SNMPsDriver();
   ~SNMPsDriver();

#ifdef USE_IN_NVCORE_RUNTIME
   // Методы от родительского класса ---------------------------------------------------------------------------//
   virtual void registerTag(const Tag& tag, void* ctx) ; 
   virtual void initialize();     
   virtual void run();
   
   virtual bool tagChanged(const Tag &tag, uint32_t paramId, VarData newValue);
   virtual bool setParameterById(uint32_t id, VarData newValue) override;
   virtual bool setRuntimeParameter(const Tag &tag, uint32_t paramId, VarData paramValue);
   virtual bool setTagParameter(const Tag& tag, uint32_t paramId, VarData newValue);
   virtual bool setTagRuntimeParameter(const Tag& tag, uint32_t paramId, VarData paramValue);
   
   virtual VarData getRuntimeParameter(uint32_t paramId);
   virtual VarData getTagRuntimeParameter(const Tag& tag, uint32_t paramId);
   
   virtual DriverContext *createTagContext() const;  
   
   // Создание узлов согласно считанным с файла тегам //
   void initializeJSONtags();     
#endif // USE_IN_NVCORE_RUNTIME   
   
   // Методы чисто под SNMP -------------------------------------------------------------------------------------//
   void setEnterpriseOID(const OIDString& oidString);        
   void setEnterpriseOID(const u32_t* const id, u32_t length);
  
   // Создание дерева //
   static bool includeMIB(snmp_mibPtr mib);
   static snmp_mibPtr createRootOfPrivateMIB(OIDString oid_string);

   //-------- Добавление узла по одному только OID ----------//
   EndNodeValueType* addNewNode(OIDString oid_string, extraType valueType, VarData startValue);
   static EndNodeValueType* addNewNodeToMIB(snmp_mibPtr mibBaseRoot, OIDString oid_string, extraType valueType, snmp_access_t access);
   static EndNodeValueType* addNewNodeToMIB(snmp_mibPtr mibBaseRoot, OIDString oid_string, extraType valueType, snmp_access_t access, VarData startValue);  // С присвоением начального значения
			    	        
   // Поиск узла среди уже созданных            
   static EndNodeValueType* findEndValuePointer(const OIDString & oidString);
   static EndNodeValueType* findEndValuePointer(struct snmp_node_instance *, u32_t nextFlag);
   static EndNodeValueType* findEndValuePointer(snmp_mibPtr mibBaseRoot, const OIDString & oidString);     
   
   // Изменение значения узла //
   static snmp_err_t updateEndNoteValue(EndNodeValueType* endNodeValue, VarData newValue);
   static snmp_err_t updateEndNoteValue(const OIDString & oidString, VarData newValue);
      
   // Приведение типов //
   static u16_t     associateToASN1type(extraType extraType);
   static extraType associateToLambdaSNMPtype(DataType runTimeType);       

   // Асинхронные уведомления (trap) -------------------------------------------------------------------------------------------
   void enableTraps();
   void disableTraps();
   EnabilityType getTrapsPermissionState();   
   void addTrapSubscriberIP(OIDString subscriberIP); 
			    
   // Включение/ отключение уведомлений // 
   void enableTrap(OIDString oidString);
   void disableTrap(OIDString oidString);
  
   // Отсылка //    
   snmp_err_t sendTrap(EndNodeValueType* nodeEndValue);
   snmp_err_t sendTrap(const OIDString & oidString);
   //---------------------------------------------------------------------------------------------------------------------------

private:         
   static snmp_nodePtr checkSubNodeExistence(snmp_treeNodePtr nodePointer, u32_t checkedOID);
   static snmp_nodePtr findLastIdenticalNode(snmp_mibPtr mibBaseRoot, const OIDInfo & OID, u32_t* startLinkingNumber);

   static snmp_mibPtr createNewMIB(OIDInfo baseOID, const snmp_nodePtr root_node);
   static snmp_treeNodePtr createTreeNode(u32_t oid);
   static snmp_scalarNodePtr createScalarNode(u32_t oid,
                                              u32_t asn1_type,
					      snmp_access_t access);
   static snmp_tableNodePtr createTableNode(u32_t oid);   

   // Работа при запросах ----------------------------------------------------------------------------------------------------//
   static s16_t getValueMethod(struct snmp_node_instance* instance, void* value);
   static s16_t getValueMethod(EndNodeValueType* nodeValue, void* valuePointer);
   static snmp_err_t setValueMethod(struct snmp_node_instance* node_instance, u16_t length, void* value);

   //----- Скаляры -----//
   static snmp_err_t scalarGetInstance(const u32_t *root_oid, u8_t root_oid_len, struct snmp_node_instance *instance);
   static snmp_err_t scalarGetNextInstance(const u32_t *root_oid, u8_t root_oid_len, struct snmp_node_instance *instance);

   //----- Таблицы -----//
   static snmp_err_t getTableInstance(const u32_t *root_oid, u8_t root_oid_len, struct snmp_node_instance *instance);
   static snmp_err_t getTableNextInstance(const u32_t *root_oid, u8_t root_oid_len, struct snmp_node_instance *instance);

   // Создание узлов ---------------------------------------------------------------------------------------------------------//
   static EndNodeValueType* addNewScalarNodeToMIB(snmp_mibPtr mibBaseRoot, const OIDInfo & oidWithoutBase, u8_t asn1_type, snmp_access_t access);
   static EndNodeValueType* addNewTableNodeToMIB(snmp_mibPtr mibBaseRoot, const OIDInfo & oidWithoutBase, u8_t asn1_type, snmp_access_t access);

   // Привязка узлов //      
   template <typename _Typename> static bool linkNewSubnode( snmp_treeNodePtr, _Typename* );
   static bool linkScalarSubnode( snmp_treeNodePtr parentTreeNode, snmp_scalarNodePtr addedScalarNode );
   static bool linkTableSubnode( snmp_treeNodePtr parentTreeNode, snmp_tableNodePtr addedTableNode );
   static bool linkTreeSubnode( snmp_treeNodePtr parentTreeNode, snmp_treeNodePtr addedTreeNode );

   // Поля  -------------------------------------------------------------------------------------//           
   static const ParameterVector parameters;   
   static struct snmp_obj_id* enterpriseID;
   CommunityStruct snmpCommunity = {0, 0, 0}; // Пароли доступа
   static u32_t amountOfRegisteredMIBS;     // Количество зареганных MIB-баз
   static snmp_mibPtr* lambdaSNMPmibs;      // Указатели на зарегистрированные MIB базы
   static DataBaseType nodesDataBase;       // База значений, соответствующих узлам     
   static snmp_mibPtr enterpriseMIBRoot; 
   u32_t amountOfTrapSubscribers = 0;     
   TagsPointersList oidsTemporaryArray;         
   EnabilityType trapsPermiison = DISABLED;  
     
#ifdef LAMBDA_MIB2 
   u16_t sysContantLength;
   u16_t sysNameLength;
   u16_t sysLocationLength;
   
   char* sysName     = NULL;   
   char* sysContact  = NULL;   
   char* sysLocation = NULL;   
   
   void createMIB2(void);
#endif
   
#ifdef SNMP_TEST
   bool createTestMIB(void);
   void snmpDriverExampleLoop();
   static size_t memoryAvaliable;
#endif
};		  

#ifdef USE_IN_NVCORE_RUNTIME
/**
  * Контекст тега
  *
  */
class SNMPDriverTagContext : virtual public DriverContext
{     
  public:
   SNMPDriverTagContext() : ParameterizedObject(snmpParameters) { }
   virtual ~SNMPDriverTagContext() = default;       
   virtual bool setParameterById(uint32_t id, VarData newValue) override;
  
   static const ParameterVector snmpParameters;
  
   EconomyStructType economyStruct = { };   	     
   Tag* tagPtr = NULL;
};
#endif // USE_IN_NVCORE_RUNTIME