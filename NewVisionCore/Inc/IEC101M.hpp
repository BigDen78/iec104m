#pragma once
#include "hal_serial.h"
#include "usart.h"
#include "IECSlaveBase.hpp"
#include "cs101_master.h"
#include "driver.hpp"
#include <deque>

#define CMD_IDLE 1u
#define CMD_TX_COMPLETE 2u

class IEC101GenericDevice : virtual public Device
{
public:
  using WriteQueue = std::deque<InformationObject>;
  typedef bool(IEC101GenericDevice::*IOHandlerFn)(Tag &tag, InformationObject io, CS101_CauseOfTransmission cot);
  enum RunState
  {
    rsInitial,
    rsUnselected,
    rsSelected,
    rsGI,
    rsReadyToRelease,
    rsOffline
  };

  IEC101GenericDevice()
    : ParameterizedObject(parameters)
    , linkAddress(1)
    , asduAddress(1)
    , giInterval(60000)
    , tsInterval(600000)
    , nextGITime(0)
    , nextTSTime(0)
    , commWindow(100)
    , runState(rsInitial)
    , master(nullptr)
    , writeQueue()
    , linkState(LL_STATE_IDLE)
    , errorCount(0)
    , offline(false)
    , offlineCheckTS(0) {}
  virtual bool registerTag(const Tag &tag, void *ctx) override;
  virtual void initialize(void *pvParameter) override;
  virtual void select(void *pvParameter) override;
  virtual bool run() override;
  virtual bool tagChanged(const Tag &tag, uint32_t paramId, VarData newValue) override;
  virtual bool canRelease() override;
  virtual bool release() override;
  virtual bool setParameterById(uint32_t id, VarData newValue) override;
  virtual bool getParameterById(uint32_t id, VarData &value) const override;
  virtual bool handleIncomingData(void *pData) override;
  virtual void handleLinkLayerMessage(void *pData) override;
  virtual void *getUnderlyingObject() override { return this; }
  uint32_t getWaitingCommandsCount() { return writeQueue.size(); }
  bool sendQueuedCommand();
  bool giCondition() { return ((LL_STATE_AVAILABLE == linkState) || (LL_STATE_IDLE == linkState)) && (clock() >= nextGITime); }
  bool tsCondition() { return ((LL_STATE_AVAILABLE == linkState) || (LL_STATE_IDLE == linkState)) && (clock() >= nextTSTime); }
  bool beginGI();
  bool issueTimeSync();
  uint16_t getLinkAddress() { return linkAddress; }

protected:
  void scheduleNextGI() { nextGITime = clock() + giInterval; }
  void scheduleNextTS() { nextTSTime = clock() + tsInterval; }
  bool handleMSPNA1(Tag &tag, InformationObject io, CS101_CauseOfTransmission cot);
  bool handleMDPNA1(Tag &tag, InformationObject io, CS101_CauseOfTransmission cot);
  bool handleMMENB1(Tag &tag, InformationObject io, CS101_CauseOfTransmission cot);
  bool handleMMENC1(Tag &tag, InformationObject io, CS101_CauseOfTransmission cot);
  bool handleMSPTB1(Tag &tag, InformationObject io, CS101_CauseOfTransmission cot);
  bool handleMDPTB1(Tag &tag, InformationObject io, CS101_CauseOfTransmission cot);
  bool handleMMETE1(Tag &tag, InformationObject io, CS101_CauseOfTransmission cot);  //
  bool handleMMETF1(Tag &tag, InformationObject io, CS101_CauseOfTransmission cot);
  bool handleCICNA1(Tag &tag, InformationObject io, CS101_CauseOfTransmission cot);

  InformationObject createIOFromTagUpdate(const Tag &tag, VarData newValue);

  uint16_t linkAddress;
  uint16_t asduAddress;
  uint32_t giInterval;
  uint32_t tsInterval;
  uint32_t nextGITime;
  uint32_t nextTSTime;
  uint16_t commWindow;
  IOAMap ioaMap;
  RunState runState;
  CS101_Master master;
  WriteQueue writeQueue;
  LinkLayerState linkState;
  uint32_t errorCount;
  bool offline;
  uint32_t offlineCheckTS;
  static const ParameterVector parameters;
  static const IOHandlerFn ioHandlers[128];
};

/*
*/
class IEC101MDriver : virtual public Driver
{
public:
  using DeviceMap = std::map<int, IEC101GenericDevice *>;
  using DeviceVector = std::vector<IEC101GenericDevice *>;
  //using DeviceHeap = std::priority_queue<IEC101GenericDevice *, std::vector<IEC101GenericDevice *>, CompareDeviceQueues>;
  enum RunState
  {
    rsInitial,
    rsRunning,
    rsRunningSingleSlave,
    rsInitializationErr
  }
  ;
  IEC101MDriver()
    : Driver()
    , ParameterizedObject(parameters)
    , dcmdSerialIdle(this, CMD_IDLE, { 0 })
    , dcmdSerialTxComplete(this, CMD_TX_COMPLETE, { 0 })
    , runState(rsInitial)
    , commTimeout(50)
    , comIdle(true)
    , comSkipCount(0)
    , waitingCommandsCnt(0)
    , nextTimeout(0)
    , useSingleCharAck(true)

  {
    this->devicesSupported = true;
  }
  virtual void registerTag(const Tag &tag, void *ctx) override { return; }
  virtual bool tagChanged(const Tag &tag, uint32_t paramId, VarData newValue) override;
  virtual bool setParameterById(uint32_t id, VarData newValue) override;
  virtual bool setRuntimeParameter(const Tag &tag, uint32_t paramId, VarData paramValue) override;
  virtual bool setTagRuntimeParameter(const Tag &tag, uint32_t paramId, VarData paramValue) override { return false; }
  virtual VarData getRuntimeParameter(uint32_t paramId) override { return NO_DATA; }
  virtual VarData getTagRuntimeParameter(const Tag &tag, uint32_t paramId) override { return NO_DATA; }
  virtual void initialize() override;
  virtual void run() override;
  virtual DriverContext *createTagContext() const override { return new IECSContext; }
  virtual bool registerDevice(Device *device, void *ctx) override;
  static const ParameterVector parameters;

protected:
  static bool asduReceivedHandlerStatic(void *parameter, int address, CS101_ASDU asdu)
  {
    DeviceMap &map = reinterpret_cast<IEC101MDriver *>(parameter)->deviceMap;
    auto itDevice = map.find(address);
    if (map.end() != itDevice)
      return itDevice->second->handleIncomingData(asdu);
    return false;
  }
  static void linkLayerStateChangedHandlerStatic(void *parameter, int address, LinkLayerState newState)
  {
    DeviceMap &map = reinterpret_cast<IEC101MDriver *>(parameter)->deviceMap;
    auto itDevice = map.find(address);
    if (map.end() != itDevice)
      itDevice->second->handleLinkLayerMessage((void *)newState);
  }
  UART_HandleTypeDef *huart;
  uint32_t baudRate;
  uint8_t stopBits;
  char parity;
  uint16_t commTimeout;
  bool useSingleCharAck;
  SerialPort port;
  CS101_Master master = 0;
  DriverCommand dcmdSerialIdle;
  DriverCommand dcmdSerialTxComplete;
  RunState runState;
  bool comIdle;
  uint32_t comSkipCount;
  DeviceVector::iterator currentDevice;
  DeviceMap deviceMap;
  uint32_t waitingCommandsCnt;
  uint32_t nextTimeout;
  //DeviceHeap devicesWithCommands;
  DeviceVector dv;
};