#pragma once
#include <type_traits>
#include <driver.hpp>
//#include "Allocator.h"

using Array = Json::array_t;

typedef Driver *(*DriverCreatorFn)();
typedef Device *(*DeviceCreatorFn)();
typedef TagTemplate *(*TemplateCreatorFn)();
typedef Tag *(*TagCreatorFn)(const TagTemplate *);

using DriverFactoryTable = std::map<String, DriverCreatorFn>;
using DeviceFactoryTable = std::map<String, DeviceCreatorFn>;
using TagFactoryTable = std::map<String, TagCreatorFn>;
using TemplateFactoryTable = std::map<String, TemplateCreatorFn>;

//using BuilderFactoryTable = std::map<std::string, DriverCreatorFn, std::less<std::string>, CcramAllocator<std::map<std::string, DriverCreatorFn>::value_type>>;
//using DriverFactoryTable = std::map<std::string, DriverCreatorFn, std::less<std::string>, CcramAllocator<std::pair<std::string const, DriverCreatorFn>,32>>;

//#define __CONCAT_IMPL(A, B) A##B
//#define CONCAT(A, B) __CONCAT_IMPL(A, B)
#define REGISTER_FACTORY_METHOD(TABLE_NAME, CLASS_TYPE, JSON_CLASS, METHOD_BODY) \
	CLASS_TYPE *Create##JSON_CLASS()                                               \
	{                                                                              \
		return METHOD_BODY;                                                          \
	}                                                                              \
	static struct JSON_CLASS##FactoryMethodRegistrator                             \
	{                                                                              \
		JSON_CLASS##FactoryMethodRegistrator()                                       \
		{                                                                            \
			TABLE_NAME[#JSON_CLASS] = Create##JSON_CLASS;                              \
		}                                                                            \
	} factoryMethodRegistrator##JSON_CLASS

class DriverBuilder
{
public:
	virtual void reset();
	virtual bool initFromJson(const Json &json);
	virtual Driver *getResult() { return driver; }
	static DriverFactoryTable driverFactoryTable;

protected:
	Driver *driver;
};

class DeviceBuilder
{
public:
	DeviceBuilder() = default;
	virtual void reset();
	virtual bool initFromJson(const Json &json);
	virtual Device *getResult() { return device; }
	static DeviceFactoryTable deviceFactoryTable;

protected:
	static bool setDeviceParameter(Device *device, const String &name, const Json &json);
	DeviceTemplate *findOrLoadTemplate(const String &templateName);
	Device *device;
	NamesOfDevTemplates devTemplatesMap;
};

class TemplateBuilder
{
public:
	TemplateBuilder() = default;
	virtual void reset(Device *device)
	{
		tpl = nullptr;
		this->device = device;
	}
	virtual bool initFromJson(const Json &json);
	virtual TagTemplate *getResult() { return tpl; }
	static TemplateFactoryTable TemplateFactoryTable;
	template <typename T>
	static TagTemplate *createTemplate()
	{
		TagTemplate *result = new TagTemplate;
		if constexpr (std::is_same<T, bool>::value)
			result->staticInfo.type = dtBool;
		else if constexpr (std::is_same<T, uint8_t>::value)
			result->staticInfo.type = dtByte;
		else if constexpr (std::is_same<T, uint16_t>::value)
			result->staticInfo.type = dtWord;
		else if constexpr (std::is_same<T, uint32_t>::value)
			result->staticInfo.type = dtDWord;
		else if constexpr (std::is_same<T, int16_t>::value)
			result->staticInfo.type = dtShortInt;
		else if constexpr (std::is_same<T, int32_t>::value)
			result->staticInfo.type = dtInt;
		else if constexpr (std::is_same<T, float>::value)
			result->staticInfo.type = dtFloat;
		else if constexpr (std::is_same<T, char *>::value)
			result->staticInfo.type = dtString;
		else if constexpr (std::is_same<T, wchar_t *>::value)
			result->staticInfo.type = dtWString;
		else
			result->staticInfo.type = dtVoid;
		return result;
	}

protected:
	static bool buildContextFromJson(Driver *driver, DriverContext* ctx, const JsonObject &json);
	static bool setTagParameter(Driver *driver, Tag *tag, const String &name, const Json &json);
	TagTemplate *tpl;
	Device *device;
};

class TagBuilder
{
public:
	TagBuilder() = default;
	virtual void reset();
	virtual bool initFromJson(const Json &json);
	virtual Tag *getResult() { return tag; }

protected:
	static bool setTagParameter(Driver *driver, Tag *tag, const String &name, const Json &json);
	static TagFactoryTable tagFactoryTable;
	template <typename T>
	static Tag *createTag(const TagTemplate *tpl)
	{
		Tag *tag = new Tag;
		if constexpr (std::is_same<T, bool>::value)
			tag->staticInfo.type = dtBool;
		else if constexpr (std::is_same<T, uint8_t>::value)
			tag->staticInfo.type = dtByte;
		else if constexpr (std::is_same<T, uint16_t>::value)
			tag->staticInfo.type = dtWord;
		else if constexpr (std::is_same<T, uint32_t>::value)
			tag->staticInfo.type = dtDWord;
		else if constexpr (std::is_same<T, int16_t>::value)
			tag->staticInfo.type = dtShortInt;
		else if constexpr (std::is_same<T, int32_t>::value)
			tag->staticInfo.type = dtInt;
		else if constexpr (std::is_same<T, float>::value)
			tag->staticInfo.type = dtFloat;
		else if constexpr (std::is_same<T, char *>::value)
			tag->staticInfo.type = dtString;
		else if constexpr (std::is_same<T, wchar_t *>::value)
			tag->staticInfo.type = dtWString;
		else
			tag->staticInfo.type = dtVoid;
		return tag;
	}
	static Tag *createTagFromTemplate(const TagTemplate *tpl);
	Tag *tag;
};
