#pragma once
#include "driver.hpp"

extern "C"
{
  #include "main.h"
  #include "ADCDriver.h"
  #include "kalman.h"
}

class AIDriverTagContext;
class AIDriver;

/**
  * @brief  Class AdcConverter uses for
  *         conversion secondary to primary physical units
  */
class AdcConverter
{
public:
  virtual float PrimaryToSecond(float primary) = 0;   ///< Conversation from phy units to signal units
  virtual float SecondToPrimary(float secondary) = 0; ///< Conversation from signal units to phy units
  AdcConverter(){ }
  virtual ~AdcConverter() = default; //to supress Pa096 warning
};

/// Points can be set in primary - physical or secondary - signal units
typedef enum
{
  P_SECONDARY,  ///< secondary - signal units [ R, mA, V ]
  P_PRIMARY     ///< primary - physical units [ A, V, W ]
}SetPoint;

using AIChannelVector     = std::vector<ADC_Channel*>;
using TagCtxLogEnableList = std::forward_list<AIDriverTagContext*>;;

class AIDriver : virtual public Driver
{
public:
	virtual void registerTag(const Tag& tag, void* ctx);
  virtual bool tagChanged(const Tag &tag, uint32_t paramId, VarData newValue);
  virtual bool setParameter(uint32_t paramId, VarData newValue);
  virtual bool setRuntimeParameter(const Tag &tag, uint32_t paramId, VarData paramValue);
  virtual bool setTagParameter(const Tag& tag, uint32_t paramId, VarData newValue);
	virtual bool setTagRuntimeParameter(const Tag& tag, uint32_t paramId, VarData paramValue);
	virtual VarData getRuntimeParameter(uint32_t paramId);
	virtual VarData getTagRuntimeParameter(const Tag& tag, uint32_t paramId);
	virtual DriverContext* createTagContext() const override;
	virtual void initialize();
	virtual void run();
  AIDriver();
  AIChannelVector  channelVector;
  TagCtxLogEnableList tagLogEnableList; 
    
  virtual bool setParameterById(uint32_t id, VarData newValue) override;
  static const ParameterVector myParameters;
  virtual bool supportsHistory() override;
  virtual bool getHistoricalData(uint64_t ts, HistoricalData *pData) override;
  
protected:
  void copyData(HistoricalData *pData, ADC_LogCntr *pADCLogCtr, uint32_t P, uint32_t posTrg, bool ringHopping);
};

class AIDriverTagContext : virtual public DriverContext
{  
public:
  AIDriverTagContext() : ParameterizedObject(myParameters), kRatio(1), calibK(0), primaryPoint(P_SECONDARY), pAdcConverter(nullptr){ }
  ADC_Channel  *adcChannel;     ///< Channel parametrs
  float         kRatio;         ///< Scaling factor (SECONDARY = PRIMARY / kRatio)
  float         calibK;         ///< Additive factor to measured value (SECONDARY + calibK)
  SetPoint      primaryPoint;   ///< P_SECONDARY or P_PRIMARY if convert type not set only P_SECONDARY are available
  AdcConverter  *pAdcConverter; ///< Ptr to convertion object
  AIDriver      *aiDriver;      ///< We need this in order to make destructor
  Tag* pTag;
  
  const float invalid = -99999.0;
  
  String sSensOK = nullptr;			//������ ��������
  String sValidOK = nullptr;			//�������� ����������
  String sWarnHi = nullptr;			//����� �� ������� ������������� ������� (����)
  String sWarnLo = nullptr;			//����� �� ������� ������������� ������� (����)
  String sAlarmHi = nullptr;			//����� �� ������� ��������� ������� (����)
  String sAlarmLo = nullptr;			//����� �� ������� ��������� ������� (����)
  String sSigNorm = nullptr;			//���������� ��������				!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  
  Tag* sensOK = nullptr;                        //������ ��������
  Tag* validOK = nullptr;                       //�������� ����������
  
  Tag* alarmLo = nullptr;                       //����� �� ������� ��������� ������� (����)
  Tag* warnLo = nullptr;                        //����� �� ������� ������������� ������� (����)
  Tag* sigNorm = nullptr;                       //���������� ��������				!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  Tag* warnHi = nullptr;                        //����� �� ������� ������������� ������� (����)
  Tag* alarmHi = nullptr;                       //����� �� ������� ��������� ������� (����)
 
  bool isValidPresent   = false;
  bool isValLimitPresent = false;
  
  
  float minOK = invalid;                            //������� ��� ����������� �������
  float maxOK = invalid;                           //������� ���� ����������� �������
  float minValid = invalid;                        //������� ��� ������������� ��������
  float maxValid = invalid;                         //������� ���� ������������� ��������
  
  float LoLo = invalid;                             //������� ��� ��������� �������
  float Lo = invalid;                               //������� ��� ������������� �������
  float Hi = invalid;                               //������� ���� ������������� �������
  float HiHi = invalid;                             //������� ���� ��������� �������
  
  ~AIDriverTagContext();
   virtual bool setParameterById(uint32_t id, VarData newValue) override;
   static const ParameterVector myParameters; 
};
