#pragma once
#include <map>
#include "driver.hpp"

enum SettingsMappingClass
{
	smcNoClass,
	smcRuntimeProperty,
	smcDriverProperty,
	smcMappingProperty,
	smcGlobalVariable,
	smcSystemVariable
};

enum SettingsTagParamIDs : uint32_t
{
	stpidClassId,
	stpidDriver,
	stpidTag,
	stpidProperty
};

class SettingsDriverContext : virtual public DriverContext
{
public:
	SettingsDriverContext() : driver(nullptr), refTag(nullptr), myClass(smcNoClass),
		paramId(0), paramIndex(-1) , ParameterizedObject(parameters)
{};
	virtual ~SettingsDriverContext() = default;
  virtual bool setParameterById(uint32_t id, VarData newValue) override;

	SettingsMappingClass myClass;
	Driver* driver;
	Tag* refTag;
	uint32_t paramId;
	uint32_t paramIndex;
	static const ParameterVector parameters;
};

class SettingsDriver : virtual public Driver
{
public:
	enum RunningState : uint32_t
	{
		rsInitializing,
		rsRunning,
		rsPendingWrite,
		rsPendingReset
	};
	SettingsDriver();
	virtual void registerTag(const Tag& tag, void* ctx) override;
	virtual bool tagChanged(const Tag &tag, uint32_t paramId, VarData newValue) override;
	virtual bool setParameterById(uint32_t id, VarData newValue) override;
	virtual bool setRuntimeParameter(const Tag &tag, uint32_t paramId, VarData paramValue) override;
	virtual bool setTagRuntimeParameter(const Tag& tag, uint32_t paramId, VarData paramValue) override;
	virtual VarData getRuntimeParameter(uint32_t paramId) override;
	virtual VarData getTagRuntimeParameter(const Tag& tag, uint32_t paramId) override;
	virtual DriverContext* createTagContext() const override;
	virtual void initialize() override;
	virtual void run() override;
protected:
	CString configPath;
	bool pendingReset;
	bool pendingWrite;
	RunningState runState;
	clock_t writeTimeout;
	clock_t resetTimeout;
	TagList sysTags;
	static const ParameterVector parameters;
};