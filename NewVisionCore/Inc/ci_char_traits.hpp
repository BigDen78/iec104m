#include <string>
struct ci_char_traits : std::char_traits<char>
{

	static bool eq(char c1, char c2)
	{
		return std::tolower(c1) == std::tolower(c2);
	}
	static bool lt(char c1, char c2)
	{
		return std::tolower(c1) < std::tolower(c2);
	}
	static int compare(const char *s1, const char *s2, size_t n)
	{
		return strncasecmp(s1, s2, n);
	}
	static const char *find(const char *s, int n, char a)
	{
		while (n-- > 0 && toupper(*s) != toupper(a))
		{
			++s;
		}
		return s;
	}
};
