#pragma once
#include "ParameterizedObject.hpp"
#include "driver.hpp"

extern "C"
{
#include "main.h"
#include "timed.h"
}

class OSCTagDriverContext  : virtual public DriverContext
{
public:
  OSCTagDriverContext() : ParameterizedObject(myParameters), condition(true) {  }
  virtual bool setParameterById(uint32_t id, VarData newValue) override;

  Tag *pTag;
	static const ParameterVector myParameters;
  bool trig;              ///< Trigger or not, if not it will be only added in oscillogram 
  bool condition;         ///< Trigger condition -> true (1) - work on risen edge, false (0) on falling edge
  Driver *historyProvider;
};

using ProviderMap = std::map<uint32_t, TagList>;

class OSCDriver : virtual public Driver
{
public: 
  enum RunningState : uint32_t
	{
		rsInitializing,
		rsRunning
	};
  
  virtual void registerTag(const Tag &tag, void *ctx);
  virtual bool tagChanged(const Tag &tag, uint32_t paramId, VarData newValue);
  virtual bool setParameterById(uint32_t id, VarData newValue) override;
  virtual bool setRuntimeParameter(const Tag &tag, uint32_t paramId, VarData paramValue) {  return false; }
  virtual bool setTagRuntimeParameter(const Tag &tag, uint32_t paramId, VarData paramValue) {  return false; }
  virtual VarData getRuntimeParameter(uint32_t paramId){ return NO_DATA; }
  virtual VarData getTagRuntimeParameter(const Tag &tag, uint32_t paramId){ return NO_DATA; }
  virtual DriverContext *createTagContext() const;
  virtual void initialize();
  virtual void run();
  OSCDriver();
  static const ParameterVector myParameters;
  
protected:
  void startOSC();
  ProviderMap     providerMap;
  RunningState    runState;

  uint8_t offset;
  uint8_t filesAmount;
};