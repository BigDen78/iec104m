#pragma once

const char* jsonStr = R"JSON(
{
  "core": {
    "queueDepth": 100
  },
  "drivers": [
  {
    "name": "Settings",
    "class": "SettingsDriver",
    "scanRate": -1,
    "queueDepth": 10,
    "stackDepth": 512,
    "parameters": {
      "file": "0:\\settings.json",
      "writeTimeout": 2000,
      "resetTimeout": 2000
    }
  },
  {
    "name": "IEC104S",
    "class": "IEC104Slave",
    "parameters": {
      "port": 2404,
      "redGroupConfig": "0:\\ipconfig.ini",
      "spontDelay": 50,
      "linkAddress": 3,
      "ASDUAddress": 3
    }
  },
  {
    "name":"Logic",
    "class":"LogicDriver",
    "parameters":{
      "spontaneousTime":1000
    }
  }
    ],
"tags": [
   {
     "type":"Word",
     "name":"Pixel_0000",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7000}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0100",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7001}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0200",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7002}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0300",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7003}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0400",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7004}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0500",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7005}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0600",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7006}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0700",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7007}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0800",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7008}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0900",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7009}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_1000",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7010}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_1100",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7011}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_1200",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7012}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_1300",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7013}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_1400",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7014}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_1500",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7015}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0001",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7016}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0101",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7017}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0201",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7018}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0301",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7019}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0401",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7020}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0501",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7021}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0601",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7022}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0701",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7023}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0801",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7024}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0901",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7025}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_1001",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7026}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_1101",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7027}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_1201",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7028}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_1301",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7029}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_1401",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7030}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_1501",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7031}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0002",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7032}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0102",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7033}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0202",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7034}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0302",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7035}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0402",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7036}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0502",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7037}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0602",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7038}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0702",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7039}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0802",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7040}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0902",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7041}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_1002",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7042}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_1102",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7043}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_1202",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7044}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_1302",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7045}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_1402",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7046}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_1502",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7047}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0003",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7048}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0103",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7049}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0203",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7050}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0303",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7051}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0403",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7052}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0503",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7053}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0603",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7054}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0703",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7055}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0803",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7056}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0903",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7057}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_1003",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7058}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_1103",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7059}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_1203",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7060}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_1303",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7061}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_1403",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7062}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_1503",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7063}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0004",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7064}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0104",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7065}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0204",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7066}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0304",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7067}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0404",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7068}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0504",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7069}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0604",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7070}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0704",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7071}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0804",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7072}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0904",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7073}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_1004",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7074}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_1104",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7075}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_1204",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7076}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_1304",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7077}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_1404",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7078}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_1504",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7079}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0005",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7080}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0105",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7081}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0205",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7082}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0305",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7083}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0405",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7084}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0505",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7085}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0605",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7086}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0705",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7087}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0805",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7088}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0905",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7089}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_1005",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7090}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_1105",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7091}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_1205",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7092}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_1305",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7093}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_1405",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7094}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_1505",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7095}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0006",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7096}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0106",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7097}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0206",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7098}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0306",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7099}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0406",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7100}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0506",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7101}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0606",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7102}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0706",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7103}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0806",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7104}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0906",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7105}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_1006",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7106}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_1106",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7107}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_1206",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7108}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_1306",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7109}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_1406",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7110}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_1506",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7111}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0007",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7112}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0107",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7113}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0207",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7114}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0307",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7115}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0407",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7116}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0507",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7117}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0607",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7118}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0707",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7119}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0807",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7120}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0907",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7121}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_1007",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7122}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_1107",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7123}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_1207",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7124}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_1307",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7125}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_1407",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7126}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_1507",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7127}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0008",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7128}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0108",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7129}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0208",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7130}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0308",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7131}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0408",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7132}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0508",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7133}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0608",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7134}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0708",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7135}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0808",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7136}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0908",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7137}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_1008",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7138}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_1108",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7139}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_1208",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7140}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_1308",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7141}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_1408",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7142}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_1508",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7143}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0009",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7144}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0109",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7145}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0209",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7146}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0309",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7147}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0409",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7148}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0509",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7149}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0609",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7150}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0709",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7151}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0809",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7152}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0909",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7153}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_1009",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7154}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_1109",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7155}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_1209",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7156}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_1309",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7157}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_1409",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7158}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_1509",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7159}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0010",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7160}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0110",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7161}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0210",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7162}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0310",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7163}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0410",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7164}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0510",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7165}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0610",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7166}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0710",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7167}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0810",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7168}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0910",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7169}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_1010",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7170}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_1110",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7171}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_1210",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7172}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_1310",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7173}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_1410",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7174}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_1510",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7175}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0011",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7176}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0111",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7177}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0211",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7178}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0311",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7179}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0411",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7180}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0511",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7181}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0611",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7182}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0711",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7183}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0811",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7184}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0911",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7185}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_1011",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7186}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_1111",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7187}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_1211",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7188}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_1311",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7189}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_1411",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7190}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_1511",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7191}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0012",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7192}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0112",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7193}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0212",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7194}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0312",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7195}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0412",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7196}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0512",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7197}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0612",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7198}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0712",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7199}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0812",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7200}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0912",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7201}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_1012",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7202}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_1112",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7203}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_1212",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7204}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_1312",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7205}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_1412",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7206}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_1512",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7207}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0013",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7208}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0113",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7209}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0213",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7210}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0313",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7211}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0413",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7212}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0513",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7213}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0613",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7214}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0713",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7215}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0813",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7216}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0913",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7217}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_1013",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7218}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_1113",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7219}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_1213",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7220}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_1313",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7221}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_1413",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7222}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_1513",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7223}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0014",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7224}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0114",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7225}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0214",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7226}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0314",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7227}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0414",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7228}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0514",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7229}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0614",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7230}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0714",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7231}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0814",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7232}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0914",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7233}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_1014",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7234}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_1114",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7235}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_1214",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7236}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_1314",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7237}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_1414",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7238}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_1514",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7239}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0015",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7240}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0115",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7241}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0215",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7242}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0315",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7243}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0415",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7244}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0515",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7245}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0615",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7246}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0715",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7247}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0815",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7248}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_0915",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7249}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_1015",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7250}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_1115",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7251}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_1215",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7252}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_1315",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7253}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_1415",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7254}
                }
   },
   {
     "type":"Word",
     "name":"Pixel_1515",
     "interType":"1",
     "parent":"Logic",
     "mappings":{
        "IEC104S" :{"spontType":35, "interType":11, "cmdType":49, "IOA":7255}
                }
   }
]
}
)JSON";