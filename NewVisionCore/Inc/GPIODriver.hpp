#pragma once
#include "ParameterizedObject.hpp"
#include "driver.hpp"

extern "C"
{
#include "main.h"
}

enum GPIOTagParamIDs : uint32_t
{
  gtpidType,
  gtpidPort,
  gtpidPin,
  gtpidPull,
  gtpidDebounce,
  gtpidImpulse,
  gtpidInvert
};

class GPIOTagDriverContext : virtual public DriverContext
{
public:
  GPIOTagDriverContext() : initInfo({0}), debounce(20), invert(false),
                           impulse(0), timer(0), timerOn(false),
                           ParameterizedObject(parameters)
  {
  }
  virtual bool setParameterById(uint32_t id, VarData newValue) override;

  GPIO_TypeDef *port;
  bool invert;            /// Inversion bitween physical signal and it's logic value
  uint16_t impulse;       /// Timeout of impulse
  uint16_t debounce;      /// Timeout of debounce
  clock_t timer;          /// Stop timer value
  bool timerOn;           /// Timer is ON?
  GPIO_PinState pinState; /// Last state of pin
  GPIO_InitTypeDef initInfo;
  Tag *pTag;
  static const ParameterVector parameters;
};

using InputsList = std::forward_list<GPIOTagDriverContext *>;
using OutputList = std::list<GPIOTagDriverContext *>;

class GPIODriver : virtual public Driver
{
public:
  virtual void registerTag(const Tag &tag, void *ctx) override;
  virtual bool tagChanged(const Tag &tag, uint32_t paramId, VarData newValue) override;
  virtual bool setParameterById(uint32_t id, VarData newValue) override { return false; }
  virtual bool setRuntimeParameter(const Tag &tag, uint32_t paramId, VarData paramValue) override { return false; };
  virtual bool setTagRuntimeParameter(const Tag &tag, uint32_t paramId, VarData paramValue) override;
  virtual VarData getRuntimeParameter(uint32_t paramId) override { return NO_DATA; }
  virtual VarData getTagRuntimeParameter(const Tag &tag, uint32_t paramId) override;
  virtual DriverContext *createTagContext() const override;
  virtual void initialize() override;
  virtual void run() override;
  GPIODriver();

protected:
  InputsList gpioInputs;
  OutputList gpioOutputs;
};