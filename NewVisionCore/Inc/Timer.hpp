#pragma once
#include <stdint.h>
#include <list>
#include <map>
#include <utility>
#include "hw_timers.h"  

class Timer
{
protected:
  bool      enabled;
  uint32_t  interval;
public:
  
  virtual void stop()  = 0;
  virtual void start() = 0;
  
  virtual void restart() 
  {
    stop();
    start();
  }
  
  virtual ~Timer() = 0;
  
  bool     isEnabled()  { return enabled;  }
  uint32_t getInterval(){ return interval; }

  virtual void setInterval(uint32_t interval) = 0;
  
  Timer() : enabled(false), interval(1) { }
};

enum IntervalType
{
  millisecond = 1000, ///< one tick per ms, but interval will be in millis 
  tenMillisec = 100,  ///< one tick per 10 ms, but interval will be in millis 
  second      = 10    ///< ten tick per sec, but interval will be in seconds 
};

class SoftTimer : public Timer
{
  uint32_t     ticks;
  IntervalType intervalType;
public:
  SoftTimer() : ticks(0), intervalType(millisecond) { }
  
  uint32_t getTicks() { return ticks; }
  void     incTicks() { ticks++; } 
  void     reset()    { ticks = 0; }
  
  virtual  void tickGetReady() = 0;
  
  void setIntervalType(IntervalType intervalType)
  {
    this->intervalType = intervalType;
  }
  
  void setInterval(uint32_t interval) override
  {
    switch(intervalType)
    {
    case second: // ten tick per sec, but interval will be in seconds 
      this->interval = interval * 10;
      break;
    case tenMillisec: // round to decade, interval set in millis
      this->interval = interval / 10;  
      break;
    default:
      this->interval = interval;
      break;
    }
  }
  
  ~SoftTimer(){}
};

class HWTimer : public Timer
{
  TimerCntr timCntr = {0};
public:  
  void stop() override
  {
    enabled = false;
    timerStop(&timCntr);
  }
  
  void start() override
  {
    enabled = true;
    timerStart(&timCntr);
  }
  
  void setInterval(uint32_t interval) override
  {
    timCntr.period = interval; 
    this->interval = interval;
    timerInit(&timCntr);
  }
  
  void setCallback(void(*f)(void*), void *params)
  {
    timCntr.func = f;
    timCntr.callbackParams = params;
    timCntr.xTask = nullptr;
  }
  
  void setTaskHandle(TaskHandle_t xTask)
  { 
    timCntr.xTask = xTask; 
    timCntr.func  = nullptr;
  }
  
  bool checkTimer()
  {
      return timCntr.tim ? true : false;
  }
  
  HWTimer(TIM_TypeDef *tim);
  
  ~HWTimer(){}
};

using SoftTimersList = std::list<SoftTimer*>;

class SoftTimersCntr 
{ 
  HWTimer        *hwTimer;
  SoftTimersList timerList;
  IntervalType   intervalType;
public:  
  
  static void timersHandler(void *x)
  {
    SoftTimersCntr *params = (SoftTimersCntr*)x;
    for(auto &it : params->timerList)
    {
      if(it->isEnabled() == true)
      {
        it->incTicks();
        if(it->getTicks() == it->getInterval())
        {
          it->tickGetReady();
          it->stop();
        }
      }
    }
  }
  
  void attachTimer(SoftTimer* tim)   
  { 
    timerList.push_back(tim); 
    
    tim->setIntervalType(intervalType);
    tim->setInterval(tim->getInterval());
  }
  
  void detachTimer(SoftTimer* tim)   { timerList.remove(tim); }
  
  bool initSoftTimers(TIM_TypeDef *tim, IntervalType interval)
  {
    hwTimer = new HWTimer(tim);
    if(hwTimer->checkTimer())
    {
      hwTimer->setCallback(&SoftTimersCntr::timersHandler, this); 
      hwTimer->setInterval(interval); 
      this->intervalType = interval;
      
      hwTimer->start();
      return true;
    }
    return false;
  }
  
  SoftTimersCntr() { }
  
  ~SoftTimersCntr() 
  { 
    hwTimer->stop();
    delete hwTimer;
  }
};

class FlagTimer : public SoftTimer
{
  bool *flag;
public:
  FlagTimer(uint32_t interval, bool *flag) : flag(flag)
  {
    this->interval = interval; // set interval as is, recount it later in attach function
  }
    
  void stop() override
  {
    enabled = false;
    reset();
  }
  
  void start() override { enabled = true; }
  
  void tickGetReady() override
  {
    if(flag)
      *flag = true;
  }
  
  ~FlagTimer(){ }
};