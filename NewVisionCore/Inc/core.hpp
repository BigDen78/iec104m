/*
NewVision Core for STM32

Neue Ohren fur neue Musik. Neue Augen fur das Fernste.
Friedrich Nietzsche.
*/

#pragma once
#include <CommandQueue.hpp>
#include <ParameterizedObject.hpp>
#include <hal_thread.h>
#include <timed.h>
#include <bitset>
#include <map>
#include <memory>
#include <type_traits>
#define MAX_DRIVERS 32

template <class _T, class _B>
constexpr _T d_cast(_B base) // Analog of Dyn cast
{
	return reinterpret_cast<_T>(base->getUnderlyingObject());
}

class Tag;
class TagTemplate;
class Driver;
class Device;
class DeviceTemplate;
class ChangeTagCommand;
class DriverContext;

using SubscriberSet = std::bitset<MAX_DRIVERS>;
using NamesOfTags = std::map<String, Tag *>;
using NamesOfDrivers = std::map<String, Driver *>;
using NamesOfDevices = std::map<String, Device *>;
using NamesOfTagTemplates = std::map<String, TagTemplate *>;
using NamesOfDevTemplates = std::map<String, DeviceTemplate *>;
using TagsVector = std::vector<Tag *>;
using DriversVector = std::vector<Driver *>;
using ContextsVector = std::vector<DriverContext *>;
using DevicesVector = std::vector<Device *>;
using TemplatesVector = std::vector<TagTemplate *>;

/**
* @brief struct that describes part of tag metadata that doesn't change at run time after initialization
*/
struct StaticTagInfo
{
	///default constructor
	StaticTagInfo() = default;
	/**
	*@brief constructor that initializes metadata
	*@param type : #DataType - type of tag
	*@param name : name of tag
	*@param arraySize : uint32_t - size of array (if tag is of array type or 0 if tag represents single value)
	*/
	StaticTagInfo(DataType type, const String &name, uint32_t arraySize = 0) : type(type), arraySize(arraySize), name(name), device(nullptr), parent(nullptr) {}
	///copy constructor
	StaticTagInfo(const StaticTagInfo &staticInfo) = delete;
	///unique id (index) of tag
	uint32_t uniqueId;
	///type of tag
	DataType type;
	///size of array if tag is of array type or 0 if tag represents single value
	uint32_t arraySize; /////////////////////////////////////////////////////////////// legacy
	///pointer to #Driver interface of driver instance owning this tag
	Driver *parent;
	///pointer to #Device interface of driver instance owning this tag
	Device *device;
	///name of the tag
	String name;
};

class TagTemplate
{
	friend class TemplateBuilder;
	friend class Core;
	friend class TagBuilder;

public:
	const String &getName() const { return staticInfo.name; }
	DriverContext *getParentContext() const { return parentContext; }

protected:
	void setName(String &&name) { staticInfo.name = name; } // Move semantic from json parser
	void setParentContext(DriverContext *ctx) { parentContext = ctx; }
	StaticTagInfo staticInfo;
	DriverContext *parentContext;
};

/**
* @brief class that incorporates part of tag metadata that can change at run time and tag data itself
*
*/
class Tag
{
	friend class Core;
	friend class TagBuilder;

public:
	///default constructor
	Tag() : valid(0), timestamp(0), pValue(0), pDefaultValue(0), staticInfo(dtVoid, "", 0){};
	///copy constructor
	Tag(const Tag &tag) = delete;
	/**
	* @brief Construct a new Tag object. Initializes metadata without actual data allocation
	*
	* @param type #DataType - type of tag
	* @param name
	* @param arraySize uint32_t - size of array (if tag is of array type or 0 if tag represents single value)
	*/
	Tag(DataType type, const String &name, uint32_t arraySize = 0) : valid(0), timestamp(0), pValue(0), pDefaultValue(0), staticInfo(type, name, arraySize) {}
	/**
	*@brief calculate size of memory area to store tag data
	*@return size of requred memory in bytes
	*/
	uint32_t getStorageSize() { return staticInfo.arraySize ? staticInfo.arraySize * dataSizeOfType[staticInfo.type] : sizeof(VarData); }
	/**
	*@brief initialize data storage
	*@param pStorage pointer to memory area to store actual tag data (area supposed to be at least Tag::getStorageSize() bytes)
	*/
	void setStorage(void *pStorage) { pValue = (VarData *)pStorage; }
	/**
	* @brief Set the Value of tag from VarData (NOT thread-safe, intended to be called inside core thread)
	*
	* @param data VarData union containing new value
	*/
	void setValue(const VarData &data)
	{
		if (pValue)
			*pValue = data;
	}
	void setDefaultValue(const VarData &data)
	{
		if (!pDefaultValue)
			pDefaultValue = new VarData;
		if (pDefaultValue)
			*pDefaultValue = data;
	}

	void setValid(bool validity) { valid = validity; }
	void updateTS(uint32_t shortTS) { timestamp = shortTSToLongTS(shortTS); }
	uint32_t getId() const { return staticInfo.uniqueId; }
	Driver *getParent() const { return staticInfo.parent; }
	Device *getDevice() const { return staticInfo.device; }
	DataType getType() const { return staticInfo.type; }
	bool getValid() const { return valid; }
	time_t getTS() const { return timestamp; }
	const String &getName() const { return staticInfo.name; }
	const VarData &getValue() const { return *pValue; }
	void registerSubscriber(uint32_t subscriberId)
	{
		if (subscriberId < MAX_DRIVERS)
			subscribers[subscriberId] = true;
	}
	void unregisterSubscriber(uint32_t subscriberId)
	{
		if (subscriberId < MAX_DRIVERS)
			subscribers[subscriberId] = false;
	}
	DriverContext *getDriverSpecificInfo(uint32_t driver_id) const { return driverContexts[driver_id]; }
	template <typename T>
	T get() const
	{
		switch (staticInfo.type)
		{
		case dtBool:
			return (T)pValue->boolVal;
		case dtByte:
			return (T)pValue->byteVal;
		case dtWord:
			return (T)pValue->wordVal;
		case dtDWord:
			return (T)pValue->dwordVal;
		case dtShortInt:
			return (T)pValue->shortVal;
		case dtInt:
			return (T)pValue->intVal;
		case dtFloat:
			return (T)pValue->fltVal;
		case dtString:
			if constexpr (std::is_array<T>() || std::is_pointer<T>())
				return (T)pValue->pStr;
			else
				return 0;
		}
		return 0;
	}
	template <typename T>
	void set(T newVal)
	{
		switch (staticInfo.type)
		{
		case dtBool:
			pValue->boolVal = static_cast<bool>(newVal);
			break;
		case dtByte:
			pValue->byteVal = static_cast<uint8_t>(newVal);
			break;
		case dtWord:
			pValue->wordVal = static_cast<uint16_t>(newVal);
			break;
		case dtDWord:
			pValue->dwordVal = static_cast<uint32_t>(newVal);
			break;
		case dtShortInt:
			pValue->shortVal = static_cast<int16_t>(newVal);
			break;
		case dtInt:
			pValue->intVal = static_cast<int32_t>(newVal);
			break;
		case dtFloat:
			pValue->fltVal = static_cast<float>(newVal);
			break;
		case dtString:
			if constexpr (std::is_array<T>() || std::is_pointer<T>())
				pValue->pStr = reinterpret_cast<T>(newVal);
			break;
		}
	}
	operator bool() const; /////////////////////////////////////// Not ready already for direct cast
	operator int() const;
	operator unsigned int() const;
	operator char() const;
	operator float() const;
	operator double() const;
	int operator=(int value);

protected:
	void setId(uint32_t newId) { staticInfo.uniqueId = newId; }
	void setParent(Driver *parent) { staticInfo.parent = parent; }
	void setDevice(Device *device) { staticInfo.device = device; }
	void setName(String &&name) { staticInfo.name = name; }
	void allocateDriverInfos(uint32_t driversCount) { driverContexts.resize(driversCount, nullptr); }
	void setDriverSpecificInfo(uint32_t driver_id, DriverContext *info) { driverContexts[driver_id] = info; }
	VarData *pValue;
	VarData *pDefaultValue;
	StaticTagInfo staticInfo;
	time_t timestamp;
	bool valid;
	SubscriberSet subscribers;
	ContextsVector driverContexts;

public:
	static const Tag NoTag;
	static Tag &NoTagRef;
};

class DriverContext : virtual public ParameterizedObject
{
public:
	DriverContext(){};
	virtual ~DriverContext() = default;
};

template <typename T>
VarData varDataCreate(void *pV)
{
	VarData data = {0};
	if constexpr (std::is_same<T, bool>::value)
		data.boolVal = *reinterpret_cast<T *>(pV);
	else if constexpr (std::is_same<T, uint8_t>::value)
		data.byteVal = *reinterpret_cast<T *>(pV);
	else if constexpr (std::is_same<T, uint16_t>::value)
		data.wordVal = *reinterpret_cast<T *>(pV);
	else if constexpr (std::is_same<T, uint32_t>::value)
		data.dwordVal = *reinterpret_cast<T *>(pV);
	else if constexpr (std::is_same<T, int16_t>::value)
		data.shortVal = *reinterpret_cast<T *>(pV);
	else if constexpr (std::is_same<T, int32_t>::value)
		data.intVal = *reinterpret_cast<T *>(pV);
	else if constexpr (std::is_same<T, float>::value)
		data.fltVal = *reinterpret_cast<T *>(pV);
	else if constexpr (std::is_same<T, char *>::value)
		data.pStr = *reinterpret_cast<T *>(pV);
	else
		data.dwordVal = 0;
	return data;
}

typedef VarData (*VDFactoryFunction)(void *);

extern VDFactoryFunction vdFactory[dtMaxType];

/**
*Core class is the core of NV runtime system which coordinates all other NV components
*each firmware supposed to create a single instance of this class and call it's Core#Run method in context of separate dedicated thread
*/
class Core
{
public:
	using QueType = InterThreadQueue<ChangeTagCommand>;

	enum CoreStateEnum 
	{
		rseUninit,
		rseReady,
		rseRunning,
		rseError
	};
	enum CoreErrorType
	{
		reOk,
		reMemAlloc,
		reUnknown
	};
	/**
	*
	*/
	void loadJson(const CString &jsonPath = "0:\\root.json");
	///perform initialization
	void initialize();
	///main loop
	void run();
	CoreStateEnum getState() { return state; }
	CoreErrorType getLastError() { return lastError; }
	DriversVector &getDrivers() { return drivers; }
	TagsVector &getTags() { return tags; }
	DevicesVector &getDevices() { return devices; }
	NamesOfTags &getTagNames() { return tagNamesMap; }
	NamesOfDrivers &getDriverNames() { return driverNamesMap; }
	NamesOfDevices &getDeviceNames() { return deviceNamesMap; }

	void setTag(const String &tagName, union VarData newData, bool valid, uint32_t sender);
	void setTag(Tag *pTag, union VarData newData, bool valid, uint32_t sender);
	void setTag(Tag *pTag, union VarData newData, bool valid, uint32_t sender, uint32_t ts);
	Tag *getTag(const String &tagName) const;
	Driver *getDriver(const String &driverName) const;
	Device *getDevice(const String &deviceName) const;
	template <typename _T>
	static void set(const char *name, _T value, bool valid = true, uint32_t sender = -1)
	{
		auto &core = Core::instance();
		auto it = core.tagNamesMap.find(name);
		if (core.tagNamesMap.end() != it)
		{
			Tag *pTag = it->second;
			set(*pTag, value, valid, sender);
		}
	}
	template <typename _T>
	static void set(Tag &tag, _T value, bool valid, uint32_t sender)
	{
		set<_T>(tag, value, valid, sender, getShortTS());
	}
	template <typename _T>
	static void set(Tag &tag, _T value, bool valid, uint32_t sender, uint32_t ts)
	{
		if (tag.staticInfo.type <= dtVoid || tag.staticInfo.type > dtString)
			return;
		auto data = vdFactory[tag.staticInfo.type](&value);
		instance().setTag(&tag, data, valid, sender, ts);
	}

	template <typename _T>
	static void setIfChanged(const char *name, _T value, bool valid = true, uint32_t sender = -1)
	{
		auto &core = Core::instance();
		auto it = core.tagNamesMap.find(name);
		if (core.tagNamesMap.end() != it)
		{
			Tag *pTag = it->second;
			setIfChanged(*pTag, value, valid, sender);
		}
	}
	template <typename _T>
	static void setIfChanged(Tag &tag, _T value, bool valid, uint32_t sender)
	{
		setIfChanged<_T>(tag, value, valid, sender, 0);
	}
	template <typename _T>
	static void setIfChanged(Tag &tag, _T value, bool valid, uint32_t sender, uint32_t ts)
	{
		bool validityChanged = tag.getValid() != valid;
		bool valueChanged = false;
		VarData data = tag.getValue();
		switch (tag.staticInfo.type)
		{
		case dtBool:
			if (validityChanged || (valueChanged = (data.boolVal != (bool)value)))
				data.boolVal = value;
			break;
		case dtVoid:
			return;
		case dtByte:
			if (validityChanged || (valueChanged = (data.byteVal != (uint8_t)value)))
				data.byteVal = (uint8_t)value;
			break;
		case dtWord:
			if (validityChanged || (valueChanged = (data.wordVal != (uint16_t)value)))
				data.wordVal = (uint16_t)value;
			break;
		case dtDWord:
			if (validityChanged || (valueChanged = (data.dwordVal != (uint32_t)value)))
				data.dwordVal = (uint32_t)value;
			break;
		case dtShortInt:
			if (validityChanged || (valueChanged = (data.shortVal != (int16_t)value)))
				data.shortVal = (int16_t)value;
			break;
		case dtInt:
			if (validityChanged || (valueChanged = (data.intVal != (int32_t)value)))
				data.intVal = (int32_t)value;
			break;
		case dtFloat:
			if (validityChanged || (valueChanged = (data.fltVal != (float)value)))
				data.fltVal = (float)value;
			break;
		case dtString:
			return;
		case dtWString:
			return;
		}
		if (valueChanged || validityChanged)
		{
			instance().setTag(&tag, data, valid, sender, ts ? ts : getShortTS());
		}
	}

	template <typename _T>
	static _T get(const char *name)
	{
		auto pTag = _instance->getTag(name);
		if (pTag)
		{
			return pTag->get<_T>();
		}
		else
		{
			return (_T)0;
		}
	}

	static Core &instance()
	{
		if (!_instance)
			_instance = new Core;
		return *_instance;
	}

protected:
	void allocTagsMemory();

private:
	Core();
	Core(const Core &) = delete;
	Core &operator=(const Core &) = delete;
	TagsVector tags;
	NamesOfTags tagNamesMap;
	DriversVector drivers;
	NamesOfDrivers driverNamesMap;
	DevicesVector devices;
	NamesOfDevices deviceNamesMap;
	CoreStateEnum state;
	CoreErrorType lastError;
	QueType *commandQueue;
	static Core *_instance;
};