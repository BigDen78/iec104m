////////////////////////////////////////////////////////
//
//  get from - IEC101M.hpp
// 

#pragma once
#include "IECSlaveBase.hpp"
//--#include "cs104_slave.h"       //-- #include "cs101_master.h"
#include "cs104_connection.h"

//-- #include "driver.hpp"
//-- #include <deque>

//-- #define CMD_IDLE 1u
//-- #define CMD_TX_COMPLETE 2u

class IEC104GenericDevice : virtual public Device
{
public:
  using WriteQueue = std::deque<InformationObject>;
  typedef bool(IEC104GenericDevice::*IOHandlerFn)(Tag &tag, InformationObject io, CS101_CauseOfTransmission cot);
  enum RunState
  {
    rsInitial,
    rsUnselected,
    rsSelected,
    rsGI,
    rsReadyToRelease,
    rsOffline
  };
  
  int Test;

  IEC104GenericDevice()
    : ParameterizedObject(parameters)
    //, linkAddress(1)
    , asduAddress(1)
    , giInterval(60000)
    , tsInterval(600000)
    , nextGITime(0)
    , nextTSTime(0)
    , commWindow(100)
    , runState(rsInitial)
    , master(nullptr)
    , writeQueue()
    , linkState(LL_STATE_IDLE)
    , errorCount(0)
    , offline(false)
    , offlineCheckTS(0) 
    {
        Test = 123456;
//        linkState = LL_STATE_IDLE;
        //linkState = LL_STATE_ERROR;
        //linkState = LL_STATE_BUSY;
        //linkState = LL_STATE_AVAILABLE;
        
  //  giInterval  =  giInterval + 1;     
    } 
      
  virtual bool registerTag(const Tag &tag, void *ctx) override;
  virtual void initialize(void *pvParameter) override;
  virtual void select(void *pvParameter) override;
  virtual bool run() override;
  virtual bool tagChanged(const Tag &tag, uint32_t paramId, VarData newValue) override;
  virtual bool canRelease() override;
  virtual bool release() override;
  virtual bool setParameterById(uint32_t id, VarData newValue) override;
  virtual bool getParameterById(uint32_t id, VarData &value) const override;
  virtual bool handleIncomingData(void *pData) override;
  virtual void handleLinkLayerMessage(void *pData) override;
  virtual void *getUnderlyingObject() override { return this; }
  uint32_t getWaitingCommandsCount() { return writeQueue.size(); }
  bool sendQueuedCommand();
  bool giCondition() { return ((LL_STATE_AVAILABLE == linkState) || (LL_STATE_IDLE == linkState)) && (clock() >= nextGITime); }
  bool tsCondition() { return ((LL_STATE_AVAILABLE == linkState) || (LL_STATE_IDLE == linkState)) && (clock() >= nextTSTime); }
  bool beginGI();
  bool issueTimeSync();
  int TestTest(int x);
  uint16_t getLinkAddress() { return linkAddress; }

protected:
  void scheduleNextGI() { nextGITime = clock() + giInterval; }
  void scheduleNextTS() { nextTSTime = clock() + tsInterval; }
  bool handleMSPNA1(Tag &tag, InformationObject io, CS101_CauseOfTransmission cot);
  bool handleMDPNA1(Tag &tag, InformationObject io, CS101_CauseOfTransmission cot);
  bool handleMMENB1(Tag &tag, InformationObject io, CS101_CauseOfTransmission cot);
  bool handleMMENC1(Tag &tag, InformationObject io, CS101_CauseOfTransmission cot);
  bool handleMSPTB1(Tag &tag, InformationObject io, CS101_CauseOfTransmission cot);
  bool handleMDPTB1(Tag &tag, InformationObject io, CS101_CauseOfTransmission cot);
  bool handleMMETE1(Tag &tag, InformationObject io, CS101_CauseOfTransmission cot);  //
  bool handleMMETF1(Tag &tag, InformationObject io, CS101_CauseOfTransmission cot);
  bool handleCICNA1(Tag &tag, InformationObject io, CS101_CauseOfTransmission cot);

  InformationObject createIOFromTagUpdate(const Tag &tag, VarData newValue);

  uint16_t linkAddress;
  uint16_t asduAddress;
  uint32_t giInterval;
  uint32_t tsInterval;
  uint32_t nextGITime;
  uint32_t nextTSTime;
  uint16_t commWindow;
  IOAMap ioaMap;
  RunState runState;
  CS104_Connection master; // CS101_Master master;
  WriteQueue writeQueue;
  LinkLayerState linkState;
  uint32_t errorCount;
  bool offline;
  uint32_t offlineCheckTS;
  static const ParameterVector parameters;
  static const IOHandlerFn ioHandlers[128];
};

//
//
/////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////
//
// get from - IEC104s.hpp
//

class IEC104MDriver : virtual public Driver  // virtual public IECSlaveBase
{
public:
	enum RunningState : uint32_t
	{
		rsInitial, 
		rsRunning,
                rsInitializationErr
	};

	IEC104MDriver()
          : Driver()
          , ParameterizedObject(parameters)
          , runState(rsInitial)
          , port(2404)
          , AsduAddress(0)  
          , waitingCommandsCnt(0)
        {
          this->devicesSupported = true;
        }

	virtual void initialize() override;

        virtual void registerTag(const Tag &tag, void *ctx) override { return; } //++
	virtual bool tagChanged(const Tag &tag, uint32_t ts, VarData newValue) override;
        virtual bool setParameterById(uint32_t paramId, VarData newValue) override;
	virtual bool setRuntimeParameter(const Tag &tag, uint32_t paramId, VarData paramValue) override;
        virtual bool setTagRuntimeParameter(const Tag &tag, uint32_t paramId, VarData paramValue) override { return false; }
	virtual VarData getRuntimeParameter(uint32_t paramId) override { return NO_DATA; }
        virtual VarData getTagRuntimeParameter(const Tag &tag, uint32_t paramId) override { return NO_DATA; } //++
	virtual void run() override;
        virtual DriverContext *createTagContext() const override { return new IECSContext; }
        virtual bool registerDevice(Device *device, void *ctx) override;        
	static const ParameterVector parameters;
        
        bool beginGI();
        bool issueTimeSync();
        
        bool giCondition() { return /*(LL_STATE_AVAILABLE == linkState) || (LL_STATE_IDLE == linkState)) &&*/ (clock() >= nextGITime); }
        bool tsCondition() { return /*(LL_STATE_AVAILABLE == linkState) || (LL_STATE_IDLE == linkState)) &&*/ (clock() >= nextTSTime); }

protected:
        static bool asduReceivedHandlerStatic(void *parameter, int address, CS101_ASDU asdu);
        static void ConnectionHandlerStatic(void* parameter, CS104_Connection connection, CS104_ConnectionEvent event);
        InformationObject createIOFromTagUpdate(const Tag &tag, VarData newValue);

        void scheduleNextGI() { nextGITime = clock() + giInterval; }
        void scheduleNextTS() { nextTSTime = clock() + tsInterval; }
        
        uint32_t giInterval;
        uint32_t tsInterval;
        uint32_t nextGITime;
        uint32_t nextTSTime;
    
        CS104_Connection master = 0; //++
//--	CS104_Slave slave = 0;
	RunningState runState;
	uint32_t port;
        uint32_t AsduAddress;
	CString IPAddr; //++
        uint32_t waitingCommandsCnt;
        IEC104GenericDevice* WorkDevice;
        
/*	void setRedGroups();
	static bool clockSyncHandlerStatic(void *p, IMasterConnection connection, CS101_ASDU asdu, CP56Time2a newTime)
	{
		return reinterpret_cast<IEC104MDriver *>(p)->clockSyncHandler(connection, asdu, newTime);
	}
	static bool interrogationHandlerStatic(void *p, IMasterConnection connection, CS101_ASDU asdu, uint8_t qoi)
	{
		return reinterpret_cast<IEC104MDriver *>(p)->interrogationHandler(connection, asdu, qoi);
	}
	static bool asduHandlerStatic(void *p, IMasterConnection connection, CS101_ASDU asdu)
	{
		return reinterpret_cast<IEC104MDriver *>(p)->asduHandler(connection, asdu);
	} */
        
        
};

//
//
/////////////////////////////////////////////////////////
