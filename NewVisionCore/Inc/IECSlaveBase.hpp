#pragma once
#include <list>
#include <map>
#include "driver.hpp"
#include "iec60870_common.h"
#include "iec60870_slave.h"
#include "cs101_information_objects.h"

using IOAMap = std::map<uint32_t, Tag *>;
using InterrogationPair = std::pair<IEC60870_5_TypeID, TagList>;
using InterrogationList = std::forward_list<InterrogationPair>;

enum TagParamIDs : uint32_t
{
  pidSpontType,
  pidInterType,
  pidCmdType,
  pidIOA
};

class IECSContext : virtual public DriverContext
{
public:
  IECSContext() : spontType((IEC60870_5_TypeID)0), interType((IEC60870_5_TypeID)0), ioa(0),
	ParameterizedObject(parameters){};
  virtual ~IECSContext() = default;
	virtual bool setParameterById(uint32_t id, VarData newValue) override;
  IEC60870_5_TypeID spontType;
  IEC60870_5_TypeID interType;
  IEC60870_5_TypeID cmdType;
  uint32_t ioa;
	static const ParameterVector parameters;
};

class IECSlaveBase : virtual public Driver
{
public:
  IECSlaveBase() : spontDelay(50){};
  virtual void registerTag(const Tag &tag, void *ctx) override;
  virtual bool setTagRuntimeParameter(const Tag &tag, uint32_t paramId, VarData paramValue) override;
  virtual VarData getTagRuntimeParameter(const Tag &tag, uint32_t paramId) override;
  virtual DriverContext *createTagContext () const override;

protected:
  bool clockSyncHandler(IMasterConnection connection, CS101_ASDU asdu, CP56Time2a newTime);
  bool interrogationHandler(IMasterConnection connection, CS101_ASDU asdu, uint8_t qoi);
  bool asduHandler(IMasterConnection connection, CS101_ASDU asdu);
  void buildInterrogationList();
  bool addIOFromTagToASDU(CS101_ASDU asdu, IEC60870_5_TypeID typeId, const Tag &tag);
  bool addIOWithTSFromTagToASDU(CS101_ASDU asdu, IEC60870_5_TypeID typeId, const Tag &tag, VarData newValue, uint32_t shortTS);
  bool executeTagAssignmentFromIO(InformationObject io, IEC60870_5_TypeID typeId, Tag &tag);
  uint32_t asduAddress;
  uint32_t spontDelay;
  IOAMap ioaMap;
  InterrogationList interList;
  CS101_ASDU spontASDU;
  IEC60870_5_TypeID currentSpontType;
};
