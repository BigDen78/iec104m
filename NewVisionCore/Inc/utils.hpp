#pragma once
#include "core.hpp"

DataType DataTypeFromTypeName(const String &typeName);

VarData getDataFromJson(DataType dataType, const Json &json);

void writeTagValueToJson(Json& j, const Tag& t);
