#pragma once
#include "IECSlaveBase.hpp"
#include "usart.h"
#include "hal_serial.h"
#include "cs101_slave.h"

using IOAMap = std::map<uint32_t, Tag *>;
using InterrogationPair = std::pair<IEC60870_5_TypeID, TagList>;
using InterrogationList = std::forward_list<InterrogationPair>;

class IEC101SDriver : virtual public IECSlaveBase
{
public:
	enum RunningState : uint32_t
	{
		rsPreInit,
		rsInitializing,
		rsRunning,
		rsSpontDataAwait,
		rsUnrecoverableError
	};
	IEC101SDriver();
	virtual bool setParameterById(uint32_t paramId, VarData newValue) override;
	virtual bool setRuntimeParameter(const Tag &tag, uint32_t paramId, VarData paramValue);
	virtual VarData getRuntimeParameter(uint32_t paramId);
	virtual void initialize();
	virtual bool tagChanged(const Tag &tag, uint32_t shortTS, VarData newValue);
	virtual void run();

protected:
	static void resetCUHandlerStatic(void *p)
	{
		reinterpret_cast<IEC101SDriver *>(p)->resetCUHandler();
	}
	static bool clockSyncHandlerStatic(void *p, IMasterConnection connection, CS101_ASDU asdu, CP56Time2a newTime)
	{
		return reinterpret_cast<IEC101SDriver *>(p)->clockSyncHandler(connection, asdu, newTime);
	}
	static bool interrogationHandlerStatic(void *p, IMasterConnection connection, CS101_ASDU asdu, uint8_t qoi)
	{
		return reinterpret_cast<IEC101SDriver *>(p)->interrogationHandler(connection, asdu, qoi);
	}
	static bool asduHandlerStatic(void *p, IMasterConnection connection, CS101_ASDU asdu)
	{
		return reinterpret_cast<IEC101SDriver *>(p)->asduHandler(connection, asdu);
	}
	static void linkLayerStateChangedHandlerStatic(void *parameter, int address, LinkLayerState newState)
	{
		reinterpret_cast<IEC101SDriver *>(parameter)->linkLayerStateChangedHandler(address, newState);
	}
	void linkLayerStateChangedHandler(int address, LinkLayerState newState);

	void resetCUHandler();
	RunningState runState;
	uint32_t linkAddress;
	UART_HandleTypeDef *huart;
	uint32_t baudRate;
	uint8_t stopBits;
	char parity;
	bool useSingleCharACK;
	SerialPort port;
	CS101_Slave slave = 0;
	DriverCommand dcmdSerialIdle;

	static const ParameterVector parameters;
};