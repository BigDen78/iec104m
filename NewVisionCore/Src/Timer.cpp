#include <Timer.hpp>

using timerParams = std::pair<IRQn_Type, uint32_t>;

HWTimer::HWTimer(TIM_TypeDef *tim)
{
  std::map<TIM_TypeDef*, timerParams> tmpMap
  {
    { TIM3, std::make_pair(TIM3_IRQn, LL_APB1_GRP1_PERIPH_TIM3)},
    { TIM4, std::make_pair(TIM4_IRQn, LL_APB1_GRP1_PERIPH_TIM4)},
    { TIM5, std::make_pair(TIM5_IRQn, LL_APB1_GRP1_PERIPH_TIM5)}
  };
  
  std::map<TIM_TypeDef*, timerParams>::const_iterator it;
  if((it = tmpMap.find(tim)) != tmpMap.end())
  {
    timCntr.tim          = tim;
    timCntr.nvicPriority = 5;
    timCntr.period       = 1;
    timCntr.irqType      = std::get<0>(it->second);
    timCntr.apbType      = std::get<1>(it->second);
  }
}

