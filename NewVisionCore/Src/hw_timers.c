#include "interrupts.h"
#include "FREERTOS.h"
#include "task.h"  
#include "hw_timers.h"  
   
/**
  * @brief  timerHandler uses as an interrupt handler callback
  */
void timerHandler(void *x)
{
  TimerCntr *pTimCntr = (TimerCntr*)x;
  if(LL_TIM_IsActiveFlag_UPDATE(pTimCntr->tim))
  {
   LL_TIM_ClearFlag_UPDATE(pTimCntr->tim);
   if(pTimCntr->xTask != NULL)
   {
     BaseType_t xHigherPriorityTaskWoken = pdFALSE;
     vTaskNotifyGiveFromISR(pTimCntr->xTask, &xHigherPriorityTaskWoken);
     portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
   }
   else if(pTimCntr->func != NULL)
   {
     pTimCntr->func(pTimCntr->callbackParams);
   }
  }
}

/**
  * @brief  Calculate prescalers to stm32 timer
  * @param  uint16_t *Prescaler - return TIM2 prescaler
  * @param  uint32_t *Autoreload - return TIM2 autoreload value
  * @param  uint32_t divizorProizved = Prescaler * Autoreload
  */
static void getTimPrescalers(uint16_t *Prescaler, uint32_t *Autoreload, uint32_t divizorProizved)
{
  uint16_t x = 1, y = 1;
  uint32_t div = 2;
  uint8_t component = 1;
  
  while (divizorProizved > 1)
  {
    while (divizorProizved % div == 0)
    {
      if(component)
      {
        x = x * div;
        if(x > 10000)
        {
          x = x / div;
          component = 0;
          y = y * div;
        }
      }
      else
        y = y * div;
      divizorProizved = divizorProizved / div;
    }
    div++;
  }
  
  *Prescaler = x;
  *Autoreload = y;
}

void timerStart(TimerCntr *pTimCntr)
{
  LL_TIM_EnableIT_UPDATE(pTimCntr->tim);
  LL_TIM_EnableCounter(pTimCntr->tim);
}

void timerStop(TimerCntr *pTimCntr)
{
  LL_TIM_DisableIT_UPDATE(pTimCntr->tim);
  LL_TIM_DisableCounter(pTimCntr->tim);
}

/**
  * @brief  timerInit inits timer struct, timer handler, also it can reInit timer
  */
void timerInit(TimerCntr *pTimCntr)
{
  timerStop(pTimCntr);                        /* In order to reInit without stop */
  setVector(pTimCntr->irqType, timerHandler, pTimCntr);
 
  LL_RCC_ClocksTypeDef rccClocks;             /* To get current tim2 frequency */
  LL_RCC_GetSystemClocksFreq(&rccClocks);
  uint32_t divizorProizved = rccClocks.PCLK1_Frequency * 2 / pTimCntr->period;

  LL_TIM_InitTypeDef TIM_InitStruct = {0};

  getTimPrescalers(&(TIM_InitStruct.Prescaler), &(TIM_InitStruct.Autoreload), divizorProizved);

  LL_APB1_GRP1_EnableClock(pTimCntr->apbType);  /* Peripheral clock enable */
  NVIC_SetPriority(pTimCntr->irqType, NVIC_EncodePriority(NVIC_GetPriorityGrouping(), pTimCntr->nvicPriority, 0));
  NVIC_EnableIRQ(pTimCntr->irqType);

  TIM_InitStruct.CounterMode   = LL_TIM_COUNTERMODE_UP;
  TIM_InitStruct.ClockDivision = LL_TIM_CLOCKDIVISION_DIV1;

  LL_TIM_Init(pTimCntr->tim, &TIM_InitStruct);
  LL_TIM_DisableARRPreload(pTimCntr->tim);
  LL_TIM_SetClockSource(pTimCntr->tim, LL_TIM_CLOCKSOURCE_INTERNAL);
  LL_TIM_SetTriggerOutput(pTimCntr->tim, LL_TIM_TRGO_UPDATE);
  LL_TIM_DisableMasterSlaveMode(pTimCntr->tim);
}