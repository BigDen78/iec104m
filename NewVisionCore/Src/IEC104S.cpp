#include <iec104s.hpp>
#include <hal_thread.h>
#include <DriverBuilder.hpp>
#include <ini.h>
#include <string>

#define CMD_IDLE 1u

REGISTER_FACTORY_METHOD(DriverBuilder::driverFactoryTable, Driver, IEC104Slave, new IEC104SDriver);

enum ParamIDs : uint32_t
{
	pidASDUAddress,
	pidSpontDelay,
	pidPort,
	pidRedGroupConfig
};

const ParameterVector IEC104SDriver::parameters{
		{"ASDUAddress", {pidASDUAddress, dtDWord, StaticParameterInfo::pfRequired}},
		{"spontDelay", {pidSpontDelay, dtWord, StaticParameterInfo::pfNone}},
		{"port", {pidPort, dtWord, StaticParameterInfo::pfNone}},
		{"redGroupConfig", {pidRedGroupConfig, dtString, StaticParameterInfo::pfNone}}};

IEC104SDriver::IEC104SDriver() : runState(rsPreInit), port(2404),
																 ParameterizedObject(parameters) {}

bool IEC104SDriver::setParameterById(uint32_t paramId, VarData newValue)
{
	switch (static_cast<ParamIDs>(paramId))
	{
	case pidASDUAddress:
		asduAddress = newValue.dwordVal & 0xffffffu;
		return true;
	case pidSpontDelay:
		if (newValue.wordVal <= 1000)
		{
			spontDelay = newValue.wordVal;
			return true;
		}
		break;
	case pidPort:
		port = newValue.wordVal;
		return true;
	case pidRedGroupConfig:
		redPath = newValue.pStr;
		return true;
	}
	return false;
}

bool IEC104SDriver::setRuntimeParameter(const Tag &tag, uint32_t paramId, VarData paramValue)
{
	return false;
}

VarData IEC104SDriver::getRuntimeParameter(uint32_t paramId)
{
	VarData result;
	result.dwordVal = 0;
	return result;
}

extern "C" uint8_t validateIP4Dotted(const char *s);

__weak uint8_t validateIP4Dotted(const char *s) { return 0; }

void IEC104SDriver::setRedGroups()
{
	char buf[32];
	char ipNum[4];
	mINI::INIFile file(redPath);
	mINI::INIStructure ini;
	if (file.read(ini))
	{
		CS104_RedundancyGroup redGroup;
		for (uint8_t redGroupNumber = 1; redGroupNumber <= 4; redGroupNumber++)
		{
			snprintf(buf, 32, "RedGroup%d", redGroupNumber);

			if (ini.has(buf))
			{
				redGroup = CS104_RedundancyGroup_create(buf);
				for (int ipIndex = 1; ipIndex <= 4; ipIndex++)
				{
					snprintf(ipNum, 4, "ip%d", ipIndex);
					if (ini[buf].has(ipNum))
					{
						std::string &ipAddr = ini[buf][ipNum];
            if(!validateIP4Dotted(ipAddr.c_str()))
						  CS104_RedundancyGroup_addAllowedClient(redGroup, ipAddr.c_str());
					}
				}
				CS104_Slave_addRedundancyGroup(slave, redGroup);
			}
		}
	}
}

void IEC104SDriver::initialize()
{
	Driver::initialize();
	buildInterrogationList();
	slave = CS104_Slave_create(64, 64);

	if (slave)
	{
		CS104_Slave_setServerMode(slave, CS104_MODE_MULTIPLE_REDUNDANCY_GROUPS);
		CS104_Slave_setLocalPort(slave, port);
		setRedGroups();
		CS104_RedundancyGroup rg_all = CS104_RedundancyGroup_create("catch-all");
		CS104_Slave_addRedundancyGroup(slave, rg_all);

		auto alParameters = CS104_Slave_getAppLayerParameters(slave);
		spontASDU = CS101_ASDU_create(alParameters, false, CS101_COT_SPONTANEOUS, 0, asduAddress, false, false);
		currentSpontType = IEC60870_5_TypeID::M_SP_NA_1;
		commandQueue->getTimer().setPeriod(spontDelay);

		CS104_Slave_setMaxOpenConnections(slave, 4);

		CS104_Slave_setClockSyncHandler(slave, IEC104SDriver::clockSyncHandlerStatic, this);
		CS104_Slave_setInterrogationHandler(slave, IEC104SDriver::interrogationHandlerStatic, this);
		CS104_Slave_setASDUHandler(slave, IEC104SDriver::asduHandlerStatic, this);

		CS104_Slave_start(slave);
		runState = rsRunning;
		return;
	}

	runState = rsUnrecoverableError;
}

bool IEC104SDriver::tagChanged(const Tag &tag, uint32_t shortTS, VarData newValue)
{
	if (rsRunning == runState || rsSpontDataAwait == runState)
	{
		auto ctx = reinterpret_cast<IECSContext *>(tag.getDriverSpecificInfo(id));
		auto typeId = ctx->spontType;
		if ((typeId != currentSpontType) && CS101_ASDU_getNumberOfElements(spontASDU))
		{
			CS104_Slave_enqueueASDU(slave, spontASDU);
			CS101_ASDU_removeAllElements(spontASDU);
		}
		if (!addIOWithTSFromTagToASDU(spontASDU, typeId, tag, newValue, shortTS))
		{
			CS104_Slave_enqueueASDU(slave, spontASDU);
			CS101_ASDU_removeAllElements(spontASDU);
			addIOWithTSFromTagToASDU(spontASDU, typeId, tag, newValue, shortTS);
		}
		runState = rsSpontDataAwait;
		currentSpontType = typeId;
		commandQueue->getTimer().rearm();
	}
	return true;
}

void IEC104SDriver::run()
{
	switch (runState)
	{
	case rsPreInit:
		runState = rsUnrecoverableError; /* can't get here in normal circumstances */
		break;
	case rsInitializing:
		break;
	case rsRunning:
		break;
	case rsSpontDataAwait:
		if (commandQueue->getTimer().ready())
		{
			if (CS101_ASDU_getNumberOfElements(spontASDU))
			{
				CS104_Slave_enqueueASDU(slave, spontASDU); /* send filled ASDU */
				CS101_ASDU_removeAllElements(spontASDU);	 /* clear ASDU */
			}
			runState = rsRunning;
		}
		break;
	case rsGI:
		break;
	case rsFileTransfer:
		break;
	case rsRecoverableError:
		break;
	case rsUnrecoverableError:
		break;
	}
}
