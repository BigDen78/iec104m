#include "adcRead.h"
#include <limits.h>
#include <stdlib.h>
#include "cmsis_os.h"
int readLLADS8320(SPI_TypeDef * spi, GPIO_TypeDef  *nssPort, uint16_t nssPin);
int readLLLTC2452(SPI_TypeDef * spi, GPIO_TypeDef  *nssPort, uint16_t nssPin);
int readADC_NONE_LL(SPI_TypeDef * spi, GPIO_TypeDef  *nssPort, uint16_t nssPin);

int readHALADS8320(SPI_HandleTypeDef * spi, GPIO_TypeDef  *nssPort, uint16_t nssPin);
int readHALLTC2452(SPI_HandleTypeDef * spi, GPIO_TypeDef  *nssPort, uint16_t nssPin);
int readADC_NONE_HAL(SPI_HandleTypeDef * spi, GPIO_TypeDef  *nssPort, uint16_t nssPin);


pReadAdcLLFunc readLLADC[maximumNum] = {
  readADC_NONE_LL,
  readLLADS8320,
  readLLLTC2452,
  NULL
};
pReadAdcHALFunc readHALADC[maximumNum] = {
  readADC_NONE_HAL,
  readHALADS8320,
  readHALLTC2452,
  NULL
};
  

int readADC_NONE_LL(SPI_TypeDef * spi, GPIO_TypeDef  *nssPort, uint16_t nssPin)
{
  return (int)(rand() % 65535);
}

int readADC_NONE_HAL(SPI_HandleTypeDef * spi, GPIO_TypeDef  *nssPort, uint16_t nssPin)
{
  return (int)(rand() % 65535);
}

/**
  * @brief  Low level function read uint16_t value from adc by spi
  * @param  spi : SPI_TypeDef* - SPI1 for example
  * @param  nssPort: GPIO_TypeDef* - chip select nssPin
  * @note   Example: return 0 -> -20mA; 65535 -> 20ma
  * @retval uint32_t value result from adc
  */
int readLLADS8320(SPI_TypeDef * spi, GPIO_TypeDef  *nssPort, uint16_t nssPin)
{
   uint32_t result = 0;
/*   time_t  stopClock = 1000 + clock();
   uint8_t rx_buf[3] = { 0, 0, 0 };
   
   LL_GPIO_ResetOutputPin(nssPort, nssPin);
   for(uint8_t i = 0;i < 3;i++)
   {
      
      while(!LL_SPI_IsActiveFlag_TXE(spi)) 
      {
        if(clock() > stopClock)
          return 0;
      }
      LL_SPI_TransmitData8 (spi, 0x55);
      while(!LL_SPI_IsActiveFlag_RXNE(spi)) 
      {
        if(clock() > stopClock)
          return 0;
      }
      rx_buf[i] = LL_SPI_ReceiveData8(spi);
   }
   LL_GPIO_SetOutputPin(nssPort, nssPin);

   result =  (rx_buf[0] & 0x03) << 14;
   result += rx_buf[1] << 6;
   result += rx_buf[2] >> 2;*/
   return result;
}
int readHALADS8320(SPI_HandleTypeDef * spi, GPIO_TypeDef  *nssPort, uint16_t nssPin)
{
  return 0;
}

int readLLLTC2452(SPI_TypeDef * spi, GPIO_TypeDef  *nssPort, uint16_t nssPin)
{
  return 0;
}

int readHALLTC2452(SPI_HandleTypeDef * spi, GPIO_TypeDef  *nssPort, uint16_t nssPin)
{
  
   int result = 0;
  
   HAL_GPIO_WritePin(nssPort ,nssPin,GPIO_PIN_RESET);
   osDelay(10);
   HAL_SPI_TransmitReceive(spi, (uint8_t*)&result,(uint8_t*)&result,sizeof(int),1000);
   osDelay(10);
   HAL_GPIO_WritePin(nssPort,nssPin,GPIO_PIN_SET);   
   
   return result;
}

