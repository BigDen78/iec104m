#include <SettingsDriver.hpp>
#include <DriverBuilder.hpp>
#include <main.h>
#include <nwUtils.hpp>
#include <fstream>
#include <sstream>
#include <algorithm>

#define CTX_CLASS_RT_PROPERTY  "RuntimeProperty"
#define CTX_CLASS_DRV_PROPERTY "DriverProperty"
#define CTX_CLASS_MAP_PROPERTY "MappingProperty"
#define CTX_CLASS_GLOB_VAR     "GlobalVariable"
#define CTX_CLASS_SYS_VAR      "SystemVariable"

REGISTER_FACTORY_METHOD(DriverBuilder::driverFactoryTable, Driver, SettingsDriver, new SettingsDriver);

enum ParamIDs : uint32_t
{
	pidSettingsPath,
	pidWriteTimeout,
	pidResetTimeout
};

const ParameterVector SettingsDriver::parameters{
	{"file", {pidSettingsPath, dtString, StaticParameterInfo::pfRequired}},
	{"writeTimeout", {pidWriteTimeout, dtDWord, StaticParameterInfo::pfNone}},
	{"resetTimeout", {pidResetTimeout, dtDWord, StaticParameterInfo::pfNone}}};

const ParameterVector SettingsDriverContext::parameters{
	{"class", {stpidClassId, dtString, StaticParameterInfo::pfRequired}},
	{"driver", {stpidDriver, dtString, StaticParameterInfo::pfNone}},
	{"tag", {stpidTag, dtString, StaticParameterInfo::pfNone}},
	{"property", {stpidProperty, dtString, StaticParameterInfo::pfNone}}};

enum GlobalSystemPropertyId
{
	gspidCPULoad,
	gspidHeapMemoryAvail,
  gspidMAC2_1,
  gspidMAC4_3,
  gspidMAC6_5
};

const ParameterVector globalSystemProperties
{
	{"CPULoad", {gspidCPULoad, dtWord, StaticParameterInfo::pfNone}},
	{"HeapMemoryAvail", {gspidHeapMemoryAvail, dtFloat, StaticParameterInfo::pfNone}},
  {"MAC2_1",{gspidMAC2_1, dtWord, StaticParameterInfo::pfNone}},
  {"MAC4_3",{gspidMAC4_3, dtWord, StaticParameterInfo::pfNone}},
  {"MAC6_5",{gspidMAC6_5, dtWord, StaticParameterInfo::pfNone}}
};

typedef VarData (*GSPReadFn)();
extern "C" __weak unsigned int xLoadPercentCPU(void)
{
	return 0;
}

extern "C" unsigned int xLoadPercentCPU(void);
VarData readCPULoad()
{
	VarData result;
	result.dwordVal = xLoadPercentCPU();
	return result;
}

VarData readHeapMemoryAvail()
{
	VarData result;
	result.fltVal = (float)xPortGetFreeHeapSize();
	return result;
}

extern "C" __weak uint16_t getMAC2_1(void){	return 0;      }
extern "C" __weak uint16_t getMAC4_3(void){	return 0;      }
extern "C" __weak uint16_t getMAC6_5(void){	return 0x0100; }

extern "C" uint16_t getMAC2_1(void);
extern "C" uint16_t getMAC4_3(void);
extern "C" uint16_t getMAC6_5(void);

VarData readMAC2_1()
{
	VarData result;
	result.wordVal = getMAC2_1();
	return result;
}

VarData readMAC4_3()
{
	VarData result;
	result.wordVal = getMAC4_3();
	return result;
}

VarData readMAC6_5()
{
	VarData result;
	result.wordVal = getMAC6_5();
	return result;
}

GSPReadFn gspReaders[] = {readCPULoad, readHeapMemoryAvail,
                          readMAC2_1, readMAC4_3, readMAC6_5};

SettingsDriver::SettingsDriver() : pendingReset(false),
					runState(rsInitializing),
					pendingWrite(false), ParameterizedObject(parameters)
{
}

void SettingsDriver::registerTag(const Tag &tag, void *ctx)
{
	if (smcSystemVariable == reinterpret_cast<SettingsDriverContext *>(ctx)->myClass)
	{
		sysTags.push_front(const_cast<Tag*>(&tag));
	}
}

bool SettingsDriver::tagChanged(const Tag &tag, uint32_t, VarData newValue)
{
	auto context = reinterpret_cast<SettingsDriverContext *>(tag.getDriverSpecificInfo(id));
	if (context)
	{
		switch (context->myClass)
		{
		case smcDriverProperty:
			context->driver->postSetRuntimeParameter(context->paramId, newValue);
			pendingReset |= (context->driver->getParameters()[context->paramId].second.flags & StaticParameterInfo::pfRebootRequired);
			break;
		case smcMappingProperty:
			context->driver->postSetTagRuntimeParameter(context->refTag, context->paramId, newValue);
			auto tagContext = context->refTag->getDriverSpecificInfo(context->driver->getId());
			pendingReset |= (tagContext->getParameters()[context->paramId].second.flags & StaticParameterInfo::pfRebootRequired);
			break;
		}
		pendingWrite = true;
	}
	return true;
}

bool SettingsDriver::setParameterById(uint32_t paramId, VarData newValue)
{
	switch (static_cast<ParamIDs>(paramId))
	{
	case pidSettingsPath:
		configPath.assign(newValue.pStr);
		return true;
	case pidWriteTimeout:
		writeTimeout = newValue.dwordVal;
		return true;
	case pidResetTimeout:
		resetTimeout = newValue.dwordVal;
		return true;
	}
	return false;
}

bool SettingsDriver::setRuntimeParameter(const Tag &tag, uint32_t paramId, VarData paramValue)
{
	return false;
}

DriverContext *SettingsDriver::createTagContext() const
{
	return new SettingsDriverContext;
}

bool SettingsDriverContext::setParameterById(uint32_t paramId, VarData newValue)
{
	auto tagParamId = static_cast<SettingsTagParamIDs>(paramId);
	switch (tagParamId)
	{
	case stpidClassId:
	{
		std::map<String, SettingsMappingClass> tmpMap{
			{CTX_CLASS_RT_PROPERTY, smcRuntimeProperty},
			{CTX_CLASS_DRV_PROPERTY, smcDriverProperty},
			{CTX_CLASS_MAP_PROPERTY, smcMappingProperty},
			{CTX_CLASS_GLOB_VAR, smcGlobalVariable},
			{CTX_CLASS_SYS_VAR, smcSystemVariable}};

		std::map<String, SettingsMappingClass>::const_iterator it;
		if ((it = tmpMap.find(newValue.pStr)) != tmpMap.end())
		{
			myClass = it->second;
			return true;
		}
	}
	break;
	case stpidDriver:
	{
		auto driver = Core::instance().getDriver(newValue.pStr);
		if (driver)
		{
			this->driver = driver;
			return true;
		}
	}
	break;
	case stpidTag:
	{
		auto tagNames = Core::instance().getTagNames();
		auto it = tagNames.find(newValue.pStr);
		if (it != tagNames.end())
		{
			refTag = it->second;
			return true;
		}
	}
	break;
	case stpidProperty:
	{
		switch (myClass)
		{
		case smcDriverProperty:
			if (!driver)
				return false;
			{
				auto drvParameter = driver->getParameterInfo(newValue.pStr);
				if (drvParameter)
				{
					this->paramId = drvParameter->id;
					return true;
				}
			}
			break;
		case smcMappingProperty:
			if (!driver || !refTag)
				return false;
			{
				auto drvTagParameter = refTag->getDriverSpecificInfo(driver->getId())->getParameterInfo(newValue.pStr); //driver->getTagParameterInfo(newValue.pStr);
				if (drvTagParameter)
				{
					this->paramId = drvTagParameter->id;
					return true;
				}
			}
			break;
		case smcSystemVariable:
			{
				auto findResult = std::find_if(globalSystemProperties.begin(), globalSystemProperties.end(),
																			 [&newValue](auto item) -> bool { return !item.first.compare(newValue.pStr);});
				if (findResult != globalSystemProperties.end())
				{
					this->paramId = findResult->second.id;
					return true;
				}
			}
			break;
		default:
			return false;
		}
	}
	break;
	}
	return false;
}

bool SettingsDriver::setTagRuntimeParameter(const Tag &tag, uint32_t paramId, VarData paramValue)
{
	return false;
}

void SettingsDriver::initialize()
{
	if (configPath.size())
	{
		VarData data;
		Json::const_iterator jsonIter;
		std::ifstream config(configPath);
		Json *json = nullptr;
		nlohmann::detail::parser<Json> *parser = nullptr;
		//config >> json;
		if (config)
		{
			json = new Json;
			parser = new nlohmann::detail::parser<Json>(nlohmann::detail::input_adapter(config), nullptr, false);
			parser->parse(false, *json);
		}

		if (tagsList)
		{
			for (auto tagIter : *tagsList)
			{
				auto context = reinterpret_cast<SettingsDriverContext *>(tagIter->getDriverSpecificInfo(id));
				if (json && ((jsonIter = json->find(tagIter->getName())) != json->end()))
				{
					data = getDataFromJson(tagIter->getType(), *jsonIter);
					Core::instance().setTag(tagIter, data, true, id);
					switch (context->myClass)
					{
					case smcDriverProperty:
						context->driver->setRuntimeParameter(Tag::NoTag, context->paramId, data);
						break;
					case smcMappingProperty:
						context->driver->setTagRuntimeParameter(*context->refTag, context->paramId, data);
						break;
					}
				}
				else
				{
					bool ok = false;
					switch (context->myClass)
					{
					case smcDriverProperty:
						data = context->driver->getRuntimeParameter(context->paramId);
						ok = true;
						break;
					case smcMappingProperty:
						data = context->driver->getTagRuntimeParameter(*context->refTag, context->paramId);
						ok = true;
						break;
					}
					if (ok)
					{
						Core::instance().setTag(tagIter, data, true, id);
					}
				}
			}
		}
	}
	Driver::initialize();
	commandQueue->getTimer().setPeriod(1000);
	commandQueue->getTimer().rearm();
	runState = rsRunning;
}

VarData SettingsDriver::getRuntimeParameter(uint32_t paramId)
{
	VarData result;
	result.dwordVal = 0;
	return result;
}

VarData SettingsDriver::getTagRuntimeParameter(const Tag &tag, uint32_t paramId)
{
	VarData result;
	result.dwordVal = 0;
	return result;
}

void SettingsDriver::run()
{
	switch (runState)
	{
	case rsInitializing:
		commandQueue->getTimer().setPeriod(1000);
		commandQueue->getTimer().rearm();
		break;
	case rsRunning:
		if (pendingWrite)
		{
			runState = rsPendingWrite;
			commandQueue->getTimer().setPeriod(writeTimeout);
			commandQueue->getTimer().rearm();
		}
		else
		{
		   std::for_each(sysTags.begin(), sysTags.end(), [this](Tag *tag) {
		      auto pId = reinterpret_cast<SettingsDriverContext *>(tag->getDriverSpecificInfo(id))->paramId;
		      Core::instance().setTag(tag, gspReaders[pId](), true, id);
		   });
			commandQueue->getTimer().setPeriod(1000);
			commandQueue->getTimer().rearm();
		}
		break;
	case rsPendingWrite:
		pendingWrite = false;
		if (commandQueue->getTimer().ready())
		{
			std::ofstream config(configPath);
			if (config)
			{
				Json json;
				for (auto tagIter : *tagsList)
				{
					if (smcSystemVariable != reinterpret_cast<SettingsDriverContext *>(tagIter->getDriverSpecificInfo(id))->myClass)
						writeTagValueToJson(json, *tagIter);
				}
				config << json;
			}
			if (pendingReset)
			{
				commandQueue->getTimer().setPeriod(resetTimeout);
				commandQueue->getTimer().rearm();
				runState = rsPendingReset;
			}
			else
				runState = rsRunning;
		}
		break;
	case rsPendingReset:
		if (pendingWrite)
			runState = rsPendingWrite;
		else if (commandQueue->getTimer().ready())
		{
			NVIC_SystemReset();
		}
		break;
	}
}
