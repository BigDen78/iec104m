#include "DriverBuilder.hpp"
#include "SNMPsDriver.hpp"
  
#ifdef USE_IN_NVCORE_RUNTIME
REGISTER_FACTORY_METHOD(DriverBuilder::driverFactoryTable, Driver, SNMPsDriver, new SNMPsDriver);
#endif

//-------------------------------------------------------------------------------------------------------------
#define LINK_SUCCESS true
#define LINK_FAULT   false

#define FOUND 	  true
#define NOT_FOUND false

#define NOT_CREATED 0
#define CREATED     1

#define CURRENT_INSTANCE 0
#define NEXT_INSTANCE    777

#define EQUAL 0
//-------------------------------------------------------------------------------------------------------------//

//-------------------------------------------------------------------------------------------------------------
// Инициализация статических параметров класса ---------------------------------------------------------------//
struct snmp_mib**   SNMPsDriver::lambdaSNMPmibs = { NULL };  
u32_t               SNMPsDriver::amountOfRegisteredMIBS = 0;
DataBaseType        SNMPsDriver::nodesDataBase = {{},{}};
snmp_mibPtr	    SNMPsDriver::enterpriseMIBRoot = { NULL };
struct snmp_obj_id* SNMPsDriver::enterpriseID;
        
#ifdef SNMP_TEST            
size_t SNMPsDriver::memoryAvaliable;
#endif 
void (*EndNodeValueType::writeCallback)(EndNodeValueType* endNodeValuePtr, VarData outerWorldVar) = NULL;

		 
#ifdef USE_IN_NVCORE_RUNTIME
const ParameterVector SNMPsDriver :: parameters
{
   { "communityString_read",   {communityString_read,  dtString, StaticParameterInfo::pfNone	 } },
   { "communityString_write",  {communityString_write, dtString, StaticParameterInfo::pfNone	 } },
   { "communityString_trap",   {communityString_trap,  dtString, StaticParameterInfo::pfNone	 } },
   { "enterpriseOID", 	       {enterpriseOID_param,   dtString, StaticParameterInfo::pfRequired } }  
};    
const ParameterVector SNMPDriverTagContext :: snmpParameters
{
   {"OID", 		    { snmpOID,			dtString, StaticParameterInfo::pfRequired }},
   {"DataTypeDetalization", { snmpDataTypeDetalization, dtString, StaticParameterInfo::pfRequired }},
   {"TrapOnChange", 	    { snmpTrapOnChange,		dtString, StaticParameterInfo::pfNone     }},
   {"Access", 	       	    { snmpAccessParam,         	dtString, StaticParameterInfo::pfNone	  }}
};

struct ToCoreMessageType
{
   ToCoreMessageType() = delete;
   ToCoreMessageType(SNMPDriverTagContext* context, u32_t param_id)
   {
      contextPtr = context;
      snmpDriverId = param_id;
   }
   SNMPDriverTagContext* contextPtr = NULL;
   u32_t snmpDriverId = 0;
};

static void writeValueCallback(EndNodeValueType* endNodeValuePtr, VarData outerWorldVar)
{
   ToCoreMessageType* messagePtr = reinterpret_cast <ToCoreMessageType *> (endNodeValuePtr->userDataReference);
   Tag* tagPtr = messagePtr->contextPtr->tagPtr;

   Core::set(*tagPtr, outerWorldVar, 1, messagePtr->snmpDriverId);
};
#endif // USE_IN_NVCORE_RUNTIME

// Функции служебные ------------------------------------------------------------------------------------------
#ifdef SNMP_TEST
void stub(void);
#endif 

char* replaceStringContent(char* old_destination, char* source); 
OIDString intObjIDtoString(const u32_t* const id, u32_t length);

// Методы класса OIDInfo ------------------------------------------------------------------------------------------------------------//
/**
 * @brief OIDInfo::OIDInfo - конструктор, сходу требует OID в виде строки, проверяет его на ошибки и производит анализ свойств
 * @param oidString
 */
OIDInfo::OIDInfo( const OIDString & oidString, IDtype newNodeType )
{
   u32_t length = 0;
   u32_t oidStringLength = oidString.length();
   
   if(oidString[oidStringLength - 1] == '.')
   {
      actuality = INVALID;
      return;
   }
   
   // Проверяем на наличие левых символов //
   for(uint32_t i = 0; i < oidStringLength; i++)
   {
      // OID не может содержать ничего кроме цифр и точек //
      if((isdigit(oidString[i]) != true))
      {
	 if(oidString[i] != '.')
	 {
	    actuality = INVALID;
	    return;
	 }
	 length++; // Количество точек - длина OID-а
      }
   }          
   
   // проверку на косячность строки тут сделай //
   uint32_t currentPosition;
   OIDString currentStringOID;    	    
   OIDString restStringOID = oidString;
   
   // Строка кривая - дальше нече делать даже //
   if(actuality == VALID)
   {              
      objID.len = 0;
      
      for(currentPosition = restStringOID.find(".", 1); currentPosition != string::npos; objID.len++)
      {
	 currentStringOID = restStringOID.substr( 1, currentPosition - 1);     	 
	 objID.id[objID.len] = stoi(currentStringOID);
	 
	 restStringOID = restStringOID.substr( currentPosition );
	 currentPosition = restStringOID.find(".", 1);
      }
      
      currentStringOID = restStringOID.substr(1);
      objID.id[objID.len] = stoi(currentStringOID);
      
      numOfNodes = ++objID.len;
   } 
   
   if(newNodeType == AUTO_TYPE)
   {
      nodeType = getOIDType(objID); 
      
      if((nodeType != TABLE) && (nodeType != SCALAR))
      {
	 actuality = INVALID;
	 return;
      }
   } else {
      actuality = checkOIDconformity(objID, newNodeType);  
      if(actuality == VALID)
      {
	 nodeType = newNodeType; 
      }
   }     
}
	
IDtype OIDInfo::getOIDType(const snmp_obj_id & oidVector)
{ 
   u32_t length = oidVector.len;
   
   if( (length >= 2) && (oidVector.id[length - 1] != 0) && (oidVector.id[length - 2] == 1) )
   {      
      nodePosition = length - 3;
      return TABLE;
   }
   
   if((length >= 1) && (oidVector.id[length - 1] == 0))
   {       
      nodePosition = length - 2;
      return SCALAR; 
   }
   
   return WRONG_TYPE;
}

ActualityType OIDInfo::checkOIDconformity(const snmp_obj_id & oidVector, IDtype nodeType)
{       
   u32_t length = oidVector.len;
   
   if(nodeType != BASE)
   {
      // Проверка на соответствие заявленному типу //
      if ( (nodeType == TABLE) && ((oidVector.id[length - 1] == 0) || (oidVector.id[length - 2] != 1)) )
      {
	  return INVALID;      
      }
      else if ( (nodeType == SCALAR) && (oidVector.id[length - 1] != 0) )
      {
	   return INVALID; 
      }   
   }
   
   return VALID;
}

// Методы EndNodeValueType ---------------------------------------------------------------------------------------------------------------//
void EndNodeValueType::setWriteCompleteCallback(void (*newCallback)(EndNodeValueType* endNodeValuePtr, VarData outerWorldVar))
{
   writeCallback = newCallback;
}

void EndNodeValueType::setRefToUserData(EndNodeValueType* endNodeValuePtr, void* userDataPtr) 
{   
   if(userDataPtr)
   {
      endNodeValuePtr->userDataReference = userDataPtr; 
   }
}

// Структура TrapInfo -------------------------------------------------------------------------------------------------------------------//
TrapInfo::TrapInfo(u32_t* id, u32_t len)
{
   trapEnability = ENABLED; 
   if(id && len)
   {
      snmp_obj_id_const_ref* newObjPtr = (snmp_obj_id_const_ref *)Lambda_Realloc(objID->id, 0, len);
      if(newObjPtr)
      {
	 objID = newObjPtr;
	 
	 uint32_t* newId = (uint32_t *)Lambda_Malloc(sizeof(uint32_t *) * len);
	 if(newId)
	 {        	    
	    Lambda_Memcpy((void *)newId, id, len * sizeof(uint32_t)); 
	   
	    objID->id = newId;	           
	    objID->len = len;
	 }
      }
   }
}

TrapInfo::~TrapInfo()
{
   Lambda_Free((void *)objID->id);
   Lambda_Free(objID);
}

// Методы класса SNMPsDriver ------------------------------------------------------------------------------------------------------------//
/**
  * Конструктор: создаем вектора с параметрами драйвера и параметрами тегов
  *
  * Примечание: Для пользователя предназначена ветвь "enterprise" с OID ".1.3.6.1.4.1"
  *		Само ядро SNMP в LWIP не даст создать
  */
SNMPsDriver::SNMPsDriver() : ParameterizedObject(parameters)
{
    // Пока пусто //
}

SNMPsDriver::~SNMPsDriver()
{    
    // Пока пусто //
}
/**
  * Замена строки на новую (для параметров)
  *
  */
char* replaceStringContent(char* old_destination, char* source)
{
   char* new_destination;

   if (old_destination != NULL)
   {
      Lambda_Free(old_destination);
   }
   uint32_t newLen =  strlen(source) + 1;
   
   new_destination = (char *)Lambda_Malloc(newLen * sizeof(char));	
   strcpy( new_destination, source );

   return new_destination;
}

OIDString intObjIDtoString(const u32_t* const id, u32_t length)
{
   OIDString retString;
   
   if(id && (length > 0))
   {
      for(u32_t i = 0; i < length; i++)
      {
	 retString.append('.' + to_string(id[i])); 
      }
   }
   
   return retString; 
}


#ifdef USE_IN_NVCORE_RUNTIME
/**
  * Параметры драйвера, считываемые со Стетхема
  *
  */
bool SNMPsDriver::setParameterById(uint32_t paramId, VarData newValue)
{
   snmpParamIDs currentParam = static_cast <snmpParamIDs> (paramId);

   //-------------------------------------------------------------------------//
   //                       	      ПАРОЛИ                                  //
   //-------------------------------------------------------------------------//
   if( currentParam == communityString_read )      // Пароль на чтение //
   {
      char* current_community = (char *)snmp_get_community();

      if ( strcmp(current_community, newValue.pStr) != NULL )
      {
         snmpCommunity.communityRead = replaceStringContent(snmpCommunity.communityRead, newValue.pStr);
         snmp_set_community(snmpCommunity.communityRead);
      }
      return true;
   }
   if( currentParam == communityString_write )    // Пароль на запись //
   {
      if ( strcmp((char *)snmp_get_community_write(), newValue.pStr ) != NULL )
      {
         snmpCommunity.communityWrite = replaceStringContent(snmpCommunity.communityWrite, newValue.pStr);
         snmp_set_community_write(snmpCommunity.communityWrite);
      }
      return true;
   }
   if( currentParam == communityString_trap )    // Пароль для трэпов (типа ловушка) //
   {
      if ( strcmp((char *)snmp_get_community_trap(), newValue.pStr ) != NULL )
      {
         snmpCommunity.communityTrap = replaceStringContent(snmpCommunity.communityTrap, newValue.pStr);
         snmp_set_community_trap(snmpCommunity.communityTrap);
      }
      return true;
   }
   if ( currentParam == enterpriseOID_param )
   {
      OIDInfo enterpriseOID(newValue.pStr, BASE);
      
      if(enterpriseOID.actuality == VALID)
      {
	 setEnterpriseOID(intObjIDtoString(enterpriseOID.objID.id, enterpriseOID.objID.len));
	 return true;
      }
   }
   return false;
}

bool SNMPsDriver::setRuntimeParameter(const Tag &tag, uint32_t paramId, VarData paramValue)
{
   return false;
}

bool SNMPsDriver::setTagParameter(const Tag &tag, uint32_t paramId, VarData newValue)
{
   return true;
}

bool SNMPsDriver::setTagRuntimeParameter(const Tag &tag, uint32_t paramId, VarData paramValue)
{
   return false;
}

VarData SNMPsDriver::getTagRuntimeParameter(const Tag &tag, uint32_t paramId)
{
   VarData result = {0};
   return  result;
}

VarData SNMPsDriver::getRuntimeParameter(uint32_t paramId)
{
   VarData result = {0};
   return  result;
}

/**
  * Установка параметров тега
  *
  */
bool SNMPDriverTagContext::setParameterById(uint32_t paramId, VarData newValue)
{
   switch (static_cast <snmpNodeParamIDs> (paramId))
   {
     case snmpOID:
      {
         economyStruct.tempInitInfoStruct->oidString = newValue.pStr;
	 return true;
      }
     case snmpDataTypeDetalization:
      {
         string newValueString = newValue.pStr;
         extraType newDataType;

         if(newValueString == "integer")
            newDataType = END_NODE_TYPE_INTEGER;
         else if(newValueString == "string")
            newDataType = END_NODE_TYPE_OCTET_STRING;
         else if(newValueString == "float")
            newDataType = END_NODE_TYPE_FLOAT;
//         else if( )
//         {
//              // по мере надобности добавлять остальные типы
//         }
         else break;

         economyStruct.tempInitInfoStruct->nodeDataType = newDataType;
         return true;
      }
     case snmpTrapOnChange:
      {
	 string newValueString = newValue.pStr;
	 
	 if((newValueString == "ON") || (newValueString == "on"))
	 {
	    economyStruct.tempInitInfoStruct->trapEnability = ENABLED;
	    return true;
	 } 
	 else  if((newValueString == "OFF") || (newValueString == "off"))
	 {
	    economyStruct.tempInitInfoStruct->trapEnability = DISABLED;
	    return true;
	 }  
      }
     case snmpAccessParam:
      {
	 snmp_access_t newAccess;
	 string newAccessString(newValue.pStr);
	 
	 if(newAccessString == "WriteOnly")  
	    newAccess = SNMP_NODE_INSTANCE_WRITE_ONLY; 	
	 else if (newAccessString == "ReadOnly") 
	    newAccess = SNMP_NODE_INSTANCE_READ_ONLY; 
	 else if (newAccessString == "NotAccessible") 
	    newAccess = SNMP_NODE_INSTANCE_NOT_ACCESSIBLE; 	 
	 else if ((newAccessString == "ReadWrite" && newAccessString == "WriteRead")) 
	    newAccess = SNMP_NODE_INSTANCE_READ_WRITE; 
	 else break;
	 
	 economyStruct.tempInitInfoStruct->access = newAccess;
	 return true;
      }
   }
   return false;
}

/**
 * @brief SNMPsDriver::createTagContext - создание контекста тэга
 * @return
 */
DriverContext* SNMPsDriver::createTagContext() const
{
   auto contextInstance = new SNMPDriverTagContext;
   
   if(contextInstance != NULL)
   {
      // Временная структура для инициализации //
      if((contextInstance->economyStruct.tempInitInfoStruct = new SNMP_InitInfoStruct) != NULL)
         return contextInstance;
   }
   return NULL;
}

/**
  *  Регистрация тега в базе данных о тегах SNMP
  *
  */
void SNMPsDriver::registerTag(const Tag &tag, void *context)
{
   SNMPDriverTagContext* addedContextPtr = reinterpret_cast <SNMPDriverTagContext *> (context);
	
   // Создаем лист зарегистрированных тегов SNMP //         
   oidsTemporaryArray.push_front(addedContextPtr);
   addedContextPtr->economyStruct.tempInitInfoStruct->tagPtr = const_cast <Tag *> (&tag);
}

/**
  * Необходимые при изменении тэга действия
  *
  */
bool SNMPsDriver::tagChanged(const Tag &tag, uint32_t paramId, VarData newValue)
{
   SNMPDriverTagContext *entry = reinterpret_cast<SNMPDriverTagContext *>(tag.getDriverSpecificInfo(getId()));
   extraType snmpDataType = associateToLambdaSNMPtype(tag.getType());
   
    // сЮДЫ НУЖНА ПРОВЕРКА соответствия типов ??-----------------------------------------------
   
   if (updateEndNoteValue(entry->economyStruct.endNodeValuePtr, newValue) == SNMP_ERR_NOERROR)        
   {
      if(entry->economyStruct.endNodeValuePtr->trapInfoPtr != NULL)    
      {
         if( entry->economyStruct.endNodeValuePtr->trapInfoPtr->trapEnability == ENABLED)
            sendTrap(entry->economyStruct.endNodeValuePtr);
      }
      return true;
   }
   return false;
}

/**
  * Инициализация драйвера SNMP, создание дерева
  *
  */
void SNMPsDriver::initialize()
{
    Driver::initialize();

#ifdef LAMBDA_MIB2 
   createMIB2();
#endif
   
#ifdef SNMP_TEST
   createTestMIB();
#endif
   
   // СОЗДАНИЕ ДЕРЕВА по тегам из root.json ----------------------------------------------------------------------------------------------
   initializeJSONtags();
   
   // Настраиваем ловушки ))--------------------------------------------------------------------------------------------------------------
   enableTraps();   
   setEnterpriseOID( enterpriseID->id, enterpriseID->len );
   addTrapSubscriberIP("192.168.137.4");
   
   //-------------------------------------------------------------------------------------------------------------------------------------
   snmp_init();
}
	          
/**
  * Основной алгоритм программы драйвера
  *
  */
void SNMPsDriver::run()
{   
#ifdef SNMP_TEST
    snmpDriverExampleLoop();
#endif 
   
    //-------------------------------------------------------------------------------------------------------------------------------------
    
    //-------------------------------------------------------------------------------------------------------------------------------------
}

#endif // USE_IN_NVCORE_RUNTIME

// Функции класса ------------------------------------------------------------------------------------------------------------------------//
//----------------------------------------------------------------------------------------------------------------------------------------//
//                                                             USER API                                                                   //
//----------------------------------------------------------------------------------------------------------------------------------------//

//----------------------------------------------------------------------------------------------------------------------------------------//
//					                 [ Создание собственной MIB ]		   					  //
//----------------------------------------------------------------------------------------------------------------------------------------//
/**
  * Описание: Создание корня дерева пользовательской MIB-библиотеки
  *
  * Принимаемое значение: Сериализованное значение OID корня
  *
  * ПРИМЕЧАНИЕ: OID узла firstNode не важен, потом объясню че за
  *             узел должен начинаться с ".1.3.6"??
*/
snmp_mibPtr SNMPsDriver::createRootOfPrivateMIB(OIDString oidString)
{
   OIDInfo mibRootOID(oidString, BASE);
   if(mibRootOID.actuality == VALID)
   {
#ifdef LAMBDA_MIB2 
      if(snmp_oid_compare(mibRootOID.objID.id, mibRootOID.objID.len, mib2.base_oid, mib2.base_oid_len) == EQUAL)
	 return NULL;
#endif
      
      u32_t mibRootLastOID = mibRootOID.objID.id[mibRootOID.numOfNodes - 1];
      snmp_treeNodePtr rootTreeNode = createTreeNode(mibRootLastOID);    
      
      if(rootTreeNode)
	 return createNewMIB( mibRootOID, &rootTreeNode->node );
   }
   
   return NULL;
}

/**
 * @brief Подключение MIB к списку подключенных библиотек
 *        Должна употребляться перед вызовом "snmp_set_mibs" !
 * @param mib - указатель на добавляему MIB *
 */
bool SNMPsDriver::includeMIB(snmp_mibPtr addedMIB)
{ 		
   for(u32_t i = 0; i < amountOfRegisteredMIBS; i++)
   {
      if(lambdaSNMPmibs[i] == addedMIB)
	 return false;
   }
   
   snmp_mibPtr* newMem = (snmp_mibPtr*)Lambda_Realloc(lambdaSNMPmibs, amountOfRegisteredMIBS, amountOfRegisteredMIBS + 1);
   
   if(newMem != NULL)
   {                   
      lambdaSNMPmibs = newMem;
      
      lambdaSNMPmibs[amountOfRegisteredMIBS] = addedMIB; 
      amountOfRegisteredMIBS++;
      
      snmp_set_mibs( (snmp_mib const**)lambdaSNMPmibs, amountOfRegisteredMIBS );    
      return true;
   }
   
   return false;
}          
//----------------------------------------------------------------------------------------------------------------------------------------//
//                                                          [ Добавление узла ]                                                    	  //
//----------------------------------------------------------------------------------------------------------------------------------------//
/**
 * @brief addNewNodeToMIB - добавление узла по одному только строковому значению OID, база, которой соответствует новый узел, будет
 *                          найдена автоматически
 * @param oid_string - значение OID (включая базу, в которую он включен)
 * @param valueType
 * @param startValue
 * @return
 */
EndNodeValueType* SNMPsDriver::addNewNode(OIDString oid_string, extraType valueType, VarData startValue /* указатель на указатель */)
{
   EndNodeValueType* newNodeValuePtr = NULL;
   for(u32_t i = 0; i < amountOfRegisteredMIBS; i++)
   {
      // Функция пока в проекте
      // подразумевается: заносит каждый новый узел в инициализационный лист, после команды "инициализация" вычисляются оптимальные базы и проводится создание дерева
   }
   return newNodeValuePtr; 
}
/**
 * @brief SNMPsDriver::addNewNodeToMIB - добавление узла, итоговый OID которого является суммой OID базы и oidString
 *        Сама определяет тип узла (табличный / скаляр), линкует его к указанной MIB и заносит в базу данных
 * @param mibBaseRoot - указатель на базу
 * @param oidString - OID добавляемого узла (без OID-а MIB-базы, к которой добавляется)
 * @param asn1_type - тип значения узла
 * @return - указатель на ячейку со значением узла
 */
EndNodeValueType* SNMPsDriver::addNewNodeToMIB(snmp_mibPtr mibBaseRoot, OIDString oidString, extraType valueType, snmp_access_t access)
{   
#ifdef LAMBDA_MIB2 
   if( mibBaseRoot == &mib2)
   {
      // Стандартные библиотеки вне моей юрисдикции //
      return NULL;        
   }
#endif
   
   EndNodeValueType* endValue = NULL;
   IDtype nodeType;
   u16_t asn1_type;
   OIDInfo oidWithoutBase(oidString, AUTO_TYPE);
   
   if(oidWithoutBase.actuality == VALID)
   {
      nodeType = oidWithoutBase.nodeType;           
      
      if( (asn1_type = associateToASN1type(valueType)) != END_NODE_INVALID_TYPE )
      {
	 if(nodeType == SCALAR) { endValue = addNewScalarNodeToMIB( mibBaseRoot, oidWithoutBase, asn1_type, access ); }
	 if(nodeType == TABLE)  { endValue =  addNewTableNodeToMIB( mibBaseRoot, oidWithoutBase, asn1_type, access ); }
	 
	 // Присваиваем начальное значение, если узел успешно создан //
	 if( endValue != NULL )
	 {
	    VarData startValue = { .intVal = 0 };  	    
	    endValue->exType = valueType;  
	    updateEndNoteValue(endValue, startValue);
	 }
      }        
   }
   
   return endValue;
}
/**
 * @brief SNMPsDriver::addNewNodeToMIB - то же самое, что функция выше, но с присвоением начального значения
 * @param mibBaseRoot - узел-корень всей MIB, к которой добавляется узел
 * @param oidString
 * @param valueType
 * @param startValue
 * @return
 */
EndNodeValueType* SNMPsDriver::addNewNodeToMIB(snmp_mibPtr mibBaseRoot, OIDString oidString, extraType valueType, snmp_access_t access, VarData startValue)
{
   EndNodeValueType* endValue = addNewNodeToMIB(mibBaseRoot, oidString, valueType, access);

   if(endValue != NULL)
      updateEndNoteValue(endValue, startValue);
   
   return endValue;
}

//----------------------------------------------------------------------------------------------------------------------------------------//
//					               	      [Traps (ловушки)]		   						  //
//----------------------------------------------------------------------------------------------------------------------------------------//
void SNMPsDriver::enableTraps(void)
{
   trapsPermiison = ENABLED;  
}

void SNMPsDriver::disableTraps(void)
{
   trapsPermiison = DISABLED;  
}

EnabilityType SNMPsDriver::getTrapsPermissionState(void)
{
   return trapsPermiison;
}

void SNMPsDriver::setEnterpriseOID(const OIDString & oidString)
{
   OIDInfo enterpriseOID(oidString, BASE);
   
   if(enterpriseOID.actuality == VALID)
   {
      enterpriseID = new snmp_obj_id(enterpriseOID.objID);
      snmp_set_device_enterprise_oid(enterpriseID);			     
   }
} 

void SNMPsDriver::setEnterpriseOID(const u32_t* const id, u32_t length)
{
   setEnterpriseOID(intObjIDtoString(id, length)); 
} 

void SNMPsDriver::addTrapSubscriberIP(OIDString subscriberIP)
{
   OIDInfo subIP('.' + subscriberIP, BASE);   // КТО СКАЗАЛ, ЧТО МИКРОСКОП - ПЛОХОЙ МОЛОТОК?)))
   
   if ((subIP.actuality == VALID) && (subIP.objID.len == 4))
   {
      ip4_addr_t ipaddr;
      IP4_ADDR(&ipaddr, subIP.objID.id[0], subIP.objID.id[1], subIP.objID.id[2], subIP.objID.id[3]);
	
      snmp_trap_dst_ip_set(amountOfTrapSubscribers, &ipaddr); 
      snmp_trap_dst_enable(amountOfTrapSubscribers++, 1);       
   }
}

void SNMPsDriver::enableTrap(OIDString oidString)
{                       
   EndNodeValueType* nodeValue = findEndValuePointer(oidString);
   
   if(nodeValue)
   {
      OIDInfo oid(oidString, AUTO_TYPE);          
      nodeValue->trapInfoPtr = new TrapInfo(&oid.objID.id[0], oid.objID.len) ;   
      
      if(nodeValue->trapInfoPtr)
      {	   
         if (nodeValue->trapInfoPtr->objID->id && (nodeValue->trapInfoPtr->objID->len != 0))
            nodeValue->trapInfoPtr->trapEnability = ENABLED;
      }        
   }
}

void SNMPsDriver::disableTrap(OIDString oidString)      // Не проверена
{
   EndNodeValueType* nodeValue = findEndValuePointer(oidString);
   
   if(nodeValue && nodeValue->trapInfoPtr)
      delete nodeValue->trapInfoPtr;
}

snmp_err_t SNMPsDriver::sendTrap(EndNodeValueType* nodeValue)
{
    snmp_err_t err_code = SNMP_ERR_GENERROR;

    if(getTrapsPermissionState() == ENABLED)
    {
        if(nodeValue)
        {
            snmp_varbind* varBindPtr = new snmp_varbind();

            varBindPtr->next = NULL;
            varBindPtr->prev = NULL;
            varBindPtr->type = associateToASN1type(nodeValue->exType); // а че если кривой тип?

            Lambda_Memcpy((void *)&varBindPtr->oid.id, (void *)nodeValue->trapInfoPtr->objID->id, nodeValue->trapInfoPtr->objID->len * sizeof(u32_t));
            varBindPtr->oid.len = nodeValue->trapInfoPtr->objID->len;

            char valBuff[SNMP_MAX_VALUE_SIZE];
            varBindPtr->value = &valBuff;

            varBindPtr->value_len = getValueMethod(nodeValue, varBindPtr->value);

            snmp_send_trap_specific(123, varBindPtr);

            delete varBindPtr;  // пока иначе не получилось почему-то
        }
    }
    return err_code;
}    

snmp_err_t SNMPsDriver::sendTrap(const OIDString & oidString)
{
   if(getTrapsPermissionState() == ENABLED)
      return sendTrap( findEndValuePointer(oidString) );
   
   return SNMP_ERR_GENERROR;
}    
//---------------------------------------------------------------------------------------------------------------------

#ifdef USE_IN_NVCORE_RUNTIME
/**
 * @brief SNMPsDriver::initializeJSONtags - создание дерева тегов, найденных в файле
 */
void SNMPsDriver::initializeJSONtags()
{
   // Создание корня деревая пользовательской MIB //
   enterpriseMIBRoot = createRootOfPrivateMIB(intObjIDtoString(enterpriseID->id, enterpriseID->len));
   includeMIB( enterpriseMIBRoot );
   
   for (auto& iterator : oidsTemporaryArray)
   {
      SNMPDriverTagContext* addedContextPtr = (SNMPDriverTagContext *)iterator;
      
      Tag* tagPtr = addedContextPtr->economyStruct.tempInitInfoStruct->tagPtr;
      SNMP_InitInfoStruct initInfo(*addedContextPtr->economyStruct.tempInitInfoStruct);
      
      // Переключение назначения участка памяти //
      delete addedContextPtr->economyStruct.tempInitInfoStruct;
      EndNodeValueType* newMem = (EndNodeValueType *)Lambda_Malloc(sizeof(EndNodeValueType));
      
      if( newMem != NULL )
	 addedContextPtr->economyStruct.endNodeValuePtr = newMem;
      
      addedContextPtr->economyStruct.endNodeValuePtr = addNewNodeToMIB(enterpriseMIBRoot, initInfo.oidString, initInfo.nodeDataType, initInfo.access);
      if( addedContextPtr->economyStruct.endNodeValuePtr != NULL )
      {
	 if(initInfo.trapEnability == ENABLED)
	 {
            enableTrap(intObjIDtoString(enterpriseMIBRoot->base_oid,enterpriseMIBRoot->base_oid_len) + initInfo.oidString);
	 }
			   
	 // Присваиваем значение по умолчанию //
	 updateEndNoteValue(addedContextPtr->economyStruct.endNodeValuePtr, tagPtr->getValue());
	
	 addedContextPtr->tagPtr = tagPtr;
	 ToCoreMessageType* message = new ToCoreMessageType(addedContextPtr, getId());
	 EndNodeValueType::setRefToUserData(addedContextPtr->economyStruct.endNodeValuePtr, (void*)message);
      }
   }
   
   EndNodeValueType::setWriteCompleteCallback(writeValueCallback);
   
   // Лист идентификаторов более не нужен //
   oidsTemporaryArray.resize(0);
}

#endif
//----------------------------------------------------------------------------------------------------------------------------------------//
//                                                          ПРИВАТНЫЕ МЕТОДЫ                                                              //
//----------------------------------------------------------------------------------------------------------------------------------------//
//****************************************************************************************************************************************//

//----------------------------------------------------------------------------------------------------------------------------------------//
//					          	    [ Создание узлов ]		   	  					  //
//----------------------------------------------------------------------------------------------------------------------------------------//
/**
  * Создание MIB дерева
  * Принимаем: rootNode - указатель на структуру первого узла дерева (корень)
  * Возвращаем: snmp_mibPtr - указатель на свою библиотеку MIB
  */
snmp_mibPtr SNMPsDriver::createNewMIB( OIDInfo baseOID, const snmp_nodePtr rootNode ) // добавить длину
{
   snmp_mibPtr basePointer = (snmp_mibPtr)Lambda_Malloc(sizeof(struct snmp_mib));
   
   if( basePointer && rootNode )
   {
      u32_t* newMem = (u32_t *)Lambda_Realloc(basePointer->base_oid, 0, baseOID.numOfNodes);
     
      if(newMem != NULL)
      {        
	 basePointer->base_oid_len = baseOID.numOfNodes;
	 
	 u32_t** hackPointer = const_cast<u32_t **> (&basePointer->base_oid);
	 *hackPointer = newMem;
	 
	 for(u32_t i = 0; i < basePointer->base_oid_len; i++)
	 {
	    newMem[i] = baseOID.objID.id[i];
	 }
	 
	 basePointer->root_node = rootNode;
	 return basePointer;
      }
   }
   return NULL;
};

/**
  *  Создание узла, имеющего нескольких потомков.
  *  Возвращает указатель на размещенный в памяти узел.
  */
snmp_treeNodePtr SNMPsDriver::createTreeNode( u32_t oid )
{
   snmp_treeNodePtr treeNode = ( snmp_treeNodePtr )Lambda_Malloc(sizeof(struct snmp_tree_node));

   if( treeNode != NULL )
   {
      treeNode->node.node_type = SNMP_NODE_TREE;
      treeNode->node.oid = oid;

      treeNode->subnode_count = 0;
      treeNode->subnodes = NULL;
   }
   return treeNode;
}

/**
  *  Создание табличного узла
  *
  *  Возвращает указатель на размещенный в памяти узел.
  */
snmp_tableNodePtr SNMPsDriver::createTableNode( u32_t oid )
{
   snmp_tableNodePtr tableNode = ( snmp_tableNodePtr )Lambda_Malloc(sizeof(struct snmp_table_node));

   if( tableNode != NULL )
   {
      tableNode->node.node.oid = oid;
      tableNode->node.node.node_type = SNMP_NODE_TABLE;

      tableNode->node.get_instance = getTableInstance;
      tableNode->node.get_next_instance = getTableNextInstance;

      tableNode->column_count = 0;
      tableNode->columns = NULL;

      tableNode->get_cell_instance = NULL; // А МНЕ ОНО НЕ НАДО Я САМ НАПИШУ ТУТ ВАЩЕ БЫЛО КАКОЕ-ТО... ОНО НЕ РАБОТАЛО ВЗЯЛ И ПЕРЕПИСАТЬ //
      tableNode->get_next_cell_instance = NULL;

      tableNode->get_value = getValueMethod;
      tableNode->set_test = snmp_set_test_ok;
      tableNode->set_value = setValueMethod;
   }
   return tableNode;
}

/**
  *  Создание скалярного (конечного узла)
  *
  *
  */
snmp_scalarNodePtr SNMPsDriver::createScalarNode(u32_t oid,
                                                 u32_t asn1_type,
						 snmp_access_t access)
{
   snmp_scalarNodePtr scalarNode = ( snmp_scalarNodePtr )Lambda_Malloc(sizeof(struct snmp_scalar_node));

   if( scalarNode != NULL )
   {
      scalarNode->node.node.node_type = SNMP_NODE_SCALAR;
      scalarNode->node.node.oid = oid;

      scalarNode->node.get_instance = scalarGetInstance;
      scalarNode->node.get_next_instance = scalarGetNextInstance;

      scalarNode->asn1_type = asn1_type;
      scalarNode->access = access; 
      
      scalarNode->get_value = getValueMethod; 
      scalarNode->set_value = setValueMethod; 
      
      scalarNode->set_test = NULL; // Эт че ваще?
   }

   return scalarNode;
}

//----------------------------------------------------------------------------------------------------------------------------------------//
//					          	[ Присваивание подузлов ]		   					  //
//----------------------------------------------------------------------------------------------------------------------------------------//
/**
  * Реаллокатор
  * currentAmount - уже существующее количество элементов массива
  */
template <typename _Typename> _Typename* Lambda_Realloc(_Typename* array, u32_t currentAmount, u32_t newAmount)
{
   if(newAmount == 0)
   {
      Lambda_Free((void *)array);
      return NULL;
   }
   
   _Typename* tempArray = (_Typename *)Lambda_Malloc(sizeof(_Typename) * newAmount);
   
   if(tempArray != NULL)
   {       
      if(currentAmount > 0)
      { 	 
         if(currentAmount > newAmount)
            currentAmount = newAmount;
	 
	 Lambda_Memcpy((void *)tempArray, (void *)array, sizeof(_Typename) * currentAmount);
	 Lambda_Free((void *)array);
      }
   }
   
   return tempArray;   
}
   
/**
  * Проверка на принадлежность узла с заданным OID текущему узлу
  *
  * Параметры:
  */
snmp_nodePtr SNMPsDriver::checkSubNodeExistence(snmp_treeNodePtr checkedNodePointer, u32_t checkedOID)
{
   if(checkedNodePointer->node.node_type == SNMP_NODE_TREE)
   {
      for(u32_t subnode_number = 0; subnode_number < checkedNodePointer->subnode_count; subnode_number++)
      {
         if(checkedNodePointer->subnodes[subnode_number]->oid == checkedOID)
            return (snmp_nodePtr)checkedNodePointer->subnodes[subnode_number];
      }
   }
   return NULL;
}

/**
 * Добавление узла в список потомков узла типа tree node
 *
*/
template <typename _Typename> bool SNMPsDriver::linkNewSubnode( snmp_treeNodePtr parentTreeNode, _Typename* subNode)
{
   u32_t currentSubnodesAmount;
   snmp_nodePtr subNodePointer = (snmp_nodePtr)subNode;  // Почему льзя? Потому что все структуры первым полем имеют структуру snmp_node
   
   if ((parentTreeNode != NULL) && (subNodePointer != NULL))
   {
      // Сравним, имеется ли так1Цой потомок уже //
      if ( checkSubNodeExistence(parentTreeNode, subNodePointer->oid) == NULL)       
      {
	 currentSubnodesAmount = parentTreeNode->subnode_count;
	 snmp_nodePtr* quickSavePointer = (snmp_nodePtr*)Lambda_Realloc(parentTreeNode->subnodes, currentSubnodesAmount, currentSubnodesAmount+1);
	 
	 if(quickSavePointer != NULL)
	 {
	    quickSavePointer[currentSubnodesAmount] = subNodePointer;
	    
	    parentTreeNode->subnodes = quickSavePointer;
	    parentTreeNode->subnode_count++;
	    
	    return LINK_SUCCESS;
	 }
      }
   }
   
   return LINK_FAULT;
}
//	 если миб 2 и птыает
// СМ. ОБЪЯВЛЕНИЯ //-------------------------------------------------------
/**
  * Добавление скалярного (не имеющего потомков) узла к ветвящему
  *
  */
bool SNMPsDriver::linkScalarSubnode ( snmp_treeNodePtr parentTreeNode, snmp_scalarNodePtr addedScalarNode )
{
   return linkNewSubnode(parentTreeNode, addedScalarNode);
}

/**
  * Добавление ветвящего потомка к ветвящему
  *
  */
bool SNMPsDriver::linkTreeSubnode ( snmp_treeNodePtr parentTreeNode, snmp_treeNodePtr addedTreeNode )   //!!!!!!!!!!!! проработай ситуацию, когда пользователь забыл присвоить конечного потомка !!!!!!!!!
{
   return linkNewSubnode(parentTreeNode, addedTreeNode);
}

/**
  * Привязываем к ветвящему узлу табличный узел-потомок
  *
  */
bool SNMPsDriver::linkTableSubnode ( snmp_treeNodePtr parentTreeNode, snmp_tableNodePtr addedTableNode )   //!!!!!!!!!!!! проработай ситуацию, когда пользователь забыл присвоить конечного потомка !!!!!!!!!
{
   return linkNewSubnode(parentTreeNode, addedTableNode);
}
			 
/**
  * Поиск последнего узла с идентичным путем
  * Пример: есть 1.3.567.89 и 1.3.567.45.767 - различие начинается на узле 4: 89/45, вернет узел с OID = 567
  */
snmp_nodePtr SNMPsDriver::findLastIdenticalNode(snmp_mibPtr mibBaseRoot, const OIDInfo & OID, u32_t* startLinkingNumber)
{
   snmp_treeNodePtr currentTreeNode = NULL;
   u32_t lastLinkedNumber = 0;
   u32_t currentNodeOID;
   u32_t lastCheckedNodeNum;
   
   currentTreeNode = (snmp_treeNodePtr)mibBaseRoot->root_node;
   
   if(OID.nodeType == SCALAR)     // Скалярный узел
      lastCheckedNodeNum = OID.numOfNodes - 1;
   else if(OID.nodeType == TABLE)   // Табличный узел
      lastCheckedNodeNum = OID.numOfNodes - 2;
   
   for(u32_t node_number = 0; node_number <= lastCheckedNodeNum; node_number++)
   {
      currentNodeOID = OID.objID.id[node_number];
      snmp_nodePtr subNode = checkSubNodeExistence(currentTreeNode, currentNodeOID);
      
      if (subNode == NULL) { break; }
      
      // Захотел пользователь два узла с OID-ами ".1.3.6.1.5.1.87" и ".1.3.6.1.5.2.0", первый - табличный, указатель на таблицу имеет
      // OID = 5. Второй - скаляр, имеет последний OID 1, но уже создан один из них - значит пользователь ошибся.
      // Пример, как можно : 1-й узел: ".1.3.6.1.5.1.87"
      //                     2-й узел: ".1.3.6.1.6.1.0"    
      if(subNode->node_type != SNMP_NODE_TREE) 
      {     
	 if(node_number != lastCheckedNodeNum)
	    return subNode; 
      } 
      currentTreeNode = (snmp_treeNodePtr)subNode;  // Если не последний узел - значит ветвящий
      lastLinkedNumber++;
   }
          
   *startLinkingNumber = lastLinkedNumber;
   return &currentTreeNode->node;
}

/**
 * @brief SNMPsDriver::addNewScalarNodeToMIB - Добавление скалярного узла в пользовательскую MIB-базу по строковому значению (например: "12.34.54")
 * @param mibBaseRoot - указатель на MIB - базу пользователя
 * @param oidString - строковое значение OID
 * @param asn1_type - тип значения узла
 * @return
 */
EndNodeValueType* SNMPsDriver::addNewScalarNodeToMIB(snmp_mibPtr mibBaseRoot, 
						     const OIDInfo & oidWithoutBase,
						     u8_t asn1_type,
						     snmp_access_t access)
{
   snmp_nodePtr addressInRAM = NULL;
   snmp_treeNodePtr parentTreeNode = NULL;
   u32_t startLinkingNumber;  // Номер узла - родителя, начиная с которого начинается создание обнаруженной несуществующей ветви дерева
   EndNodeValueType nodeValue;
   
   // Ищем узел, начиная с которого отсутствует привязка к базе //       
   parentTreeNode = (snmp_treeNodePtr)findLastIdenticalNode(mibBaseRoot, oidWithoutBase, &startLinkingNumber);
					         
   if(parentTreeNode)
   {
      if(parentTreeNode->node.node_type == SNMP_NODE_TREE)
      {
	 // Создаем ветвь, начиная с узла, на котором обнаружилось ее отсутсвие //
	 for(u32_t node_number = startLinkingNumber; node_number < oidWithoutBase.numOfNodes - 2; node_number++)
	 {
	    snmp_treeNodePtr newTreeNode = createTreeNode( oidWithoutBase.objID.id[node_number] );
	    linkTreeSubnode(parentTreeNode, newTreeNode);
	    
	    parentTreeNode = newTreeNode;
	 }
	 	 
	 // Создание последнего узла и привязка к tree node (родителю) //
	 snmp_scalarNodePtr endScalarNode = createScalarNode(oidWithoutBase.objID.id[oidWithoutBase.numOfNodes - 2],   // Создание
							     asn1_type,
							     access);
         linkScalarSubnode( parentTreeNode, endScalarNode );
	 
	 // Занесение адреса узла в базу //
	 addressInRAM = &endScalarNode->node.node; 				  
	 auto iterator = nodesDataBase.ScalarDataBase.insert( make_pair(addressInRAM, nodeValue));
	 
	 if(iterator.first != nodesDataBase.ScalarDataBase.end())
	 {
	    EndNodeValueType* nodeValueAddress = &iterator.first->second;
	    return nodeValueAddress;
	 }
      }
#ifdef SNMP_TEST
      else if ((parentTreeNode->node.node_type == SNMP_NODE_SCALAR) && (parentTreeNode->node.oid == oidWithoutBase.objID.id[oidWithoutBase.objID.len - 2]))
      {
	 // Повторная попытка создать узел
      }
#endif
   }
 
   // Иначе - вообще что-то левое произошло
   return NULL;
}

/**
 * @brief SNMPsDriver::addNewTableNodeToMIB - Добавление табличного узла в пользовательскую MIB-базу по строковому значению (например: "12.34.54")
 * @param mibBaseRoot
 * @param oidString
 * @param asn1_type
 * @return
 */
EndNodeValueType* SNMPsDriver::addNewTableNodeToMIB(snmp_mibPtr mibBaseRoot, const OIDInfo & oidWithoutBase, u8_t asn1_type, snmp_access_t access)
{
   snmp_treeNodePtr parentTreeNode = NULL;
   snmp_tableNodePtr endTableNode;
   u32_t startLinkingNumber;  // Номер узла - родителя, начиная с которого начинается создание обнаруженной несуществующей ветви дерева
   TableDataBaseType* tableBasePtr = &nodesDataBase.TableDataBase;
   snmp_nodePtr newTableNodePointer = NULL;
   
   // Ищем узел, начиная с которого отсутствует привязка к базе //
   // Пример: есть 1.3.567.89.2.58.0 и 1.3.567.45.767 - различие начинается на узле 4: 89/45 //
   snmp_nodePtr lastIdenticalNode = findLastIdenticalNode (mibBaseRoot, oidWithoutBase, &startLinkingNumber);
     
   if(lastIdenticalNode)
   {
      u32_t expectedLinkNum = oidWithoutBase.numOfNodes - 3;
      snmp_nodePtr currentNodePointer = lastIdenticalNode;
      
      if(currentNodePointer->node_type == SNMP_NODE_TREE)
      {  
	 // Создаем ветвь, начиная с узла, на котором обнаружилось расхождение идентификаторов //
	 for(u32_t node_number = startLinkingNumber; node_number < expectedLinkNum; node_number++)
	 {
	    snmp_treeNodePtr newTreeNode = createTreeNode( oidWithoutBase.objID.id[node_number] );
	    
	    if( newTreeNode == NULL) { return NULL; }    // Не смогли выделить память -> трындец
	    if( linkTreeSubnode((snmp_treeNodePtr)currentNodePointer, newTreeNode) == false) { return NULL; }  // Примерно то же самое
	    
	    currentNodePointer = &newTreeNode->node;
	 }    
	 
	 parentTreeNode = (snmp_treeNodePtr)currentNodePointer;

	 newTableNodePointer = (snmp_nodePtr)(createTableNode( oidWithoutBase.objID.id[expectedLinkNum] ));
	 if( newTableNodePointer != NULL )
         {
            if( linkTableSubnode( parentTreeNode, (snmp_tableNodePtr)newTableNodePointer ) == false )
               return NULL;
	 }
      }
      else if(currentNodePointer->node_type == SNMP_NODE_TABLE)
      {  
	 // крайний узел найден и уже создан прежде //
	newTableNodePointer = currentNodePointer; 
      } else {
	 return NULL;
      }
      
      // OID последних двух узлов (см.определение переменной) //
      u32_t precellOID = oidWithoutBase.objID.id[oidWithoutBase.numOfNodes - 2];
      
      // Предпоследний OID табличного узла должен быть 1, например 1.3.6.4.5.1.1"
      if(precellOID == 1)
      {      	    
	 u32_t cellOID = oidWithoutBase.objID.id[oidWithoutBase.numOfNodes - 1];
	 	 	 
	 // Защита от добавления уже созданной ячейки таблицы //
	 auto cellsMapPtr = (*tableBasePtr).find(newTableNodePointer);
	 
	 if(cellsMapPtr != (*tableBasePtr).end())
	 {
	    TableCellsListType* nodeCellsTable = &cellsMapPtr->second;
	    if(nodeCellsTable->find(cellOID) != nodeCellsTable->end())
	    {
	       // Ячейка уже существует -> весь OID повторяет уже имеющийся //
	       return NULL;
	    }
	 }    	    

	 EndNodeValueType* nodeValueAddress = &((*tableBasePtr)[newTableNodePointer])[cellOID].nodeEndValue; // rebus specially for DY
	 
	 // тут должна быть какая-то проверка, чувствую, но пока не знаю, какая
	 endTableNode = (snmp_tableNodePtr)newTableNodePointer;
	 
	 // Расширяем таблицу конфигураций //
	 snmp_table_col_def* newConfigTablePtr = (snmp_table_col_def *)Lambda_Realloc(endTableNode->columns,
										      endTableNode->column_count,
										      endTableNode->column_count + 1);
	 
	 if(newConfigTablePtr != NULL)
	 {
	    endTableNode->columns = newConfigTablePtr;
	    snmp_table_col_def* nodeConfigTableHackPtr = (snmp_table_col_def *)endTableNode->columns;
	    endTableNode->column_count++;
	    u32_t columnsAmount = endTableNode->column_count;
	    u32_t columnNumber = columnsAmount - 1;
	    
	    nodeConfigTableHackPtr[columnNumber].access = access;
	    nodeConfigTableHackPtr[columnNumber].asn1_type = asn1_type;
	    nodeConfigTableHackPtr[columnNumber].index = oidWithoutBase.objID.id[oidWithoutBase.objID.len - 1]; 

	    // Произошло новое выделение памяти, перезаписываем указатели на ячейки конфигурационной таблицы //
	    for(columnNumber = 0; columnNumber < columnsAmount; columnNumber++)
	    {
	       cellOID = nodeConfigTableHackPtr[columnNumber].index;
	       
	       auto it =  ((*tableBasePtr)[newTableNodePointer]).find(cellOID);
	       if( it != ((*tableBasePtr)[newTableNodePointer]).end() )
	       {
		  it->second.nodeConfigPointer = &nodeConfigTableHackPtr[columnNumber];
	       }
	    }
	    
	    return nodeValueAddress;
	 }
      }
   }
   
   // Высвобождение памяти под порожняковое место
   return NULL;
}

//----------------------------------------------------------------------------------------------------------------------------------------//
//					               	[ Конвертация типов узлов ]	   					 	  //
//----------------------------------------------------------------------------------------------------------------------------------------//
/**
 * @brief SNMPsDriver::associateToLambdaSNMPtype - приведение типа от  NewVisionCore к формату SNMPDriver (не к ASN1, хотя во многом
 *                                                 это по сути одно и то же, ежже (©)КамаПуля
 * @param runTimeType - тип тега в представлении ядра нового видения
 * @return
 */
extraType SNMPsDriver::associateToLambdaSNMPtype(DataType runTimeType)
{
   switch (runTimeType)
   {
     case dtBool:
     case dtByte:
     case dtWord:
     case dtDWord:
      {
	 return END_NODE_TYPE_UNSIGNED32;
      }
     case dtShortInt:
     case dtInt:
      {
	 return END_NODE_TYPE_INTEGER;
      }
      
     case dtString:
      {
	 return END_NODE_TYPE_OCTET_STRING;
      }       
      // Добавочные типы: //
     case dtFloat:
      {
	 return END_NODE_TYPE_FLOAT;
      }
     default:
      {
#ifdef SNMP_TEST
	 stub();
#endif
	 return END_NODE_INVALID_TYPE;
      }
   }
}

/**
 * @brief SNMPsDriver::associateToASN1type - приведение типа к ASN1, на случай таких типов, как FLOAT (самописных)
 * @param extraType
 * @return
 */
u16_t SNMPsDriver::associateToASN1type(extraType exType)
{
   switch (exType)
   {
      // Стандартные типы //
     case SNMP_ASN1_TYPE_END_OF_CONTENT:
     case SNMP_ASN1_TYPE_NULL:
     case SNMP_ASN1_TYPE_INTEGER:
     case SNMP_ASN1_TYPE_OCTET_STRING:
     case SNMP_ASN1_TYPE_OBJECT_ID:
     case SNMP_ASN1_TYPE_SEQUENCE:
     case SNMP_ASN1_TYPE_IPADDRESS:
     case SNMP_ASN1_TYPE_COUNTER32:
     case SNMP_ASN1_TYPE_UNSIGNED32:
     case SNMP_ASN1_TYPE_TIMETICKS:
     case SNMP_ASN1_TYPE_OPAQUE:      
#if LWIP_HAVE_INT64
     case SNMP_ASN1_TYPE_COUNTER64:
#endif
      {
	 return (uint8_t)exType;
      }
      
      // Свои типы, не имеющиеся в SMMP (не стандарт де-fuckто
     case END_NODE_TYPE_FLOAT:
      {
	 return SNMP_ASN1_TYPE_OCTET_STRING;
      }            
   }
   
   return END_NODE_INVALID_TYPE;
}
//----------------------------------------------------------------------------------------------------------------------------------------//
//					               [ Работа с конечными значениями ]	   					  //
//----------------------------------------------------------------------------------------------------------------------------------------//
/**
 * @brief SNMPsDriver::findEndValuePointer - поиск конечного значения в имеющейся базе по строковому OID
 * @param mibBaseRoot
 * @param oidString
 * @return - указатель на значение узла
 */
EndNodeValueType* SNMPsDriver::findEndValuePointer(snmp_mibPtr mibBaseRoot, const OIDString & oidString)
{
   OIDInfo oid(oidString, AUTO_TYPE);
   u32_t startLinkingNumber;

   if(oid.actuality == VALID)
   {
      snmp_nodePtr lastIdenticalNode = findLastIdenticalNode(mibBaseRoot, oid, &startLinkingNumber);

      if(lastIdenticalNode)
      {
         struct snmp_node_instance nodeInstance = {lastIdenticalNode};
         nodeInstance.node = lastIdenticalNode;

         if(oid.nodeType == TABLE)
         {
            nodeInstance.instance_oid.id[0] = oid.objID.id[oid.objID.len - 2];
            nodeInstance.instance_oid.id[1] = oid.objID.id[oid.objID.len - 1];
            nodeInstance.instance_oid.len = 2;
         }

         EndNodeValueType* endNodeValue = findEndValuePointer(&nodeInstance, CURRENT_INSTANCE);
         return endNodeValue;
      }
   }

   return NULL;
}

/**
 * @brief SNMPsDriver::findEndValuePointer - поиск значения только по строковому OID
 * @param oidString
 * @return
 */
EndNodeValueType* SNMPsDriver::findEndValuePointer(const OIDString & oidString) /// ПРОВЕРИТЬ НА МИБ2 !!!!!!!!!!!!!!!!! по идее должен найти
{
   OIDInfo OID(oidString, AUTO_TYPE);
   EndNodeValueType* endNodeValue = NULL;
   struct snmp_node_instance nodeInstance;
   
   if(OID.actuality == VALID)
   {      
     if( snmp_get_node_instance_from_oid((u32_t *)&OID.objID.id, OID.objID.len , &nodeInstance) == SNMP_ERR_NOERROR)
        endNodeValue = findEndValuePointer(&nodeInstance, CURRENT_INSTANCE);
   }

   return endNodeValue;
}

/**
 * @brief SNMPsDriver::findEndValuePointer: Возвращает указатель на структуру, содержащую информацию о значении узла
 *                                          Также, производит изменения в nodeInstance - выставляет OID в случае работы с табличным узлом
 * @param nodeInstance - внутренняя структура реализации SNMP из LWIP
 * @param nextFlag - флаг, принимает значения: CURRENT_INSTANCE - получаем текущий экземпляр
 *                                             NEXT_INSTANCE - запрашиваем следующий за текущим экземпляр
 * @return
 */
EndNodeValueType* SNMPsDriver::findEndValuePointer(struct snmp_node_instance *nodeInstance, u32_t nextFlag)
{
   snmp_nodePtr nodePtr = (snmp_nodePtr)nodeInstance->node;
   EndNodeValueType* nodeValue = NULL;

   if (nodePtr->node_type == SNMP_NODE_TABLE)
   {
      u32_t cellOID;
      EndTableNodeValueType* valueCellPointer;
      TableCellsListType* pointedTableType = &nodesDataBase.TableDataBase[nodePtr];   // есть ли? проверять жинадо !!!!!!!!!!!!!!!!

      // Пригнали к нам указатель на table node, но не конкретизировали номер ячейки //
      // Ну тогда выдадаём первую, че делать 					     //
      if(nodeInstance->instance_oid.len == 0)
      {
        auto iterator = pointedTableType->begin();

        cellOID = iterator->first;
        valueCellPointer = &(iterator->second);
      }
      else
      {
         if(nodeInstance->instance_oid.id[0] == 1)   
         {
            cellOID = nodeInstance->instance_oid.id[1];

            auto iterator = pointedTableType->find(cellOID);

            // Если запрошена ячейка не указанная, а следующая за ней //
            if(nextFlag == NEXT_INSTANCE)
            {
               if( iterator != pointedTableType->end() )
                  iterator++;
               else return NULL;
            }

            if( iterator != pointedTableType->end() )
            {
               cellOID = iterator->first;
               valueCellPointer = &(iterator->second);
            }
            else return NULL;
         }
      }

      nodeInstance->instance_oid.id[0] = 1;
      nodeInstance->instance_oid.id[1] = cellOID;
      nodeInstance->instance_oid.len = 2;
							      
      nodeInstance->asn1_type = valueCellPointer->nodeConfigPointer->asn1_type;
      nodeInstance->access = valueCellPointer->nodeConfigPointer->access;

      snmp_tableNodePtr table_node = (snmp_tableNodePtr)nodePtr;
      nodeInstance->get_value = table_node->get_value;
      nodeInstance->set_test  = table_node->set_test;
      nodeInstance->set_value = table_node->set_value;

      nodeValue = &valueCellPointer->nodeEndValue;
      nodeInstance->reference.const_ptr = (void *)nodeValue;
   }
   else if (nodePtr->node_type == SNMP_NODE_SCALAR)
   {
      snmp_scalarNodePtr scalarInterpretation = (snmp_scalarNodePtr)nodePtr;

      auto iterator = nodesDataBase.ScalarDataBase.find(nodePtr);
      if( iterator != nodesDataBase.ScalarDataBase.end() )
      {
         if(iterator != nodesDataBase.ScalarDataBase.end())
         {
            nodeInstance->asn1_type = scalarInterpretation->asn1_type;
            nodeInstance->access = scalarInterpretation->access;
	   
	    nodeValue = &iterator->second;
            nodeInstance->reference.const_ptr = (void *)nodeValue;
         }
      }
   }
   return nodeValue;
}

/**
 * @brief SNMPsDriver::getValueMethod - метод, передаваемый в LWIP для получения фактического значения узла
 * @param instance
 * @param valuePointer
 * @return - длина полученного значения
 */
s16_t SNMPsDriver::getValueMethod(struct snmp_node_instance* instance, void* valuePointer)
{   
   return getValueMethod((EndNodeValueType *)instance->reference.const_ptr, valuePointer);
}

s16_t SNMPsDriver::getValueMethod(EndNodeValueType* nodeValue, void* valuePointer)
{
    if(nodeValue)                                                       
    {
       switch (nodeValue->exType)         
       {
         case END_NODE_TYPE_INTEGER:    	
          {
             int* intValPtr = (int *)valuePointer;
             *intValPtr =  nodeValue->value.intVal;

             return sizeof(int);
	  }
	 case END_NODE_TYPE_UNSIGNED32: 
	  {
	     u32_t* intValPtr = (u32_t *)valuePointer;
	     *intValPtr =  nodeValue->value.intVal;
	     
	     return sizeof(u32_t);
	  }   	  
	 case END_NODE_TYPE_OCTET_STRING:
          {
             u32_t length = sizeof(char) * nodeValue->length;
             Lambda_Memcpy(valuePointer, nodeValue->value.pStr, length);

             return length;
          }
         case END_NODE_TYPE_TIMETICKS:
          {
             u32_t* intValPtr = (u32_t *)valuePointer;
             *intValPtr =  nodeValue->value.dwordVal;

             return sizeof(u32_t);
          }
         case END_NODE_TYPE_FLOAT:
          {
             u32_t length = sizeof(char) * nodeValue->length;
             Lambda_Memcpy(valuePointer, nodeValue->value.pStr, length);
             return length;
          }
          // НУ ТУТ ЧИСТО ДОБИВАТЬ ТИПЫ, КОТОРЫЕ ПОНАДОБЯТСЯ
       }
    }
    return 0;
}

/**
 * @brief SNMPsDriver::updateEndNoteValue - обновление значения узла на новое
 * @param oidString - строковое значение OID
 * @param newValue
 * @return
 */
snmp_err_t SNMPsDriver::updateEndNoteValue( const OIDString & oidString, VarData newValue )
{  
   EndNodeValueType* endNodeValue = findEndValuePointer(oidString);
 
   if(endNodeValue)
        return updateEndNoteValue(endNodeValue, newValue);

    return SNMP_ERR_GENERROR;
}

/**
 * @brief SNMPsDriver::updateEndNoteValue - обновление значения узла на новое
 * @param endNodeValue - указатель на ячейку с необходимыми данными об узле
 * @param asn1_type
 * @param newValue
 * @return
 */
snmp_err_t SNMPsDriver::updateEndNoteValue( EndNodeValueType* endNodeValue, VarData newValue )
{
   if(endNodeValue)
   {   
      VarData previousValue = endNodeValue->value;
      u32_t previousLength = endNodeValue->length;
      
      switch (endNodeValue->exType)
      {
	case END_NODE_RESET_VALUE:
	 {
	    endNodeValue->length = 0;
	    endNodeValue->value.intVal = 0;
	    break;
	 }
	case SNMP_ASN1_TYPE_INTEGER:
	 {        
	    endNodeValue->value.intVal = newValue.intVal;
	    endNodeValue->length = 1;
	    break;
	 }
	case SNMP_ASN1_TYPE_UNSIGNED32:
	 {
	    endNodeValue->value.dwordVal = newValue.dwordVal;
	    endNodeValue->length = 1;
	    break;
	 }
	case SNMP_ASN1_TYPE_TIMETICKS:
	 {
	    endNodeValue->value.dwordVal = newValue.dwordVal;
	    endNodeValue->length = 1;
	    break;
	 }        
	case SNMP_ASN1_TYPE_OCTET_STRING:
	 {
	    u32_t oldLength = endNodeValue->length;
	    u32_t newLength = strlen(newValue.pStr) + 1;         // точно ниче не попутано?
	    char* newMem = (char *)Lambda_Realloc(endNodeValue->value.pStr, oldLength, newLength);
	    
	    if ( newMem != NULL )
	    {
	       endNodeValue->value.pStr = newMem;
	       endNodeValue->length = newLength;
	       
	       Lambda_Memcpy(endNodeValue->value.pStr, newValue.pStr, newLength);
	       endNodeValue->value.pStr[newLength - 1] = 0; // мог бы сэкономить этот байт, но мне страшно.
	    }
	    break;
	 }       
	case END_NODE_TYPE_FLOAT:
	 {
	    u32_t oldLength = endNodeValue->length;
	    string newFloatString = to_string(newValue.fltVal);
	    u32_t newLength = newFloatString.length();
	    
	    char* newMem = (char *)Lambda_Realloc(endNodeValue->value.pStr, oldLength, newLength);
	    if ( newMem != NULL )
	    {
	       endNodeValue->value.pStr = newMem;
	       endNodeValue->length = newLength;
	       
	       Lambda_Memcpy(endNodeValue->value.pStr, newFloatString.data(), newLength);
	    }
	    break;
	 } 
	default:
	 {
	    return SNMP_ERR_GENERROR;
	 }
      }
      
      if ((previousValue.dwordVal != endNodeValue->value.dwordVal) || (previousLength != endNodeValue->length))
      {
	 if(endNodeValue->userDataReference)
	    endNodeValue->writeCallback(endNodeValue, newValue); 
      }
      return SNMP_ERR_NOERROR;
   }
   return SNMP_ERR_GENERROR;
}
	
/**
 * @brief SNMPsDriver::setValueMethod - метод, передаваемый в LWIP для выставления
 * @param instance
 * @param length
 * @param value
 * @return
 */
snmp_err_t SNMPsDriver::setValueMethod(struct snmp_node_instance* instance, u16_t length, void* value)
{
   VarData newValue;  
   EndNodeValueType* nodeValue = (EndNodeValueType *)instance->reference.const_ptr; // Ссылка на ранее найденное место значения в базе
   
   if(nodeValue != NULL) 
   { 
      switch (nodeValue->exType)
      {
	case END_NODE_TYPE_OCTET_STRING:
	case END_NODE_TYPE_IPADDRESS:
	case END_NODE_TYPE_OPAQUE:
	 {
	    newValue.pStr = (char *)value;       	    
	    break;
	 }
	case END_NODE_TYPE_FLOAT:
	 {
	    // Тут проверки на длину нужны !!!!!!!!!!!!!	    
	    float floatVal= atof((char *)value);
	    newValue.fltVal = floatVal;
	    break;
	 }
	default:
	 {
	    newValue.dwordVal =  *((u32_t *)value);
	    break;
	 }
      }
      return updateEndNoteValue(nodeValue, newValue); 
   }
   return SNMP_ERR_NOSUCHINSTANCE;
}
       
/**
 * @brief SNMPsDriver::scalarGetInstance - получение экземпляра инфы о скалярном узле
 *        Прототип продиктован разрабами LWIP.
 * @param root_oid - OID корня дерева
 * @param root_oid_len
 * @param instance - инфа совокупная об узле
 * @return
 */
snmp_err_t SNMPsDriver::scalarGetInstance(const u32_t *root_oid, u8_t root_oid_len, struct snmp_node_instance *instance)
{
   snmp_scalarNodePtr scalarNodePtr = (snmp_scalarNodePtr)instance->node;
   EndNodeValueType* nodeValuePtr;

   // Поиск данного узла в базе //
   nodeValuePtr = findEndValuePointer(instance, CURRENT_INSTANCE);
   if(nodeValuePtr != NULL)
   {
      instance->instance_oid.len = 1;
      instance->instance_oid.id[0] = 0;

      /* scalar only has one dedicated instance: .0 */
      if ((instance->instance_oid.len != 1) || (instance->instance_oid.id[0] != 0))
         return SNMP_ERR_NOSUCHINSTANCE;

      instance->access    = scalarNodePtr->access;
      instance->asn1_type = scalarNodePtr->asn1_type;
      instance->get_value = scalarNodePtr->get_value;
      instance->set_test  = scalarNodePtr->set_test;
      instance->set_value = scalarNodePtr->set_value;
	         
      return SNMP_ERR_NOERROR;
   }
   return  SNMP_ERR_NOSUCHINSTANCE;
}

/**
 * @brief SNMPsDriver::scalarGetNextInstance - получение инфы о следующем за указанным узле
 * @param root_oid
 * @param root_oid_len
 * @param instance
 * @return
 */
snmp_err_t SNMPsDriver::scalarGetNextInstance(const u32_t* root_oid, u8_t root_oid_len, struct snmp_node_instance* instance)
{
   // Ядро SNMP в первый раз попишет сюды вообще левый OID, мы подтверждаем, что это туфта. Он не сдается, и раскручивает у себя //
   // дерево дальше и снова скидывает нас в эту функцию. А почему не равно нулю - сделал как у разрабов LWIP в оригинале         //
   if (instance->instance_oid.len == 0)
       return scalarGetInstance(root_oid, root_oid_len, instance);

   return SNMP_ERR_NOSUCHINSTANCE;
}

/**
 * @brief SNMPsDriver::getTableInstance - поиск информации о табличном узле и работа с ячейками
 * @param root_oid
 * @param root_oid_len
 * @param instance
 * @return
 */
snmp_err_t SNMPsDriver::getTableInstance(const u32_t *root_oid, u8_t root_oid_len, struct snmp_node_instance *instance)
{
   EndNodeValueType* nodeValuePtr = findEndValuePointer(instance, CURRENT_INSTANCE);
  
   if(nodeValuePtr)
       return SNMP_ERR_NOERROR;

   return SNMP_ERR_NOSUCHINSTANCE;
}

/**
 * @brief SNMPsDriver::getTableNextInstance - получение информации о ячейке, последующей за указанной
 * @param root_oid
 * @param root_oid_len
 * @param instance
 * @return
 */
snmp_err_t SNMPsDriver::getTableNextInstance(const u32_t *root_oid, u8_t root_oid_len, struct snmp_node_instance *instance)
{
   EndNodeValueType* nodeValuePtr = findEndValuePointer(instance, NEXT_INSTANCE);

   if(nodeValuePtr != NULL)
       return SNMP_ERR_NOERROR;

   return SNMP_ERR_NOSUCHINSTANCE;
}


#ifdef LAMBDA_MIB2

void SNMPsDriver::createMIB2(void)
{
   includeMIB( (snmp_mibPtr)&mib2 );

   sysName     = replaceStringContent( sysName,     (char *)"Impulse Aoutomation" );
   sysContact  = replaceStringContent( sysContact,  (char *)"POOR" );
   sysLocation = replaceStringContent( sysLocation, (char *)"Saint-Petersburg" );

   sysNameLength = strlen(sysName);
   sysContantLength = strlen(sysContact);
   sysLocationLength = strlen(sysLocation);

   snmp_mib2_set_sysname( (u8_t*)sysName, &sysNameLength, 20 );              // Последний параметр - резерв памяти под новое название
   snmp_mib2_set_syscontact( (u8_t*)sysContact, &sysContantLength, 30 );     // Если пользователю приспичит перезаписать поле
   snmp_mib2_set_syslocation( (u8_t*)sysLocation, &sysLocationLength, 30 );  // Пишешь 0 - можешь только считывать
}

#endif // LAMBDA_MIB2

//----------------------------------------------------------------------------------------------------------------------------------------//
//----------------------------------------------------------------------------------------------------------------------------------------//
#ifdef SNMP_TEST

char message_1[] = "Fuck Valve Software";
char message_2[] = "Billy is goat";
char message_3[] = "Gaben is goat too";
char message_4[] = "Tim Sweeney is CHMO";
char message_5[] = "Karmak - TOP";

/**
 * @brief SNMPsDriver::createTestMIB - создание тестовой MIB
 * @return
 */
bool SNMPsDriver::createTestMIB(void)
{
   snmp_mibPtr testMIB = createRootOfPrivateMIB(".1.3.6.8000");        
   includeMIB( testMIB );
   
   VarData value;
				   
   // ПРИМЕР СКАЛЯРНЫХ УЗЛОВ -------------------------------------------------------------------------------------------------//
   value.dwordVal = 1771;
   addNewNodeToMIB(testMIB, ".123.657.1771.0", END_NODE_TYPE_TIMETICKS, SNMP_NODE_INSTANCE_READ_ONLY, value);
   value.dwordVal = 1772;
   addNewNodeToMIB(testMIB, ".123.657.1772.0", END_NODE_TYPE_TIMETICKS, SNMP_NODE_INSTANCE_READ_ONLY, value);
   value.intVal = 1773;
   addNewNodeToMIB(testMIB, ".123.657.1773.0", END_NODE_TYPE_INTEGER,   SNMP_NODE_INSTANCE_READ_ONLY, value);
	                                                                                    
   auto node_1_2_1_2_771_5_0   = addNewNodeToMIB(testMIB, ".1.2.1.2.771.5.0",   END_NODE_TYPE_INTEGER, SNMP_NODE_INSTANCE_WRITE_ONLY);
   auto node_4_12_70_80_90_1_0 = addNewNodeToMIB(testMIB, ".4.12.70.80.90.1.0", END_NODE_TYPE_INTEGER, SNMP_NODE_INSTANCE_WRITE_ONLY);
  
   updateEndNoteValue(node_1_2_1_2_771_5_0, value);  
   updateEndNoteValue(node_4_12_70_80_90_1_0, value);  
   
   value.pStr = (char *)&message_1[0];
   addNewNodeToMIB(testMIB, ".123.657.6.777.1770.0", END_NODE_TYPE_OCTET_STRING, SNMP_NODE_INSTANCE_READ_WRITE, value);
   value.pStr = (char *)&message_2[0];
   addNewNodeToMIB(testMIB, ".123.657.6.777.1771.0", END_NODE_TYPE_OCTET_STRING, SNMP_NODE_INSTANCE_READ_WRITE, value);
   value.pStr = (char *)&message_3[0];
   addNewNodeToMIB(testMIB, ".123.657.6.777.1772.0", END_NODE_TYPE_OCTET_STRING, SNMP_NODE_INSTANCE_READ_WRITE, value);
   
   // ПРИМЕР ТАБЛИЧНЫХ УЗЛОВ --------------------------------------------------------------------------------------------------------------
   value.intVal = 1;
   addNewNodeToMIB(testMIB, ".1.2.1.491.770.1.1", END_NODE_TYPE_INTEGER,      SNMP_NODE_INSTANCE_READ_ONLY,  value);   
   value.pStr = (char *)&message_4[0];
   addNewNodeToMIB(testMIB, ".1.2.1.491.770.1.3", END_NODE_TYPE_OCTET_STRING, SNMP_NODE_INSTANCE_READ_WRITE, value);  
   value.intVal = 2;
   addNewNodeToMIB(testMIB, ".1.2.1.491.770.1.2", END_NODE_TYPE_INTEGER,      SNMP_NODE_INSTANCE_READ_ONLY,  value);

   value.intVal = 34;
   addNewNodeToMIB(testMIB, ".1.2.1.491.770.1.34", END_NODE_TYPE_INTEGER, SNMP_NODE_INSTANCE_READ_WRITE, value);
   value.intVal = 9;
   addNewNodeToMIB(testMIB, ".1.2.1.491.770.1.9",  END_NODE_TYPE_INTEGER, SNMP_NODE_INSTANCE_READ_WRITE, value);
   value.intVal = 30;
   addNewNodeToMIB(testMIB, ".1.2.1.491.790.1.30", END_NODE_TYPE_INTEGER, SNMP_NODE_INSTANCE_READ_WRITE, value);   
											
   value.intVal = 255;
   addNewNodeToMIB(testMIB, ".17.985.1.555",  END_NODE_TYPE_INTEGER, SNMP_NODE_INSTANCE_READ_WRITE, value);

   //---------------------------------------------------------------------------------------------------------------------------//
   //                                                      Прикладные примеры 							//
   //---------------------------------------------------------------------------------------------------------------------------//     
   
   // СВОБОДНОЕ МЕСТО В КУЧЕ ---------------------------------------------------------------------------------------------------//
   addNewNodeToMIB(testMIB, ".1000.0", END_NODE_TYPE_UNSIGNED32, SNMP_NODE_INSTANCE_READ_ONLY);
   enableTrap(".1.3.6.8000.1000.0");  	 // Включаем ловушку )))
 
   // РАЗРЕШЕНИЕ НА РАБОТУ ЛОВУШЕК ---------------------------------------------------------------------------------------------//	
   value.intVal = 1;
   updateEndNoteValue(addNewNodeToMIB(testMIB, ".2000.0", END_NODE_TYPE_UNSIGNED32, SNMP_NODE_INSTANCE_READ_WRITE), value);   // 1 - разрешены, 0 - запрещены
		   
   // ПРИМЕР ПОВТОРЯЮЩИХСЯ УЗЛОВ -----------------------------------------------------------------------------------------------//
   value.intVal = 30;
   addNewNodeToMIB(testMIB, ".1.2.1.491.800.1.30", END_NODE_TYPE_INTEGER, SNMP_NODE_INSTANCE_READ_WRITE, value);      
   value.intVal = 77;
   addNewNodeToMIB(testMIB, ".1.2.1.491.800.1.30", END_NODE_TYPE_INTEGER, SNMP_NODE_INSTANCE_READ_WRITE, value);   
   
   value.pStr = (char *)&message_5[0];
   addNewNodeToMIB(testMIB, ".123.657.6.777.1773.0", END_NODE_TYPE_OCTET_STRING, SNMP_NODE_INSTANCE_READ_WRITE, value);
   value.intVal = 1773;
   addNewNodeToMIB(testMIB, ".123.657.6.777.1773.0", END_NODE_TYPE_INTEGER,      SNMP_NODE_INSTANCE_READ_WRITE, value);      
   
   addNewNodeToMIB(testMIB, ".1.2.1.491.780.50.29", END_NODE_TYPE_INTEGER, SNMP_NODE_INSTANCE_READ_WRITE);   
   addNewNodeToMIB(testMIB, ".1.2.1.491.780.50.29", END_NODE_TYPE_INTEGER, SNMP_NODE_INSTANCE_READ_WRITE);         
   
   // КОСЯЧНЫЕ OID-ы -----------------------------------------------------------------------------------------------------------//
   addNewNodeToMIB(testMIB, ".17.985.2.556",   END_NODE_TYPE_INTEGER, SNMP_NODE_INSTANCE_READ_WRITE, value);
   addNewNodeToMIB(testMIB, ".17.985.2.557",   END_NODE_TYPE_INTEGER, SNMP_NODE_INSTANCE_READ_WRITE, value);
   addNewNodeToMIB(testMIB, ".17.As85.2.557",  END_NODE_TYPE_INTEGER, SNMP_NODE_INSTANCE_READ_WRITE);
   addNewNodeToMIB(testMIB, ".1-.2.557",       END_NODE_TYPE_OCTET_STRING, SNMP_NODE_INSTANCE_READ_WRITE);
   addNewNodeToMIB(testMIB, ".1-.2.557",       END_NODE_TYPE_OCTET_STRING, SNMP_NODE_INSTANCE_READ_WRITE);
   addNewNodeToMIB(testMIB, ".9/098",  	       END_NODE_TYPE_INTEGER, SNMP_NODE_INSTANCE_READ_WRITE);
   addNewNodeToMIB(testMIB, ".9.098.0.",       END_NODE_TYPE_INTEGER, SNMP_NODE_INSTANCE_READ_WRITE); 
   
 #warning "Test MIB is Created! Undefine SNMP_TEST constant at "SNMPsDriver.hpp"
   
   return true;
}        
    
void SNMPsDriver::snmpDriverExampleLoop()
{         
   static bool firstRearm = true;
   if(firstRearm)
   {
      firstRearm = false;
      commandQueue->getTimer().setPeriod(2000);
      commandQueue->getTimer().rearm();  
   }
   
   if((commandQueue->getTimer().isArmed()) &&  (commandQueue->getTimer().ready()))	
   {      
      VarData newVal;   
      memoryAvaliable = newVal.dwordVal = xPortGetFreeHeapSize();
      updateEndNoteValue(".1.3.6.8000.1000.0", newVal); 
      
      // Проверка флага разрешения ловли //    
      EndNodeValueType* endValue = findEndValuePointer(".1.3.6.8000.2000.0");
      if(endValue->value.dwordVal == 1) {
	 enableTraps();     	 
      } else {
	 disableTraps();     
      }
      
      sendTrap(".1.3.6.8000.1000.0");  // количество свободной памяти в куче  
      commandQueue->getTimer().rearm();  
   }  
}
		 
void stub(void)
{
   // Пришли сюды - ништяк) Смотрим стек вызовов)) /
   asm(" BKPT 255");
}
#endif   
