#include <driver.hpp>

const Driver::ThreadSafeFn DriverCommand::driverMethods[dctMax] = {
    &Driver::tagChanged, &Driver::setTagRuntimeParameter,
    &Driver::setRuntimeParameter};

void Driver::driverThreadProc(void *pData)
{
  Driver *drv = reinterpret_cast<Driver*>(pData);
  DriverCommand cmd;
  while (1)
  {
    if (drv->commandQueue->pop(&cmd))
      cmd.execute();
    drv->run();
  }
}

void Driver::initialize()
{
  commandQueue = new Driver::QueType(queueDepth, name.c_str(), scanRate);
  xTaskCreate(Driver::driverThreadProc, ("thr" + name).c_str(), stackDepth,
              this, tskIDLE_PRIORITY + 3, &threadHandle);
}

void Driver::postTagChanged(const Tag &tag, VarData newData, uint32_t ts)
{
  DriverCommand cmd{this, &tag, newData, ts};
  commandQueue->push(&cmd);
}

void Driver::postSetRuntimeParameter(uint32_t paramId, VarData paramValue)
{
  DriverCommand cmd{this, paramId, paramValue};
  commandQueue->push(&cmd);
}

void Driver::postSetTagRuntimeParameter(Tag *tag, uint32_t paramId,
                                        VarData paramValue)
{
  DriverCommand cmd{this, tag, paramId, paramValue};
  commandQueue->push(&cmd);
}
