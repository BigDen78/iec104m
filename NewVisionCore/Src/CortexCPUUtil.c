#include <CortexCPUUtil.h>
#include <FreeRTOS.h>
#include <task.h>
#include <stdint.h>

#define TRC_REG_DEMCR (*(volatile uint32_t*)0xE000EDFC)
#define TRC_REG_DWT_CTRL (*(volatile uint32_t*)0xE0001000)
#define TRC_REG_DWT_CYCCNT (*(volatile uint32_t*)0xE0001004)
#define TRC_REG_DWT_EXCCNT (*(volatile uint32_t*)0xE000100C)

#define TRC_REG_ITM_LOCKACCESS (*(volatile uint32_t*)0xE0001FB0)
#define TRC_ITM_LOCKACCESS_UNLOCK (0xC5ACCE55)
/* Bit mask for TRCENA bit in DEMCR - Global enable for DWT and ITM */
#define TRC_DEMCR_TRCENA (1 << 24)

/* Bit mask for NOPRFCNT bit in DWT_CTRL. If 1, DWT_EXCCNT is not supported */
#define TRC_DWT_CTRL_NOPRFCNT (1 << 24)

/* Bit mask for NOCYCCNT bit in DWT_CTRL. If 1, DWT_CYCCNT is not supported */
#define TRC_DWT_CTRL_NOCYCCNT (1 << 25)

/* Bit mask for EXCEVTENA_ bit in DWT_CTRL. Set to 1 to enable DWT_EXCCNT */
#define TRC_DWT_CTRL_EXCEVTENA (1 << 18)

/* Bit mask for EXCEVTENA_ bit in DWT_CTRL. Set to 1 to enable DWT_CYCCNT */
#define TRC_DWT_CTRL_CYCCNTENA (1)


typedef struct tskTaskControlBlock
{
	volatile StackType_t	*pxTopOfStack;	/*< Points to the location of the last item placed on the tasks stack.  THIS MUST BE THE FIRST MEMBER OF THE TCB STRUCT. */

#if ( portUSING_MPU_WRAPPERS == 1 )
	xMPU_SETTINGS	xMPUSettings;		/*< The MPU settings are defined as part of the port layer.  THIS MUST BE THE SECOND MEMBER OF THE TCB STRUCT. */
#endif

	ListItem_t			xStateListItem;	/*< The list that the state list item of a task is reference from denotes the state of that task (Ready, Blocked, Suspended ). */
	ListItem_t			xEventListItem;		/*< Used to reference a task from an event list. */
	UBaseType_t			uxPriority;			/*< The priority of the task.  0 is the lowest priority. */
	StackType_t			*pxStack;			/*< Points to the start of the stack. */
	char				pcTaskName[ configMAX_TASK_NAME_LEN ];/*< Descriptive name given to the task when created.  Facilitates debugging only. */ /*lint !e971 Unqualified char types are allowed for strings and single characters only. */

#if ( ( portSTACK_GROWTH > 0 ) || ( configRECORD_STACK_HIGH_ADDRESS == 1 ) )
	StackType_t		*pxEndOfStack;		/*< Points to the highest valid address for the stack. */
#endif

#if ( portCRITICAL_NESTING_IN_TCB == 1 )
	UBaseType_t		uxCriticalNesting;	/*< Holds the critical section nesting depth for ports that do not maintain their own count in the port layer. */
#endif

#if ( configUSE_TRACE_FACILITY == 1 )
	UBaseType_t		uxTCBNumber;		/*< Stores a number that increments each time a TCB is created.  It allows debuggers to determine when a task has been deleted and then recreated. */
	UBaseType_t		uxTaskNumber;		/*< Stores a number specifically for use by third party trace code. */
#endif

#if ( configUSE_MUTEXES == 1 )
	UBaseType_t		uxBasePriority;		/*< The priority last assigned to the task - used by the priority inheritance mechanism. */
	UBaseType_t		uxMutexesHeld;
#endif

#if ( configUSE_APPLICATION_TASK_TAG == 1 )
	TaskHookFunction_t pxTaskTag;
#endif

#if( configNUM_THREAD_LOCAL_STORAGE_POINTERS > 0 )
	void			*pvThreadLocalStoragePointers[ configNUM_THREAD_LOCAL_STORAGE_POINTERS ];
#endif

#if( configGENERATE_RUN_TIME_STATS == 1 )
	uint32_t		ulRunTimeCounter;	/*< Stores the amount of time the task has spent in the Running state. */
#endif

#if ( configUSE_NEWLIB_REENTRANT == 1 )
	/* Allocate a Newlib reent structure that is specific to this task.
	Note Newlib support has been included by popular demand, but is not
	used by the FreeRTOS maintainers themselves.  FreeRTOS is not
	responsible for resulting newlib operation.  User must be familiar with
	newlib and must provide system-wide implementations of the necessary
	stubs. Be warned that (at the time of writing) the current newlib design
	implements a system-wide malloc() that must be provided with locks. */
	struct	_reent xNewLib_reent;
#endif

#if( configUSE_TASK_NOTIFICATIONS == 1 )
	volatile uint32_t ulNotifiedValue;
	volatile uint8_t ucNotifyState;
#endif

	/* See the comments above the definition of
	tskSTATIC_AND_DYNAMIC_ALLOCATION_POSSIBLE. */
#if( tskSTATIC_AND_DYNAMIC_ALLOCATION_POSSIBLE != 0 ) /*lint !e731 Macro has been consolidated for readability reasons. */
	uint8_t	ucStaticallyAllocated; 		/*< Set to pdTRUE if the task is a statically allocated to ensure no attempt is made to free the memory. */
#endif

#if( INCLUDE_xTaskAbortDelay == 1 )
	uint8_t ucDelayAborted;
#endif

} tskTCB;

typedef tskTCB TCB_t;
static volatile unsigned int uxPercentLoadCPU                   = (unsigned int) 0U; // Last CPU load calculated
static volatile unsigned int uxIdleTickCount                    = (unsigned int) 0U; // How many ticks were in idle task
static volatile unsigned int uxCPULoadCount                     = (unsigned int) 0U; // For 0 to configTICKRATE_HZ we will count idle tasks
static volatile uint32_t uxIdleIn = 0;
extern TCB_t * volatile pxCurrentTCB;

void vTraceInitCortexM()
{
	/* Ensure that the DWT registers are unlocked and can be modified. */
	TRC_REG_ITM_LOCKACCESS = TRC_ITM_LOCKACCESS_UNLOCK;
	/* Make sure DWT is enabled, if supported */
	TRC_REG_DEMCR |= TRC_DEMCR_TRCENA;
	do
	{
		/* Verify that DWT is supported */
		if (TRC_REG_DEMCR == 0)
			break;
		/* Verify that DWT_CYCCNT is supported */
		if (TRC_REG_DWT_CTRL & TRC_DWT_CTRL_NOCYCCNT)
			break;
		/* Reset the cycle counter */
		TRC_REG_DWT_CYCCNT = 0;
		/* Enable the cycle counter */
		TRC_REG_DWT_CTRL |= TRC_DWT_CTRL_CYCCNTENA;
	} while(0);	/* breaks above jump here */
}

unsigned int xLoadPercentCPU (void)
{
	return uxPercentLoadCPU;
}


__weak void vApplicationTickHook( void )
{
	/* This function will be called by each tick interrupt if
	configUSE_TICK_HOOK is set to 1 in FreeRTOSConfig.h. User code can be
	added here, but the tick hook is called from an interrupt context, so
	code must not attempt to block, and only the interrupt safe FreeRTOS API
	functions can be used (those that end in FromISR()). */
	/* In the top of where the timer tick enters */
	//TCB_t * pxTCB;

	uxCPULoadCount++;
	if (uxCPULoadCount == configTICK_RATE_HZ)
	{
		uint32_t uxCPUTicks = TRC_REG_DWT_CYCCNT;
		TRC_REG_DWT_CYCCNT = 0;
		uxCPULoadCount = 0;
		uxPercentLoadCPU = 100 - ((uint64_t)uxIdleTickCount * 100 / uxCPUTicks);
		uxIdleTickCount = 0;
		uxIdleIn = 0;
	}
}

void _traceTASK_SWITCHED_OUT()
{
	if (*(uint32_t *)(pxCurrentTCB->pcTaskName) == 0x454C4449) //IDLE
	{
		uint32_t uxIdleOut = TRC_REG_DWT_CYCCNT;
		uxIdleTickCount += (uxIdleOut - uxIdleIn);
	}
}

extern void _traceTASK_SWITCHED_IN()
{
	if (*(uint32_t *)(pxCurrentTCB->pcTaskName) == 0x454C4449) //IDLE
	{
		uxIdleIn = TRC_REG_DWT_CYCCNT;
	}
}
