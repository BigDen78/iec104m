#include <utils.hpp>
/*
#include "Allocator.h"

__no_init uint8_t ccram_heap[CCRAM_POOL_SIZE] @".ccram";

static volatile uint32_t uCCRAMOffset = 0;

extern "C" uint32_t AtomicAddAndFetch(volatile uint32_t *ptr, uint32_t value);

void *ccramAlloc(size_t n)
{
	AtomicAddAndFetch(&uCCRAMOffset, n);
	if (uCCRAMOffset <= CCRAM_POOL_SIZE)
		return (void *)(ccram_heap + uCCRAMOffset - n);
	else
		return nullptr;
}
 */
/*
DataType DataTypeFromTypeName(const String &typeName)
{
	if (!typeName.compare("Bool"))
		return dtBool;
	else if (!typeName.compare("Byte"))
		return dtByte;
	else if (!typeName.compare("Word"))
		return dtWord;
	else if (!typeName.compare("DWord"))
		return dtDWord;
	else if (!typeName.compare("ShortInt"))
		return dtShortInt;
	else if (!typeName.compare("Int"))
		return dtInt;
	else if (!typeName.compare("Float"))
		return dtFloat;
	else if (!typeName.compare("String"))
		return dtString;
	else if (!typeName.compare("WString"))
		return dtWString;
	return dtVoid;
}
*/
VarData getDataFromJson(DataType dataType, const Json &json)
{
	VarData data;
	switch (dataType)
	{
	case dtBool:
		data.boolVal = (bool)json.get<Json::boolean_t>();
		break;
	case dtByte:
		data.byteVal = (uint8_t)json.get<Json::number_unsigned_t>();
		break;
	case dtWord:
		data.wordVal = (uint16_t)json.get<Json::number_unsigned_t>();
		break;
	case dtDWord:
		data.dwordVal = (uint32_t)json.get<Json::number_unsigned_t>();
		break;
	case dtShortInt:
		data.shortVal = (int16_t)json.get<Json::number_integer_t>();
		break;
	case dtInt:
		data.intVal = (int32_t)json.get<Json::number_integer_t>();
		break;
	case dtFloat:
		data.fltVal = (float)json.get<Json::number_float_t>();
		break;
	case dtString:
	{
		auto strParam = json.get<Json::string_t>();
		data.pStr = new char[strParam.length() + 1];
		strncpy(data.pStr, strParam.c_str(), strParam.length());
		data.pStr[strParam.length()] = '\0';
	}
	break;
	}
	return data;
}

void writeTagValueToJson(Json &j, const Tag &t)
{
	switch (t.getType())
	{
	case dtBool:
		j[t.getName()] = t.get<bool>();
		break;
	case dtByte:
		j[t.getName()] = t.get<uint8_t>();
		break;
	case dtWord:
		j[t.getName()] = t.get<uint16_t>();
		break;
	case dtDWord:
		j[t.getName()] = t.get<uint32_t>();
		break;
	case dtShortInt:
		j[t.getName()] = t.get<int16_t>();
		break;
	case dtInt:
		j[t.getName()] = t.get<int32_t>();
		break;
	case dtFloat:
		j[t.getName()] = t.get<float>();
		break;
	}
}
