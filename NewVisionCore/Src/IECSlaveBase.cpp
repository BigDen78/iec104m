#include <IECSlaveBase.hpp>

using IOATree = std::map<uint32_t, Tag *, std::greater<uint32_t>>;
using FTTree = std::map<IEC60870_5_TypeID, IOATree, std::greater<IEC60870_5_TypeID>>;

const ParameterVector IECSContext::parameters{
	{"spontType", {pidSpontType, dtByte, StaticParameterInfo::pfNone}},
	{"interType", {pidInterType, dtByte, StaticParameterInfo::pfNone}},
	{"cmdType", {pidCmdType, dtByte, StaticParameterInfo::pfNone}},
	{"IOA", {pidIOA, dtDWord, StaticParameterInfo::pfRequired}}};

void printCP56Time2a(CP56Time2a time)
{
	printf("%02i:%02i:%02i %02i/%02i/%04i", CP56Time2a_getHour(time),
				 CP56Time2a_getMinute(time),
				 CP56Time2a_getSecond(time),
				 CP56Time2a_getDayOfMonth(time),
				 CP56Time2a_getMonth(time),
				 CP56Time2a_getYear(time) + 2000);
}

IEC60870_5_TypeID typeIdFromByte(uint8_t typeId, TagParamIDs paramId, bool &error)
{
	switch (paramId)
	{
	case pidSpontType:
		if (M_SP_TB_1 == typeId || M_DP_TB_1 == typeId || M_BO_TB_1 == typeId ||
				M_ME_TD_1 == typeId || M_ME_TE_1 == typeId || M_ME_TF_1 == typeId)
		{
			error = false;
			return static_cast<IEC60870_5_TypeID>(typeId);
		}
		break;
	case pidInterType:
		if (M_SP_NA_1 == typeId || M_DP_NA_1 == typeId || M_BO_NA_1 == typeId ||
				M_ME_NA_1 == typeId || M_ME_NB_1 == typeId || M_ME_NC_1 == typeId)
		{
			error = false;
			return static_cast<IEC60870_5_TypeID>(typeId);
		}
		break;
	case pidCmdType:
		if (C_SC_NA_1 == typeId || C_DC_NA_1 == typeId || C_BO_NA_1 == typeId ||
				C_SE_NA_1 == typeId || C_SE_NB_1 == typeId || C_SE_NC_1 == typeId)
		{
			error = false;
			return static_cast<IEC60870_5_TypeID>(typeId);
		}
		break;
	}
	error = true;
	return static_cast<IEC60870_5_TypeID>(typeId);
}

IEC60870_5_TypeID giTypeFromSpontType(IEC60870_5_TypeID spontType)
{
	switch (spontType)
	{
	case M_SP_TB_1:
		return M_SP_NA_1;
	case M_DP_TB_1:
		return M_DP_NA_1;
	case M_ME_TE_1:
		return M_ME_NB_1;
	case M_ME_TF_1:
		return M_ME_NC_1;
	case C_SE_NB_1:
		return M_ME_NB_1;
	case C_SE_NC_1:
		return M_ME_NC_1;
	}
	return (IEC60870_5_TypeID)0;
}

bool IECSContext::setParameterById(uint32_t paramId, VarData newValue)
{
	bool error = false;
	auto tagParamId = static_cast<TagParamIDs>(paramId);
	switch (tagParamId)
	{
	case pidSpontType:
		spontType = typeIdFromByte(newValue.byteVal, tagParamId, error);
		return !error;
	case pidInterType:
		interType = typeIdFromByte(newValue.byteVal, tagParamId, error);
		return !error;
	case pidCmdType:
		cmdType = typeIdFromByte(newValue.byteVal, tagParamId, error);
		return !error;
	case pidIOA:
		ioa = newValue.dwordVal;
		return true;
	}
	return false;
}

bool IECSlaveBase::setTagRuntimeParameter(const Tag &tag, uint32_t paramId, VarData paramValue)
{
	return false;
}

VarData IECSlaveBase::getTagRuntimeParameter(const Tag &tag, uint32_t paramId)
{
	VarData result;
	result.dwordVal = 0;
	return result;
}

DriverContext *IECSlaveBase::createTagContext() const
{
	auto ret = new IECSContext;
	if (ret)
	{
		ret->cmdType = static_cast<IEC60870_5_TypeID>(0);
		ret->interType = ret->cmdType;
		ret->spontType = ret->cmdType;
	}
	return ret;
}

void IECSlaveBase::registerTag(const Tag &tag, void *context)
{
	auto ctx = reinterpret_cast<IECSContext *>(context);
	ioaMap[ctx->ioa] = const_cast<Tag *>(&tag);
}

void IECSlaveBase::buildInterrogationList()
{
	FTTree ftTree;

	for (auto iter : ioaMap)
	{
		auto ctx = reinterpret_cast<IECSContext *>(iter.second->getDriverSpecificInfo(id));
		if (ctx->interType)
			ftTree[ctx->interType][ctx->ioa] = iter.second;
	}

	for (auto iter : ftTree)
	{
		interList.push_front({iter.first, TagList()});
		for (auto iter1 : iter.second)
		{
			interList.front().second.push_front(iter1.second);
		}
	}
}

bool IECSlaveBase::clockSyncHandler(IMasterConnection connection, CS101_ASDU asdu, CP56Time2a newTime)
{
	printf("Process time sync command with time ");
	printCP56Time2a(newTime);
	printf("\n");

	uint64_t newSystemTimeInMs = CP56Time2a_toMsTimestamp(newTime);

	/* Set time for ACT_CON message */
	CP56Time2a_setFromMsTimestamp(newTime, Hal_getTimeStampInMs());

	/* update system time here */
	Hal_setTimeInMs(newSystemTimeInMs);

	return true;
}

bool IECSlaveBase::interrogationHandler(IMasterConnection connection, CS101_ASDU asdu, uint8_t qoi)
{
	//General Interrogation
	if (20 == qoi)
	{
		CS101_AppLayerParameters alParams = IMasterConnection_getApplicationLayerParameters(connection);
		//send positive confirmation  (false in third parameter means positive!!!)
		IMasterConnection_sendACT_CON(connection, asdu, false);
		for (auto& itType : interList)
		{
			auto newAsdu = CS101_ASDU_create(alParams, false, CS101_COT_INTERROGATED_BY_STATION, 0, this->asduAddress, false, false);
			auto typeId = itType.first;
			for (auto& itTag : itType.second)
			{
				if (!addIOFromTagToASDU(newAsdu, typeId, *itTag)) //if no place left for IO in ASDU
				{
					//send filled ASDU
					IMasterConnection_sendASDU(connection, newAsdu);
					//clear ASDU
					CS101_ASDU_removeAllElements(newAsdu);
					//try insert io again
					addIOFromTagToASDU(newAsdu, typeId, *itTag);
				}
			}
			if (CS101_ASDU_getNumberOfElements(newAsdu))
			{
				IMasterConnection_sendASDU(connection, newAsdu);
			}
			CS101_ASDU_destroy(newAsdu);
		}
		IMasterConnection_sendACT_TERM(connection, asdu);
	}
	else
	{
		//we only support General Interrogation,
		//interrogation request for any other group responded with negative confirmation
		//(true in third parameter means negative!!!)
		IMasterConnection_sendACT_CON(connection, asdu, true);
	}
	return true;
}

bool IECSlaveBase::asduHandler(IMasterConnection connection, CS101_ASDU asdu)
{
	if (CS101_ASDU_getCOT(asdu) == CS101_COT_ACTIVATION)
	{
		InformationObject io = CS101_ASDU_getElement(asdu, 0);
		auto ioa = InformationObject_getObjectAddress(io);
		auto itTag = ioaMap.find(ioa);
		if (ioaMap.end() != itTag)
		{
			auto ctx = reinterpret_cast<IECSContext *>(itTag->second->getDriverSpecificInfo(id));
			if (ctx->cmdType == CS101_ASDU_getTypeID(asdu))
			{
				if (executeTagAssignmentFromIO(io, ctx->cmdType, *(itTag->second)))
					CS101_ASDU_setCOT(asdu, CS101_COT_ACTIVATION_CON);
				else
					CS101_ASDU_setCOT(asdu, CS101_COT_ACTIVATION_TERMINATION);
			}
			else
				CS101_ASDU_setCOT(asdu, CS101_COT_UNKNOWN_TYPE_ID);
		}
		else
			CS101_ASDU_setCOT(asdu, CS101_COT_UNKNOWN_IOA);
    InformationObject_destroy(io);
	}
	else
		CS101_ASDU_setCOT(asdu, CS101_COT_UNKNOWN_COT);
	IMasterConnection_sendASDU(connection, asdu);
	return true;
}

bool IECSlaveBase::addIOFromTagToASDU(CS101_ASDU asdu, IEC60870_5_TypeID typeId, const Tag &tag)
{
	uint8_t createBuf[64]; //temporary stack buffer for info obj initia
	QualityDescriptor qd = tag.getValid() ? IEC60870_QUALITY_GOOD : IEC60870_QUALITY_NON_TOPICAL;
	int ioa = reinterpret_cast<IECSContext *>(tag.getDriverSpecificInfo(id))->ioa;
	InformationObject io = nullptr;
	switch (typeId)
	{
	case M_SP_NA_1:
		io = (InformationObject)SinglePointInformation_create((SinglePointInformation)createBuf, ioa, tag.get<bool>(), qd);
		break;
	case M_DP_NA_1:
		io = (InformationObject)DoublePointInformation_create((DoublePointInformation)createBuf, ioa, (DoublePointValue)tag.get<uint8_t>(), qd);
		break;
	case M_ME_NB_1:
		io = (InformationObject)MeasuredValueScaled_create((MeasuredValueScaled)createBuf, ioa, tag.get<int>(), qd);
		break;
	case M_ME_NC_1:
		io = (InformationObject)MeasuredValueShort_create((MeasuredValueShort)createBuf, ioa, tag.get<float>(), qd);
		break;
	}
	if (io)
		return CS101_ASDU_addInformationObject(asdu, io);
	else
		//if we failed to initialize io from tag we return 'true' here
		//this unobvious choice is dictated by semanticts of return value whithin which
		//'false' is meaning 'no more place in asdu'
		return true;
}

bool IECSlaveBase::addIOWithTSFromTagToASDU(CS101_ASDU asdu, IEC60870_5_TypeID typeId, const Tag &tag, VarData newValue, uint32_t shortTS)
{
	uint8_t createBuf[64]; //temporary stack buffer for info obj initialization
	QualityDescriptor qd = tag.getValid() ? IEC60870_QUALITY_GOOD : IEC60870_QUALITY_NON_TOPICAL;
	int ioa = reinterpret_cast<IECSContext *>(tag.getDriverSpecificInfo(id))->ioa;
	//uint32_t ts tag
	sCP56Time2a ts;

	CP56Time2a_createFromMsTimestamp(&ts, shortTSToLongTS(shortTS));
	InformationObject io = nullptr;
	switch (typeId)
	{
	case M_SP_TB_1:
		io = (InformationObject)SinglePointWithCP56Time2a_create((SinglePointWithCP56Time2a)createBuf, ioa, newValue.boolVal, qd, &ts); // tag.get<bool>()
		break;
	case M_DP_TB_1:
		io = (InformationObject)DoublePointWithCP56Time2a_create((DoublePointWithCP56Time2a)createBuf, ioa, (DoublePointValue)newValue.byteVal, qd, &ts); // tag.get<uint8_t>()
		break;
	case M_ME_TE_1:
		io = (InformationObject)MeasuredValueScaledWithCP56Time2a_create((MeasuredValueScaledWithCP56Time2a)createBuf, ioa, newValue.intVal, qd, &ts); // tag.get<int>()
		break;
	case M_ME_TF_1:
		io = (InformationObject)MeasuredValueShortWithCP56Time2a_create((MeasuredValueShortWithCP56Time2a)createBuf, ioa, newValue.fltVal, qd, &ts); // tag.get<float>()
		break;
	}
	if (io)
		return CS101_ASDU_addInformationObject(asdu, io);
	else
		//if we failed to initialize io from tag we return 'true' here
		//this unobvious choice is dictated by semanticts of return value whithin which
		//'false' is meaning 'no more place in asdu'
		return true;
}

bool IECSlaveBase::executeTagAssignmentFromIO(InformationObject io, IEC60870_5_TypeID typeId, Tag &tag)
{
	switch (typeId)
	{
	case C_SC_NA_1:
		{
			auto sc = reinterpret_cast<SingleCommand>(io);
			Core::instance().set(tag, SingleCommand_getState(sc), true, -1);
		}
		return true;
	case C_DC_NA_1:
		{
			auto dc = reinterpret_cast<DoubleCommand>(io);
			Core::instance().set<uint8_t>(tag, DoubleCommand_getState(dc), true, -1);
		}
		return true;
	case C_BO_NA_1:
		{
			auto bsc = reinterpret_cast<Bitstring32Command>(io);
			Core::instance().set(tag, Bitstring32Command_getValue(bsc), true, -1);
		}
		return true;
	case C_SE_NA_1:
		{
			auto scn = reinterpret_cast<SetpointCommandNormalized>(io);
			Core::instance().set(tag, SetpointCommandNormalized_getValue(scn), true, -1);
		}
		return true;
	case C_SE_NB_1:
		{
			auto scs = reinterpret_cast<SetpointCommandScaled>(io);
			Core::instance().set(tag, SetpointCommandScaled_getValue(scs), true, -1);
		}
		return true;
	case C_SE_NC_1:
		{
			auto scs = reinterpret_cast<SetpointCommandShort>(io);
			Core::instance().set(tag, SetpointCommandShort_getValue(scs), true, -1);
		}
		return true;
	}
	return false;
}
