#include <map>
#include <core.hpp>
#include <GPIODriver.hpp>
#include <DriverBuilder.hpp>

REGISTER_FACTORY_METHOD(DriverBuilder::driverFactoryTable, Driver, GPIODriver, new GPIODriver);

using GpioPortMap = std::map<char, GPIO_TypeDef *>;

const ParameterVector GPIOTagDriverContext::parameters{{"type", {gtpidType, dtString, StaticParameterInfo::pfRequired}},
                                                       {"port", {gtpidPort, dtString, StaticParameterInfo::pfRequired}},
                                                       {"pin", {gtpidPin, dtByte, StaticParameterInfo::pfRequired}},
                                                       {"pull", {gtpidPull, dtString, StaticParameterInfo::pfNone}},
                                                       {"debounce", {gtpidDebounce, dtWord, StaticParameterInfo::pfRuntimeChangeable}},
                                                       {"impulse", {gtpidImpulse, dtWord, StaticParameterInfo::pfRuntimeChangeable}},
                                                       {"invert", {gtpidInvert, dtBool, StaticParameterInfo::pfRebootRequired | StaticParameterInfo::pfRuntimeChangeable}}};

GPIODriver::GPIODriver() : ParameterizedObject(ParameterizedObject::NO_PARAMETERS)
{
}

uint16_t gpioPin[] = {
    GPIO_PIN_0,
    GPIO_PIN_1,
    GPIO_PIN_2,
    GPIO_PIN_3,
    GPIO_PIN_4,
    GPIO_PIN_5,
    GPIO_PIN_6,
    GPIO_PIN_7,
    GPIO_PIN_8,
    GPIO_PIN_9,
    GPIO_PIN_10,
    GPIO_PIN_11,
    GPIO_PIN_12,
    GPIO_PIN_13,
    GPIO_PIN_14,
    GPIO_PIN_15};

bool GPIOTagDriverContext::setParameterById(uint32_t paramId, VarData newValue)
{
  auto tagParamId = static_cast<GPIOTagParamIDs>(paramId);
  switch (tagParamId)
  {
  case gtpidType:
    if ('O' == newValue.pStr[0])
      initInfo.Mode = GPIO_MODE_OUTPUT_PP;
    else if ('I' == newValue.pStr[0])
      initInfo.Mode = GPIO_MODE_INPUT;
    else
      return false;
    break;
  case gtpidPort:
  {
#ifdef OLD_F407
    GpioPortMap gpioPortMap = {{'A', GPIOA},
                               {'B', GPIOB},
                               {'C', GPIOC},
                               {'D', GPIOD},
                               {'E', GPIOE},
                               {'F', GPIOF},
                               {'G', GPIOG},
                               {'H', GPIOH},
                               {'I', GPIOI},
                               /*{'J', GPIOJ},
                               {'K', GPIOK}*/};
#else
     
    GpioPortMap gpioPortMap = {{'A', GPIOA},
                               {'B', GPIOB},
                               {'C', GPIOC},
                               {'D', GPIOD},
                               {'E', GPIOE},
                               {'F', GPIOF},
                               {'G', GPIOG},
                               {'H', GPIOH},
                               {'I', GPIOI},
                               {'J', GPIOJ},
                               {'K', GPIOK}};
    
#endif //OLD_F407
    
    GpioPortMap::iterator itPort;
    if ((itPort = gpioPortMap.find(newValue.pStr[0])) != gpioPortMap.end())
    {
      port = itPort->second;
    }
    else
      return false;
    break;
  }
  case gtpidPin:
    if (newValue.byteVal < 16)
    {
      initInfo.Pin = gpioPin[newValue.byteVal];
    }
    else
      return false;
    break;
  case gtpidPull:
    if ('U' == newValue.pStr[0])
      initInfo.Pull = GPIO_PULLUP;
    else if ('D' == newValue.pStr[0])
      initInfo.Pull = GPIO_PULLDOWN;
    else
      initInfo.Pull = GPIO_NOPULL;
    break;
  case gtpidDebounce:
    debounce = newValue.wordVal;
    break;
  case gtpidImpulse:
    impulse = newValue.wordVal;
    break;
  case gtpidInvert:
    invert = newValue.boolVal;
    break;
  default:
    break;
  }
  return true;
}

bool GPIODriver::setTagRuntimeParameter(const Tag &tag, uint32_t paramId, VarData paramValue)
{
  GPIOTagDriverContext *entry = reinterpret_cast<GPIOTagDriverContext *>(tag.getDriverSpecificInfo(getId()));
  switch (paramId)
  {
  case gtpidDebounce:
    entry->debounce = paramValue.wordVal;
    break;
  case gtpidImpulse:
    entry->impulse = paramValue.wordVal;
    break;
  case gtpidInvert:
    entry->invert = paramValue.boolVal;
  default:
    break;
  }
  return true;
}

VarData GPIODriver::getTagRuntimeParameter(const Tag &tag, uint32_t paramId)
{
  VarData result;
  GPIOTagDriverContext *entry = reinterpret_cast<GPIOTagDriverContext *>(tag.getDriverSpecificInfo(getId()));
  switch (paramId)
  {
  case gtpidDebounce:
    result.wordVal = entry->debounce;
    break;
  case gtpidImpulse:
    result.wordVal = entry->impulse;
    break;
  case gtpidInvert:
    result.boolVal = entry->invert;
  default:
    break;
  }
  return result;
}

DriverContext *GPIODriver::createTagContext() const
{
  return new GPIOTagDriverContext;
}

void GPIODriver::registerTag(const Tag &tag, void *ctx)
{
  GPIOTagDriverContext *entry = reinterpret_cast<GPIOTagDriverContext *>(ctx);
  entry->pTag = &(const_cast<Tag &>(tag));
  entry->initInfo.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(entry->port, &(entry->initInfo));

  if (GPIO_MODE_INPUT == entry->initInfo.Mode)
    gpioInputs.push_front(entry);
  else
    gpioOutputs.push_back(entry);
}

bool GPIODriver::tagChanged(const Tag &tag, uint32_t paramId, VarData newValue)
{
  GPIOTagDriverContext *entry = reinterpret_cast<GPIOTagDriverContext *>(tag.getDriverSpecificInfo(getId()));
  if (entry->initInfo.Mode == GPIO_MODE_OUTPUT_PP)
  {
    entry->pinState = newValue.boolVal ? GPIO_PIN_SET : GPIO_PIN_RESET; /// non-invert output
    HAL_GPIO_WritePin(entry->port, entry->initInfo.Pin, entry->pinState);
    if (false == entry->invert)
    {
      if (GPIO_PIN_RESET == entry->pinState) /* if invert OFF impulse is only for GPIO_PIN_SET */
      {
        entry->timerOn = false;
        return true;
      }
    }
    else
    {
      if (GPIO_PIN_SET == entry->pinState) /* if invert ON impulse is only for GPIO_PIN_RESET */
      {
        entry->timerOn = false;
        return true;
      }
    }

    if (entry->impulse > 0)
    {
      if (true == entry->timerOn)
      {
        OutputList::iterator iter = std::find_if(gpioOutputs.begin(), gpioOutputs.end(), [&](auto it) -> bool {
          return (it->pTag == entry->pTag);
        });
        (*iter)->timer = clock() + (clock_t)(*iter)->impulse;
      }
      else
      {
        gpioOutputs.push_back(entry);
        entry->timerOn = true;
        entry->timer = clock() + (clock_t)entry->impulse;
      }
    }
  }
  return true;
}

void GPIODriver::initialize()
{
  Driver::initialize();

  bool boolVal;

  for (auto it : gpioOutputs) /* Set start values for outputs */
  {
    it->pinState = it->invert ? GPIO_PIN_SET : GPIO_PIN_RESET;
    HAL_GPIO_WritePin(it->port, it->initInfo.Pin, it->pinState);
    boolVal = it->pinState ? true : false;
    Core::set(*it->pTag, boolVal, true, getId());
    gpioOutputs.pop_front();
  }

  for (auto it : gpioInputs) /* Get start values from inputs */
  {
    GPIO_PinState currentPinState = HAL_GPIO_ReadPin(it->port, it->initInfo.Pin);
    if (it->invert)
      boolVal = currentPinState ? false : true;
    else
      boolVal = currentPinState ? true : false;
    Core::set(*it->pTag, boolVal, true, getId());
  }
}

void GPIODriver::run()
{
  bool boolVal;
  for (auto it : gpioInputs)
  {
    GPIO_PinState currentPinState = HAL_GPIO_ReadPin(it->port, it->initInfo.Pin);
    if (it->pinState != currentPinState) /* Changed? */
    {
      it->pinState = currentPinState ? GPIO_PIN_SET : GPIO_PIN_RESET;

      if (it->timerOn)
      {
        it->timerOn = false;
      }
      else /* Start timer */
      {
        it->timerOn = true;
        it->timer = clock() + (clock_t)it->debounce;
      }
    }
    else
    {
      if ((it->timerOn) && (clock() > it->timer))
      {
        it->timerOn = false;
        if (it->invert)
        {
          boolVal = currentPinState ? false : true;
        }
        else
        {
          boolVal = currentPinState ? true : false;
        }
        Core::set(*it->pTag, boolVal, true, getId());
      }
    }
  }

  gpioOutputs.erase(remove_if(gpioOutputs.begin(), 
			      gpioOutputs.end(),
			      [&](auto it) -> bool {
				 if (false == it->timerOn) /* PinState was changed to non-impulse before timeout has ended */
				    return true;
				 else
				 {
				    if (clock() > it->timer)
				    {
				       it->timerOn = false;
				       it->pinState = it->invert ? GPIO_PIN_SET : GPIO_PIN_RESET;
				       HAL_GPIO_WritePin(it->port, it->initInfo.Pin, it->pinState);
				       boolVal = it->pinState ? true : false;
				       Core::set(*it->pTag, boolVal, true, getId());
				       return true;
				    }
				 }
				 return false;
			      }),
  gpioOutputs.end());
}
