#include "Core.hpp"
#include "LogicDriver.hpp"
#include "DriverBuilder.hpp"
#include "ParameterizedObject.hpp"

REGISTER_FACTORY_METHOD(DriverBuilder::driverFactoryTable, Driver, LogicDriver, new LogicDriver);

const ParameterVector LogicDriver::parameters
{
  {"spontaneousTime",{timer, dtWord,  StaticParameterInfo::pfRuntimeChangeable | StaticParameterInfo::pfRequired}},
};

LogicDriver::LogicDriver() : ParameterizedObject(LogicDriver::parameters){}

void LogicDriver::registerTag(const Tag &tag, void *ctx)
{
}

bool LogicDriver::tagChanged(const Tag &tag, uint32_t, VarData newValue)
{
  return true;
}

bool LogicDriver::setRuntimeParameter(const Tag &tag, uint32_t paramId, VarData paramValue)
{
  switch (static_cast<LogicParams>(paramId))
  {
  case timer:
    spontaneousTime = paramValue.wordVal;
    break;
  }
  return true;
}

bool LogicDriver::setParameterById(uint32_t id, VarData newValue)
{
  switch (static_cast<LogicParams>(id))
  {
  case timer:
    spontaneousTime = newValue.wordVal;
    return true;
    break;
  }
  return false;
}

DriverContext* LogicDriver::createTagContext() const
{
   return new LogicDriverTagContext;
}


bool LogicDriver::setTagRuntimeParameter(const Tag &tag, uint32_t paramId, VarData paramValue)
{
  return false;
}

VarData LogicDriver::getRuntimeParameter(uint32_t paramId)
{
  VarData result;
  result.dwordVal = 0;
  return result;
}

VarData LogicDriver::getTagRuntimeParameter(const Tag &tag, uint32_t paramId)
{
  VarData result;
  result.dwordVal = 0;
  return result;
}

void LogicDriver::initialize()
{
  Driver::initialize();

  commandQueue->getTimer().setPeriod(spontaneousTime);
  commandQueue->getTimer().rearm();
}

void LogicDriver::run()
{
   if(commandQueue->getTimer().getPeriod()!=spontaneousTime)
   {
      commandQueue->getTimer().setPeriod(spontaneousTime);
   }

   if((commandQueue->getTimer().isArmed()) &&  (commandQueue->getTimer().ready()))
   {                 
     //static uint16_t val = 0;

#ifdef __USE_SLAVE_JSON
     
 //  Random Inc 
     uint16_t x = rand() % 16;
     uint16_t y = rand() % 16;
     
     char TagName[11] = "Pixel_0000";
     char XVal[3];
     char YVal[3]; 
     if (x < 10) sprintf(&XVal[0], "0%i", x); else sprintf(&XVal[0], "%i", x);
     if (y < 10) sprintf(&YVal[0], "0%i", y); else sprintf(&YVal[0], "%i", y);
     
     TagName[6] = XVal[0];
     TagName[7] = XVal[1];
     TagName[8] = YVal[0];
     TagName[9] = YVal[1];
     
     const String TagName_(TagName);
     Tag* tmp = Core::instance().getTag(TagName_);
     uint16_t tmp2 = tmp->getValue().wordVal;
     tmp2++;
     Core::setIfChanged(TagName,tmp2,true,id); // for Master
     
#endif

      
#if defined(__USE_MASTER_JSON) || defined(__USE_MASTER_JSON2)
     
     for (int16_t x = 0; x < 16; x++)
       for (int16_t y = 15; y >= 0; y--) {
         
         char TagName[11] = "Pixel_0000";
         char XVal[3];
         char YVal[3]; 
         if (x < 10) sprintf(&XVal[0], "0%i", x); else sprintf(&XVal[0], "%i", x);
         if (y < 10) sprintf(&YVal[0], "0%i", y); else sprintf(&YVal[0], "%i", y);
         
         TagName[6] = XVal[0];
         TagName[7] = XVal[1];
         TagName[8] = YVal[0];
         TagName[9] = YVal[1];
         
         const String TagName_(TagName);
         Tag* tmp = Core::instance().getTag(TagName_);
         uint16_t tmp2 = tmp->getValue().wordVal;
         Core::setIfChanged(TagName,0,true,id); // for Master
         
         if ((y < 15) && (tmp2 != 0)) 
         {
            tmp2--;
            if (x < 10) sprintf(&XVal[0], "0%i", x); else sprintf(&XVal[0], "%i", x);
            if (y + 1 < 10) sprintf(&YVal[0], "0%i", y + 1); else sprintf(&YVal[0], "%i", y + 1);
             
            TagName[6] = XVal[0];
            TagName[7] = XVal[1];
            TagName[8] = YVal[0];
            TagName[9] = YVal[1];
             
            const String TagName_(TagName);
            Core::setIfChanged(TagName,tmp2,true,id); // for Master
         }
       }
     
#endif     
     
     commandQueue->getTimer().rearm();
   }         

}