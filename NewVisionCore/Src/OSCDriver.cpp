#include <map>
#include "Core.hpp"
#include "OSCDriver.hpp"
#include "DriverBuilder.hpp"

REGISTER_FACTORY_METHOD(DriverBuilder::driverFactoryTable, Driver, OSCDriver, new OSCDriver);

enum ParamIDs : uint32_t
{
  otpidOffset,       ///< Offset from trigger to history in percent from 0 to 20 inclusive 
  otpidFilesAmount,  ///< Number of files with raw oscillograms in ringbuffer 
  otpidLogSize       ///< oscillo can be 100 - 3000 ms
};

enum OSCTagParamIDs : uint32_t
{
  otpidTrig,     ///< Trigger or not, if not it will be only added in oscillogram 
  otpidCondition ///< Trigger condition -> true (1) - work on rising edge, false (0) on falling edge
};

const ParameterVector OSCDriver::myParameters
{
  {"offset",       { otpidOffset, dtByte, StaticParameterInfo::pfNone }},
  {"filesAmount",  { otpidFilesAmount, dtByte, StaticParameterInfo::pfNone }}
};

const ParameterVector OSCTagDriverContext::myParameters
{
  {"trig",      { otpidTrig, dtBool, StaticParameterInfo::pfRequired }},
  {"condition", { otpidCondition, dtBool, StaticParameterInfo::pfNone }}
};

OSCDriver::OSCDriver() : ParameterizedObject(myParameters),
                         runState(rsInitializing) { }

bool OSCDriver::setParameterById(uint32_t paramId, VarData newValue)
{
  switch(static_cast<ParamIDs>(paramId))
	{
	case otpidOffset:
      offset = newValue.byteVal;
      return true;
  case otpidFilesAmount:
      filesAmount = newValue.byteVal;
      return true;
  default:
    break;
	}
	return false;
}

bool OSCTagDriverContext::setParameterById(uint32_t paramId, VarData newValue)
{
  auto tagParamId = static_cast<OSCTagParamIDs>(paramId);
  switch (tagParamId)
  {
  case otpidTrig:
    trig = newValue.boolVal;
    break;
  case otpidCondition:
    condition = newValue.boolVal;
    break;
  default:
    break;
  }
  return true;
}

void OSCDriver::registerTag(const Tag &tag, void *ctx)
{
  // OSCTagDriverContext *entry = reinterpret_cast<OSCTagDriverContext *>(ctx);
  // discreteTags.push_front(entry);
}

DriverContext *OSCDriver::createTagContext() const
{
  return new OSCTagDriverContext;
}

void OSCDriver::startOSC()
{
  HistoricalData *pData = new HistoricalData;
  pData->pData = nullptr;
  
  uint64_t ts = getLongTS() - 1;
  
  for(auto it : providerMap)
  {
    //pData->pTagList = it.second;
    Core::instance().getDrivers()[it.first]->getHistoricalData(ts, pData);
  }
  
  if(pData->pData)
    delete pData->pData;
  delete pData; 
}

bool OSCDriver::tagChanged(const Tag &tag, uint32_t paramId, VarData newValue)
{
  if(runState == rsRunning)
  {
    OSCTagDriverContext *entry = reinterpret_cast<OSCTagDriverContext *>(tag.getDriverSpecificInfo(getId()));
    if(entry->trig)
    {
        startOSC();
    }
    else 
    {

    }
  }
  return true;
}

void OSCDriver::initialize()
{
  auto driverCount = Core::instance().getDrivers().size();
  
  for(auto it : Core::instance().getTags())
  {
    if (auto ctx = it->getDriverSpecificInfo(this->id))
    {
      for(auto driverId =0; driverId < driverCount; driverId++)
      {
        if(it->getDriverSpecificInfo(driverId) && Core::instance().getDrivers()[driverId ]->supportsHistory())
        {
          reinterpret_cast<OSCTagDriverContext*>(ctx)->historyProvider=
            Core::instance().getDrivers()[driverId ];
          providerMap[driverId].push_front(it);
        }
      }    
    }   
  }
  
  Driver::initialize();
  
  commandQueue->getTimer().setPeriod(500);
  commandQueue->getTimer().rearm();
}

void OSCDriver::run()
{
  switch(runState)
	{
	case rsInitializing:
    if(commandQueue->getTimer().ready())
		{
			runState = rsRunning;
		}
		break;
	case rsRunning:
		break;
  }
}
