#include <map>
#include "Core.hpp"
#include "MBSlaveDriver.hpp"
#include "DriverBuilder.hpp"

#define CMD_IDLE 1u

REGISTER_FACTORY_METHOD(DriverBuilder::driverFactoryTable, Driver, MBSlaveDriver, new MBSlaveDriver);

enum ParamIDs : uint32_t // <- Параметры драйвера
{
  mbspidLinkAddress, 
  mbspidUART, 
  mbspidBaudRate, 
  mbspidStopBits, 
  mbspidParity
};

enum MBSlaveTagParamIDs : uint32_t // <- Параметры тега
{
  mbstpidType, 
  mbspidRegAddr
};

bool MBDatabaseType::addNewRegister(RegisterType type, uint32_t address, SpanRankEnum spanRank, Tag* tagPtr)
{  
  if(type != UNDEFINED)
  {
    (*dataBaseArray[type])[address].first = tagPtr;
    (*dataBaseArray[type])[address].second = spanRank;
    
    return true;
  }
  
  return NULL;
}

errorCode MBDatabaseType::getRegisterValue(RegisterType type, uint32_t address, uint16_t* valuePtr)
{  
  Tag* mbsTag = NULL;
  errorCode error = ILLEGAL_DATA_ADDRESS;
  
  auto iterator = dataBaseArray[type]->find(address);    
  if(iterator != dataBaseArray[type]->end())
  {
    mbsTag = iterator->second.first;

    if(mbsTag != NULL)
    {
      VarData value = mbsTag->getValue();  
      MBSlaveTagDriverContext* cntx = reinterpret_cast<MBSlaveTagDriverContext*> (mbsTag->getDriverSpecificInfo(ownerId));
      
      if(cntx != NULL)
      {
        if (sDouble == cntx->span)
        {
          SpanRankEnum partOfData = iterator->second.second;
          
          if(partOfData == sLo)
            *valuePtr = static_cast<uint16_t>(value.dwordVal & 0x0000FFFF);
          else if(partOfData == sHi)
            *valuePtr = static_cast<uint16_t>((value.dwordVal & 0xFFFF0000) >> 16);
        }
        else
        {
          *valuePtr = value.wordVal;
        }
        error = RETURN_OK;
      }
    }
  }
  else
  {
    *valuePtr = 0;
    error = RETURN_OK;
  }
  
  return error;
}

errorCode MBDatabaseType::changeRegisterValue(RegisterType type, uint16_t address, uint16_t rawValue)
{  
  Tag* mbsTag = NULL;
  errorCode error = ILLEGAL_DATA_ADDRESS;
  
  auto iterator = dataBaseArray[type]->find(address);

  if(iterator != dataBaseArray[type]->end())
  {
    mbsTag = iterator->second.first;

    if(mbsTag != NULL)
    {
      DataType tagType = mbsTag->getType();
      VarData value;

      MBSlaveTagDriverContext* cntx = reinterpret_cast<MBSlaveTagDriverContext*> (mbsTag->getDriverSpecificInfo(ownerId));

      if (cntx != NULL)
      {
        if(sDouble == cntx->span)
        {
          SpanRankEnum partOfData = iterator->second.second;
          
          if(sHi == partOfData)
          {
            cntx->data.wordmData[1] = static_cast<uint16_t>(rawValue);
            cntx->writeMask |= (1<<1);
          } 
          if(sLo == partOfData)
          {
            cntx->data.wordmData[0] = static_cast<uint16_t>(rawValue);
            cntx->writeMask |= (1<<0);
          }            
          if(cntx->writeMask == 0x03)
          {
            value.dwordVal = cntx->data.dwordmData;
            
            if(tagType == dtFloat)
              Core::setIfChanged(*mbsTag, value.fltVal, true, ownerId);
            else if(tagType == dtDWord)
              Core::setIfChanged(*mbsTag, value.dwordVal, true, ownerId);
            else if(tagType == dtInt)
              Core::setIfChanged(*mbsTag, value.intVal, true, ownerId);
            
            cntx->writeMask = 0x00;
          }
        }
        else
        {
          value.wordVal = static_cast<uint16_t>(rawValue);
          
          if (tagType == dtBool)
            Core::setIfChanged(*mbsTag, value.boolVal, true, ownerId);
          else if(tagType == dtByte)
            Core::setIfChanged(*mbsTag, value.byteVal, true, ownerId);
          else if(tagType == dtWord)
            Core::setIfChanged(*mbsTag, value.wordVal, true, ownerId);
          else if(tagType == dtShortInt)
            Core::setIfChanged(*mbsTag, value.shortVal, true, ownerId);
        }
        error = RETURN_OK;
      }
    }
  }

  return error;
}

errorCode MBMessageParser::modbusFrameParser(uint8_t* modbusFrame, uint32_t length, uint8_t* responsePtr)
{
  RegisterType dataType = getDataType(modbusFrame);
  errorCode error = ILLEGAL_FUNCTION;

  if(dataType != UNDEFINED)
  {
    uint16_t firstAddress = (modbusFrame[1] << 8) | modbusFrame[2];
    RequestType requestType = checkRequestType(modbusFrame);

    if(requestType == GET_REQUEST)
    {
      uint16_t regsAmount = (modbusFrame[3] << 8) | modbusFrame[4];

      pduBufferStruct.pduBufferPtr = this->responseBufferPtr + 1;
      error = processGetRequest(dataType, firstAddress, regsAmount, modbusFrame[0]);
    }
    else if (requestType == SET_SINGLE_REQUEST)
    {
      uint16_t regValue = (modbusFrame[3] << 8) | modbusFrame[4];

      pduBufferStruct.pduBufferPtr = this->responseBufferPtr + 1;
      error = processSetSingleRequest(dataType, modbusFrame[0], firstAddress, regValue); // <- теперь это другая функция !
    }
    else if (requestType == SET_MULTIPLE_REQUEST)
    {
      
      uint16_t regsAmount = (modbusFrame[3] << 8) | modbusFrame[4];

      pduBufferStruct.pduBufferPtr = this->responseBufferPtr + 1;       // <- сдвигаем указатель, т.к. первый байт в буфере-ответе для ID
      error = processSetMultipleRequest(dataType, modbusFrame[0], firstAddress, regsAmount, modbusFrame);  // <- теперь это другая функция !
    }
  }

  return error;
}

const ParameterVector MBSlaveDriver::myParameters
{
  {"linkAddress", { mbspidLinkAddress, dtDWord, StaticParameterInfo::pfRequired }},
  {"UART", { mbspidUART, dtByte, StaticParameterInfo::pfRequired }},
  {"baudRate", { mbspidBaudRate, dtDWord, StaticParameterInfo::pfRequired }},
  {"stopBits", { mbspidStopBits, dtByte, StaticParameterInfo::pfRequired }},
  {"parity", { mbspidParity, dtString, StaticParameterInfo::pfRequired }}
};

const ParameterVector MBSlaveTagDriverContext::myParameters
{
  {"type",       { mbstpidType,      dtString, StaticParameterInfo::pfRequired }},
  {"regAddr",    { mbspidRegAddr,    dtDWord,  StaticParameterInfo::pfRequired }}
};

MBSlaveDriver::MBSlaveDriver() : dcmdSerialIdle(this, CMD_IDLE, {0}), ParameterizedObject(MBSlaveDriver::myParameters){ }

bool MBSlaveDriver::setParameterById(uint32_t paramId, VarData newValue)
{
  switch(static_cast<ParamIDs>(paramId))
  {
  case mbspidLinkAddress:
    {
        linkAddress = newValue.dwordVal & 0xffffu;
        return true;
    }
  case mbspidUART:
    {
        if ((newValue.byteVal > 0) && (newValue.byteVal <= MAX_HUART) && (huarts[newValue.byteVal - 1]))
        {
            huart = huarts[newValue.byteVal - 1];
            return true;
        }
        return false;
    }
  case mbspidBaudRate:
    {
        baudRate = newValue.dwordVal;
        return true;
    }
  case mbspidStopBits:
    {
        if (1 == newValue.byteVal || 2 == newValue.byteVal)
        {
            stopBits = newValue.byteVal;
            return true;
        }
        return false;
    }
  case mbspidParity:
    {
        if ('E' == newValue.pStr[0] || 'O' == newValue.pStr[0] || 'N' == newValue.pStr[0])
        {
            parity = newValue.pStr[0];
            return true;
        }
        return false;
    }
  }
  return false;
}

bool MBSlaveTagDriverContext::setParameterById(uint32_t paramId, VarData newValue)
{
  auto tagParamId = static_cast<MBSlaveTagParamIDs>(paramId);
  bool result = true;
  
  switch (tagParamId)
  {
  case mbstpidType:
    {
      std::string typeStr(newValue.pStr);
      
      if ("DI" == typeStr)
      {
        type = DI;
      }
      else if ("AI" == typeStr)
      {
        type = AI;
      }
      else if ("DO" == typeStr)
      {
        type = DO;
      }
      else if ("AO" == typeStr)
      {
        type = AO;
      } 
      else 
      {
        result = false;
      }
   
      break;
    }
  case mbspidRegAddr:
    {
      address = newValue.dwordVal;
      
      // Че может пойти не так?
      break;
    }
  default:
    {
      result = false;
      break;
    }
  }
  
  return result;
}

void MBSlaveDriver::registerTag(const Tag &tag, void* ctx)
{
  if(&tag != NULL)
  {
    RegisterType type;
    uint32_t address;
    SpanRankEnum spanRank = sLo;
    Tag* tagPtr = const_cast <Tag *> (&tag);
    DataType tagType = tagPtr->getType();
    
    MBSlaveTagDriverContext* cntx = reinterpret_cast<MBSlaveTagDriverContext *> (ctx);
    if(cntx != NULL)
    {
      address = cntx->address;
      type = cntx->type;
                  
      if( (dtBool == tagType) || (dtByte == tagType) || (dtWord == tagType) || (dtShortInt == tagType))
      {
        this->regsBase.addNewRegister(type, address, spanRank, tagPtr);
        cntx->span = sSingle;
      }
      else if( (dtDWord == tagType) || (dtInt == tagType) || (dtFloat == tagType) )
      {
        this->regsBase.addNewRegister(type, address, spanRank, tagPtr);
        spanRank = sHi;
        this->regsBase.addNewRegister(type, address+1, spanRank, tagPtr);
        cntx->span = sDouble;
      }
    }
  }
}

DriverContext* MBSlaveDriver::createTagContext() const
{
  return new MBSlaveTagDriverContext;
}

bool MBSlaveDriver::tagChanged(const Tag &tag, uint32_t paramId, VarData newValue)
{
  return true;
}

bool MBSlaveDriver::setRuntimeParameter(const Tag &tag, uint32_t paramId, VarData paramValue)
{  
   if (CMD_IDLE == paramId) 
   {
      if(rsWaitingRequest == currentState)
      {    
        modbusRTUParser();
        return true;
      }
      else 
      {
	 
      }
   } 
	
   return false;  
}

void MBSlaveDriver::initialize()
{ 
  Driver::initialize(); // - инициализация
  
  currentState = rsInitializing;
  serialMBS = new Serial;  // - открытие порта
  regsBase.setOwnerId(id);
  // За это ответственен Tatarianec //
  if (serialMBS)
  {
    if(SERIAL_NO_ERROR == Serial_init(huart, baudRate, serialMBS))
    {
      if(SERIAL_NO_ERROR == Serial_setParity(serialMBS, parity))
      {
        if(SERIAL_NO_ERROR == Serial_setStopBits(serialMBS, stopBits))
        {          
          //SerialPort_subscribeToEvents(serialMBS, commandQueue->getHandle(), &dcmdSerialIdle, NULL);  // - подписка драйвера на событие "IDLE"
          Serial_setIdleQueue(serialMBS, commandQueue->getHandle(), &dcmdSerialIdle, NULL);
          Serial_startReceive(serialMBS);
          //currentState = rsRunning;
          currentState = rsWaitingRequest;    
          
          parser.regsDataBasePtr = &regsBase;
          parser.responseBufferPtr = responseBuffer;
        }
      }
    }       
  }
       //runState = rsUnrecoverableError;
}

void MBSlaveDriver::run()
{  
  return;
}

 /** Подсчет CRC16
  * Входные параметры:
  * - *buf - указатель на начало массива посылки (uint8)
  * - len - длина массива
  * Возвращаемое значение:
  * - uint16_t crc - возвращаемое значение CRC16
  **/
uint16_t MBSlaveDriver::getCRC(uint8_t *buf, uint8_t len)
{
  // Устанавливаем стартовое значение 
  unsigned int crc = 0xFFFF;
  // Подсчитываем crc 
  for (int pos = 0; pos < len; pos++){
    crc ^= (unsigned int)buf[pos];		// XOR byte into least sig. byte of crc 		
    for (int i = 8; i != 0; i--) { 		// Loop over each bit                	
      if ((crc & 0x0001) != 0) {   	// If the LSB is set 			
        crc >>= 1;         	// Shift right and XOR 0xA001     
        crc ^= 0xA001;
      }			
      else 				// Else LSB is not set       
        crc >>= 1;		// Just shift right 
    }
  }
  // Возвращаем значение crc16 
  return crc;
}

bool MBSlaveDriver::checkID(void)
{  
  if(linkAddress == serialMBS->rxBuffer[0])
  {
    return true;
  }
  else
  {
    return false;
  }
}

bool MBSlaveDriver::checkCRC(void)
{
  uint16_t receivedCRC = (dataBufferPtr[bufLen - 2] | (dataBufferPtr[bufLen - 1] << 8));
  uint16_t calculatedCRC = getCRC(dataBufferPtr, bufLen - 2);
 
  if (calculatedCRC == receivedCRC)
  {
    return true;
  }
  else
  {
    return false;
  }
}

void MBSlaveDriver::modbusRTUParser(void)
{  
  dataBufferPtr = (uint8_t *)serialMBS->rxBuffer;  
  bufLen = serialMBS->rxCount;
  errorCode error = RETURN_ERROR;  
  
  if(bufLen >= 8)
  {
    if( checkID() )
    {
      if( checkCRC() )
      {
        dataBufferPtr++;     
        error = parser.modbusFrameParser(dataBufferPtr, (bufLen - 3), responseBuffer);
        
        if(RETURN_OK == error)
        {        
          responseBuffer[0] = linkAddress;
          responseLen = parser.pduBufferStruct.pduLength;
          responseLen++; 
          
          uint16_t crc = getCRC(responseBuffer, responseLen);
          
          responseBuffer[responseLen] = (uint8_t)crc;
          responseLen++;
          responseBuffer[responseLen] = (uint8_t)(crc >> 8);
          
          responseLen++;        
          Serial_writeBytes(serialMBS, responseBuffer, responseLen);
          
          return;
        }
      }
    }
    else if( 0 == dataBufferPtr[0] )
    {
      if( checkCRC() )
      {
        dataBufferPtr++;
        RequestType type = parser.checkRequestType(dataBufferPtr);
        if( (SET_SINGLE_REQUEST == type) || (SET_MULTIPLE_REQUEST == type) )
        {
          parser.modbusFrameParser(dataBufferPtr, (bufLen - 3), responseBuffer);
          Serial_startReceive(serialMBS);
          return;
        }
        else
        {
          error = ILLEGAL_FUNCTION;
        }
      }
    }
  }
  
  sendErrorMessage(error);
}    

void MBSlaveDriver::sendErrorMessage(errorCode error)
{
  dataBufferPtr = (uint8_t *)serialMBS->rxBuffer;  
   
  responseBuffer[0] = dataBufferPtr[0];
  responseBuffer[1] = (uint8_t)(0x80 | dataBufferPtr[1]);
  responseBuffer[2] = error;
 
  uint16_t crc = MBSlaveDriver::getCRC(responseBuffer, 3);
  
  responseBuffer[3] = (uint8_t)crc;
  responseBuffer[4] = (uint8_t)(crc >> 8);
    
  Serial_writeBytes(serialMBS, responseBuffer, 5);
}

bool MBDatabaseType::checkRange(RegisterType regType, uint16_t firstReg, uint16_t lastReg)
{
  bool checkResult = false;
  
  for (auto address = firstReg; address < lastReg; ++address)
  {
    auto iterator = dataBaseArray[regType]->find(address);    
    if(iterator != dataBaseArray[regType]->end())
    {
      checkResult = true;
      break;
    }
  }
  
  return checkResult;
}

errorCode MBMessageParser::processGetRequest(RegisterType regType, uint16_t firstReg, uint16_t regsAmount, uint8_t funCode )
{  
  uint32_t currentRegAddr = firstReg;
  uint32_t lastRegAddr = firstReg + regsAmount;
  errorCode error = RETURN_ERROR;
  pduBufferStruct.pduBufferPtr[0] = funCode;
  uint16_t regValue;  
  
  if(DI == regType || DO == regType)
  {
    if(regsDataBasePtr->checkRange(regType, firstReg, lastRegAddr))
    {
      pduBufferStruct.pduBufferPtr[1] = (regsAmount / 8) + !!(regsAmount % 8); // <- сколько байт далее в передаче
    
      pduBufferStruct.pduLength = pduBufferStruct.pduBufferPtr[1] + 2;

      for(uint32_t i = 2; i < pduBufferStruct.pduLength; i++)  
      {
        pduBufferStruct.pduBufferPtr[i] = 0x00;
        for(uint32_t bitNum = 0; bitNum < 8; bitNum++)
        {          
          regsDataBasePtr->getRegisterValue(regType, currentRegAddr, &regValue);
          
          pduBufferStruct.pduBufferPtr[i] |= ((regValue & 0x0001) << bitNum);          
          currentRegAddr++;  

          if(currentRegAddr == lastRegAddr)
          {
            return RETURN_OK;
          }            
        }
      }
    }
    else
    {
      error = ILLEGAL_DATA_ADDRESS;
    }
  }
  else if(AI == regType || AO == regType)
  {
    if(regsDataBasePtr->checkRange(regType, firstReg, lastRegAddr))
    {  
      pduBufferStruct.pduLength = 2 + regsAmount * 2; 
      pduBufferStruct.pduBufferPtr[1] = regsAmount * 2;
      pduBufferStruct.pduBufferPtr += 2;

      for(; currentRegAddr < lastRegAddr; currentRegAddr++)
      {
        regsDataBasePtr->getRegisterValue(regType, currentRegAddr, &regValue);

        *pduBufferStruct.pduBufferPtr = regValue >> 8; 
        *(pduBufferStruct.pduBufferPtr + 1) = regValue;

        pduBufferStruct.pduBufferPtr += 2;  
      }
      error = RETURN_OK;
    }
    else
    {
      error = ILLEGAL_DATA_ADDRESS;
    }
  }
  
  return error; 
}

errorCode MBMessageParser::processSetSingleRequest(RegisterType regType, uint8_t funCode, uint16_t firstReg, uint16_t regValue)
{
  uint32_t currentRegAddr = (uint32_t)firstReg;
  errorCode error = RETURN_ERROR;
  pduBufferStruct.pduBufferPtr[0] = funCode;
  pduBufferStruct.pduBufferPtr[1] = (firstReg >> 8) & 0xFF;
  pduBufferStruct.pduBufferPtr[2] = firstReg & 0xFF;
  pduBufferStruct.pduLength = 5;
  
  if(forceSingleCoil == funCode)
  {
    uint16_t discreteValue;
    if(0xFF00 == regValue)
    {
      discreteValue = 1;
    }
    else if(0 == regValue)
    {
      discreteValue = 0;
    }
    else
    {
      error = ILLEGAL_DATA_VALUE;
      return error;
    } 
    
    error = regsDataBasePtr->changeRegisterValue(regType, currentRegAddr, discreteValue);
    if(RETURN_OK == error)
    { 
      pduBufferStruct.pduBufferPtr[3] = (regValue >> 8) & 0xFF;
      pduBufferStruct.pduBufferPtr[4] = regValue & 0xFF;
    }
  } 
  else if (presetSingleRegister == funCode)
  {    
    error = regsDataBasePtr->changeRegisterValue(regType, currentRegAddr, regValue);
    if(RETURN_OK == error)
    {
      pduBufferStruct.pduBufferPtr[3] = (regValue >> 8) & 0xFF;
      pduBufferStruct.pduBufferPtr[4] = regValue & 0xFF;
    }
  }
  
  return error; 
}

errorCode MBMessageParser::processSetMultipleRequest(RegisterType regType, uint8_t funCode, uint16_t firstReg, uint16_t regsAmount, uint8_t* modbusFrame)
{
  uint16_t currentRegAddr = firstReg;
  uint16_t lastRegAddr = firstReg + regsAmount;
  uint16_t currentByteCnt = 6;
  errorCode error = RETURN_ERROR;
  pduBufferStruct.pduBufferPtr[0] = funCode;
  pduBufferStruct.pduBufferPtr[1] = (firstReg >> 8) & 0xFF;
  pduBufferStruct.pduBufferPtr[2] = firstReg & 0xFF;
  pduBufferStruct.pduBufferPtr[3] = (regsAmount >> 8) & 0xFF;
  pduBufferStruct.pduBufferPtr[4] = regsAmount & 0xFF;
  pduBufferStruct.pduLength = 5;
  
  if(forceMultipleCoils == funCode)
  {
    if(regsDataBasePtr->checkRange(regType, firstReg, lastRegAddr))
    {
      bool currentRegValue;
      uint8_t currentBitCnt = 0;
      uint8_t currentByteCnt = 6;
      
      for(; currentRegAddr < lastRegAddr; currentRegAddr++)
      {       
        currentRegValue = modbusFrame[currentByteCnt] & (1 << currentBitCnt) ;

        regsDataBasePtr->changeRegisterValue(regType, currentRegAddr, currentRegValue);
        
        ++currentBitCnt;
        if(currentBitCnt > 7)
        {
          currentBitCnt = 0;
          ++currentByteCnt;
        }      
      }
      error = RETURN_OK;
    }
    else
    {
      error = ILLEGAL_DATA_ADDRESS;
    }
  }
  else if(presetMultipleRegisters == funCode)
  {  
    if(regsDataBasePtr->checkRange(regType, firstReg, lastRegAddr))
    {
      uint16_t currentRegValue;
      
      for(; currentRegAddr < lastRegAddr; currentRegAddr++)
      {  
        
        currentRegValue = (modbusFrame[currentByteCnt] << 8) | modbusFrame[currentByteCnt+1];
        
        regsDataBasePtr->changeRegisterValue(regType, currentRegAddr, currentRegValue);
        
        currentByteCnt+=2;
      }
      error = RETURN_OK;
    }
    else
    {
      error = ILLEGAL_DATA_ADDRESS;
    }
  }
  
  return error; 
}

RequestType MBMessageParser::checkRequestType(uint8_t *dataBuf)
{     
  if(dataBuf[0] >= readCoilStatus && dataBuf[0] <= readInputRegisters)
  {
    return GET_REQUEST;
  }
  
  if(forceSingleCoil == dataBuf[0] || presetSingleRegister == dataBuf[0])
  {
    return SET_SINGLE_REQUEST;
  }
  
  if(forceMultipleCoils == dataBuf[0] || presetMultipleRegisters == dataBuf[0])
  {
    return SET_MULTIPLE_REQUEST;
  }  
  
  return INVALID_REQUEST;
}

RegisterType MBMessageParser::getDataType(uint8_t *dataBuf)
{
    switch(dataBuf[0]) {
    case readCoilStatus:
        return DO;
      break;
    case readInputStatus:
        return DI;
      break;
    case readHoldingRegisters:
        return AO;
      break;
    case readInputRegisters:
        return AI;
      break;
    case forceSingleCoil:
        return DO;
      break;
    case presetSingleRegister:
        return AO;
      break;
    case forceMultipleCoils:
        return DO;
      break;
    case presetMultipleRegisters:
        return AO;
      break;
    default:
        return UNDEFINED;
      break;
    }
}