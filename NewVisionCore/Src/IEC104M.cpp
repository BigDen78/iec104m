#include <iec104m.hpp>
#include <hal_thread.h>
#include <DriverBuilder.hpp>
#include <ini.h>
#include <string>
#include <IEC101M.hpp>

#define CMD_IDLE 1u

// Settings:
// Project options\C/C++ Compiler\Preprocessor\Defined symbols
// +LWIP_DNS
// +LWIP_SOCKET
// +LWIP_IPV4

REGISTER_FACTORY_METHOD(DriverBuilder::driverFactoryTable, Driver, IEC104MasterDriver, new IEC104MDriver);
REGISTER_FACTORY_METHOD(DeviceBuilder::deviceFactoryTable, Device, IEC104GenericSlave, new IEC104GenericDevice);

//-- Json file params IDs
/*
enum ParamIDs : uint32_t
{
  pidASDUAddress,
//  pidSpontDelay,
  pidPort,
  pidIPAddr,
  pidGIInterval,
  pidTSInterval,
  pidCommWindow
}; 
*/

enum IEC104DeviceParamId : uint32_t
{
  idpidAsduAddress,
  idpidGIInterval,
  idpidTSInterval
//  idpidLinkAddress,
//  idpidCommWindow
};

enum IEC104DriverParamIDs : uint32_t
{
  pidPort,
  pidIPAddr,
  pidAsduAddress //,
//  pidGIInterval,
//  pidTSInterval
};

const ParameterVector IEC104GenericDevice::parameters
{
  {"asduAddress", {idpidAsduAddress, dtWord, StaticParameterInfo::pfNone}},
  {"giInterval", {idpidGIInterval, dtDWord, StaticParameterInfo::pfNone}},
  {"tsInterval", {idpidTSInterval, dtDWord, StaticParameterInfo::pfNone}} // ,
//  {"linkAddress", {idpidLinkAddress, dtWord, StaticParameterInfo::pfRequired}},
//  {"commWindow", {idpidCommWindow, dtWord, StaticParameterInfo::pfNone}}
};

//-- Json file params vector
const ParameterVector IEC104MDriver::parameters
{
  {"port", {pidPort, dtWord, StaticParameterInfo::pfNone}},
  {"IPAddr", {pidIPAddr, dtString, StaticParameterInfo::pfNone}},
  {"AsduAddress", {pidAsduAddress, dtWord, StaticParameterInfo::pfNone}},
//  {"giInterval", {pidGIInterval, dtWord, StaticParameterInfo::pfNone}},
//  {"tsInterval", {pidTSInterval, dtWord, StaticParameterInfo::pfNone}}
};


/*
bool IEC104MDriver::beginGI()
{
//  if (CS104_Master_isChannelReady(master, linkAddress))
  {
//    CS104_Master_useSlaveAddress(master, linkAddress);
    CS104_Connection_sendInterrogationCommand(master, CS101_COT_ACTIVATION, AsduAddress, IEC60870_QOI_STATION);
    scheduleNextGI();
    return true;
  }
  return false;
}

bool IEC104MDriver::issueTimeSync()
{

  if (CS101_Master_isChannelReady(master, linkAddress))
  {
    struct sCP56Time2a newTime;
    CS101_Master_useSlaveAddress(master, linkAddress);
    CP56Time2a_createFromMsTimestamp(&newTime, Hal_getTimeStampInMs());
    CS101_Master_sendClockSyncCommand(master, 1, &newTime);
    scheduleNextTS();
    return true;
  }
  return false;
}
*/

//-- set Parameter from Json file by Id
bool IEC104MDriver::setParameterById(uint32_t paramId, VarData newValue)
{
  switch (paramId) // static_cast<ParamIDs>(
  {
	case pidPort:
		port = newValue.wordVal;
		return true;
	case pidIPAddr:
		IPAddr = newValue.pStr;
		return true;
	case pidAsduAddress:
		AsduAddress = newValue.wordVal;
		return true;
/*        case pidGIInterval:
                giInterval = newValue.wordVal;
		return true;
        case pidTSInterval:
                tsInterval = newValue.wordVal;
		return true; */
  }
  return false;
}

bool IEC104MDriver::setRuntimeParameter(const Tag &tag, uint32_t paramId, VarData paramValue)
{
  // do not understand what is it !!!
  return false;
}

/*
void IEC104MDriver::setRedGroups()
{
	char buf[32];
	char ipNum[4];
	mINI::INIFile file(redPath);
	mINI::INIStructure ini;
	if (file.read(ini))
	{
		CS104_RedundancyGroup redGroup;
		for (uint8_t redGroupNumber = 1; redGroupNumber <= 4; redGroupNumber++)
		{
			snprintf(buf, 32, "RedGroup%d", redGroupNumber);

			if (ini.has(buf))
			{
				redGroup = CS104_RedundancyGroup_create(buf);
				for (int ipIndex = 1; ipIndex <= 4; ipIndex++)
				{
					snprintf(ipNum, 4, "ip%d", ipIndex);
					if (ini[buf].has(ipNum))
					{
						std::string &ipAddr = ini[buf][ipNum];
            if(!validateIP4Dotted(ipAddr.c_str()))
						  CS104_RedundancyGroup_addAllowedClient(redGroup, ipAddr.c_str());
					}
				}
				CS104_Slave_addRedundancyGroup(slave, redGroup);
			}
		}
	}
}
*/


// extern "C" uint8_t validateIP4Dotted(const char *s);

void IEC104MDriver::initialize()
{
	Driver::initialize();
//--	buildInterrogationList(); //-- what is it and why?

        master = CS104_Connection_create(IPAddr.c_str(), port); //++

        bool temp = devicesList->empty();
        WorkDevice = 0;
        for (auto itDev : *devicesList)
        {
          itDev->initialize(master);
          WorkDevice = d_cast<IEC104GenericDevice *>(itDev);
        }        

        CS104_Connection_setConnectionHandler(master, ConnectionHandlerStatic, this); //++
        CS104_Connection_setASDUReceivedHandler(master, IEC104MDriver::asduReceivedHandlerStatic, this); //++

        /* uncomment to log messages */
        //CS104_Connection_setRawMessageHandler(con, rawMessageHandler, NULL);

        bool ConnectRes =  CS104_Connection_connect(master);
	if (ConnectRes  && master)
	{
                CS104_Connection_sendStartDT(master); //++
          
//--		CS104_Slave_setServerMode(slave, CS104_MODE_MULTIPLE_REDUNDANCY_GROUPS);
//--		setRedGroups();
//--		CS104_RedundancyGroup rg_all = CS104_RedundancyGroup_create("catch-all");
//--		CS104_Slave_addRedundancyGroup(slave, rg_all);
//--		auto alParameters = CS104_Slave_getAppLayerParameters(slave);
//--		spontASDU = CS101_ASDU_create(alParameters, false, CS101_COT_SPONTANEOUS, 0, asduAddress, false, false);
//--		currentSpontType = IEC60870_5_TypeID::M_SP_NA_1;
//--		commandQueue->getTimer().setPeriod(spontDelay);
//--		CS104_Slave_setMaxOpenConnections(slave, 4);
//--		CS104_Slave_setClockSyncHandler(slave, IEC104MDriver::clockSyncHandlerStatic, this);
//--		CS104_Slave_setInterrogationHandler(slave, IEC104MDriver::interrogationHandlerStatic, this);
//--		CS104_Slave_setASDUHandler(slave, IEC104MDriver::asduHandlerStatic, this);
//--		CS104_Slave_start(slave);
                
                commandQueue->getTimer().setPeriod(20);
                commandQueue->getTimer().rearm();
                
		runState = rsRunning;
		return;
	}

        commandQueue->getTimer().setPeriod(5000);
        commandQueue->getTimer().rearm();
	runState = rsInitializationErr; // rsUnrecoverableError;
}

InformationObject IEC104MDriver::createIOFromTagUpdate(const Tag &tag, VarData newValue)
{
  auto ctx = reinterpret_cast<IECSContext *>(tag.getDriverSpecificInfo(getId()));
  InformationObject io = nullptr;
  switch (ctx->cmdType)
  {
  case C_SC_NA_1:
    io = (InformationObject)SingleCommand_create(NULL, ctx->ioa, newValue.boolVal, false, 0);
    break;
  case C_DC_NA_1:
    io = (InformationObject)DoubleCommand_create(NULL, ctx->ioa, newValue.intVal, false, 0);
    break;
  case C_SE_NB_1:
    io = (InformationObject)SetpointCommandScaled_create(NULL, ctx->ioa, newValue.intVal, false, 0);
    break;
  case C_SE_NC_1:
    io = (InformationObject)SetpointCommandShort_create(NULL, ctx->ioa, newValue.fltVal, false, 0);
    break;
  default:
    break;
  }
  return io;
}

bool IEC104MDriver::tagChanged(const Tag &tag, uint32_t paramId /*shortTS*/, VarData newValue)
{
  auto device = tag.getDevice();
  if (device)
  {
    auto iecDevice = d_cast<IEC104GenericDevice*>(device);
    if(device->tagChanged(tag, paramId, newValue)) {}
      waitingCommandsCnt++;
  } /* else {
   
    // if (LL_STATE_ERROR == this->linkState) return false;
    auto io = createIOFromTagUpdate(tag, newValue);
    CS104_Connection_sendProcessCommandEx(master, CS101_COT_ACTIVATION, AsduAddress, io);
    InformationObject_destroy(io);
    
  } */
  return true;
}

void IEC104MDriver::run()
{
  static int test = 1;
  test++;
  
  
	switch (runState)
	{
	case rsInitial:
	//	runState = rsUnrecoverableError; /* can't get here in normal circumstances */
		break;
//	case rsInitializing:
//		break;
	case rsRunning:
                  if (commandQueue->getTimer().isLate())
                  {
                    commandQueue->getTimer().rearm();
                    //comIdle = false; 
                    if (WorkDevice) {
                      if(waitingCommandsCnt)
                      {

                        if (WorkDevice->sendQueuedCommand()) waitingCommandsCnt--; else runState = rsInitializationErr;
                      } 
                      else
                      {
                        if (WorkDevice->giCondition())
                        {
                          WorkDevice->beginGI();
                        }
                        else if (WorkDevice->tsCondition())
                        {
                          WorkDevice->issueTimeSync();
                        }
                        else
                        {
  //                        CS101_Master_pollSingleSlave(master, WorkDevice-> getLinkAddress());
                        }
                      } 
                    } 
                    /*
                    else {                      
                        if (giCondition())
                        {
                          beginGI();
                        }
                        else if (tsCondition())
                        {
                          issueTimeSync();
                        }
                    } */
//                    CS101_Master_run(master);
                  }
          
		break;
		break;
//--	case rsGI:
//--		break;
//--	case rsFileTransfer:
//--		break;
//--	case rsRecoverableError:
//--		break;
	case rsInitializationErr:
          
          
                if (commandQueue->getTimer().isLate())
                {
                    bool ConnectRes =  CS104_Connection_connect(master);
                    if (ConnectRes  && master)
                    {
                        CS104_Connection_sendStartDT(master); //++
                        commandQueue->getTimer().setPeriod(20);
                        commandQueue->getTimer().rearm();
                        runState = rsRunning;
                        return;
                    } else commandQueue->getTimer().rearm();
                }
		break;
	}
}

bool IEC104MDriver::registerDevice(Device *device, void *ctx)
{
//  WorkDevice = reinterpret_cast<IEC104GenericDevice *>(device);
//--  VarData linkAddr = NO_DATA;
//--  if (device->getParameterById(idpidLinkAddress, linkAddr))
//--  {
//--    deviceMap[linkAddr.dwordVal] = d_cast<IEC101GenericDevice *>(device);
//--    //reinterpret_cast<IEC101GenericDevice *>(device->getUnderlyingObject());
   return true;
//--  }
//-- return false;
}

bool IEC104MDriver::asduReceivedHandlerStatic(void *parameter, int address, CS101_ASDU asdu)
{
    IEC104MDriver *test = reinterpret_cast<IEC104MDriver *>(parameter);
  
    return test->WorkDevice->handleIncomingData(asdu);
    
//    return reinterpret_cast<IEC104MDriver *>(parameter)->WorkDevice->handleIncomingData(asdu);
           
//--          DeviceMap &map = reinterpret_cast<IEC104MDriver *>(parameter)->deviceMap;
//--          auto itDevice = map.find(address);
//--          if (map.end() != itDevice)
//--            return itDevice->second->handleIncomingData(asdu);
//--          return false;
}

void IEC104MDriver::ConnectionHandlerStatic(void* parameter, CS104_Connection connection, CS104_ConnectionEvent event)
{
   
//  connection
    event = (CS104_ConnectionEvent)1;
            
//--          DeviceMap &map = reinterpret_cast<IEC104MDriver *>(parameter)->deviceMap;
//--          auto itDevice = map.find(address);
//--          if (map.end() != itDevice)
//--            return itDevice->second->handleIncomingData(asdu);
//--          return false;
}

// --------------------------------------------------------------------------


const IEC104GenericDevice::IOHandlerFn IEC104GenericDevice::ioHandlers[128]
{
  0,
  &IEC104GenericDevice::handleMSPNA1,           //    M_SP_NA_1 = 1,
  0,           																	//    M_SP_TA_1 = 2,
  &IEC104GenericDevice::handleMDPNA1,           //    M_DP_NA_1 = 3,
  0,           																	//    M_DP_TA_1 = 4,
  0,           																	//    M_ST_NA_1 = 5,
  0,           																	//    M_ST_TA_1 = 6,
  0,           																	//    M_BO_NA_1 = 7,
  0,           																	//    M_BO_TA_1 = 8,
  0,           																	//    M_ME_NA_1 = 9,
  0,           																	//    M_ME_TA_1 = 10,
  &IEC104GenericDevice::handleMMENB1,           //    M_ME_NB_1 = 11,
  0,           																	//    M_ME_TB_1 = 12,
  &IEC104GenericDevice::handleMMENC1,           //    M_ME_NC_1 = 13,
  0,           																	//    M_ME_TC_1 = 14,
  0,           																	//    M_IT_NA_1 = 15,
  0,           																	//    M_IT_TA_1 = 16,
  0,           																	//    M_EP_TA_1 = 17,
  0,           																	//    M_EP_TB_1 = 18,
  0,           																	//    M_EP_TC_1 = 19,
  0,           																	//    M_PS_NA_1 = 20,
  0,           																	//    M_ME_ND_1 = 21,
  0,           																	//22
  0,           																	//23
  0,           																	//24
  0,           																	//25
  0,           																	//26
  0,           																	//27
  0,           																	//28
  0,           																	//29
  &IEC104GenericDevice::handleMSPTB1,           //    M_SP_TB_1 = 30,
  &IEC104GenericDevice::handleMDPTB1,           //    M_DP_TB_1 = 31,
  0,           																	//    M_ST_TB_1 = 32,
  0,           																	//    M_BO_TB_1 = 33,
  0,           																	//    M_ME_TD_1 = 34,
  &IEC104GenericDevice::handleMMETE1,           //    M_ME_TE_1 = 35,
  &IEC104GenericDevice::handleMMETF1,           //    M_ME_TF_1 = 36,
  0,           																	//    M_IT_TB_1 = 37,
  0,           																	//    M_EP_TD_1 = 38,
  0,           																	//    M_EP_TE_1 = 39,
  0,           																	//    M_EP_TF_1 = 40,
  0,           																	//41
  0,           																	//42
  0,           																	//43
  0,           																	//44
  0,           																	//    C_SC_NA_1 = 45,
  0,           																	//    C_DC_NA_1 = 46,
  0,           																	//    C_RC_NA_1 = 47,
  0,           																	//    C_SE_NA_1 = 48,
  0,           																	//    C_SE_NB_1 = 49,
  0,           																	//    C_SE_NC_1 = 50,
  0,           																	//    C_BO_NA_1 = 51,
  0,           																	//52
  0,           																	//53
  0,           																	//54
  0,           																	//55
  0,           																	//56
  0,           																	//57
  0,           																	//    C_SC_TA_1 = 58,
  0,           																	//    C_DC_TA_1 = 59,
  0,           																	//    C_RC_TA_1 = 60,
  0,           																	//    C_SE_TA_1 = 61,
  0,           																	//    C_SE_TB_1 = 62,
  0,           																	//    C_SE_TC_1 = 63,
  0,           																	//    C_BO_TA_1 = 64,
  0,           																	//65
  0,           																	//66
  0,           																	//67
  0,           																	//68
  0,           																	//69
  0,           																	//    M_EI_NA_1 = 70,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,            //80
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,            //90
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  &IEC104GenericDevice::handleCICNA1,           //    C_IC_NA_1 = 100,
  0,           																	//    C_CI_NA_1 = 101,
  0,           																	//    C_RD_NA_1 = 102,
  0,           																	//    C_CS_NA_1 = 103,
  0,           																	//    C_TS_NA_1 = 104,
  0,           																	//    C_RP_NA_1 = 105,
  0,           																	//    C_CD_NA_1 = 106,
  0,           																	//    C_TS_TA_1 = 107,
  0,           																	//108
  0,           																	//109
  0,           																	//    P_ME_NA_1 = 110,
  0,           																	//    P_ME_NB_1 = 111,
  0,           																	//    P_ME_NC_1 = 112,
  0,           																	//    P_AC_NA_1 = 113,
  0,           																	//114
  0,           																	//115
  0,           																	//116
  0,           																	//117
  0,           																	//118
  0,           																	//119
  0,           																	//    F_FR_NA_1 = 120,
  0,           																	//    F_SR_NA_1 = 121,
  0,           																	//    F_SC_NA_1 = 122,
  0,           																	//    F_LS_NA_1 = 123,
  0,           																	//    F_AF_NA_1 = 124,
  0,           																	//    F_SG_NA_1 = 125,
  0,           																	//    F_DR_TA_1 = 126,
  0										                          //    F_SC_NB_1 = 127
};

/**
* @brief  handleIncomingData - common logic for all types of received ASDU
* @param  void *pData
* @retval true - OK, false - OK
*/
bool IEC104GenericDevice::handleIncomingData(void *pData)
{
  bool result = false;
  auto asdu = reinterpret_cast<CS101_ASDU>(pData);
  IOHandlerFn fn = ioHandlers[CS101_ASDU_getTypeID(asdu)];
  if (!fn)
    return false;
  auto cot = CS101_ASDU_getCOT(asdu);
  int index = 0;
  for (; index < CS101_ASDU_getNumberOfElements(asdu); index++)
  {
    auto io = CS101_ASDU_getElement(asdu, index);
    auto ioa = InformationObject_getObjectAddress(io);
    if (0 != ioa)
    {
      auto itTag = ioaMap.find(ioa);
      if (ioaMap.end() != itTag)
        result |= (this->*fn)(*itTag->second, io, cot);
    }
    else
    {
      result |= (this->*fn)(Tag::NoTagRef, io, cot);
    }
    InformationObject_destroy(io);
  }
  return result;
}

void IEC104GenericDevice::handleLinkLayerMessage(void *pMessage)
{
/*  LinkLayerState lls = (LinkLayerState)((uint32_t)pMessage);
  linkState = lls;
  if (LL_STATE_ERROR == linkState)
  {
    runState = rsOffline;
    offlineCheckTS = getShortTS() + 20000;
    writeQueue.clear();
    for (auto it : ioaMap)
    {
      Core::instance().setTag(it.second, it.second->getValue(), false, id);
    }
  } */
}

bool IEC104GenericDevice::setParameterById(uint32_t id, VarData newValue)
{
  auto paramId = static_cast<IEC104DeviceParamId>(id);
  switch (paramId)
  {
      case idpidAsduAddress:
        asduAddress = newValue.wordVal;
        return true;
      case idpidGIInterval:
        giInterval = newValue.dwordVal * 1000;
        return true;
      case idpidTSInterval:
        tsInterval = newValue.dwordVal * 1000;
        return true;
/*      case idpidLinkAddress:
        linkAddress = newValue.wordVal;
        return true; */
/*      case idpidCommWindow:
        commWindow = newValue.dwordVal;
        return true; */
  }
  return false;
}

bool IEC104GenericDevice::getParameterById(uint32_t id, VarData &newValue) const
{
  auto paramId = static_cast<IEC104DeviceParamId>(id);
  switch (paramId)
  {
  case idpidAsduAddress:
    newValue.wordVal = asduAddress;
    return true;
  case idpidGIInterval:
    newValue.dwordVal = giInterval / 1000;
    return true;
  case idpidTSInterval:
    newValue.dwordVal = tsInterval / 1000;
    return true;
/*  case idpidLinkAddress:
    newValue.wordVal = linkAddress;
    return true; */
/*  case idpidCommWindow:
    newValue.dwordVal = commWindow;
    return true; */
  }
  return false;
}

bool IEC104GenericDevice::registerTag(const Tag &tag, void *context)
{
  auto ctx = reinterpret_cast<IECSContext *>(context);
  ioaMap[ctx->ioa] = const_cast<Tag *>(&tag);
  return true;
}

void IEC104GenericDevice::initialize(void *pvParameter) //+Add a new slave connection
{
  
  if (rsInitial == runState)
  {
    auto master = reinterpret_cast<CS104_Connection>(pvParameter);
    //+Add a new slave connection
    //-- CS101_Master_addSlave(master, linkAddress);
    this->master = master;
    //-- auto llParameters = CS101_Master_getLinkLayerParameters(master);
    //-- if (llParameters->timeoutForAck > commWindow)
    //--   commWindow = llParameters->timeoutForAck;
    runState = rsUnselected;
  }
}

void IEC104GenericDevice::select(void *pvParameter)
{
  //	timer = reinterpret_cast<QTimer *>(pvParameter);
/*--  
  if(rsSelected != runState && rsInitial != runState && rsOffline != runState)
  { */
//    CS101_Master_useSlaveAddress(master, linkAddress);
   /* //timer->setPeriod(commWindow);
    //timer->rearm(); */
    runState = rsSelected;
  /*}*/
}

bool IEC104GenericDevice::sendQueuedCommand()
{
  if (CS104_Connection_sendProcessCommandEx(master, CS101_COT_ACTIVATION, asduAddress, writeQueue.front())) {
    InformationObject_destroy(writeQueue.front());
    writeQueue.pop_front();
    return true;
  } else return false;
}

/**
* @brief  Sends GI command
* @param  No param
* @retval true - OK, false - Error
*/
bool IEC104GenericDevice::beginGI()
{
//  if (CS104_Master_isChannelReady(master, linkAddress))
  {
//    CS104_Master_useSlaveAddress(master, linkAddress);
    CS104_Connection_sendInterrogationCommand(master, CS101_COT_ACTIVATION, asduAddress, IEC60870_QOI_STATION);
    scheduleNextGI();
    return true;
  }
  return false;
}

/**
* @brief  Sends Time Sync Command
* @param  No param
* @retval true - OK, false - Error
*/
bool IEC104GenericDevice::issueTimeSync()
{
  /*--
  if (CS101_Master_isChannelReady(master, linkAddress))
  {
    struct sCP56Time2a newTime;
    CS101_Master_useSlaveAddress(master, linkAddress);
    CP56Time2a_createFromMsTimestamp(&newTime, Hal_getTimeStampInMs());
    CS101_Master_sendClockSyncCommand(master, 1, &newTime);
    scheduleNextTS();
    return true;
  }
*/
  return false;
}

bool IEC104GenericDevice::run()
{
  return true;
}

bool IEC104GenericDevice::tagChanged(const Tag &tag, uint32_t paramId, VarData newValue)
{
  if (LL_STATE_ERROR == this->linkState)
    return false;
  auto io = createIOFromTagUpdate(tag, newValue);
  if (io)
  {
    writeQueue.push_back(io);
    return true;
  }
  return false;
}

bool IEC104GenericDevice::canRelease()
{
  auto result = (rsReadyToRelease == runState) && (LL_STATE_BUSY != linkState) || (rsOffline == runState);
  return result;
}

bool IEC104GenericDevice::release()
{
  if (rsOffline != runState)
    runState = rsUnselected;
  return true;
}

InformationObject IEC104GenericDevice::createIOFromTagUpdate(const Tag &tag, VarData newValue)
{
  auto ctx = reinterpret_cast<IECSContext *>(tag.getDriverSpecificInfo(getParent()->getId()));
  InformationObject io = nullptr;
  switch (ctx->cmdType)
  {
  case C_SC_NA_1:
    io = (InformationObject)SingleCommand_create(NULL, ctx->ioa, newValue.boolVal, false, 0);
    break;
  case C_DC_NA_1:
    io = (InformationObject)DoubleCommand_create(NULL, ctx->ioa, newValue.intVal, false, 0);
    break;
  case C_SE_NB_1:
    io = (InformationObject)SetpointCommandScaled_create(NULL, ctx->ioa, newValue.intVal, false, 0);
    break;
  case C_SE_NC_1:
    io = (InformationObject)SetpointCommandShort_create(NULL, ctx->ioa, newValue.fltVal, false, 0);
    break;
  default:
    break;
  }
  return io;
}

uint32_t CP56Time2a_toShortMsTimestamp_(CP56Time2a self)
{
  auto longTS = CP56Time2a_toMsTimestamp(self);
  return longTSToShortTS(longTS);
}

bool IEC104GenericDevice::handleMMETE1(Tag &tag, InformationObject io, CS101_CauseOfTransmission cot)
{
  auto mvs = reinterpret_cast<MeasuredValueScaledWithCP56Time2a>(io);
  auto value = MeasuredValueScaled_getValue(reinterpret_cast<MeasuredValueScaled>(io));
  auto shortTS = CP56Time2a_toShortMsTimestamp_(MeasuredValueScaledWithCP56Time2a_getTimestamp(mvs));
  Core::setIfChanged(tag, value, true, parent->getId(), shortTS);
  return true;
}

bool IEC104GenericDevice::handleMMENC1(Tag &tag, InformationObject io, CS101_CauseOfTransmission cot)
{
  auto mvs = reinterpret_cast<MeasuredValueShort>(io);
  auto value = MeasuredValueShort_getValue(mvs);
  auto qty = MeasuredValueShort_getQuality(mvs) == IEC60870_QUALITY_GOOD;
  Core::setIfChanged(tag, value, qty, parent->getId());
  return true;
}

/**
* @brief  TK36 frame handler Float with time stamp
* @param  Tag &tag, InformationObject io, CS101_CauseOfTransmission cot
* @retval true - OK, false - Error
*/
bool IEC104GenericDevice::handleMMETF1(Tag &tag, InformationObject io, CS101_CauseOfTransmission cot)
{
  auto mvs = reinterpret_cast<MeasuredValueShortWithCP56Time2a>(io);
  auto value = MeasuredValueShort_getValue(reinterpret_cast<MeasuredValueShort>(io));
  auto shortTS = CP56Time2a_toShortMsTimestamp_(MeasuredValueShortWithCP56Time2a_getTimestamp(mvs));
  Core::setIfChanged(tag, value, true, parent->getId(), shortTS);

  return true;
}

bool IEC104GenericDevice::handleMSPNA1(Tag &tag, InformationObject io, CS101_CauseOfTransmission cot)
{
  auto sp = reinterpret_cast<SinglePointInformation>(io);
  auto value = SinglePointInformation_getValue(sp);
  auto qty = SinglePointInformation_getQuality(sp) == IEC60870_QUALITY_GOOD;
  Core::setIfChanged(tag, value, qty, parent->getId());
  return true;
}

bool IEC104GenericDevice::handleMDPNA1(Tag &tag, InformationObject io, CS101_CauseOfTransmission cot)
{
  auto dp = reinterpret_cast<DoublePointInformation>(io);
  auto value = DoublePointInformation_getValue(dp);
  auto qty = DoublePointInformation_getQuality(dp) == IEC60870_QUALITY_GOOD;
  Core::setIfChanged(tag, value, qty, parent->getId());
  return true;
}

bool IEC104GenericDevice::handleMMENB1(Tag &tag, InformationObject io, CS101_CauseOfTransmission cot)
{
  auto mvs = reinterpret_cast<MeasuredValueScaled>(io);
  auto value = MeasuredValueScaled_getValue(mvs);
  auto qty = MeasuredValueScaled_getQuality(mvs) == IEC60870_QUALITY_GOOD;
  Core::setIfChanged(tag, value, qty, parent->getId());
  return true;
}

bool IEC104GenericDevice::handleMSPTB1(Tag &tag, InformationObject io, CS101_CauseOfTransmission cot)
{
  auto sp = reinterpret_cast<SinglePointWithCP56Time2a>(io);
  auto value = SinglePointInformation_getValue((SinglePointInformation)sp);
  auto shortTS = CP56Time2a_toShortMsTimestamp_(SinglePointWithCP56Time2a_getTimestamp(sp));
  Core::setIfChanged(tag, value, true, parent->getId(), shortTS);
  return true;
}

bool IEC104GenericDevice::handleMDPTB1(Tag &tag, InformationObject io, CS101_CauseOfTransmission cot)
{
  auto dp = reinterpret_cast<DoublePointWithCP56Time2a>(io);
  auto value = DoublePointInformation_getValue(DoublePointInformation(dp));
  auto shortTS = CP56Time2a_toShortMsTimestamp_(DoublePointWithCP56Time2a_getTimestamp(dp));
  Core::setIfChanged(tag, value, true, parent->getId(), shortTS);
  return true;
}

bool IEC104GenericDevice::handleCICNA1(Tag &tag, InformationObject io, CS101_CauseOfTransmission cot)
{
  InterrogationCommand ic = reinterpret_cast<InterrogationCommand>(io);
  auto qoi = InterrogationCommand_getQOI(ic);
  if (qoi == 20)
  {
    if (CS101_COT_ACTIVATION_CON == cot)
      runState = rsGI;
    else if (CS101_COT_ACTIVATION_TERMINATION && rsGI == runState)
      runState = rsSelected;
  }
  return true;
}
