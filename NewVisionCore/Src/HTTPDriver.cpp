#include "core.hpp"
#include "HTTPDriver.hpp"
#include "DriverBuilder.hpp"
#include <sstream>

/**
*       I think it is posible to remove SignalMapHTTP
*       because it is good idea to send bool as std::string too.
*       You can rewrite JavaScript and optimize work with maps in this file
*
*       This is my last driver for that runtime in this company.
*       So think about your time and all will be fine.
*
*       Good luck!
* 
*       Lantsev D. 09.07.2020
*/

REGISTER_FACTORY_METHOD(DriverBuilder::driverFactoryTable, Driver, HTTPDriver, new HTTPDriver);

HTTPDriver        *pHttpDriver = nullptr;///< In order to call HTTPDriver class from lwip task
SemaphoreHandle_t xHTTPSemaphore;

enum HTTPTagParamIDs : uint32_t
{
  otpidTitle,  ///< Name of param in HTTPD table
  otpidCMD     ///< Command enable
};

const ParameterVector HTTPTagDriverContext::myParameters
{
  {"title", { otpidTitle, dtString, StaticParameterInfo::pfRequired }},
  {"cmd",   { otpidCMD,   dtBool,   StaticParameterInfo::pfNone }}
};

/**
* @brief  Function send cmd to runtime from client
* @param  params : param index in map,
*         example: "hid0" for bool, "0" for all another types
* @param  values : value of param
* @param  errSSI : buf for error msg
* @retval uint8_t : 1 - /500.shtml, 0 - norm
*/
extern "C" uint8_t sendCmdToRuntime(char* param, char* value, char* errSSI)
{
  return pHttpDriver->parseCmdFromAJAX(param, value, errSSI) ? 0 : 1;
}

/**
* @brief  Function send data by AJAX request in JSON format to client 
* @param  pcInsert : buffer for dat in JSON format
* @param  type : 1 - All data, 0 - Spontanious data 
* @retval uint32_t lenght of Cstring in bytes, 0 - no data
* @note   If you see problems you should know that
*         pcInsert max size is connected with LWIP_HTTPD_MAX_TAG_INSERT_LEN
*         I think it is a good idea to use LWIP_HTTPD_SSI_MULTIPART soon.
*         
*         This code cause the increase of TCPIP_THREAD_STACKSIZE, may be it was not last increase!
*/
extern "C" uint32_t sendDataToAJAX(char* pcInsert, uint8_t type)
{ 
  return pHttpDriver->mapsToJSON(pcInsert, type);
}  

bool HTTPTagDriverContext::setParameterById(uint32_t paramId, VarData newValue)
{
  auto tagParamId = static_cast<HTTPTagParamIDs>(paramId);
  switch (tagParamId)
  {
  case otpidTitle:
    {    
     /* Example of correct copy Runtime dtString (char*) to our own char*

       uint8_t strLength = strlen(newValue.pStr) + 1;
       char* tagName = new char[strLength];
       strncpy(tagName, newValue.pStr, strLength); */
      
      std::stringstream sStr;
      sStr << newValue.pStr;
      tagName = sStr.str();
    }
    break;
  case otpidCMD:
      cmdEnable = newValue.boolVal;
    break;
  default:
    break;
  }
  return true;
}

void HTTPDriver::registerTag(const Tag &tag, void *ctx)
{
  auto entry = reinterpret_cast<HTTPTagDriverContext*>(ctx);
  Tag* pTag  = const_cast<Tag*>(&tag);
  
  if(tag.getType() == dtBool)
  {
    if(entry->cmdEnable)
      cntrolMap.insert({ entry->tagName, false });
    else
      signalMap.insert({ entry->tagName, false });
  }
  else
  {
    if(entry->cmdEnable)
      pointsMap.insert({ entry->tagName, "" });
    else
      analogMap.insert({ entry->tagName, "" });
  }
  
  httpTags.push_front(pTag);
}

DriverContext *HTTPDriver::createTagContext() const
{
  return new HTTPTagDriverContext;
}

bool HTTPDriver::tagChanged(const Tag &tag, uint32_t paramId, VarData newValue)
{
  HTTPTagDriverContext *entry = reinterpret_cast<HTTPTagDriverContext *>(tag.getDriverSpecificInfo(getId()));
  Tag* pTag  = const_cast<Tag*>(&tag);
  
  if( xSemaphoreTake( xHTTPSemaphore, ( TickType_t ) 100 ) == pdTRUE )
  {
    if(tag.getType() == dtBool)
    {
      if(entry->cmdEnable)
        cntrolMap[entry->tagName] = newValue.boolVal;
      else
        signalMap[entry->tagName] = newValue.boolVal;
    }
    else
    {
      DataType tagType = tag.getType();
      std::string str;
      switch(tagType)
      {
      case dtByte:
        str = std::to_string(newValue.byteVal);
        break;
      case dtWord:
        str = std::to_string(newValue.wordVal);
        break;
      case dtShortInt:
        str = std::to_string(newValue.shortVal);
        break;
      case dtDWord:
        str = std::to_string(newValue.dwordVal);
        break;
      case dtInt:
        str = std::to_string(newValue.intVal);
        break;
      case dtFloat:
        str = std::to_string(newValue.fltVal);
        break;
      }
      
      if(entry->cmdEnable)
        pointsMap[entry->tagName] = str;
      else
        analogMap[entry->tagName] = str;
    }
      
    if(spontTags.size() < maxNumOfSpontTags + 1)  
    {
     if(tag.getType() == dtBool)
     {
       spontTags.push_front(pTag);
     }
     else /* Add only uniq tags in order to not send many AI signals */
     {
        auto it{ std::find_if(spontTags.begin(),
                             spontTags.end(),
                             [pTag](Tag *cTag){ return pTag == cTag;})};
        if(it == spontTags.end())
          spontTags.push_front(pTag);
     } 
    }
    
    xSemaphoreGive(xHTTPSemaphore);
  }
  
  return true;
}

void HTTPDriver::initialize()
{
  Driver::initialize(); 
  commandQueue->getTimer().setPeriod(500);
  commandQueue->getTimer().rearm();
  pHttpDriver = this;
  
  xHTTPSemaphore = xSemaphoreCreateMutex();
  if(xHTTPSemaphore == NULL)
    asm("bkpt 255");
}

void HTTPDriver::run()
{
  switch(runState)
	{
	case rsInitializing:
    if(commandQueue->getTimer().ready())
		{
			runState = rsRunning;
		}
		break;
	case rsRunning:
		break;
  }
}

struct StrEqv 
{
    std::string   m_str;
    HTTPDriver*   m_driver;
    
    StrEqv(HTTPDriver* driver, const std::string& strTagName) : m_str(strTagName),
                                                                m_driver(driver){}
    
    bool operator() (Tag *tag)
    {
        HTTPTagDriverContext *entry = 
          reinterpret_cast<HTTPTagDriverContext *>(tag->getDriverSpecificInfo(m_driver->getId()));
        return entry->tagName == m_str;
    }
};

bool HTTPDriver::parseCmdFromAJAX(char* param, char* values, char* errSSI)
{
  bool result = false;
  if(runState == rsRunning)
  {
    if( xSemaphoreTake( xHTTPSemaphore, ( TickType_t ) 100 ) == pdTRUE )
    {
      if(param[0] == 'h') /* Bool type cmd */
      {
        param += 3;
        uint32_t paramInd = atoi(param);
        
        if(paramInd > cntrolMap.size())
          result = false;
        else
        {
          auto mapIt = cntrolMap.begin(); 
          std::advance(mapIt, paramInd);
          
          auto listIt{std::find_if(httpTags.begin(),
                                   httpTags.end(),
                                   StrEqv(this, mapIt->first))};
          if(listIt != httpTags.end())
          {
            Core::set(**listIt, static_cast<bool>(values), true, -1);
            result = true;
          }
        }
      }
      else /* Another types */
      {
        uint32_t paramInd = atoi(param);
        
        auto mapIt = pointsMap.begin();
        std::advance(mapIt, paramInd);
        
        auto listIt{ std::find_if(httpTags.begin(),
                                  httpTags.end(),
                                  StrEqv(this, mapIt->first))};
        if(listIt != httpTags.end())
        {
          DataType tagType = (*listIt)->getType();
          switch(tagType)
          {
          case dtByte:
            Core::set(**listIt, static_cast<uint8_t>(atoi(values)), true, -1);
            break;
          case dtWord:
            Core::set(**listIt, static_cast<uint16_t>(atoi(values)), true, -1);
            break;
          case dtShortInt:
            Core::set(**listIt, static_cast<int16_t>(atoi(values)), true, -1);
            break;
          case dtDWord:
            Core::set(**listIt, static_cast<uint32_t>(atoi(values)), true, -1);
            break;
          case dtInt:
            Core::set(**listIt, static_cast<int32_t>(atoi(values)), true, -1);
            break;
          case dtFloat:
            Core::set(**listIt, static_cast<float>(atof(values)), true, -1);
            break;
          }
          result = true;
        }
      }
      xSemaphoreGive(xHTTPSemaphore);
    }
  }
  if(result == false)
      sprintf(errSSI, "%s", "Invalid data parameter!");
  return result;
}

uint32_t HTTPDriver::mapsToJSON(char* pcInsert, uint8_t type)
{
  uint32_t strLength = 0;
  
  if(runState == rsRunning)
  {
    if( xSemaphoreTake( xHTTPSemaphore, ( TickType_t ) 100 ) == pdTRUE )
    {
      nlohmann::json j;
            
      if(type == 1) /* General Interagation in first http request */
      {
        if(signalMap.size()){
           nlohmann::json jMap(signalMap);
           j["signal"] = jMap;
        }
        if(analogMap.size()){
           nlohmann::json jMap(analogMap);
           j["analog"] = jMap;
        }
        if(cntrolMap.size()){
           nlohmann::json jMap(cntrolMap);
           j["cntrol"] = jMap;
        }
        if(pointsMap.size()){
           nlohmann::json jMap(pointsMap);
           j["points"] = jMap;
        }
      }
      else  /* Spontanious data must be set to json below! */
      {
        if(spontTags.size() > 0)
        {
          AnalogMapHTTP  analogJsonMap;
          AnalogMapHTTP  pointsJsonMap;
          SignalMapHTTP  signalJsonMap;
          SignalMapHTTP  cntrolJsonMap;
          
          for(const auto& it : spontTags)
          {            
            HTTPTagDriverContext *entry = 
              reinterpret_cast<HTTPTagDriverContext *>(it->getDriverSpecificInfo(getId()));
            if(entry->cmdEnable == true)
            {
              if(it->getType() == dtBool)
                cntrolJsonMap.insert({entry->tagName, cntrolMap[entry->tagName]});
              else
                pointsJsonMap.insert({entry->tagName, pointsMap[entry->tagName]});
            }
            else
            {
              if(it->getType() == dtBool)
                signalJsonMap.insert({entry->tagName, signalMap[entry->tagName]});
              else
                analogJsonMap.insert({entry->tagName, analogMap[entry->tagName]});
            }
          }
          
          if(pointsJsonMap.size()){
            nlohmann::json jMap(pointsJsonMap);
            j["points"] = jMap;
          }
          if(analogJsonMap.size()){
            nlohmann::json jMap(analogJsonMap);
            j["analog"] = jMap;
          }
          if(signalJsonMap.size()){
            nlohmann::json jMap(signalJsonMap);
            j["signal"] = jMap;
          }
          if(cntrolJsonMap.size()){
            nlohmann::json jMap(cntrolJsonMap);
            j["cntrol"] = jMap;
          }
        }
      }
      spontTags.clear();
      xSemaphoreGive(xHTTPSemaphore);
      
      std::string str = j.dump();
      strLength = sprintf(pcInsert, "%s", str.c_str()); 
    }
  }
  
  return strLength;
}