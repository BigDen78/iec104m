#include "DriverBuilder.hpp"
#include "ZNOYMasterDriver.hpp"
#include "iec101s.hpp"
#include "hal_thread.h"
#include "DriverBuilder.hpp"

REGISTER_FACTORY_METHOD(DriverBuilder::driverFactoryTable, Driver, ZNOYMasterDriver, new ZNOYMasterDriver);
//-------------------------------------------------------------------------------------------------------------
#define CMD_IDLE 1u

enum ParamIDs : uint32_t
{
   pidSlaveAddress,
   pidRequestFrequencyMS,
   pidAttemptsAmount,
   pidUART,
   pidBaudRate,
   pidStopBits,
   pidParity
};

enum ZnoyParamsEnum : uint32_t
{
   pidRegisterAddress      // 0->65535
};

registersArray ZNOYMasterDriver::Registers;// = {{},{}};

const ParameterVector ZNOYMasterDriver :: znoyParameters
{
   {"SlaveAddress",       { pidSlaveAddress,       dtDWord,  StaticParameterInfo::pfRequired }},
   {"RequestFrequencyMS", { pidRequestFrequencyMS, dtDWord,  StaticParameterInfo::pfRequired }},
   {"AttemptsAmount",     { pidAttemptsAmount,     dtDWord,  StaticParameterInfo::pfRequired }},   // количество попыток 
   {"UART", 		  { pidUART, 	           dtByte,   StaticParameterInfo::pfRequired }},
   {"BaudRate",  	  { pidBaudRate, 	   dtDWord,  StaticParameterInfo::pfRequired }},
   {"StopBits", 	  { pidStopBits, 	   dtByte,   StaticParameterInfo::pfRequired }},
   {"Parity", 		  { pidParity, 	           dtString, StaticParameterInfo::pfRequired }}
};

const ParameterVector ZNOYMasterTagContext :: znoyTagParameters
{
   {"Address", { pidRegisterAddress, dtWord, StaticParameterInfo::pfRequired }}
};

// Функции служебные ------------------------------------------------------------------------------------------
static uint16_t GetCRC16(uint8_t *buf, uint8_t len);
void stub(void);    

//-------------------------------------------------------------------------------------------------------------     	 
ZNOYMasterDriver::ZNOYMasterDriver() : dcmdSerialIdle(this, CMD_IDLE, {0}), ParameterizedObject(znoyParameters)
{

}

bool ZNOYMasterDriver::setParameterById(uint32_t paramId, VarData newValue)
{            
  if(paramId == pidSlaveAddress)
  {
    slaveAddress = newValue.dwordVal;
    return true; 
  }
  else if (paramId == pidRequestFrequencyMS)
  {
    requestFrequencyMS = newValue.dwordVal;
    return true;  
  }
  else if (paramId == pidAttemptsAmount)
  {
    attemptsAmount = newValue.dwordVal;
    return true; 
  }
  else if (paramId == pidUART)
  {
    if ((newValue.byteVal > 0) && (newValue.byteVal <= MAX_HUART) && (huarts[newValue.byteVal - 1]))
    {
      huart = huarts[newValue.byteVal - 1];
      return true;
    }             
  }
  else if (paramId == pidBaudRate)
  {
    baudRate = newValue.dwordVal;
    return true;  
  }
  else if (paramId == pidStopBits)
  {
    if (newValue.byteVal == 1 || newValue.byteVal == 2)
    {
      stopBits = newValue.byteVal;
      return true;
    } 
  }
  else if (paramId == pidParity)
  {
    if (newValue.pStr[0] == 'E' || newValue.pStr[0] == 'O' || newValue.pStr[0] == 'N')
    {
      parity = newValue.pStr[0];
      return true;
    } 
  }
  return false; 
}
    
bool ZNOYMasterDriver::setTagParameter(const Tag &tag, uint32_t paramId, VarData newValue)
{          	   
   return false;
}

bool ZNOYMasterDriver::setTagRuntimeParameter(const Tag &tag, uint32_t paramId, VarData paramValue)
{
   return false;
}

VarData ZNOYMasterDriver::getTagRuntimeParameter(const Tag &tag, uint32_t paramId)
{
   VarData result = {0};
   return  result;
}

VarData ZNOYMasterDriver::getRuntimeParameter(uint32_t paramId)
{
   VarData result = {0};
   return  result;
}

/**
  * Установка параметров тега
  *
  */
bool ZNOYMasterTagContext::setParameterById(uint32_t paramId, VarData newValue)
{   
   if(paramId == pidRegisterAddress)
   {
      address = newValue.wordVal;
      return true;
   }
   return false;
}

DriverContext* ZNOYMasterDriver::createTagContext() const
{
   auto newTagContext = new ZNOYMasterTagContext;
   return newTagContext;
}

void ZNOYMasterDriver::registerTag(const Tag &tag, void *context)
{
   ZNOYMasterTagContext* addedTag = reinterpret_cast <ZNOYMasterTagContext *> (context);

   addedTag->tagPtr = const_cast<Tag *>(&tag);
   Registers[addedTag->address] = addedTag;
}

/**
  * Необходимые при изменении тэга действия
  *
  */
bool ZNOYMasterDriver::tagChanged(const Tag &tag, uint32_t paramId, VarData newValue)
{
   ZNOYMasterTagContext *entry = reinterpret_cast<ZNOYMasterTagContext *>(tag.getDriverSpecificInfo(getId()));
  
   return false;
}

void ZNOYMasterDriver::initialize()
{
   Driver::initialize();
   //---------------------------------------------------------------------------------------------------------//
   currentState = rsInitializing;
   port =  SerialPort_create(huart, baudRate, parity, stopBits);
     
   if(port)
   {
      SerialPort_subscribeToEvents(port, commandQueue->getHandle(), &dcmdSerialIdle, NULL);
      SerialPort_open(port);
     
      currentState = rsRunning; 
      
      commandQueue->getTimer().setPeriod(0);
      commandQueue->getTimer().rearm();
      return;
   }
}

bool ZNOYMasterDriver::setRuntimeParameter(const Tag &tag, uint32_t paramId, VarData paramValue)
{
  if (CMD_IDLE == paramId) 
  {
    if(rsAnswerWaiting == currentState)
    {    
      // оппача пришел пакет //
      if(parseRequest(port))
      {
        currentState = rsRunning;
      }
      
      return true;
    }
    else 
    {
      // перезапустить порт
    }
  }
	
  return false;
}

/**
  * Бесконечный цикл программы драйвера
  *
  */
void ZNOYMasterDriver::run()
{       
   uint32_t firstRegNumber = Registers.begin()->first;
   static uint32_t attemptsDoneNumber = 0;                // количество попыток связаться
   requestedAmount = 2;                         // запрашиваемое количество регистров
   
   if((commandQueue->getTimer().isArmed()) &&  (commandQueue->getTimer().ready()))
   {        	
     // прошел таймаут - еще ждет, значит обрыв связи //         
     if(currentState == rsAnswerWaiting)
     {    
       attemptsDoneNumber++;
       
       if(attemptsDoneNumber >= attemptsAmount)
       {             	    
         currentState = rsRunning;
         markRegistersAsInvalid(firstRegNumber, requestedAmount);       // Скидываем в -300
       }
     } 
     
     if(currentState == rsRunning)
     {
       attemptsDoneNumber = 0;   
     }
     
     requestHoldingRegs(firstRegNumber, requestedAmount); 
     currentState = rsAnswerWaiting;     
     
     commandQueue->getTimer().setPeriod(requestFrequencyMS);
     commandQueue->getTimer().rearm();
   }
}
		
void ZNOYMasterDriver::requestHoldingRegs(uint16_t startNum, uint16_t amount)
{
   uint32_t requestCRC = 0; 
   auto firstRegister = Registers.begin();
   uint32_t registersAmount = amount * 2;
   
   // Запрос формируем//
   requestBuffer[0] = slaveAddress;         // slaveAddress;
   requestBuffer[1] = 0x03;      // функция для вычитывания
   
   requestBuffer[2] = (startNum & 0xff00) >> 8;
   requestBuffer[3] = (startNum & 0x00ff);
   
   // количество регистров //
   requestBuffer[4] = (registersAmount & 0xff00) >> 8;
   requestBuffer[5] = (registersAmount & 0x00ff);        
  
   requestCRC = GetCRC16(requestBuffer, 6);
   requestBuffer[6] = (uint8_t)(requestCRC); 
   requestBuffer[7] = (uint8_t)(requestCRC >> 8);
	          
   SerialPort_write(port , requestBuffer, 0, 8);   
      
   currentState = rsAnswerWaiting;
}

bool ZNOYMasterDriver::parseRequest(SerialPort port)
{
   uint8_t* inBuffer = port->serial.rxBuffer;
   uint32_t numOfBytes = 0;
   uint16_t value;
   
   if(port->serial.rxCount != 0)
   {
     if(inBuffer[0] == slaveAddress)
     {
       if(inBuffer[1] == 0x03)
       {
         numOfBytes = inBuffer[2];
         
         if(requestedAmount == numOfBytes / 4)
         {
           // From here is my code (Lantsev), all problems with other code is not in my power=)     
           uint8_t ind = 3;
           for(auto &it:Registers)
           {
             value = (inBuffer[ind] << 8) | inBuffer[ind+1];
             Core::setIfChanged(*it.second->tagPtr, (float)value * 0.02f - 273.15f, true, getId());
             ind = ind + 2;
           }        
           return true;
         }
       }
	 
	 // Тут контроль CRC должен быть
      }
   }
   
   return false;
}

void ZNOYMasterDriver::setAllRegistersValue(uint16_t startNum, uint16_t amount, float value)
{
   for(auto regNum = Registers.begin(); regNum != Registers.end(); regNum++)
   {
      regNum->second->value = value;
      Core::setIfChanged(*(regNum->second->tagPtr), value, true, id);
   }
}

void ZNOYMasterDriver::markRegistersAsInvalid(uint16_t startNum, uint16_t amount)
{
   setAllRegistersValue(startNum, amount, -300);
}

 /** Подсчет CRC16
  * Входные параметры:
  * - *buf - указатель на начало массива посылки (uint8)
  * - len - длина массива
  * Возвращаемое значение:
  * - uint16_t crc - возвращаемое значение CRC16
  **/
uint16_t GetCRC16(uint8_t *buf, uint8_t len) 
{
        // Устанавливаем стартовое значение 
        unsigned int crc = 0xFFFF;
        // Подсчитываем crc 
        for (int pos = 0; pos < len; pos++){
		crc ^= (unsigned int)buf[pos];		// XOR byte into least sig. byte of crc 		
                for (int i = 8; i != 0; i--) { 		// Loop over each bit                	
			if ((crc & 0x0001) != 0) {   	// If the LSB is set 			
                        	crc >>= 1;         	// Shift right and XOR 0xA001     
                        	crc ^= 0xA001;
                	}			
                	else 				// Else LSB is not set       
                        	crc >>= 1;		// Just shift right 
                }
        }
        // Возвращаем значение crc16 
        return crc;
}

// Функции класса ------------------------------------------------------------------------------------------------------------------------//
//----------------------------------------------------------------------------------------------------------------------------------------//
//					                 [ Создание собственной MIB ]		   					  //
//----------------------------------------------------------------------------------------------------------------------------------------//
