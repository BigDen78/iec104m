
#include "interrupts.h"
#include "ADCDriver.h"

#include "kalman.h"
#include "timed.h"
#include "hw_timers.h"
#include <limits.h>


QueueHandle_t hQueue;    ///< Handle of queue to transmit adc_read messanges

static ADC_Period periodType;  ///< Select TIM base frequency
static ADC_LogCntr adcLogCtr = { 0, 0, 0, {{0},{0}}, 0, 0 , {{0},{0}}, {{0},{0}}, NULL, NULL }; ///< Comtrade logger control struct
static uint8_t numberChannels = 0;             ///< Count of ADC_Channel
static ADC_Channel **adcChannel;               ///< Pointer to vector of ADC_Channel
static TimerCntr   tim2Cntr;

static void ADC_RingMeasure(ADC_Channel *adcChannel, uint8_t logChNum);
static void ADC_Task(void *pvParametrs);
static void sendToDriver(void* dcmdAdcRdy);

/**
  * @brief AdcTask is the body for tim2 task
  */
static void ADC_Task(void* pvParametrs)
{
  bool getNotifyForOSC = false;
  adcLogCtr.systemTs = getLongTS();

  for(;;)
  {
    uint32_t ulNotifiedValue;
    xTaskNotifyWait( 0x00, ULONG_MAX, &ulNotifiedValue, portMAX_DELAY );

    if(ulNotifiedValue == 0x02) /* Accept notify from getHistoricalData*/
    {
      getNotifyForOSC =  true;
    }
    else
    {
      uint8_t logChNum = 0;
      for(uint8_t i = 0; i < numberChannels; i++)
      {
        ADC_RawType tmpRead;
 
#ifndef TEST_UPS
        if(adcChannel[i]->pADC_BoardSpec->HAL_SPI != NULL)
        {
          
          tmpRead = readHALADC[adcChannel[i]->pADC_BoardSpec->adcTypes](adcChannel[i]->pADC_BoardSpec->HAL_SPI,
                                                                        adcChannel[i]->pADC_BoardSpec->ADC_CSPorts,
                                                                        adcChannel[i]->pADC_BoardSpec->ADC_CSPins);
        }
        else if(adcChannel[i]->pADC_BoardSpec->LL_SPI != NULL)
        {
          tmpRead = readLLADC[adcChannel[i]->pADC_BoardSpec->adcTypes](adcChannel[i]->pADC_BoardSpec->LL_SPI,
                                                                        adcChannel[i]->pADC_BoardSpec->ADC_CSPorts,
                                                                        adcChannel[i]->pADC_BoardSpec->ADC_CSPins);
        }
        else{
          
        tmpRead = 0;
        
        }
        
#endif

        adcChannel[i]->fBuffer[adcChannel[i]->windowCnt] = Kalman((sKalman*)adcChannel[i]->fParams, tmpRead);
        adcChannel[i]->windowCnt++;

        /* Added data to log buffer */
        if((adcLogCtr.logSize) && (adcChannel[i]->loggerEn) &&
           (!adcLogCtr.logBuffers[adcLogCtr.currentBuffer].busy))
        {
          if((logChNum == 0) && (getNotifyForOSC == true))
          {
            adcLogCtr.currentBuffer = 1;
            adcLogCtr.logBuffers[0].busy = true;
            getNotifyForOSC = false;
          }
          ADC_RingMeasure(adcChannel[i], logChNum);
          logChNum++;
        }

        if(adcChannel[i]->windowCnt == adcChannel[i]->numPoints)
        {
          ADC_RawType rawValue;
          adcChannel[i]->windowCnt = 0;

          arm_mean_f32(adcChannel[i]->fBuffer, adcChannel[i]->numPoints, &rawValue);

          float apert = fabsf(rawValue - adcChannel[i]->lastValue);
          if(apert > adcChannel[i]->aperture)   /* Check aperture */
          {
            adcChannel[i]->lastValue = rawValue;
            adcChannel[i]->value = ADC_ConvertRawToSecondary(adcChannel[i], rawValue);
            sendToDriver(adcChannel[i]->dcmdAdcRdy);
          }
        }
      }

      if((adcLogCtr.logSize) && (!adcLogCtr.logBuffers[adcLogCtr.currentBuffer].busy))
      {
        if((logChNum == adcLogCtr.enabledLogChannels))
          adcLogCtr.bufPos[adcLogCtr.currentBuffer]++;

        if(adcLogCtr.bufPos[adcLogCtr.currentBuffer] ==
           adcLogCtr.nSamples[adcLogCtr.currentBuffer])
        {
          if(adcLogCtr.currentBuffer == 1)
          {
            adcLogCtr.logBuffers[1].busy = true;
            xTaskNotify(adcLogCtr.xLoggerTask, 0, eNoAction);
          }
          else
          {
            adcLogCtr.bufPos[0] = 0;
            adcLogCtr.systemTs = getLongTS();
          }
        }
      }
    }
  }
}

/**
  * @brief Send command to C++ Driver of Runtime
  * @note  Calls AIDriver::updateTag
  */
static void sendToDriver(void* dcmdAdcRdy)
{
  if(hQueue)
  {
    BaseType_t xHigherPriorityTaskWoken = pdFALSE;
    xQueueSendToBackFromISR(hQueue, dcmdAdcRdy,
                                    &xHigherPriorityTaskWoken);
    portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
  }
}

/**
  * @brief  Routine function that move ring ptr and put measurements in ring buffer
  * @param  ADC_Channel *adcChannel
  * @param  uint8_t      logChNum
  */
static void ADC_RingMeasure(ADC_Channel *adcChannel, uint8_t logChNum)
{
  adcLogCtr.logBuffers[adcLogCtr.currentBuffer].padcChannels[logChNum][adcLogCtr.bufPos[adcLogCtr.currentBuffer]] =
  (ADC_ChannelsLogger)adcChannel->fBuffer[adcChannel->windowCnt - 1];
}

/**
  * @brief  ADC_InitTask uses in order to init TIM and create adcTask
  */
void ADC_InitTask(void)
{
  adcLogCtr.enabledLogChannels = 0;
  for(uint8_t i = 0;i < numberChannels;i++)  /* Create point buffers for each using channel */
  {
    adcChannel[i]->fBuffer = pvPortMalloc(sizeof(ADC_RawType) * adcChannel[i]->numPoints);
    if(adcChannel[i]->loggerEn)
      adcLogCtr.enabledLogChannels++;
  }

  if(adcLogCtr.enabledLogChannels)
  {
    adcLogCtr.samplesPerMs = ADC_TIM_GetPeriod() / 1000;
    adcLogCtr.nSamples[1]  = adcLogCtr.samplesPerMs * adcLogCtr.logSize;
    if(adcLogCtr.nSamples[1] != 0)
    {
      if(!(adcLogCtr.logBuffers[0].padcChannels = (ADC_ChannelsLogger**)pvPortMalloc(sizeof(ADC_ChannelsLogger*) * adcLogCtr.enabledLogChannels)))
        asm("bkpt 255");

      if(!(adcLogCtr.logBuffers[1].padcChannels = (ADC_ChannelsLogger**)pvPortMalloc(sizeof(ADC_ChannelsLogger*) * adcLogCtr.enabledLogChannels)))
        asm("bkpt 255");

      /* How points will be in ring buffer */
      adcLogCtr.nSamples[0] = adcLogCtr.nSamples[1] / 100 * 20;

      for(uint8_t i = 0; i < adcLogCtr.enabledLogChannels; i++)
      {
        if(!(adcLogCtr.logBuffers[0].padcChannels[i] =
             (ADC_ChannelsLogger*)pvPortMalloc(sizeof(ADC_ChannelsLogger)
                                               * adcLogCtr.nSamples[0])))
          asm("bkpt 255");
        if(!(adcLogCtr.logBuffers[1].padcChannels[i] =
             (ADC_ChannelsLogger*)pvPortMalloc(sizeof(ADC_ChannelsLogger)
                                               * adcLogCtr.nSamples[1])))
          asm("bkpt 255");
      }
    }
    else /* Log will be disabled in order to wrong periodType */
    {
       ADC_SetLogSize(0);
    }
  }
  else
  {
    ADC_SetLogSize(0);
  }

  if(xTaskCreate(ADC_Task,(const char*)"AdcTask", 512, NULL, tskIDLE_PRIORITY+4, &adcLogCtr.xAdcTask) == pdPASS)
  {
    tim2Cntr.apbType = LL_APB1_GRP1_PERIPH_TIM2;
    tim2Cntr.period  = (uint32_t)periodType;
    tim2Cntr.xTask   = adcLogCtr.xAdcTask;
    tim2Cntr.irqType = TIM2_IRQn;
    tim2Cntr.nvicPriority = 5;
    tim2Cntr.tim     = TIM2;
  
    timerInit(&tim2Cntr);
    timerStart(&tim2Cntr);
  }
  else
    asm("bkpt 255");
}

/**
  * @brief  Function set TIM2 period
  * @param  periodType : AdcPeriod
  */
void ADC_TIM_SetPeriod(ADC_Period pType)
{
   periodType = pType;
}

/**
  * @brief  Function get TIM2 period
  * @param  periodType : AdcPeriod
  */
ADC_Period ADC_TIM_GetPeriod(void)
{
   return periodType;
}


/**
  * @brief  adcTask is the body for tim2 task
  * @param  y : float - point in line in secondary measuaring units
  */
float ADC_ConvertRawToSecondary(ADC_Channel *pADC_Channel, ADC_RawType x)
{
  float y1 = pADC_Channel->zeroPoint;
  float y2 = pADC_Channel->highPoint;
  ADC_RawType x2 = pADC_Channel->pADC_BoardSpec->highRawPoint;
  return (y1 + (((float)x) / ((float)x2)) * (y2 - y1));
}

/**
  * @brief  Straight line equation
  * @retval x : ADC_RawType - point in line in raw adc readings
  */
ADC_RawType ADC_ConvertSecondaryToRaw(ADC_Channel *pADC_Channel, float y)
{
  float y1 = pADC_Channel->zeroPoint;
  float y2 = pADC_Channel->highPoint;
  ADC_RawType x2 = pADC_Channel->pADC_BoardSpec->highRawPoint;
  return (ADC_RawType)((float)x2 * (y - y1) / (y2 - y1));
}

/**
  * @brief  set logSize of oscillo in raw points
  * @param  logSize : uint32_t
  */
void ADC_SetLogSize(uint32_t logSize)
{
  adcLogCtr.logSize = logSize;
}

/**
  * @brief  get logSize of oscillo in raw points
  * @retval logSize : uint32_t
  */
uint32_t ADC_GetLogSize(void)
{
  return adcLogCtr.logSize;
}

/**
  * @brief  get logSize of oscillo in raw points
  * @retval logSize : uint32_t
  */
ADC_LogCntr* ADC_GetLogCtrStruct(void)
{
  return &adcLogCtr;
}

/**
  * @brief  Constructor for ADC_Channel
  */
void ADC_Init(ADC_Channel *pADC_Channel)
{
  pADC_Channel->numPoints  = 10;
  pADC_Channel->aperture   = 0;
  pADC_Channel->windowCnt  = 0;
  pADC_Channel->filterType = ADC_KALMAN;
  pADC_Channel->loggerEn   = false;
  pADC_Channel->fBuffer    = NULL;
  pADC_Channel->fParams    = NULL;
  pADC_Channel->value      = 0;
  pADC_Channel->lastValue  = -1000000;
  pADC_Channel->test       = false;
}

/**
  * @brief Set queue to send adc_raw to C++ Runtime Driver
  */
void ADC_SetRDYQueue(QueueHandle_t hQ)
{
	if(hQ)
		hQueue = hQ;
}

/**
  * @brief  Function set amount of active channels
  * @param  num : uint8_t - amount of active channels
  */
void ADC_SetNumChannels(uint8_t num)
{
  numberChannels = num;
}

/**
  * @brief  Function set pointer to vector of Active channels
  * @param  padcChannel : ADC_Channel*
  */
void ADC_SetChannelsPointer(ADC_Channel **padcChannel)
{
  adcChannel = padcChannel;
}
