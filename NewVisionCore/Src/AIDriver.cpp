#include <map>
#include "Core.hpp"
#include "AIDriver.hpp"
#include "DriverBuilder.hpp"
#include "spi.h"

REGISTER_FACTORY_METHOD(DriverBuilder::driverFactoryTable, Driver,  AIDriver, new AIDriver);

const uint8_t ADC_CANNELS_CNT = 8; ///< Max value of ADC cannels on board

/// Board specific arrays with hardware AI params
ADC_BoardSpec adcBoardSpec[ADC_CANNELS_CNT] =
{
#ifndef TEST_UPS
  
  {NULL, NULL,NULL, 0, ADC_NONE, 0},
  {NULL, NULL,NULL, 0, ADC_NONE, 0},
  {NULL, NULL,NULL, 0, ADC_NONE, 0},
  {NULL, NULL,NULL, 0, ADC_NONE, 0},
  {NULL, NULL,NULL, 0, ADC_NONE, 0},
  {NULL, NULL,NULL, 0, ADC_NONE, 0},
  {NULL, NULL,NULL, 0, ADC_NONE, 0},
  {NULL, NULL,NULL, 0, ADC_NONE, 0}
#else
  {NULL, NULL, 0, ADC_NONE, 0},
  {NULL, NULL, 0, ADC_NONE, 0},
  {NULL, NULL, 0, ADC_NONE, 0},
  {NULL, NULL, 0, ADC_NONE, 0},
  {NULL, NULL, 0, ADC_NONE, 0},
  {NULL, NULL, 0, ADC_NONE, 0},
  {NULL, NULL, 0, ADC_NONE, 0},
  {NULL, NULL, 0, ADC_NONE, 0}
#endif
};

enum ParamIDs : uint32_t
{
  atpidLogSize,     ///< oscillo can be 100 - 3000 ms
  atpidPeriodType   ///< Select TIM2 base frequency
};

enum AITagParamIDs : uint32_t
{
  atpidTest,         ///< Channel will be in simulation mode
  atpidAInum,        ///< Index in adcPlatSpec array in config 1 channel is 1 not zero
  atpidNumPoints,    ///< Number of points to be filtering per one Runtime send
  atpidZeroPoint,
  atpidHighPoint,
  atpidKRatio,       ///< Scaling factor (tag_value = secondary_adc * kRatio)
  atpidCalibK,       ///< Additive factor to measured value (SECONDARY + calibK) 
  atpidConvertType,  ///< Conversation class type from P_SECONDARY to P_PRIMARY or not any class
  atpidPrimaryPoint, ///< (P_SECONDARY = false) or (P_PRIMARY = true) if convert type not set only P_SECONDARY are available
  atpidAperture,     ///< Can be in PRIMARY or SECONDARY if convert type not set only P_SECONDARY are available
  atpidLoggerEn,
  atpidFilter,        ///< Index of function of filtering
  
  atpidPVminOK,
  atpidPVmaxOK,
  atpidPVminValid,
  atpidPVmaxValid,
  atpidPVLoLo,
  atpidPVLo,
  atpidPVHi,
  atpidPVHiHi,
  
  atpidSensOK,
  atpidValidOK,
  atpidWarnHi,
  atpidWarnLo,
  atpidAlarmHi,
  atpidAlarmLo,
  atpidSigNorm,
  
};

const ParameterVector AIDriverTagContext::myParameters
{
  {"test",              {atpidTest,             dtBool,         StaticParameterInfo::pfNone}},
  {"AInum",             {atpidAInum,            dtByte,         StaticParameterInfo::pfRequired}},
  {"numPoints",         {atpidNumPoints,        dtByte,         StaticParameterInfo::pfNone}},
  {"zeroPoint",         {atpidZeroPoint,        dtFloat,        StaticParameterInfo::pfRequired}},
  {"highPoint",         {atpidHighPoint,        dtFloat,        StaticParameterInfo::pfRequired}},
  {"kRatio",            {atpidKRatio,           dtFloat,        StaticParameterInfo::pfRuntimeChangeable}},
  {"calibK",            {atpidCalibK,           dtFloat,        StaticParameterInfo::pfRuntimeChangeable}},
  {"convertType",       {atpidConvertType,      dtByte,         StaticParameterInfo::pfNone}},
  {"primaryPoint",      {atpidPrimaryPoint,     dtBool,         StaticParameterInfo::pfRuntimeChangeable}},
  {"aperture",          {atpidAperture,         dtFloat,        StaticParameterInfo::pfRuntimeChangeable}},
  {"loggerEn",          {atpidLoggerEn,         dtBool,         StaticParameterInfo::pfRuntimeChangeable | StaticParameterInfo::pfRebootRequired}},
  {"filter",            {atpidFilter,           dtByte,         StaticParameterInfo::pfNone}},
  
  {"PVminOK",           {atpidPVminOK,          dtFloat,        StaticParameterInfo::pfNone| StaticParameterInfo::pfRuntimeChangeable}},
  {"PVmaxOK",           {atpidPVmaxOK,          dtFloat,        StaticParameterInfo::pfNone| StaticParameterInfo::pfRuntimeChangeable}},
  {"PVminValid",        {atpidPVminValid,       dtFloat,        StaticParameterInfo::pfNone| StaticParameterInfo::pfRuntimeChangeable}},
  {"PVmaxValid",        {atpidPVmaxValid,       dtFloat,        StaticParameterInfo::pfNone| StaticParameterInfo::pfRuntimeChangeable}},
  {"PVLoLo",            {atpidPVLoLo,           dtFloat,        StaticParameterInfo::pfNone| StaticParameterInfo::pfRuntimeChangeable}},
  {"PVLo",              {atpidPVLo,             dtFloat,        StaticParameterInfo::pfNone| StaticParameterInfo::pfRuntimeChangeable}},
  {"PVHi",              {atpidPVHi,             dtFloat,        StaticParameterInfo::pfNone| StaticParameterInfo::pfRuntimeChangeable}},
  {"PVHiHi",            {atpidPVHiHi,           dtFloat,        StaticParameterInfo::pfNone| StaticParameterInfo::pfRuntimeChangeable}},
  
  {"SensOK",            {atpidSensOK,           dtString,       StaticParameterInfo::pfNone }},
  {"ValidOK",           {atpidValidOK,          dtString,       StaticParameterInfo::pfNone }},
  {"WarnHi",            {atpidWarnHi,           dtString,       StaticParameterInfo::pfNone }},
  {"WarnLo",            {atpidWarnLo,           dtString,       StaticParameterInfo::pfNone }},
  {"AlarmHi",           {atpidAlarmHi,          dtString,       StaticParameterInfo::pfNone }},
  {"AlarmLo",           {atpidAlarmLo,          dtString,       StaticParameterInfo::pfNone }},
  {"SigNorm",           {atpidSigNorm,          dtString,       StaticParameterInfo::pfNone }}
  
  
};

const ParameterVector AIDriver::myParameters
{
  {"logSize", {atpidLogSize, dtDWord, StaticParameterInfo::pfRuntimeChangeable | StaticParameterInfo::pfRebootRequired}},
  {"periodType", {atpidPeriodType, dtByte, StaticParameterInfo::pfRuntimeChangeable | StaticParameterInfo::pfRebootRequired}}
};

AIDriver::AIDriver() : ParameterizedObject(myParameters) {}

DriverContext *AIDriver::createTagContext() const
{
  AIDriverTagContext *pAITagDriverContext = new (std::nothrow) AIDriverTagContext;
  ADC_Channel *padcChannel = new (std::nothrow) ADC_Channel;
  pAITagDriverContext->aiDriver = const_cast<AIDriver*>(this);
  pAITagDriverContext->adcChannel = padcChannel;
  ADC_Init(padcChannel); /* Set some default params */
  const_cast<AIDriver*>(this)->channelVector.push_back(padcChannel);
  return pAITagDriverContext;
}

AIDriverTagContext::~AIDriverTagContext()
{
  if(this->pAdcConverter)
    delete this->pAdcConverter;
  delete this->adcChannel;
  aiDriver->channelVector.pop_back();
}

/**
* @brief  Tell driver builder about our driver parametrs
*/
bool AIDriver::setParameterById(uint32_t paramId, VarData newValue)
{
  switch (static_cast<ParamIDs>(paramId))
  {
  case atpidLogSize:
    if((newValue.wordVal > 99) && (newValue.wordVal < 3001))
      ADC_SetLogSize(newValue.wordVal);
    else
      ADC_SetLogSize(0);
    return true;
  case atpidPeriodType:
    {
      ADC_Period setPeriod = ADC_TIM_100US;
      switch(newValue.byteVal)
      {
      case 1:
        setPeriod = ADC_TIM_1S;
        break;
      case 2:
        setPeriod = ADC_TIM_500MS;
        break;
      case 3:
        setPeriod = ADC_TIM_1MS;
        break;
      default:
        break;
      }
      ADC_TIM_SetPeriod(setPeriod);
    }
    return true;
  }
  return false;
}

/**
* @brief  Tell driver builder about our tag parametrs
*/
bool AIDriverTagContext::setParameterById(uint32_t paramId, VarData newValue)
{
  auto tagParamId = static_cast<AITagParamIDs>(paramId);
  switch (tagParamId)
  {
  case atpidTest:
    adcChannel->test = newValue.boolVal;
    break;
  case atpidAInum:
    {
      uint8_t numberOfChannel = newValue.byteVal - 1;
      if (((adcChannel->test) || (adcBoardSpec[numberOfChannel].adcTypes != ADC_NONE))
          && (numberOfChannel < ADC_CANNELS_CNT))
      {
        adcChannel->pADC_BoardSpec = &adcBoardSpec[numberOfChannel];
        if(adcChannel->test)
          adcChannel->pADC_BoardSpec->highRawPoint = 65535;
      }
      else
        return false;
    }
    break;
  case atpidNumPoints:
    if(newValue.byteVal > 0)
      adcChannel->numPoints = newValue.byteVal;
    break;
  case atpidZeroPoint:
    adcChannel->zeroPoint = newValue.fltVal;
    break;
  case atpidHighPoint:
    if(newValue.fltVal > adcChannel->zeroPoint)
      adcChannel->highPoint = newValue.fltVal;
    else
      return false;
    break;
  case atpidKRatio:
    if(newValue.fltVal != 0)
      kRatio = newValue.fltVal;
    else
      return false;
    break;
  case atpidCalibK:
    calibK = newValue.fltVal;
    break;
  case atpidConvertType:
    pAdcConverter = nullptr;
    break;
  case atpidPrimaryPoint:
    if(pAdcConverter == nullptr)
      primaryPoint = P_SECONDARY;
    else
      primaryPoint = newValue.boolVal ? P_PRIMARY : P_SECONDARY;
      break;
  case atpidAperture:
    if(newValue.fltVal > 0)
      adcChannel->aperture = ADC_ConvertSecondaryToRaw(adcChannel, newValue.fltVal) - ADC_ConvertSecondaryToRaw(adcChannel, 0);
    else
      adcChannel->aperture = 0;
    break;
  case atpidLoggerEn:
    if(ADC_GetLogCtrStruct()->logSize)
      adcChannel->loggerEn = newValue.boolVal;
    else
      adcChannel->loggerEn = false;
    break;
  case atpidFilter:
    break;
    
  case atpidPVminOK:
    minOK = newValue.fltVal;
    break;
  case atpidPVmaxOK:
    maxOK = newValue.fltVal;
     break;
  case atpidPVminValid:
    minValid = newValue.fltVal;
     break;
  case atpidPVmaxValid:
    maxValid = newValue.fltVal;
     break;
    
  case atpidPVLoLo:
    LoLo = newValue.fltVal;
    break;
  case atpidPVLo:
    Lo = newValue.fltVal;
    break;
  case atpidPVHi:
    Hi = newValue.fltVal;
    break;
  case atpidPVHiHi:
    HiHi = newValue.fltVal;
    break;
    
  case atpidSensOK:
    sSensOK = newValue.pStr;
    isValidPresent = true;
    break;
  case atpidValidOK:
    sValidOK = newValue.pStr;
    isValidPresent = true;
    break;
  case atpidWarnHi:
    sWarnHi = newValue.pStr;
    isValLimitPresent = true;
    break;
  case atpidWarnLo:
    sWarnLo = newValue.pStr;
    isValLimitPresent = true;    
    break;
  case atpidAlarmHi:
    sAlarmHi = newValue.pStr;
    isValLimitPresent = true;
    break;
  case atpidAlarmLo:
    sAlarmLo = newValue.pStr;
    isValLimitPresent = true;
    break;
  case atpidSigNorm:
    sSigNorm = newValue.pStr;
    isValLimitPresent = true;
    break;
    
  default:
    break;
  }
  return true;
}

/**
* @brief Calls after all setParameterById() calls
*/
void AIDriver::registerTag(const Tag &tag, void *ctx)
{
  auto entry = reinterpret_cast<AIDriverTagContext *>(ctx);
  if(entry->adcChannel->filterType == ADC_KALMAN)
  {
    sKalman *pKalman = new(std::nothrow) sKalman;
    Kalman_init(pKalman, VAR_VALUE_DEFAULT, VAR_PROCESS_DEFAULT);
    entry->adcChannel->fParams = pKalman;
  }
  
  if(entry->adcChannel->loggerEn == true)
  {
    tagLogEnableList.push_front(entry);
  }
  
  DriverCommand *pDCommand = new(std::nothrow) DriverCommand(this, &tag, { 0 }, 0);
  entry->adcChannel->dcmdAdcRdy = pDCommand;
}

/**
* @brief Set default psevdoTag Driver params
*/
VarData AIDriver::getRuntimeParameter(uint32_t paramId)
{
  VarData result;
  auto tagParamId = static_cast<AITagParamIDs>(paramId);
  switch(tagParamId)
  {
  case atpidLogSize:
    result.dwordVal = ADC_GetLogSize();
    break;
  default:
    result.dwordVal = 0;
    break;
  }
  return result;
}

/**
* @brief Set default psevdoTag Tag params from saved json
*/
VarData AIDriver::getTagRuntimeParameter(const Tag& tag, uint32_t paramId)
{
  VarData result;
  AIDriverTagContext *ctx = reinterpret_cast<AIDriverTagContext *>(tag.getDriverSpecificInfo(getId()));
  switch (paramId)
  {
  case atpidAperture:
    result.fltVal = ctx->adcChannel->highPoint + ADC_ConvertRawToSecondary(ctx->adcChannel, ctx->adcChannel->aperture);
    break;
  case atpidKRatio:
    result.fltVal = ctx->kRatio;
    break;
  case atpidCalibK:
    result.fltVal = ctx->calibK;
    break;
    
  default:
    result.dwordVal = 0;
    break;
  }
  return result;
}

/**
* @brief Calls from Runtime in order to update our PsevdoTag value connect to Driver
* @note  StaticParameterInfo::pfRuntimeChangeable
*/
bool AIDriver::setRuntimeParameter(const Tag &tag, uint32_t paramId, VarData paramValue)
{
  auto paramIDs = static_cast<ParamIDs>(paramId);
  switch(paramIDs)
  {
  case atpidLogSize:
    if((paramValue.wordVal > 99) && (paramValue.wordVal < 3001))
    {
      ADC_SetLogSize(paramValue.wordVal);
      return true;
    }
    break;
  case atpidPeriodType:
    {
      ADC_Period setPeriod = ADC_TIM_100US;
      switch(paramValue.byteVal)
      {
      case 1:
        setPeriod = ADC_TIM_1S;
        break;
      case 2:
        setPeriod = ADC_TIM_500MS;
        break;
      case 3:
        setPeriod = ADC_TIM_1MS;
        break;
      default:
        break;
      }
      ADC_TIM_SetPeriod(setPeriod);
      return true;
    }
  default:
    break;
  }
  return false;
}

/**
* @brief Calls from Runtime in order to update our PsevdoTag value connect to Tag
* @note  StaticParameterInfo::pfRuntimeChangeable
*/
bool AIDriver::setTagRuntimeParameter(const Tag& tag, uint32_t paramId, VarData paramValue)
{
  AIDriverTagContext *ctx = reinterpret_cast<AIDriverTagContext *>(tag.getDriverSpecificInfo(getId()));
  switch(paramId)
  {
  case atpidAperture:
    ctx->adcChannel->aperture = ADC_ConvertSecondaryToRaw(ctx->adcChannel, paramValue.fltVal) - ADC_ConvertSecondaryToRaw(ctx->adcChannel, 0);
    break;
  case atpidKRatio:
    if(paramValue.fltVal)
      ctx->kRatio = paramValue.fltVal;
    else
      return false;
    break;
    
  case atpidCalibK:
    ctx->calibK = paramValue.fltVal;
    break;
    
  case atpidPVminOK:
    ctx->minOK = paramValue.fltVal;
    break;
  case atpidPVmaxOK:
    ctx->maxOK  = paramValue.fltVal;
    break;
  case atpidPVminValid:
    ctx->minValid = paramValue.fltVal;
    break;
  case atpidPVmaxValid:
    ctx->maxValid = paramValue.fltVal;
    break;
  case atpidPVLoLo:
    ctx->LoLo = paramValue.fltVal;
    break;
  case atpidPVLo:
    ctx->Lo = paramValue.fltVal;
    break;
  case atpidPVHi:
    ctx->Hi = paramValue.fltVal;
    break;
  case atpidPVHiHi:
    ctx->HiHi = paramValue.fltVal;
    break;
  }
  return true;
}

/**
* @brief Calls from Runtime in order to update our Tag value
* @note  return true in order to have the same signature
*/
bool AIDriver::tagChanged(const Tag &tag, uint32_t, VarData newValue)
{
  AIDriverTagContext *ctx = reinterpret_cast<AIDriverTagContext *>(tag.getDriverSpecificInfo(getId()));
  
  float tempValue = ((ctx->adcChannel->value * ctx->kRatio) + ctx->calibK);
  
  if(ctx->isValidPresent)
  {
    if(tempValue >= ctx->maxValid && ctx->maxValid != ctx->invalid)
    {
      Core::setIfChanged(*ctx->validOK, false , true, getId(), getShortTS());
      
      if(tempValue >= ctx->maxOK && ctx->maxOK != ctx->invalid)
      {
        Core::setIfChanged(*ctx->sensOK, false , true, getId(), getShortTS());
      }
    }
    else if( tempValue <= ctx->minValid && ctx->minValid != ctx->invalid)
    {
      Core::setIfChanged(*ctx->validOK, false , true, getId(), getShortTS());
      
      if(tempValue <= ctx->minOK &&  ctx->minOK != ctx->invalid)
      {
        Core::setIfChanged(*ctx->sensOK, false , true, getId(), getShortTS());
      }
    }
    else 
    {
      Core::setIfChanged(*ctx->sensOK, true , true, getId(), getShortTS());
      Core::setIfChanged(*ctx->validOK, true , true, getId(), getShortTS());
    }
  }
  
  if(ctx->isValLimitPresent)
  {
    if(tempValue >= ctx->Hi && ctx->Hi != ctx->invalid)
    {
      Core::setIfChanged(*ctx->warnLo, false , true, getId(), getShortTS());
      Core::setIfChanged(*ctx->alarmLo, false , true, getId(), getShortTS());
      
      Core::setIfChanged(*ctx->sigNorm, false , true, getId(), getShortTS());
      Core::setIfChanged(*ctx->warnHi, true , true, getId(), getShortTS());
      
      if(tempValue >= ctx->HiHi && ctx->HiHi != ctx->invalid)
      {
        Core::setIfChanged(*ctx->alarmHi, true , true, getId(), getShortTS());
      }
    }
    else if(tempValue <= ctx->Lo && ctx->Lo != ctx->invalid)
    {
      Core::setIfChanged(*ctx->warnHi, false , true, getId(), getShortTS());
      Core::setIfChanged(*ctx->alarmHi, false , true, getId(), getShortTS());
      Core::setIfChanged(*ctx->sigNorm, false , true, getId(), getShortTS());
      Core::setIfChanged(*ctx->warnLo, true , true, getId(), getShortTS());
      
      if(tempValue <= ctx->LoLo  && ctx->LoLo != ctx->invalid)
      {
        Core::setIfChanged(*ctx->alarmLo, true , true, getId(), getShortTS());
      }
    }
    else // all Good
    {
      if(ctx->isValidPresent)
      {
        if(ctx->sensOK->get<bool>() && ctx->validOK->get<bool>()){
          Core::setIfChanged(*ctx->sigNorm, true , true, getId(), getShortTS());
        }
        else
        {
          Core::setIfChanged(*ctx->sigNorm, false , true, getId(), getShortTS());
        }
      }
      else
      {
        Core::setIfChanged(*ctx->sigNorm, true , true, getId(), getShortTS());
      }
      
      Core::setIfChanged(*ctx->warnHi, false , true, getId(), getShortTS());
      Core::setIfChanged(*ctx->alarmHi, false , true, getId(), getShortTS());
      Core::setIfChanged(*ctx->warnLo, false , true, getId(), getShortTS());
      Core::setIfChanged(*ctx->alarmLo, false , true, getId(), getShortTS());
    }
  }
  
  Core::set(const_cast<Tag&>(tag), tempValue , true, getId(), getShortTS());
  return true;
}

/**
* @brief Calls ones time in drivers thread before run()
*/
void AIDriver::initialize()
{
  Driver::initialize();
  
  for(auto tagPtr: *tagsList)
  {
    auto context = reinterpret_cast<AIDriverTagContext *>(tagPtr->getDriverSpecificInfo(id));
    
    context->sensOK = Core::instance().getTag(context->sSensOK);                 
    context->validOK = Core::instance().getTag(context->sValidOK);  
    context->warnHi = Core::instance().getTag(context->sWarnHi);                        
    context->warnLo = Core::instance().getTag(context->sWarnLo);                        
    context->alarmHi = Core::instance().getTag(context->sAlarmHi);                       
    context->alarmLo = Core::instance().getTag(context->sAlarmLo);                       
    context->sigNorm = Core::instance().getTag(context->sSigNorm);    
  }
  
  ADC_SetNumChannels(channelVector.size());
  ADC_SetChannelsPointer(channelVector.data());
  ADC_SetRDYQueue(commandQueue->getHandle());
  ADC_InitTask();
}

void AIDriver::run(){ }

void AIDriver::copyData(HistoricalData *pData, ADC_LogCntr *pADCLogCtr, uint32_t P, uint32_t posTrg, bool ringHopping)
{
  /* Copy data from ring buffer to pData */
  uint32_t totalWrPoints = 0, ind = 0;
  if(ringHopping)
  {
    /* Copy before ring hopping */
    for(uint32_t i = posTrg, ch = 0; i < pADCLogCtr->nSamples[0]; i++, ch = 0) 
    {
      for(auto it : tagLogEnableList)
      {
        pData->pData[totalWrPoints].fltVal = 
          (ADC_ConvertRawToSecondary(it->adcChannel, pADCLogCtr->logBuffers[0].padcChannels[ch][i]) 
           * it->kRatio) + it->calibK;
        totalWrPoints++;
        ch++;
      }
      ind++;
    }
    
    /* Copy after ring hopping */
    for(uint32_t i = 0, ch = 0; i < pADCLogCtr->bufPos[0]; i++, ch = 0) 
    {
      for(auto it : tagLogEnableList)
      {
        pData->pData[totalWrPoints].fltVal = (ADC_ConvertRawToSecondary(it->adcChannel, pADCLogCtr->logBuffers[0].padcChannels[ch][i]) 
                                              * it->kRatio) + it->calibK;
        totalWrPoints++;
        ch++;
      }
      ind++;
    }
  }
  else
  {
    for(uint32_t i = posTrg, ch = 0; i < pADCLogCtr->bufPos[0]; i++, ch = 0) 
    {
      for(auto it : tagLogEnableList)
      {
        pData->pData[totalWrPoints].fltVal = (ADC_ConvertRawToSecondary(it->adcChannel, pADCLogCtr->logBuffers[0].padcChannels[ch][i]) 
                                              * it->kRatio) + it->calibK;
        totalWrPoints++;
        ch++;
      }
      ind++;
    }
  }
  
  /* Copy data from linear buffer to pData */
  for(uint32_t i = 0, ch = 0; i < pADCLogCtr->nSamples[1] - ind; i++, ch = 0) 
  {
    for(auto it : tagLogEnableList)
    {
      pData->pData[totalWrPoints].fltVal = (ADC_ConvertRawToSecondary(it->adcChannel, pADCLogCtr->logBuffers[1].padcChannels[ch][i]) 
                                            * it->kRatio) + it->calibK;
      totalWrPoints++;
      ch++;
    }     
  }
}

bool AIDriver::getHistoricalData(uint64_t ts, HistoricalData *pData)
{
  bool result = false;
  if(ADC_GetLogSize()) 
  {
    ADC_LogCntr *pADCLogCtr = ADC_GetLogCtrStruct();
    pADCLogCtr->xLoggerTask = xTaskGetCurrentTaskHandle();
    xTaskNotify( pADCLogCtr->xAdcTask, 0x02, eSetValueWithOverwrite ); 
    
    /* Find position with asked timestamp in ring buffer */
    
    pData->samplesPerMs  = pADCLogCtr->samplesPerMs;
    pData->nSamples      = pADCLogCtr->nSamples[1];
    pData->tsBegin       = ts;
    
    uint64_t trigMs      = ts;
    uint64_t bufStartMs  = pADCLogCtr->systemTs;
    
    uint32_t currentPos = pADCLogCtr->bufPos[0];
    uint32_t p, posTrg  = 0;
    bool ringHopping    = false;
    
    if(trigMs >= bufStartMs)    
    {
      p = (trigMs - bufStartMs) * pData->samplesPerMs;
      if(p > pADCLogCtr->nSamples[0])
        result = false;
      else
      {
        posTrg = currentPos - p;
        result = true;
      }
    }
    else if(trigMs < bufStartMs) 
    {
      ringHopping = true;
      p = (bufStartMs - trigMs) * pData->samplesPerMs;
      if(p > pADCLogCtr->nSamples[0])
        result = false;
      else
      {
        posTrg = pADCLogCtr->nSamples[0] - p;
        if(currentPos > posTrg)
          result = false;
        else    
          result = true;
      }
    }
    
    if(result) /* Asked timestamp is correct and exists in AIDrivers buffer */
    {
      uint32_t ulNotifiedValue;
      xTaskNotifyWait(0x00, ULONG_MAX, &ulNotifiedValue, portMAX_DELAY); 
      
      pData->pData = new VarData[pADCLogCtr->nSamples[1] * pADCLogCtr->enabledLogChannels];
      if(!pData->pData)
        result = false;
      else
      {
        copyData(pData, pADCLogCtr, p, posTrg, ringHopping);
      }
    }  
    
    /* Return buffer to init state */  
    pADCLogCtr->bufPos[0] = 0;
    pADCLogCtr->bufPos[1] = 0;
    pADCLogCtr->systemTs = getLongTS();
    pADCLogCtr->logBuffers[0].busy = false;
    pADCLogCtr->logBuffers[1].busy = false;
    pADCLogCtr->currentBuffer = 0;
  }
  return result;
}

bool AIDriver::supportsHistory()
{
  return true;
}  
