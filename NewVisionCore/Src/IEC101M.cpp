#include <DriverBuilder.hpp>
#include <IECSlaveBase.hpp>
#include <algorithm>
#include <IEC101M.hpp>

REGISTER_FACTORY_METHOD(DriverBuilder::driverFactoryTable, Driver, IEC101MasterDriver, new IEC101MDriver);
REGISTER_FACTORY_METHOD(DeviceBuilder::deviceFactoryTable, Device, IEC101GenericSlave, new IEC101GenericDevice);

enum IEC101DeviceParamId : uint32_t
{
  idpidLinkAddress,
  idpidAsduAddress,
  idpidGIInterval,
  idpidTSInterval,
  idpidCommWindow
};

enum IEC101DriverParamIDs : uint32_t
{
  idrpidUART,
  idrpidBaudRate,
  idrpidStopBits,
  idrpidParity,
  idpidCommTimeout,
  idpidUseSingleCharACK
};

const ParameterVector IEC101GenericDevice::parameters
{
  {"linkAddress", {idpidLinkAddress, dtWord, StaticParameterInfo::pfRequired}},
  {"asduAddress", {idpidAsduAddress, dtWord, StaticParameterInfo::pfNone}},
  {"giInterval", {idpidGIInterval, dtDWord, StaticParameterInfo::pfNone}},
  {"tsInterval", {idpidTSInterval, dtDWord, StaticParameterInfo::pfNone}},
  {"commWindow", {idpidCommWindow, dtWord, StaticParameterInfo::pfNone}}
};

const ParameterVector IEC101MDriver::parameters
{
  {"UART", {idrpidUART, dtByte, StaticParameterInfo::pfRequired}},
  {"baudRate", {idrpidBaudRate, dtDWord, StaticParameterInfo::pfRequired}},
  {"stopBits", {idrpidStopBits, dtByte, StaticParameterInfo::pfNone}},
  {"parity", {idrpidParity, dtString, StaticParameterInfo::pfNone}},
  {"commTimeout", {idpidCommTimeout, dtWord, StaticParameterInfo::pfNone}},
  {"useSingleCharACK", {idpidCommTimeout, dtBool, StaticParameterInfo::pfNone		}	},
};

const IEC101GenericDevice::IOHandlerFn IEC101GenericDevice::ioHandlers[128]
{
  0,
  &IEC101GenericDevice::handleMSPNA1,           //    M_SP_NA_1 = 1,
  0,           																	//    M_SP_TA_1 = 2,
  &IEC101GenericDevice::handleMDPNA1,           //    M_DP_NA_1 = 3,
  0,           																	//    M_DP_TA_1 = 4,
  0,           																	//    M_ST_NA_1 = 5,
  0,           																	//    M_ST_TA_1 = 6,
  0,           																	//    M_BO_NA_1 = 7,
  0,           																	//    M_BO_TA_1 = 8,
  0,           																	//    M_ME_NA_1 = 9,
  0,           																	//    M_ME_TA_1 = 10,
  &IEC101GenericDevice::handleMMENB1,           //    M_ME_NB_1 = 11,
  0,           																	//    M_ME_TB_1 = 12,
  &IEC101GenericDevice::handleMMENC1,           //    M_ME_NC_1 = 13,
  0,           																	//    M_ME_TC_1 = 14,
  0,           																	//    M_IT_NA_1 = 15,
  0,           																	//    M_IT_TA_1 = 16,
  0,           																	//    M_EP_TA_1 = 17,
  0,           																	//    M_EP_TB_1 = 18,
  0,           																	//    M_EP_TC_1 = 19,
  0,           																	//    M_PS_NA_1 = 20,
  0,           																	//    M_ME_ND_1 = 21,
  0,           																	//22
  0,           																	//23
  0,           																	//24
  0,           																	//25
  0,           																	//26
  0,           																	//27
  0,           																	//28
  0,           																	//29
  &IEC101GenericDevice::handleMSPTB1,           //    M_SP_TB_1 = 30,
  &IEC101GenericDevice::handleMDPTB1,           //    M_DP_TB_1 = 31,
  0,           																	//    M_ST_TB_1 = 32,
  0,           																	//    M_BO_TB_1 = 33,
  0,           																	//    M_ME_TD_1 = 34,
  &IEC101GenericDevice::handleMMETE1,           //    M_ME_TE_1 = 35,
  &IEC101GenericDevice::handleMMETF1,           //    M_ME_TF_1 = 36,
  0,           																	//    M_IT_TB_1 = 37,
  0,           																	//    M_EP_TD_1 = 38,
  0,           																	//    M_EP_TE_1 = 39,
  0,           																	//    M_EP_TF_1 = 40,
  0,           																	//41
  0,           																	//42
  0,           																	//43
  0,           																	//44
  0,           																	//    C_SC_NA_1 = 45,
  0,           																	//    C_DC_NA_1 = 46,
  0,           																	//    C_RC_NA_1 = 47,
  0,           																	//    C_SE_NA_1 = 48,
  0,           																	//    C_SE_NB_1 = 49,
  0,           																	//    C_SE_NC_1 = 50,
  0,           																	//    C_BO_NA_1 = 51,
  0,           																	//52
  0,           																	//53
  0,           																	//54
  0,           																	//55
  0,           																	//56
  0,           																	//57
  0,           																	//    C_SC_TA_1 = 58,
  0,           																	//    C_DC_TA_1 = 59,
  0,           																	//    C_RC_TA_1 = 60,
  0,           																	//    C_SE_TA_1 = 61,
  0,           																	//    C_SE_TB_1 = 62,
  0,           																	//    C_SE_TC_1 = 63,
  0,           																	//    C_BO_TA_1 = 64,
  0,           																	//65
  0,           																	//66
  0,           																	//67
  0,           																	//68
  0,           																	//69
  0,           																	//    M_EI_NA_1 = 70,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,            //80
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,            //90
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  0,
  &IEC101GenericDevice::handleCICNA1,           //    C_IC_NA_1 = 100,
  0,           																	//    C_CI_NA_1 = 101,
  0,           																	//    C_RD_NA_1 = 102,
  0,           																	//    C_CS_NA_1 = 103,
  0,           																	//    C_TS_NA_1 = 104,
  0,           																	//    C_RP_NA_1 = 105,
  0,           																	//    C_CD_NA_1 = 106,
  0,           																	//    C_TS_TA_1 = 107,
  0,           																	//108
  0,           																	//109
  0,           																	//    P_ME_NA_1 = 110,
  0,           																	//    P_ME_NB_1 = 111,
  0,           																	//    P_ME_NC_1 = 112,
  0,           																	//    P_AC_NA_1 = 113,
  0,           																	//114
  0,           																	//115
  0,           																	//116
  0,           																	//117
  0,           																	//118
  0,           																	//119
  0,           																	//    F_FR_NA_1 = 120,
  0,           																	//    F_SR_NA_1 = 121,
  0,           																	//    F_SC_NA_1 = 122,
  0,           																	//    F_LS_NA_1 = 123,
  0,           																	//    F_AF_NA_1 = 124,
  0,           																	//    F_SG_NA_1 = 125,
  0,           																	//    F_DR_TA_1 = 126,
  0										                          //    F_SC_NB_1 = 127
};

bool IEC101MDriver::setParameterById(uint32_t id, VarData newValue)
{
  switch (static_cast<IEC101DriverParamIDs>(id))
  {
  case idrpidUART:
    if ((newValue.byteVal > 0) && (newValue.byteVal <= MAX_HUART) && (huarts[newValue.byteVal - 1]))
    {
      huart = huarts[newValue.byteVal - 1];
      return true;
    }
    return false;
  case idrpidBaudRate:
    baudRate = newValue.dwordVal;
    return true;
  case idrpidStopBits:
    if (newValue.byteVal == 1 || newValue.byteVal == 2)
    {
      stopBits = newValue.byteVal;
      return true;
    }
    return false;
  case idrpidParity:
    if (newValue.pStr[0] == 'E' || newValue.pStr[0] == 'O' || newValue.pStr[0] == 'N')
    {
      parity = newValue.pStr[0];
      return true;
    }
    return false;
  case idpidCommTimeout:
    commTimeout = newValue.wordVal;
    return true;
  case idpidUseSingleCharACK:
    useSingleCharAck = newValue.boolVal;
    return true;
  }
  return false;
}

bool IEC101MDriver::setRuntimeParameter(const Tag &tag, uint32_t paramId, VarData paramValue)
{
  if (rsRunning == runState)
  {
    switch (paramId)
    {
    case CMD_IDLE:
      comIdle = true;
      return true;
    case CMD_TX_COMPLETE:
      comIdle = false;
      nextTimeout = getShortTS() + commTimeout;
      return true;
    default:
      return false;
    }
  }
  return false;
}

bool IEC101MDriver::registerDevice(Device *device, void *ctx)
{
  VarData linkAddr = NO_DATA;
  if (device->getParameterById(idpidLinkAddress, linkAddr))
  {
    deviceMap[linkAddr.dwordVal] = d_cast<IEC101GenericDevice *>(device);
    //reinterpret_cast<IEC101GenericDevice *>(device->getUnderlyingObject());
    return true;
  }
  return false;
}

bool IEC101MDriver::tagChanged(const Tag &tag, uint32_t paramId, VarData newValue)
{
  //reinterpret_cast<IEC101GenericDevice *>(device->getUnderlyingObject());
  auto device = tag.getDevice();
  if (device)
  {
    auto iecDevice = d_cast<IEC101GenericDevice*>(device);
    if(device->tagChanged(tag, paramId, newValue))
      waitingCommandsCnt++;
  }
  return true;
}

void IEC101MDriver::initialize()
{
  Driver::initialize();
  port = SerialPort_create(huart, baudRate, parity, stopBits);
  sLinkLayerParameters llParameters;
  llParameters.addressLength = 1;
  llParameters.timeoutForAck = commTimeout;
  llParameters.timeoutRepeat = 2 * commTimeout;
  llParameters.useSingleCharACK = useSingleCharAck;
  if (port)
  {
    SerialPort_subscribeToEvents(port, commandQueue->getHandle(), &dcmdSerialIdle, &dcmdSerialTxComplete);
    master = CS101_Master_create(port, &llParameters, NULL, IEC60870_LINK_LAYER_UNBALANCED);
    if (master)
    {
      CS101_Master_setASDUReceivedHandler(master, asduReceivedHandlerStatic, this);            // Set handler for each master
      CS101_Master_setLinkLayerStateChanged(master, linkLayerStateChangedHandlerStatic, this); // Set LinkStatusChangedHandler for each master
      SerialPort_open(port);
      for (auto itDev : *devicesList)
      {
        itDev->initialize(master);
        dv.push_back(d_cast<IEC101GenericDevice *>(itDev));
      }
      if (dv.size() > 0)
      {
        currentDevice = dv.begin();
        commandQueue->getTimer().setPeriod(20);
        commandQueue->getTimer().rearm();
      }
      else
        runState = rsInitializationErr;
      return;
    }
  }
  runState = rsInitializationErr;
}

void IEC101MDriver::run()
{
  switch (runState)
  {
  case rsInitial:
    runState = dv.size() == 1 ? rsRunningSingleSlave : rsRunning;
    break;
  case rsRunning:
    if (comIdle)
    {
      if (commandQueue->getTimer().isLate())
      {
        commandQueue->getTimer().rearm();
        //comIdle = false;
        if(waitingCommandsCnt)
        {
          auto deviceWithCommands = *(std::find_if(dv.begin(), dv.end(), [](auto dev){return dev->getWaitingCommandsCount() > 0;}));
          if (deviceWithCommands->sendQueuedCommand())
            waitingCommandsCnt--;
        }
        else
        {
          DeviceVector::iterator itDev = dv.end();
          if ((itDev = std::find_if(dv.begin(), dv.end(), [](auto dev){return dev->giCondition();})) != dv.end())
          {
            (*itDev)->beginGI();
          }
          else if ((itDev = std::find_if(dv.begin(), dv.end(), [](auto dev){return dev->tsCondition();})) != dv.end())
          {
            (*itDev)->issueTimeSync();
          }
          else
          {
            CS101_Master_pollSingleSlave(master, (*currentDevice)->getLinkAddress());
            if (++currentDevice == dv.end())
              currentDevice = dv.begin();
          }
        }
        CS101_Master_run(master);
      }
    }
    else
    {
      CS101_Master_run(master);
      if (getShortTS() > nextTimeout)
      {
        comIdle = true;
        commandQueue->getTimer().rearm();
        comSkipCount++;
      }
    }
    break;
  case rsRunningSingleSlave:
    if (comIdle)
    {
      if (commandQueue->getTimer().isLate())
      {
        commandQueue->getTimer().rearm();
        //comIdle = false;
        if(waitingCommandsCnt)
        {

          if (dv[0]->sendQueuedCommand())
            waitingCommandsCnt--;
        }
        else
        {
          if (dv[0]->giCondition())
          {
            dv[0]->beginGI();
          }
          else if (dv[0]->tsCondition())
          {
            dv[0]->issueTimeSync();
          }
          else
          {
            CS101_Master_pollSingleSlave(master, dv[0]->getLinkAddress());
          }
        }
        CS101_Master_run(master);
      }
    }
    else
    {
      CS101_Master_run(master);
      if (getShortTS() > nextTimeout)
      {
        comIdle = true;
        commandQueue->getTimer().rearm();
        comSkipCount++;
      }
    }
    break;
  case rsInitializationErr:
    break;
  default:
    break;
  }
}

/**
* @brief  handleIncomingData - common logic for all types of received ASDU
* @param  void *pData
* @retval true - OK, false - OK
*/
bool IEC101GenericDevice::handleIncomingData(void *pData)
{
  bool result = false;
  auto asdu = reinterpret_cast<CS101_ASDU>(pData);
  IOHandlerFn fn = ioHandlers[CS101_ASDU_getTypeID(asdu)];
  if (!fn)
    return false;
  auto cot = CS101_ASDU_getCOT(asdu);
  int index = 0;
  for (; index < CS101_ASDU_getNumberOfElements(asdu); index++)
  {
    auto io = CS101_ASDU_getElement(asdu, index);
    auto ioa = InformationObject_getObjectAddress(io);
    if (0 != ioa)
    {
      auto itTag = ioaMap.find(ioa);
      if (ioaMap.end() != itTag)
        result |= (this->*fn)(*itTag->second, io, cot);
    }
    else
    {
      result |= (this->*fn)(Tag::NoTagRef, io, cot);
    }
    InformationObject_destroy(io);
  }
  return result;
}

void IEC101GenericDevice::handleLinkLayerMessage(void *pMessage)
{
  LinkLayerState lls = (LinkLayerState)((uint32_t)pMessage);
  linkState = lls;
  if (LL_STATE_ERROR == linkState)
  {
    runState = rsOffline;
    offlineCheckTS = getShortTS() + 20000;
    writeQueue.clear();
    for (auto it : ioaMap)
    {
      Core::instance().setTag(it.second, it.second->getValue(), false, id);
    }
  }
}

bool IEC101GenericDevice::setParameterById(uint32_t id, VarData newValue)
{
  auto paramId = static_cast<IEC101DeviceParamId>(id);
  switch (paramId)
  {
  case idpidLinkAddress:
    linkAddress = newValue.wordVal;
    return true;
  case idpidAsduAddress:
    asduAddress = newValue.wordVal;
    return true;
  case idpidGIInterval:
    giInterval = newValue.dwordVal * 1000;
    return true;
  case idpidTSInterval:
    tsInterval = newValue.dwordVal * 1000;
    return true;
  case idpidCommWindow:
    commWindow = newValue.dwordVal;
    return true;
  }
  return false;
}

bool IEC101GenericDevice::getParameterById(uint32_t id, VarData &newValue) const
{
  auto paramId = static_cast<IEC101DeviceParamId>(id);
  switch (paramId)
  {
  case idpidLinkAddress:
    newValue.wordVal = linkAddress;
    return true;
  case idpidAsduAddress:
    newValue.wordVal = asduAddress;
    return true;
  case idpidGIInterval:
    newValue.dwordVal = giInterval / 1000;
    return true;
  case idpidTSInterval:
    newValue.dwordVal = tsInterval / 1000;
    return true;
  case idpidCommWindow:
    newValue.dwordVal = commWindow;
    return true;
  }
  return false;
}

bool IEC101GenericDevice::registerTag(const Tag &tag, void *context)
{
  auto ctx = reinterpret_cast<IECSContext *>(context);
  ioaMap[ctx->ioa] = const_cast<Tag *>(&tag);
  return true;
}

void IEC101GenericDevice::initialize(void *pvParameter)
{
  if (rsInitial == runState)
  {
    auto master = reinterpret_cast<CS101_Master>(pvParameter);
    CS101_Master_addSlave(master, linkAddress);
    this->master = master;
    auto llParameters = CS101_Master_getLinkLayerParameters(master);
    if (llParameters->timeoutForAck > commWindow)
      commWindow = llParameters->timeoutForAck;
    runState = rsUnselected;
  }
}

void IEC101GenericDevice::select(void *pvParameter)
{
  //	timer = reinterpret_cast<QTimer *>(pvParameter);
  if(rsSelected != runState && rsInitial != runState && rsOffline != runState)
  {
    CS101_Master_useSlaveAddress(master, linkAddress);
    //timer->setPeriod(commWindow);
    //timer->rearm();
    runState = rsSelected;
  }
}

bool IEC101GenericDevice::sendQueuedCommand()
{
  if (CS101_Master_isChannelReady(master, linkAddress))
  {
    CS101_Master_useSlaveAddress(master, linkAddress);
    CS101_Master_sendProcessCommand(master, CS101_COT_ACTIVATION, asduAddress, writeQueue.front());
    InformationObject_destroy(writeQueue.front());
    writeQueue.pop_front();
    return true;
  }
  return false;
}

/**
* @brief  Sends GI command
* @param  No param
* @retval true - OK, false - Error
*/
bool IEC101GenericDevice::beginGI()
{
  if (CS101_Master_isChannelReady(master, linkAddress))
  {
    CS101_Master_useSlaveAddress(master, linkAddress);
    CS101_Master_sendInterrogationCommand(master, CS101_COT_ACTIVATION, asduAddress, IEC60870_QOI_STATION);
    scheduleNextGI();
    return true;
  }
  return false;
}

/**
* @brief  Sends Time Sync Command
* @param  No param
* @retval true - OK, false - Error
*/
bool IEC101GenericDevice::issueTimeSync()
{
  if (CS101_Master_isChannelReady(master, linkAddress))
  {
    struct sCP56Time2a newTime;
    CS101_Master_useSlaveAddress(master, linkAddress);
    CP56Time2a_createFromMsTimestamp(&newTime, Hal_getTimeStampInMs());
    CS101_Master_sendClockSyncCommand(master, 1, &newTime);
    scheduleNextTS();
    return true;
  }
  return false;
}

bool IEC101GenericDevice::run()
{
  return true;
}

bool IEC101GenericDevice::tagChanged(const Tag &tag, uint32_t paramId, VarData newValue)
{
  if (LL_STATE_ERROR == this->linkState)
    return false;
  auto io = createIOFromTagUpdate(tag, newValue);
  if (io)
  {
    writeQueue.push_back(io);
    return true;
  }
  return false;
}

bool IEC101GenericDevice::canRelease()
{
  auto result = (rsReadyToRelease == runState) && (LL_STATE_BUSY != linkState) || (rsOffline == runState);
  return result;
}

bool IEC101GenericDevice::release()
{
  if (rsOffline != runState)
    runState = rsUnselected;
  return true;
}

InformationObject IEC101GenericDevice::createIOFromTagUpdate(const Tag &tag, VarData newValue)
{
  auto ctx = reinterpret_cast<IECSContext *>(tag.getDriverSpecificInfo(getParent()->getId()));
  InformationObject io = nullptr;
  switch (ctx->cmdType)
  {
  case C_SC_NA_1:
    io = (InformationObject)SingleCommand_create(NULL, ctx->ioa, newValue.boolVal, false, 0);
    break;
  case C_DC_NA_1:
    io = (InformationObject)DoubleCommand_create(NULL, ctx->ioa, newValue.intVal, false, 0);
    break;
  case C_SE_NB_1:
    io = (InformationObject)SetpointCommandScaled_create(NULL, ctx->ioa, newValue.intVal, false, 0);
    break;
  case C_SE_NC_1:
    io = (InformationObject)SetpointCommandShort_create(NULL, ctx->ioa, newValue.fltVal, false, 0);
    break;
  default:
    break;
  }
  return io;
}

uint32_t CP56Time2a_toShortMsTimestamp(CP56Time2a self)
{
  auto longTS = CP56Time2a_toMsTimestamp(self);
  return longTSToShortTS(longTS);
}

bool IEC101GenericDevice::handleMMETE1(Tag &tag, InformationObject io, CS101_CauseOfTransmission cot)
{
  auto mvs = reinterpret_cast<MeasuredValueScaledWithCP56Time2a>(io);
  auto value = MeasuredValueScaled_getValue(reinterpret_cast<MeasuredValueScaled>(io));
  auto shortTS = CP56Time2a_toShortMsTimestamp(MeasuredValueScaledWithCP56Time2a_getTimestamp(mvs));
  Core::setIfChanged(tag, value, true, parent->getId(), shortTS);
  return true;
}

bool IEC101GenericDevice::handleMMENC1(Tag &tag, InformationObject io, CS101_CauseOfTransmission cot)
{
  auto mvs = reinterpret_cast<MeasuredValueShort>(io);
  auto value = MeasuredValueShort_getValue(mvs);
  auto qty = MeasuredValueShort_getQuality(mvs) == IEC60870_QUALITY_GOOD;
  Core::setIfChanged(tag, value, qty, parent->getId());
  return true;
}

/**
* @brief  TK36 frame handler Float with time stamp
* @param  Tag &tag, InformationObject io, CS101_CauseOfTransmission cot
* @retval true - OK, false - Error
*/
bool IEC101GenericDevice::handleMMETF1(Tag &tag, InformationObject io, CS101_CauseOfTransmission cot)
{
  auto mvs = reinterpret_cast<MeasuredValueShortWithCP56Time2a>(io);
  auto value = MeasuredValueShort_getValue(reinterpret_cast<MeasuredValueShort>(io));
  auto shortTS = CP56Time2a_toShortMsTimestamp(MeasuredValueShortWithCP56Time2a_getTimestamp(mvs));
  Core::setIfChanged(tag, value, true, parent->getId(), shortTS);
  return true;
}

bool IEC101GenericDevice::handleMSPNA1(Tag &tag, InformationObject io, CS101_CauseOfTransmission cot)
{
  auto sp = reinterpret_cast<SinglePointInformation>(io);
  auto value = SinglePointInformation_getValue(sp);
  auto qty = SinglePointInformation_getQuality(sp) == IEC60870_QUALITY_GOOD;
  Core::setIfChanged(tag, value, qty, parent->getId());
  return true;
}

bool IEC101GenericDevice::handleMDPNA1(Tag &tag, InformationObject io, CS101_CauseOfTransmission cot)
{
  auto dp = reinterpret_cast<DoublePointInformation>(io);
  auto value = DoublePointInformation_getValue(dp);
  auto qty = DoublePointInformation_getQuality(dp) == IEC60870_QUALITY_GOOD;
  Core::setIfChanged(tag, value, qty, parent->getId());
  return true;
}

bool IEC101GenericDevice::handleMMENB1(Tag &tag, InformationObject io, CS101_CauseOfTransmission cot)
{
  auto mvs = reinterpret_cast<MeasuredValueScaled>(io);
  auto value = MeasuredValueScaled_getValue(mvs);
  auto qty = MeasuredValueScaled_getQuality(mvs) == IEC60870_QUALITY_GOOD;
  Core::setIfChanged(tag, value, qty, parent->getId());
  return true;
}

bool IEC101GenericDevice::handleMSPTB1(Tag &tag, InformationObject io, CS101_CauseOfTransmission cot)
{
  auto sp = reinterpret_cast<SinglePointWithCP56Time2a>(io);
  auto value = SinglePointInformation_getValue((SinglePointInformation)sp);
  auto shortTS = CP56Time2a_toShortMsTimestamp(SinglePointWithCP56Time2a_getTimestamp(sp));
  Core::setIfChanged(tag, value, true, parent->getId(), shortTS);
  return true;
}

bool IEC101GenericDevice::handleMDPTB1(Tag &tag, InformationObject io, CS101_CauseOfTransmission cot)
{
  auto dp = reinterpret_cast<DoublePointWithCP56Time2a>(io);
  auto value = DoublePointInformation_getValue(DoublePointInformation(dp));
  auto shortTS = CP56Time2a_toShortMsTimestamp(DoublePointWithCP56Time2a_getTimestamp(dp));
  Core::setIfChanged(tag, value, true, parent->getId(), shortTS);
  return true;
}

bool IEC101GenericDevice::handleCICNA1(Tag &tag, InformationObject io, CS101_CauseOfTransmission cot)
{
  InterrogationCommand ic = reinterpret_cast<InterrogationCommand>(io);
  auto qoi = InterrogationCommand_getQOI(ic);
  if (qoi == 20)
  {
    if (CS101_COT_ACTIVATION_CON == cot)
      runState = rsGI;
    else if (CS101_COT_ACTIVATION_TERMINATION && rsGI == runState)
      runState = rsSelected;
  }
  return true;
}
