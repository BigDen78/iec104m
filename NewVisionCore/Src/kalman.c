#include <stdint.h>
#include "kalman.h"

void Kalman_init(sKalman *S, float varValue, float varProcess)
{
	S->varValue = varValue;
	S->varProcess = varProcess;
	S->pc = 0.0;
	S->gain = 0.0;
	S->p = P_DEFAULT;
	S->xp = 0.0;
	S->zp = 0.0;
	S->xe = 0.0;
}

ADC_RawType Kalman(sKalman *S, ADC_RawType value) 
{
	S->pc = S->p + S->varProcess;
	S->gain = S->pc/(S->pc + S->varValue);

	S->xp = S->xe;
	S->zp = S->xp;

	S->xe = S->gain * (value - S->zp) + S->xp;

	return (ADC_RawType)S->xe;
}
