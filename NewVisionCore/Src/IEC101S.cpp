#include <IEC101S.hpp>
#include <hal_thread.h>
#include <DriverBuilder.hpp>

#define CMD_IDLE 1u

REGISTER_FACTORY_METHOD(DriverBuilder::driverFactoryTable, Driver, IEC101Slave, new IEC101SDriver);

enum ParamIDs : uint32_t
{
	pidLinkAddress,
	pidASDUAddress,
	pidUART,
	pidBaudRate,
	pidStopBits,
	pidParity,
	pidSpontDelay,
	pidUseSingleCharAck
};

const ParameterVector IEC101SDriver::parameters{
		{"linkAddress", {pidLinkAddress, dtDWord, StaticParameterInfo::pfRequired}},
		{"ASDUAddress", {pidASDUAddress, dtDWord, StaticParameterInfo::pfRequired}},
		{"UART", {pidUART, dtByte, StaticParameterInfo::pfRequired}},
		{"baudRate", {pidBaudRate, dtDWord, StaticParameterInfo::pfRequired}},
		{"stopBits", {pidStopBits, dtByte, StaticParameterInfo::pfNone}},
		{"parity", {pidParity, dtString, StaticParameterInfo::pfNone}},
		{"spontDelay", {pidSpontDelay, dtWord, StaticParameterInfo::pfNone}},
		{"useSingleCharACK",{pidUseSingleCharAck, dtBool, StaticParameterInfo::pfNone}}
};

IEC101SDriver::IEC101SDriver() : runState(rsPreInit), stopBits(1), parity('N'),
																 dcmdSerialIdle(this, CMD_IDLE, {0}),
																 ParameterizedObject(parameters), useSingleCharACK(true)
{
}

bool IEC101SDriver::setParameterById(uint32_t paramId, VarData newValue)
{
	switch (static_cast<ParamIDs>(paramId))
	{
	case pidLinkAddress:
		linkAddress = newValue.dwordVal & 0xffffu;
		return true;
	case pidASDUAddress:
		asduAddress = newValue.dwordVal & 0xffffffu;
		return true;
	case pidUART:
		if ((newValue.byteVal > 0) && (newValue.byteVal <= MAX_HUART) && (huarts[newValue.byteVal - 1]))
		{
			huart = huarts[newValue.byteVal - 1];
			return true;
		}
		return false;
	case pidBaudRate:
		baudRate = newValue.dwordVal;
		return true;
	case pidStopBits:
		if (newValue.byteVal == 1 || newValue.byteVal == 2)
		{
			stopBits = newValue.byteVal;
			return true;
		}
		return false;
	case pidParity:
		if (newValue.pStr[0] == 'E' || newValue.pStr[0] == 'O' || newValue.pStr[0] == 'N')
		{
			parity = newValue.pStr[0];
			return true;
		}
		return false;
	case pidSpontDelay:
		if (newValue.wordVal <= 1000)
		{
			spontDelay = newValue.wordVal;
			return true;
		}
		return false;
	case pidUseSingleCharAck:
		useSingleCharACK = newValue.boolVal;
		return true;
	}
	return false;
}

bool IEC101SDriver::setRuntimeParameter(const Tag &tag, uint32_t paramId, VarData paramValue)
{
	if (CMD_IDLE == paramId && (rsRunning == runState || rsSpontDataAwait == runState))
	{
		CS101_Slave_run(slave);
		return true;
	}
	return false;
}

VarData IEC101SDriver::getRuntimeParameter(uint32_t paramId)
{
	VarData result;
	result.dwordVal = 0;
	return result;
}

void IEC101SDriver::initialize()
{
	Driver::initialize();
	buildInterrogationList();
	port = SerialPort_create(huart, baudRate, parity, stopBits);

	sLinkLayerParameters llParameters;
	llParameters.addressLength = 1;
	llParameters.timeoutForAck = 50;
	llParameters.timeoutRepeat = 100;
	llParameters.useSingleCharACK = this->useSingleCharACK;

	if (port)
	{
		SerialPort_subscribeToEvents(port, commandQueue->getHandle(), &dcmdSerialIdle, NULL);
		slave = CS101_Slave_create(port, &llParameters, NULL, IEC60870_LINK_LAYER_UNBALANCED);
		if (slave)
		{
			auto alParameters = CS101_Slave_getAppLayerParameters(slave);
			spontASDU = CS101_ASDU_create(alParameters, false, CS101_COT_SPONTANEOUS, 0, asduAddress, false, false);
			currentSpontType = IEC60870_5_TypeID::M_SP_NA_1;
			commandQueue->getTimer().setPeriod(spontDelay);
			CS101_Slave_setLinkLayerAddress(slave, linkAddress);
			CS101_Slave_setClockSyncHandler(slave, IEC101SDriver::clockSyncHandlerStatic, this);
			CS101_Slave_setInterrogationHandler(slave, IEC101SDriver::interrogationHandlerStatic, this);
			CS101_Slave_setASDUHandler(slave, IEC101SDriver::asduHandlerStatic, this);
			CS101_Slave_setResetCUHandler(slave, IEC101SDriver::resetCUHandlerStatic, this);
			CS101_Slave_setLinkLayerStateChanged(slave, IEC101SDriver::linkLayerStateChangedHandlerStatic, this);
			CS101_Slave_setIdleTimeout(slave, 500);
			SerialPort_open(port);
			runState = rsRunning;
			return;
		}
	}
	runState = rsUnrecoverableError;
}

bool IEC101SDriver::tagChanged(const Tag &tag, uint32_t shortTS, VarData newValue)
{
	if (rsRunning == runState || rsSpontDataAwait == runState)
	{
		auto ctx = reinterpret_cast<IECSContext *>(tag.getDriverSpecificInfo(id));
		auto typeId = ctx->spontType;
		if ((typeId != currentSpontType) && CS101_ASDU_getNumberOfElements(spontASDU))
		{
			CS101_Slave_enqueueUserDataClass2(slave, spontASDU);
			CS101_ASDU_removeAllElements(spontASDU);
		}
		if (!addIOWithTSFromTagToASDU(spontASDU, typeId, tag, newValue, shortTS))
		{
			CS101_Slave_enqueueUserDataClass2(slave, spontASDU);
			CS101_ASDU_removeAllElements(spontASDU);
			addIOWithTSFromTagToASDU(spontASDU, typeId, tag, newValue, shortTS);
		}
		runState = rsSpontDataAwait;
		currentSpontType = typeId;
		commandQueue->getTimer().rearm();
	}
	return true;
}

void IEC101SDriver::run()
{
	switch (runState)
	{
	case rsPreInit:
		//can't get here in normal circumstances
		runState = rsUnrecoverableError;
		break;
	case rsRunning:
		break;
	case rsSpontDataAwait:
		if (commandQueue->getTimer().ready())
		{
			if (CS101_ASDU_getNumberOfElements(spontASDU))
			{
				//send filled ASDU
				CS101_Slave_enqueueUserDataClass2(slave, spontASDU);
				//clear ASDU
				CS101_ASDU_removeAllElements(spontASDU);
			}
			runState = rsRunning;
		}
		break;
	}
}

void IEC101SDriver::resetCUHandler()
{
	printf("Received reset CU\n");
	CS101_Slave_flushQueues(this->slave);
}

void IEC101SDriver::linkLayerStateChangedHandler(int address, LinkLayerState newState)
{
	switch (newState)
	{
	case LL_STATE_AVAILABLE:
		break;
	case LL_STATE_BUSY:
		break;
	case LL_STATE_IDLE:
		break;
	case LL_STATE_ERROR:
			printf("Received error from link layer\r\n");
			CS101_Slave_flushQueues(this->slave);
			SerialPort_discardInBuffer(this->port);
		break;
	}
}
