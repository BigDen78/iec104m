#include "Core.hpp"
#include "HeatingControlDevice.hpp"
#include "DriverBuilder.hpp"

#include "stdio.h"
#include "main.h"
#include "usart.h"

REGISTER_FACTORY_METHOD(DriverBuilder::driverFactoryTable, Driver, HeatingControlDevice, new HeatingControlDevice);


const ParameterVector HeatingControlDevice::parameters
{
  {"spontaneousTime",{timer, dtWord,  StaticParameterInfo::pfRuntimeChangeable | StaticParameterInfo::pfRequired}}
};

HeatingControlDevice::HeatingControlDevice() : ParameterizedObject(HeatingControlDevice::parameters){}

void HeatingControlDevice::registerTag(const Tag &tag, void *ctx)
{
}

bool HeatingControlDevice::tagChanged(const Tag &tag, uint32_t, VarData newValue)
{
  return true;
}

bool HeatingControlDevice::setRuntimeParameter(const Tag &tag, uint32_t paramId, VarData paramValue)
{
  switch (static_cast<HeatingParams>(paramId))
  {
  case timer:
    spontaneousTime = paramValue.wordVal;
    break;
  }
  return true;
}

bool HeatingControlDevice::setParameterById(uint32_t id, VarData newValue)
{
  switch (static_cast<HeatingParams>(id))
  {
  case timer:
    spontaneousTime = newValue.wordVal;
    return true;
    break;
  }
  return false;
}

DriverContext* HeatingControlDevice::createTagContext() const
{
   return nullptr;
}


bool HeatingControlDevice::setTagRuntimeParameter(const Tag &tag, uint32_t paramId, VarData paramValue)
{
  return false;
}

VarData HeatingControlDevice::getRuntimeParameter(uint32_t paramId)
{
  VarData result;
  result.dwordVal = 0;
  return result;
}

VarData HeatingControlDevice::getTagRuntimeParameter(const Tag &tag, uint32_t paramId)
{
  VarData result;
  result.dwordVal = 0;
  return result;
}

void HeatingControlDevice::initialize()
{
  Driver::initialize();
  
  commandQueue->getTimer().setPeriod(spontaneousTime);
  commandQueue->getTimer().rearm();
  
  CommutationBlock::setStaticVar(); // use for test ONLY

}

void HeatingControlDevice::run()
{
  
  
 /* if(commandQueue->getTimer().getPeriod()!=spontaneousTime)


  {
    commandQueue->getTimer().setPeriod(spontaneousTime);
  }
  if((commandQueue->getTimer().isArmed()) &&  (commandQueue->getTimer().ready()))
  {
    commandQueue->getTimer().rearm();
  } */ 
}