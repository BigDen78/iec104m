#include <ParameterizedObject.hpp>
#include <nwUtils.hpp>

const ParameterVector ParameterizedObject::NO_PARAMETERS{};

bool isMatchchingJsonType(DataType type, const Json &json)
{
  switch (type)
  {
  case dtBool:
    return json.is_boolean();
  case dtByte:
  case dtWord:
  case dtDWord:
    return json.is_number_unsigned();
  case dtShortInt:
  case dtInt:
    return json.is_number_integer();
  case dtFloat:
    return json.is_number_float();
  case dtString:
  case dtWString:
    return json.is_string();
  }
  return false;
}

bool ParameterizedObject::ReadParameterByNameFromJson(const String &name, const Json &json)
{
  auto paramInfo = getParameterInfo(name);
  if (paramInfo)
  {
    VarData data = getDataFromJson(paramInfo->type, json);
    bool result = setParameterById(paramInfo->id, data);
    if ((dtString == paramInfo->type) && data.pStr)
      delete data.pStr;
    return result;
  }
  return false;
}

bool ParameterizedObject::initFromJson(const JsonObject &json)
{
  bool ok = true;
  if (myParameters.size() > 0)
  {
    /*iterate through parameters*/
    for (auto it : myParameters)
    {
      JsonObject::const_iterator jsonItem;
      // find item within json object that corresponds to tag's parameter
      if ((jsonItem = json.find(it.first)) != json.end())
      {
        //if parameter has an incorrect json type or its value is not accepted
        //than we cant initialize tag context properly
        if (!isMatchchingJsonType(it.second.type, jsonItem->second) ||
            !ReadParameterByNameFromJson(it.first, jsonItem->second))
        {
            ok = false;
          break;
        }
      }
      //if there's no item within json object that corresponds to given parameter
      //and this parameter is required than we can't initialize properly
      else
      {
        if (it.second.flags & StaticParameterInfo::pfRequired)
        {
          ok = false;
          break;
        }
        else
          continue;
      }
    }
  }
  return ok;
}