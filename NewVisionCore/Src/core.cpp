#include <algorithm>
#include <fstream>
#include <strstream>
#include "core.hpp"
#include "driver.hpp"
#include "DriverBuilder.hpp"

#ifdef __USE_BUILTIN_JSON
#ifdef __USE_SLAVE_JSON
#include "ConfigJson-slave.hpp"
#endif
#ifdef __USE_MASTER_JSON
#include "ConfigJson-master.hpp"
#endif
#ifdef __USE_MASTER_JSON2
#include "ConfigJson-master-NoTemp.hpp"
#endif
#endif

#define DRIVERS_NAME_KEY "drivers"
#define DEVICES_NAME_KEY "devices"
#define TAGS_NAME_KEY    "tags"

Core *Core::_instance = 0;

Core::Core() : state(rseUninit), lastError(reOk) {}

VDFactoryFunction vdFactory[dtMaxType] = {varDataCreate<void>, varDataCreate<bool>, varDataCreate<uint8_t>,
																 varDataCreate<uint16_t>, varDataCreate<uint32_t>, varDataCreate<int16_t>,
																 varDataCreate<int32_t>, varDataCreate<float>, varDataCreate<char *>,
																 nullptr, nullptr};

void Core::loadJson(const CString &jsonPath)
{
  Json json;
#ifdef __USE_BUILTIN_JSON
  std::istrstream config(jsonStr);
#else
  std::ifstream config(jsonPath);
#endif
  auto parser = nlohmann::detail::parser<Json>(nlohmann::detail::input_adapter(config), nullptr, false);

  parser.parse(false, json);

  DriverBuilder dBuilder;

  for (auto iterator : json[DRIVERS_NAME_KEY])
  {
    dBuilder.reset();
    if (dBuilder.initFromJson(iterator))
    {
      drivers.push_back(dBuilder.getResult());
      driverNamesMap.insert_or_assign(dBuilder.getResult()->getName(),
                                      dBuilder.getResult());
    }
  }

  DeviceBuilder deBuilder;

  for (auto iterator : json[DEVICES_NAME_KEY])
  {
    deBuilder.reset();
    if (deBuilder.initFromJson(iterator))
    {
      devices.push_back(deBuilder.getResult());
      deviceNamesMap.insert_or_assign(deBuilder.getResult()->getName(),
                                      deBuilder.getResult());
    }
  }

  TagBuilder tBuilder;

  for (auto iterator : json[TAGS_NAME_KEY])
  {
    tBuilder.reset();
    if (tBuilder.initFromJson(iterator))
    {
      tags.push_back(tBuilder.getResult());
      tagNamesMap.insert_or_assign(tBuilder.getResult()->getName(),
                                   tBuilder.getResult());
    }
  }
}

void Core::allocTagsMemory()
{
  if (rseUninit != state)
    return;
  uint32_t uMemBytesRequred = 0;
  // first pass - calculate total required memory
  for (auto tag : tags)
  {
    uMemBytesRequred += tag->getStorageSize();
  }
  uint8_t *ptr =
      uMemBytesRequred ? new uint8_t[uMemBytesRequred] : nullptr;

  if (ptr)
  {
    memset(ptr, 0, uMemBytesRequred);
    // second pass - assign each tag it's required memory
    for (auto tag : tags)
    {
      tag->setStorage(ptr);
      if (tag->pDefaultValue)
      {
        *tag->pValue = *tag->pDefaultValue;
        delete tag->pDefaultValue;
        tag->pDefaultValue = nullptr;
				tag->valid = true;
				tag->timestamp = getShortTS();
			}
      ptr += tag->getStorageSize();
    }
    state = rseReady;
  }
  else
  {
    state = rseError;
    lastError = reMemAlloc;
  }
}

void Core::initialize()
{
  allocTagsMemory();
  commandQueue = new Core::QueType(100, "Core", 10);
  for (auto driverIterator : drivers)
  {
    auto tagList = new TagList;
    for_each(tags.begin(), tags.end(), [&](Tag *tag) {
      if (tag->getParent() == driverIterator)
        tagList->push_front(tag);
    });
    driverIterator->setTagsList(tagList);
    if (driverIterator->supportsDevices())
    {
      auto devList = new DeviceList;
      for_each(devices.begin(), devices.end(), [&](Device *dev) {
        if (dev->getParent() == driverIterator)
          devList->push_front(dev);
      });
      driverIterator->setDevicesList(devList);
    }
    driverIterator->initialize();
  }
}

void Core::run()
{
  ChangeTagCommand cmd;

  if (rseReady == state)
  {
    state = rseRunning;
    for (;;)
    {
      if (commandQueue->pop(&cmd))
      {
        cmd.execute();
        const SubscriberSet &set = cmd.getTag()->subscribers;
        auto senderId = cmd.getSenderId();
        for (auto driver : drivers)
        {
          if (set[driver->getId()] && (senderId != driver->getId()))
            driver->postTagChanged(*cmd.getTag(), cmd.getData(), cmd.getTS());
        }
      }
    }
  }
}

void Core::setTag(const String &tagName, VarData newData, bool valid, uint32_t sender)
{
  auto pTag = tagNamesMap.find(tagName);
  if (tagNamesMap.end() != pTag)
    setTag(pTag->second, newData, valid, sender);
}

void Core::setTag(Tag *pTag, VarData newData, bool valid, uint32_t sender)
{
  ChangeTagCommand cmd(pTag, newData, valid, sender);
  commandQueue->push(&cmd);
}

void Core::setTag(Tag *pTag, VarData newData, bool valid, uint32_t sender, uint32_t ts)
{
  ChangeTagCommand cmd(pTag, newData, valid, sender, ts);
  commandQueue->push(&cmd);
}

Tag *Core::getTag(const String &tagName) const
{
  auto pTag = tagNamesMap.find(tagName);
  if (tagNamesMap.end() != pTag)
    return pTag->second;
  return nullptr;
}

Device *Core::getDevice(const String &deviceName) const
{
  auto pDevice = deviceNamesMap.find(deviceName);
  if (deviceNamesMap.end() != pDevice)
    return pDevice->second;
  return nullptr;
}

Driver *Core::getDriver(const String &driverName) const
{
  auto pDriver = driverNamesMap.find(driverName);
  if (driverNamesMap.end() != pDriver)
    return pDriver->second;
  return nullptr;
}

extern "C" void startRuntimeTask(void *argument)
{
  Core &core = Core::instance();
  core.loadJson();
  core.initialize();
	//vTraceEnable(TRC_START);

  if (Core::rseReady == core.getState())
    core.run();
  else
    vTaskDelete(NULL);
}