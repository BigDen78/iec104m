#include <FreeRTOS.h>
#include <task.h>
#include <command.hpp>

const Tag Tag::NoTag{dtVoid, "NoTag", 0};
Tag& Tag::NoTagRef = const_cast<Tag&>(NoTag);

Tag::operator int() const
{
	if (0 == staticInfo.arraySize && pValue)
	{
		switch (staticInfo.type)
		{
		case dtBool:
			return static_cast<int>(pValue->boolVal);
		case dtByte:
			return static_cast<int>(pValue->byteVal);
		case dtWord:
			return static_cast<int>(pValue->wordVal);
		case dtDWord:
			return static_cast<int>(pValue->dwordVal);
		case dtShortInt:
			return static_cast<int>(pValue->shortVal);
		case dtInt:
			return pValue->intVal;
		case dtFloat:
			return static_cast<int>(pValue->fltVal);
		default:
			return 0;
		}
	}
	return 0;
}

int Tag::operator=(int val)
{
	if (0 == staticInfo.arraySize && pValue)
	{
		switch (staticInfo.type)
		{
		case dtBool:
			return (pValue->boolVal = static_cast<bool>(val));
		case dtByte:
			return (pValue->byteVal = static_cast<uint8_t>(val));
		case dtWord:
			return (pValue->wordVal = static_cast<uint16_t>(val));
		case dtDWord:
			return (pValue->dwordVal = static_cast<uint32_t>(val));
		case dtShortInt:
			return (pValue->shortVal = static_cast<int16_t>(val));
		case dtInt:
			return (pValue->intVal = val);
		case dtFloat:
			pValue->fltVal = static_cast<float>(val);
			return val;
			return val;
		default:
			return 0;
		}
	}
	return 0;
}
