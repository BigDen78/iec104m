#include <fstream>
#include <functional>

#include <DriverBuilder.hpp>
#include <nwUtils.hpp>

//#include "ci_char_traits.hpp"

#define CLASS_NAME_KEY "class"
#define OBJECT_NAME_KEY "name"
#define OBJECT_PARENT_KEY "parent"
#define PARAMETERS_NAME_KEY "parameters"

#define TAG_TYPE_KEY "type"
#define TAG_DEFAULT_KEY "defaultValue"
#define TAG_MAPPING_KEY "mappings"
#define TAG_BASE_KEY "base"
#define TAG_DEVICE_KEY "device"

#define DRIVER_STACK_DEPTH_KEY "stackDepth"
#define DRIVER_QUEUE_DEPTH_KEY "queueDepth"
#define DRIVER_SCAN_RATE_KEY "scanRate"

#define DEVICE_TEMPLATE_KEY "template"
#define TAG_TEMPLATES_KEY "templates"

// DriverFactoryTable::allocator_type allocator()
DriverFactoryTable DriverBuilder::driverFactoryTable;
DeviceFactoryTable DeviceBuilder::deviceFactoryTable;
TemplateFactoryTable TemplateBuilder::TemplateFactoryTable = {
  { "Bool", createTemplate<bool> },
  { "Byte", createTemplate<uint8_t> },
  { "Word", createTemplate<uint16_t> },
  { "Dword", createTemplate<uint32_t> },
  { "ShortInt", createTemplate<int16_t> },
  { "Int", createTemplate<int32_t> },
  { "Float", createTemplate<float> },
  { "String", createTemplate<char *> },
  { "WString", createTemplate<wchar_t *> } 
};

TagFactoryTable TagBuilder::tagFactoryTable = {
  { "Bool", createTag<bool> },
  { "Byte", createTag<uint8_t> },
  { "Word", createTag<uint16_t> },
  { "Dword", createTag<uint32_t> },
  { "ShortInt", createTag<int16_t> },
  { "Int", createTag<int32_t> },
  { "Float", createTag<float> },
  { "String", createTag<char *> },
  { "WString", createTag<wchar_t *> },
  { "Instance", createTagFromTemplate } 
};

void DriverBuilder::reset() { driver = nullptr; }

bool DriverBuilder::initFromJson(const Json &json)
{
  bool ok = true;
  if (json.contains(CLASS_NAME_KEY))
  {
    String className(json[CLASS_NAME_KEY]);
    auto itFn = driverFactoryTable.find(className);
    if (itFn != driverFactoryTable.end())
    {
      driver = (*itFn->second)();
    }
    else
    {
      driver = nullptr;
    }
  }
  ok = driver;
  if (ok)
  {
    auto itJson = json.find(DRIVER_QUEUE_DEPTH_KEY);
    if (itJson != json.end() && itJson->is_number_unsigned())
    {
      driver->setQueueDepth(itJson->get<Json::number_unsigned_t>());
    }
    itJson = json.find(DRIVER_STACK_DEPTH_KEY);
    if (itJson != json.end() && itJson->is_number_unsigned())
    {
      driver->setStackDepth(itJson->get<Json::number_unsigned_t>());
    }
    itJson = json.find(DRIVER_SCAN_RATE_KEY);
    if (itJson != json.end() && itJson->is_number_integer())
    {
      driver->setScanRate(itJson->get<Json::number_integer_t>());
    }
    auto paramContainer = driver->getParameters();
    /*if driver has a list of parameters*/
    if (paramContainer.size() > 0)
    {
      /*are parameters present in json?*/
      if (json.contains(PARAMETERS_NAME_KEY) and
          json.at(PARAMETERS_NAME_KEY).is_object())
      {
        auto jsonObj = json[PARAMETERS_NAME_KEY].get<JsonObject>();
        ok = driver->initFromJson(jsonObj);
      }
      /*driver has settable parameters but there's no 'parameters' clause in
      json  */
      else
        ok = false;
    }
  }
  if (ok)
  {
    if (json.contains(OBJECT_NAME_KEY))
      driver->setName(json[OBJECT_NAME_KEY]);
    driver->setId(Core::instance().getDrivers().size());
  }
  
  if (!ok && driver)
  {
    delete driver;
    driver = nullptr;
  }
  return ok;
}

void TagBuilder::reset() { tag = nullptr; }

bool TagBuilder::setTagParameter(Driver *driver,
  Tag *tag,
  const String &name,
  const Json &json)
{
  auto ctx = tag->getDriverSpecificInfo(driver->getId());
  auto paramInfo = ctx->getParameterInfo(name);
  if (paramInfo)
  {
    VarData data = getDataFromJson(paramInfo->type, json);
    bool result = ctx->setParameterById(paramInfo->id, data);
    if ((dtString == paramInfo->type) && data.pStr)
      delete data.pStr;
    return result;
  }
  return false;
}

bool TagBuilder::initFromJson(const Json &json)
{
  /*name and type MUST be specified for each tag*/
  if (json.contains(OBJECT_NAME_KEY) && json.contains(TAG_TYPE_KEY))
  {
    bool bTagInvalidatedByParent = false;
    TagTemplate *base = nullptr;
    Driver *parentDrv = nullptr;
    Device *device = nullptr;
    /*find parent driver by instance name*/
    if (json.contains(OBJECT_PARENT_KEY))
      parentDrv =
        Core::instance().getDriver(json[OBJECT_PARENT_KEY].get<String>());
    else if (json.contains(TAG_DEVICE_KEY))
    {
      device = Core::instance().getDevice(json[TAG_DEVICE_KEY].get<String>());
      if (device)
        parentDrv = const_cast<Driver *>(device->getParent());
      else
        bTagInvalidatedByParent = true;
    }
    if (parentDrv)
    {
      auto tagCreateFn = tagFactoryTable.find(json[TAG_TYPE_KEY].get<String>());
      if (tagCreateFn != tagFactoryTable.end())
      {
        if (json.contains(TAG_BASE_KEY))
          base = device->getTemplate()->getTemplate(json[TAG_BASE_KEY].get<String>());
        tag = (*tagCreateFn->second)(base);
      }
      /*id of the new tag is equal to current tags count; first tag id is zero*/
      tag->setId(Core::instance().getTags().size());
      tag->setName(json[OBJECT_NAME_KEY].get<String>());
      
      /*prepare space for storing driver-specific infos for new tag*/
      tag->allocateDriverInfos(Core::instance().getDrivers().size());
      /*tags can not exist without parent*/
      tag->setParent(parentDrv);
      tag->setDevice(device);
      tag->registerSubscriber(parentDrv->getId());
      if (base)
      {
        tag->setDriverSpecificInfo(parentDrv->getId(),
          base->getParentContext());
        device->registerTag(*tag, base->getParentContext());
      }
      /*process tag mappings (if there are any)*/
      if (json.contains(TAG_MAPPING_KEY))
      {
        auto jsonMappings = json[TAG_MAPPING_KEY].get<JsonObject>();
        /*if mappings list contains data for parent diver*/
        auto itParentContext = jsonMappings.find(parentDrv->getName());
        if (itParentContext != jsonMappings.end())
        {
          /*if parent driver supports tag context creation*/
          auto tagContext = parentDrv->createTagContext();
          if (tagContext)
          {
            tag->setDriverSpecificInfo(parentDrv->getId(), tagContext);
            bTagInvalidatedByParent = !tagContext->initFromJson(
                                                                itParentContext->second.get<JsonObject>());
            if (bTagInvalidatedByParent)
            {
              tag->setDriverSpecificInfo(parentDrv->getId(), NULL);
              delete tagContext;
            }
            else
            {
              parentDrv->registerTag(*tag, tagContext);
            }
          }
        }
        if (!bTagInvalidatedByParent)
        {
          /*for each mapping*/
          for (auto itMappinngEntry : jsonMappings)
          {
            /*find driver by instance name*/
            auto driver = Core::instance().getDriver(itMappinngEntry.first);
            /*if no such driver - proceed to next mapping entry*/
            if (!driver || driver == parentDrv)
              continue;
            auto tagContext = driver->createTagContext();
            if (tagContext)
            {
              tag->setDriverSpecificInfo(driver->getId(), tagContext);
              if (tagContext->initFromJson(
                                           itMappinngEntry.second.get<JsonObject>()))
              {
                driver->registerTag(*tag, tagContext);
                tag->registerSubscriber(driver->getId());
              }
              else
              {
                tag->setDriverSpecificInfo(driver->getId(), nullptr);
                delete tagContext;
              }
            }
          }
        }
      }
    }
    else
      bTagInvalidatedByParent = true;
    if (!bTagInvalidatedByParent)
    {
      if (json.contains(TAG_DEFAULT_KEY))
        tag->setDefaultValue(getDataFromJson(tag->getType(), json.at(TAG_DEFAULT_KEY)));
      return true;
    }
  }
  if (tag)
    delete tag;
  tag = nullptr;
  return false;
}

Tag *TagBuilder::createTagFromTemplate(const TagTemplate *tpl)
{
  Tag *result = new Tag;
  if (result)
    result->staticInfo.type = tpl->staticInfo.type;
  return result;
}

void DeviceBuilder::reset() { device = nullptr; }

DeviceTemplate *DeviceBuilder::findOrLoadTemplate(const String &templateName)
{
  auto itTemplate = devTemplatesMap.find(templateName);
  DeviceTemplate *result = nullptr;
  if (devTemplatesMap.end() == itTemplate)
  {
    Json json;
    CString templatePath{"0:\\templates\\"};
    //CString templatePath{"0:\\"};
    templatePath += templateName.c_str();
    templatePath += ".json";
/*    
    std::ofstream create(templatePath);

    create << "   ""templates"": [                             " << std::endl;
    create << "      {                                         " << std::endl; 
    create << "        ""type"":""Word"",                      " << std::endl;
    create << "        ""name"":""IEC104T.Test104"",       " << std::endl;
    create << "        ""parameters"": {                       " << std::endl; 
    create << "			""spontType"": 35,       " << std::endl;
    create << "			""interType"": 11,       " << std::endl;
    create << "			""IOA"": 7001,           " << std::endl; 
    create << "			""cmdType"": 49          " << std::endl;
    create << "		}                                " << std::endl;
    create << "      }                                         " << std::endl;
    create << "        ]                                       " << std::endl;
    create.close();
*/  
    std::ifstream tpl(templatePath);

    if (tpl.is_open())
    {
      result = new DeviceTemplate;
      if (result)
      {
        auto parser = nlohmann::detail::parser<Json>(
                                                     nlohmann::detail::input_adapter(tpl), nullptr, false);
        parser.parse(false, json);
        TemplateBuilder tBuilder;
        for (auto iterator : json[TAG_TEMPLATES_KEY])
        {
          tBuilder.reset(device);
          if (tBuilder.initFromJson(iterator))
            result->registerTemplate(tBuilder.getResult());
        }
        if (result->getTemplateNames().size() == 0)
        {
          delete result;
          result = nullptr;
        }
        else
        {
          devTemplatesMap[templateName] = result;
        }
      }
    }
  }
  else
  {
    result = itTemplate->second;
  }
  return result;
}

bool DeviceBuilder::initFromJson(const Json &json)
{
  bool ok = false;
  Driver *driver = nullptr;
  DeviceTemplate *deviceTemplate = nullptr;
  if (json.contains(CLASS_NAME_KEY) && json.contains(OBJECT_NAME_KEY) &&
      json.contains(OBJECT_PARENT_KEY) && json.contains(DEVICE_TEMPLATE_KEY))
  {
    String className(json[CLASS_NAME_KEY]);
    //deviceFactoryTable.begin
    
    auto it= deviceFactoryTable.begin();
    //it->first
    int test_size = deviceFactoryTable.size();
    auto itFn = deviceFactoryTable.find(className);
    if (itFn != deviceFactoryTable.end())
      device = (*itFn->second)();
    else
      device = nullptr;
  }
  ok = device;
  if (ok)
  {
    /*find parent driver by instance name*/
    driver = Core::instance().getDriver(json[OBJECT_PARENT_KEY].get<String>());
    if (driver)
      device->setParent(driver);
    else
      ok = false;
  }
  
  if (ok)
  {
    auto templateName = json[DEVICE_TEMPLATE_KEY].get<String>();
    auto deviceTemplate = findOrLoadTemplate(templateName);
    ok = deviceTemplate;
    device->setTemplate(deviceTemplate);
  }
  
  if (ok)
  {
    auto paramContainer = device->getParameters();
    /*if device has a list of parameters*/
    if (paramContainer.size() > 0)
    {
      /*are parameters present in json?*/
      if (json.contains(PARAMETERS_NAME_KEY) and
          json.at(PARAMETERS_NAME_KEY).is_object())
      {
        auto jsonObj = json[PARAMETERS_NAME_KEY].get<JsonObject>();
        ok = device->initFromJson(jsonObj);
      }
      /*device has settable parameters but there's no 'parameters' clause in
      json  */
      else
        ok = false;
    }
  }
  
  if (ok)
  {
    device->setName(json[OBJECT_NAME_KEY]);
    device->setId(Core::instance().getDevices().size());
    ok = driver->registerDevice(device, deviceTemplate);
  }
  
  if (!ok)
  {
    if (device)
      delete device;
    device = nullptr;
  }
  return ok;
}

bool TemplateBuilder::initFromJson(const Json &json)
{
  if (json.contains(TAG_TYPE_KEY) && json.contains(OBJECT_NAME_KEY))
  {
    auto templateCreateFn =
      TemplateFactoryTable.find(json[TAG_TYPE_KEY].get<String>());
    
    if (templateCreateFn != TemplateFactoryTable.end())
    {
      tpl = (*templateCreateFn->second)();
    }
    if (tpl)
    {
      tpl->setName(json[OBJECT_NAME_KEY].get<String>());
      if (device)
      {
        /*process template parameters*/
        if (json.contains(PARAMETERS_NAME_KEY) &&
            json[PARAMETERS_NAME_KEY].is_object())
        {
          auto parameters = json[PARAMETERS_NAME_KEY].get<JsonObject>();
          /*if parent driver supports tag context creation*/
          auto tplContext = device->getParent()->createTagContext();
          if (tplContext)
          {
            tpl->setParentContext(tplContext);
            if (tplContext->initFromJson(parameters))
              return true;
            else
            {
              tpl->setParentContext(nullptr);
              delete tplContext;
            }
          }
        }
      }
    }
  }
  return false;
}