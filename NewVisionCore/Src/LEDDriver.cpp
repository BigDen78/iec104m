#include <map>
#include "Core.hpp"
#include "LEDDriver.hpp"
#include "DriverBuilder.hpp"

REGISTER_FACTORY_METHOD(DriverBuilder::driverFactoryTable, Driver, LEDDriver, new LEDDriver);
#ifndef DISABLE_LED
#ifdef OLD_F407
#define AS1115   1
#endif
#define BU2099FV 2
#endif //DISABLE_LED
#define LED_TYPE_ID      0
#define LED_NUM_ID       1
#define LED_INVERT_ID    2

extern "C" {
#ifdef AS1115
#include "as1115.h"
#endif
#ifdef BU2099FV
#include "bu2099fv.h"
extern LED_REG lreg;
#endif
}

const ParameterVector LEDDriverTagContext::myParameters
{
  {"type",   { LED_TYPE_ID, dtString, StaticParameterInfo::pfRequired }},
  {"num",    { LED_NUM_ID, dtByte, StaticParameterInfo::pfRequired }},
  {"invert", { LED_INVERT_ID, dtBool, StaticParameterInfo::pfNone }}
};


bool LEDDriverTagContext::setParameterById(uint32_t id, VarData newValue)
{
  switch (id)
  {
  case LED_TYPE_ID:
    switch(newValue.pStr[0])
    {
#ifdef AS1115
    case 'A':
      driverType = AS1115;
      break;
#endif
#ifdef BU2099FV
    case 'B':
      driverType = BU2099FV;
      break;
#endif
    default:
      return false;
    }
    break;
  case LED_NUM_ID:
  {
    uint8_t maxLeds = 0, minLeds = 0;
    switch(driverType)
    {
#ifdef AS1115
    case AS1115:
      maxLeds = 64; /* MAX 64 for AS1115 */
      break;
#endif
#ifdef BU2099FV
    case BU2099FV:
      maxLeds = 12; /* MAX 12 for BU2099FV */
      minLeds = 1;  /* The first led is "Run" it works without Runtime */
      break;
#endif
    default:
      return false;
    }
    if((newValue.byteVal < maxLeds) && (newValue.byteVal >= minLeds))
      ledNumber = newValue.byteVal;
    else
      return false;
    break;
  }
  case LED_INVERT_ID:
    invert = newValue.boolVal;
    break;
  default:
    break;
  }
  return true;
}

DriverContext *LEDDriver::createTagContext() const
{
  return new LEDDriverTagContext;
}

void LEDDriver::registerTag(const Tag &tag, void *ctx)
{
  LEDDriverTagContext *entry = reinterpret_cast<LEDDriverTagContext *>(ctx);
  entry->pTag = &(const_cast<Tag &>(tag));
  if(entry->invert)
  {
    entry->pinState = 1;
    switch(entry->driverType)
    {
#ifdef AS1115
    case AS1115:
      as1115_set_led(entry->pinState, entry->ledNumber);
      break;
#endif
#ifdef BU2099FV
    case BU2099FV:
      lreg.all |= (entry->pinState << entry->ledNumber);
      setLed(lreg.all);
      break;
#endif
    default:
      break;
    }
  }
}

VarData LEDDriver::getTagRuntimeParameter(const Tag& tag, uint32_t paramId)
{
	VarData result;
	result.dwordVal = 0;
	return result;
}

VarData LEDDriver::getRuntimeParameter(uint32_t paramId)
{
	VarData result;
	result.dwordVal = 0;
	return result;
}

bool LEDDriver::tagChanged(const Tag &tag, uint32_t, VarData newValue)
{
  LEDDriverTagContext *entry = reinterpret_cast<LEDDriverTagContext *>(tag.getDriverSpecificInfo(getId()));
  if(entry->invert)
    newValue.boolVal ? entry->pinState = 0 : entry->pinState = 1;
  else
    newValue.boolVal ? entry->pinState = 1 : entry->pinState = 0;
  switch(entry->driverType)
  {
#ifdef AS1115
  case AS1115:
    as1115_set_led(entry->pinState, entry->ledNumber);
    break;
#endif
#ifdef BU2099FV
  case BU2099FV:
    entry->pinState ? lreg.all |= (entry->pinState << entry->ledNumber) : lreg.all &=~(!entry->pinState << entry->ledNumber);
    setLed(lreg.all);
    break;
#endif
    default:
      break;
  }
	return true;
}

void LEDDriver::initialize()
{
  Driver::initialize();
}

void LEDDriver::run()
{

}
